import numpy as np
import math
from scipy.optimize import curve_fit

## Python API Test용 ##
def LoopBackTest(pos, intensity, dir,slope,refAbs, refMl ):

    outSize = np.array(len(pos),dtype =int)

    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl

    output =(outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize)

    return output



## AlignFunXIncrease ##
def AlignFunXIncrease(x, offset, refAbs, refMl):           
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)       
    refLeft = refAbs
    refRight = refMl
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
        
    ratioRight = 0.5
    ratioLeft = 1-ratioRight
        
    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w / 2 * (xprim[i] + d / 2) + (refLeft + refRight)/2
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            y[i] = ratioRight*refRight + ratioLeft*refLeft
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w  * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0
    return y


## AlignFunXDecrease ##
def AlignFunXDecrease(x, offset, refAbs, refMl):        
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)    
    refLeft = refMl
    refRight = refAbs
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
               
    ratioLeft = 0.5
    ratioRight = 1-ratioLeft

    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w /2 * (xprim[i] + d / 2) + (refLeft + refRight) / 2 
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)  
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            #y[i] = (refRight + refLeft) /2
            y[i] = (ratioRight*refRight + ratioLeft*refLeft)
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            #y[i] = (refRight - refLeft) / w /2* (xprim[i] - d / 2) + (refLeft + refRight) /2
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0

    return y

## AlignFunYIncrease ##
def AlignFunYIncrease(x, offset, refAbs, refMl):    
    l = 30./1000.
    N = len(x)    
    refBottom = refAbs
    refTop = refMl

    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
    
    for i in range(N):
        if xprim[i] < -l / 2:
            y[i] = refBottom
        elif xprim[i] >= -l / 2 and xprim[i] < l / 2:
            y[i] = (refTop - refBottom) / l * (xprim[i] + l / 2) + refBottom   
        elif xprim[i] >= l / 2:
            y[i] = refTop
        else:
            y[i] = 0        
    
    return y

## AlignFunYDecrease ##
def AlignFunYDecrease(x, offset, refAbs, refMl):    
    l = 30./1000.
    N = len(x)    
    refBottom = refMl
    refTop = refAbs

    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
    
    for i in range(N):
        if xprim[i] < -l / 2:
            y[i] = refBottom
        elif xprim[i] >= -l / 2 and xprim[i] < l / 2:
            y[i] = (refTop - refBottom) / l * (xprim[i] + l / 2) + refBottom   
        elif xprim[i] >= l / 2:
            y[i] = refTop
        else:
            y[i] = 0

    return y


## FineAlignCurveFit ##
def FineAlignCurveFit(pos, intensity, dir, slope, refAbs, refMl):
    #  함수 입력은 전부 np.array 임
    init = [(pos[0] + pos[-1]) / 2, refAbs[0], refMl[0]]

    if dir == 0 and slope == 1:        
        popt, pcov = curve_fit(AlignFunXIncrease, pos, intensity, p0 =init)
    if dir == 0 and slope == -1:                
        popt, pcov = curve_fit(AlignFunXDecrease, pos, intensity, p0 =init)
    if dir == 1 and slope == 1:        
        popt, pcov = curve_fit(AlignFunYIncrease, pos, intensity, p0 =init)
    if dir == 1 and slope == -1:        
        popt, pcov = curve_fit(AlignFunYDecrease, pos, intensity, p0 =init)

    targetIntensity = np.zeros((1,), dtype=np.float64)
    targetPos = np.zeros((1,), dtype=np.float64)
    targetPos[0] = popt[0]

    #임시
    if dir == 0 and slope == 1:
        intensity_fit = AlignFunXIncrease(pos,*popt)       
        targetIntensity[0]  = AlignFunXIncrease(targetPos,*popt);        
    if dir == 0 and slope == -1:
        intensity_fit = AlignFunXDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunXDecrease(targetPos,*popt);        
    if dir == 1 and slope == 1:
        intensity_fit = AlignFunYIncrease(pos,*popt)
        targetIntensity[0]  = AlignFunYIncrease(targetPos,*popt);        
    if dir == 1 and slope == -1:
        intensity_fit = AlignFunYDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunYDecrease(targetPos,*popt);        
        
    # 임시    
    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl
    outSize = np.array(len(pos),dtype =int)
    

    
    output = (outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize, popt, intensity_fit, targetIntensity)

    #output = (popt, )
    
    return output

def Homography_solve(xin, yin, xout, yout):

    pin = np.vstack([xin, yin])
    pout = np.vstack([xout, yout])
    # pin, pout 사이즈 같고, m=2, n>4 이어야함 
    n= pin.shape[1]
    x = pout[0, :]; y = pout[1,:]; X = pin[0,:]; Y = pin[1,:]
    rows0 = np.zeros((3, n))
    rowsXY = -1 * np.vstack([X, Y, np.ones((1,n))])
    hx = np.vstack([rowsXY, rows0, x*X,x*Y,x])
    hy = np.vstack([rows0, rowsXY,y*X, y*Y,y])
    h = np.hstack( [hx, hy])

    U, s, V = np.linalg.svd(h, full_matrices = True)    

    h = np.array(U[:,8])
    #h = U[:,8].reshape(3,3)

    return (h,xin,yin,xout,yout)
    



## FFT ##
# x: data n: span, T: data sampling interval
def EPhaseFFT(x, n, t):
    N = n[0]
    T = t[0]
    frequency = np.fft.rfftfreq(N, d=T)
    amplitude = np.fft.rfft(x,n=N,norm=None) #complex
    #magnitude = np.abs(amplitude)/n*2
    magnitude = np.abs(amplitude)
    phase = np.angle(amplitude) #(-pi, pi]
    #phase = np.angle(amplitude)*180/np.pi
    n_array = np.array([magnitude.shape[0]], dtype = np.int32)

    return (magnitude, phase, frequency, n_array)
    




## AlignFunXIncrease ##
def AlignFunXIncrease2(x, offset, refAbs, refMl, ratio):           
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)       
    refLeft = refAbs
    refRight = refMl
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
        
    #ratioRight = 0.5
    ratioRight = ratio
    ratioLeft = 1-ratioRight
        
    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w / 2 * (xprim[i] + d / 2) + (refLeft + refRight)/2
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            y[i] = ratioRight*refRight + ratioLeft*refLeft
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w  * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0
    return y


## AlignFunXDecrease ##
def AlignFunXDecrease2(x, offset, refAbs, refMl, ratio):        
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)    
    refLeft = refMl
    refRight = refAbs
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
               
    #ratioLeft = 0.5
    ratioLeft = ratio
    ratioRight = 1-ratioLeft

    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w /2 * (xprim[i] + d / 2) + (refLeft + refRight) / 2 
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)  
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            #y[i] = (refRight + refLeft) /2
            y[i] = (ratioRight*refRight + ratioLeft*refLeft)
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            #y[i] = (refRight - refLeft) / w /2* (xprim[i] - d / 2) + (refLeft + refRight) /2
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0

    return y


## FineAlignCurveFit ##
def FineAlignCurveFit2(pos, intensity, dir, slope, refAbs, refMl):
    #  함수 입력은 전부 np.array 임
    init = [(pos[0] + pos[-1]) / 2, refAbs[0], refMl[0], 0.5]

    if dir == 0 and slope == 1:        
        popt, pcov = curve_fit(AlignFunXIncrease2, pos, intensity, p0 =init)
    if dir == 0 and slope == -1:                
        popt, pcov = curve_fit(AlignFunXDecrease2, pos, intensity, p0 =init)
    if dir == 1 and slope == 1:        
        popt, pcov = curve_fit(AlignFunYIncrease2, pos, intensity, p0 =init)
    if dir == 1 and slope == -1:        
        popt, pcov = curve_fit(AlignFunYDecrease2, pos, intensity, p0 =init)

    targetIntensity = np.zeros((1,), dtype=np.float64)
    targetPos = np.zeros((1,), dtype=np.float64)
    targetPos[0] = popt[0]

    #임시
    if dir == 0 and slope == 1:
        intensity_fit = AlignFunXIncrease2(pos,*popt)       
        targetIntensity[0]  = AlignFunXIncrease2(targetPos,*popt);        
    if dir == 0 and slope == -1:
        intensity_fit = AlignFunXDecrease2(pos,*popt)
        targetIntensity[0]  = AlignFunXDecrease2(targetPos,*popt);        
    if dir == 1 and slope == 1:
        intensity_fit = AlignFunYIncrease(pos,*popt)
        targetIntensity[0]  = AlignFunYIncrease2(targetPos,*popt);        
    if dir == 1 and slope == -1:
        intensity_fit = AlignFunYDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunYDecrease2(targetPos,*popt);        
        
    # 임시    
    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl
    outSize = np.array(len(pos),dtype =int)
    

    
    output = (outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize, popt, intensity_fit, targetIntensity)

    #output = (popt, )
    
    return output
#angle = 180 - abs(abs(a1 - a2) - 180); 
####################### MAIN ########################## 


#x = np.linspace(172.76676899999998, 172.77076900000012, 41)
#offset = 172.768669
##refAbs =2000
##refMl = 8000


#refAbs = np.array([2000.])
#refMl = np.array([8000.])

#refAbs[0]=2000
#refMl[0]=8000
##dir = 0
##slope = 1
##y = AlignFunXIncrease(x, offset, refAbs, refMl)

#dir = 0
#slope = -1
#y = AlignFunXDecrease(x, offset, refAbs, refMl)
#plt.plot(x, y)

##dir = 1
##slope = 1
##y = AlignFunYIncrease(x, offset, refAbs, refMl)

##dir = 1
##slope = -1
##y = AlignFunYDecrease(x, offset, refAbs, refMl)

##plt.plot(x, y)

#np.random.seed(1729)
#err_noise = 1 + (np.random.rand(y.size) - 0.5) * 2 * 0.05 # 5%
#y_noise = y * err_noise
#plt.plot(x, y_noise)

#FineAlignCurveFit(x, y_noise, dir, slope, refAbs, refMl)




def tip_tilt(x, y, z):
    x_array = np.array(x)
    y_array = np.array(y)
    z_array = np.array(z)
    array_number = len(x)

    #z_array = 600 - z_array

    #print(x_array)
    #print(y_array)
    #print(z_array)

    n_x = np.array([])  # 법선 벡터의 X좌표 array
    n_y = np.array([])  # 법선 벡터의 X좌표 array
    n_z = np.array([])  # 법선 벡터의 X좌표 array

    for i in range(0, array_number - 2):
        for j in range(i + 1, array_number - 1):
            for k in range(j + 1, array_number):
                p1 = np.array(( x_array[i], y_array[i], z_array[i] ))
                p2 = np.array(( x_array[j], y_array[j], z_array[j] ))
                p3 = np.array(( x_array[k], y_array[k], z_array[k] ))

                p12 = p2 - p1
                p13 = p3 - p1

                if (np.cross(p12, p13)[2] > 0):
                    p_cross = np.cross(p12, p13)
                else:
                    p_cross = np.cross(p13, p12)


                #print(p12)
                #print(p13)


                temp_x = p_cross[0]
                temp_y = p_cross[1]
                temp_z = p_cross[2]

                n_x = np.append(n_x,temp_x)
                n_y = np.append(n_y,temp_y)
                n_z = np.append(n_z,temp_z)


    x = np.average(n_x)  # 법선벡터의 X성분들의 평균값
    y = np.average(n_y)  # 법선벡터의 Y성분들의 평균값
    z = np.average(n_z)  # 법선벡터의 Z성분들의 평균값

                #print("(",x_array[i], y_array[i], z_array[i],"), ",end="")
                #print("(",x_array[j], y_array[j], z_array[j],"), ",end="")
                #print("(",x_array[k], y_array[k], z_array[k],")")

    n_vector = np.array([x,y,z])
#   print(n_vector)

    theta = math.atan(y / z)   # 방향(부호) 확인해야 함.
    pi = math.atan(x / (y * np.sin(theta) + z * np.cos(theta)))

    theta_urad = theta * 1e6
    pi_urad = pi * 1e6

#    print(theta/(10**6))
#    print(pi)

    # 순서 중요함!! 반드시 theta 부터 돌리고, pi 돌려야 함.
    # Tx가 theta인지 pi인지 확실하지 않으므로, 설비에서 테스트 해봐야 함.

    Tx = np.array([theta_urad])
    Ty = np.array([pi_urad])

    output = (Tx,Ty)

    return output
