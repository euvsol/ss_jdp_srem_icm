#include "pch.h"
#include "CSqareOneStageControl.h"

#include <iostream>
#include <vector>
#include <sstream>

using namespace std;

CSqareOneStageControl::CSqareOneStageControl(void)
{
	m_hGetReturnEvent = NULL;
	m_hSetReturnEvent = NULL;
	m_hStopReturnEvent = NULL;
	m_hMoveCompleteEvent = NULL;

	m_hGetReturnEvent   = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hSetReturnEvent   = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hStopReturnEvent  = CreateEvent(NULL, TRUE, FALSE, NULL);
	m_hMoveCompleteEvent = CreateEvent(NULL, TRUE, FALSE, NULL);

	m_bStopFlag = FALSE;
	m_isMoving = FALSE;

	char m_storeBuff[] =  "\0" ; //< 통신 끊김용 버퍼
	m_nRemianBuffIndex = 0; //< 통신 끊긴 버퍼 위치 인덱스
	m_strReceiveEndOfStreamSymbol = "\n";

	m_bOldMoveCompleteFlag = FALSE;
}

CSqareOneStageControl::~CSqareOneStageControl(void)
{
	CloseHandle(m_hGetReturnEvent);
	m_hGetReturnEvent = NULL;
	CloseHandle(m_hSetReturnEvent);
	m_hSetReturnEvent = NULL;
	CloseHandle(m_hStopReturnEvent);
	m_hStopReturnEvent = NULL;
	CloseHandle(m_hMoveCompleteEvent);
	m_hMoveCompleteEvent = NULL;
}

int	CSqareOneStageControl::ReceiveData(char *lParam)
{
	m_strReceivedMsg = lParam;
	SaveLogFile("SQOneControlLog", (LPSTR)(LPCTSTR)_T(("SQONE -> PC : ") + m_strReceivedMsg.Mid(0, m_strReceivedMsg.GetLength()-1))); //통신 상태 기록.
	makeDataSet(lParam);
	return 0;
}

int CSqareOneStageControl::sendGetCommand()
{
	int nRet = 0;
	ResetEvent(m_hGetReturnEvent);
	m_strSendMsg = "Get\r\n";
	SaveLogFile("SQOneControlLog", _T((LPSTR)(LPCTSTR)("PC -> SQONE : " + m_strSendMsg.Mid(0,3))));	//통신 상태 기록.
	nRet = Send((LPSTR)(LPCTSTR)m_strSendMsg, 0 ,1);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> SQONE : SEND FAIL (%d)"), nRet);
		SaveLogFile("SQOneControlLog", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
		return IDS_ERROR_SEND_FAIL;
	}
	nRet = WaitEvent(m_hGetReturnEvent, 2000);

	return nRet;
}

int CSqareOneStageControl::sendSetCommand(double dCommandX, double dCommandY, double dCommandZ, double dCommandRx, double dCommandRy, double dCommandRz, double dCommandCx, double dCommandCy, double dCommandCz)
{
	int nRet = 0;
	ResetEvent(m_hSetReturnEvent);
	ResetEvent(m_hMoveCompleteEvent);
	m_strSendMsg.Format(_T("Set,%f,%f,%f,%f,%f,%f,%f,%f,%f\r\n"), dCommandX, dCommandY, dCommandZ, dCommandRx, dCommandRy, dCommandRz, dCommandCx, dCommandCy, dCommandCz);
	SaveLogFile("SQOneControlLog", _T((LPSTR)(LPCTSTR)("PC -> SQONE : " + m_strSendMsg.Mid(0, m_strSendMsg.GetLength() - 2))));
	nRet = Send((LPSTR)(LPCTSTR)m_strSendMsg, 0, 1);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> SQONE : SEND FAIL (%d)"), nRet);
		SaveLogFile("SQOneControlLog", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
		return IDS_ERROR_SEND_FAIL;
	}
	nRet = WaitEvent(m_hSetReturnEvent, 3000);

  	return nRet;
}

int CSqareOneStageControl::sendStopCommand()
{
	int nRet = 0;
	m_strSendMsg = "Stop\r\n";
	SaveLogFile("SQOneControlLog", m_strSendMsg.Mid(0, m_strSendMsg.GetLength() - 2));
	ResetEvent(m_hStopReturnEvent);
	nRet = Send((LPSTR)(LPCTSTR)m_strSendMsg, 0, 1);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> SQONE : SEND FAIL (%d)"), nRet);
		SaveLogFile("SQOneControlLog", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
		return IDS_ERROR_SEND_FAIL;
	}
	nRet = WaitEvent(m_hStopReturnEvent, 2000);
	m_isMoving = FALSE;
	SetEvent(m_hMoveCompleteEvent);
	return nRet;
}

int CSqareOneStageControl::parseData(char * bufrecv)
{
	int nRet = 0;
	CString strRecvData;
	strRecvData.Format(_T("%s"), bufrecv);
	strRecvData.Replace(_T("\r"), _T(","));
	vector<CString> vecRecvData;
	CString strText(_T(""));

	int nIdx = 0;
	while (FALSE != AfxExtractSubString(strText, bufrecv, nIdx++, _T(',')))
	{
		vecRecvData.push_back(strText);
	}

	int nDataCnt = (int)vecRecvData.size();
	if (nDataCnt == 12)
	{
		m_SQOneParseData.X = atof(vecRecvData[0]);
		m_SQOneParseData.Y = atof(vecRecvData[1]);
		m_SQOneParseData.Z = atof(vecRecvData[2]);
		m_SQOneParseData.Rx = atof(vecRecvData[3]);
		m_SQOneParseData.Ry = atof(vecRecvData[4]);
		m_SQOneParseData.Rz = atof(vecRecvData[5]);
		m_SQOneParseData.Cx = atof(vecRecvData[6]);
		m_SQOneParseData.Cy = atof(vecRecvData[7]);
		m_SQOneParseData.Cz = atof(vecRecvData[8]);
		m_SQOneParseData.TargetExceedRanged = (bool)atoi(vecRecvData[9]);
		m_SQOneParseData.TrisphereStatus = (bool)atoi(vecRecvData[10]);
		m_SQOneParseData.MoveComplete = (bool)atoi(vecRecvData[11]);
		if (m_SQOneParseData.MoveComplete != m_bOldMoveCompleteFlag)
		{
			if (m_SQOneParseData.MoveComplete == TRUE)
			{
				SetEvent(m_hMoveCompleteEvent);
			}
		}
		m_bOldMoveCompleteFlag = m_SQOneParseData.MoveComplete;
		SetEvent(m_hGetReturnEvent);
	}
	else if (nDataCnt == 3)
	{
		m_SQOneParseData.VaildControlPoint = (bool)atoi(vecRecvData[0]);
		//SQOneParseData.TrisphereStatus = (bool)atoi(vecRecvData[1]);
		//SQOneParseData.MoveComplete = (bool)atoi(vecRecvData[2]);
		SetEvent(m_hSetReturnEvent);
	}
	else if (nDataCnt == 1)
	{
		SetEvent(m_hStopReturnEvent);
	}
	else
	{
		nRet = -1;
	}
	return nRet;

}

int CSqareOneStageControl::waitUntilMoveComplete(double dCommandX, double dCommandY, double dCommandZ, double dCommandRx, double dCommandRy, double dCommandRz, double dCommandCx, double dCommandCy, double dCommandCz)
{
	int nRet = -1, tempRet = 0;
	m_isMoving = TRUE;
	tempRet = sendSetCommand(dCommandX, dCommandY, dCommandZ, dCommandRx, dCommandRy, dCommandRz, dCommandCx, dCommandCy, dCommandCz);
	if (tempRet == 0)
	{
		//Sleep(1000);
		if (WaitEvent(m_hMoveCompleteEvent, 30000) == WAIT_OBJECT_0)
		{
			nRet = 0;
		}
		else
		{
			tempRet = sendSetCommand(dCommandX, dCommandY, dCommandZ, dCommandRx, dCommandRy, dCommandRz, dCommandCx, dCommandCy, dCommandCz);
			if (tempRet == 0)
			{
				if (WaitEvent(m_hMoveCompleteEvent, 30000) == WAIT_OBJECT_0)
				{
					nRet = 0;
				}
				else
				{
					sendStopCommand();
					//Sleep(3000);
					tempRet = sendSetCommand(dCommandX, dCommandY, dCommandZ, dCommandRx, dCommandRy, dCommandRz, dCommandCx, dCommandCy, dCommandCz);
					if (tempRet == 0)
					{
						if (WaitEvent(m_hMoveCompleteEvent, 30000) == WAIT_OBJECT_0)
						{
							nRet = 0;
						}
						else
						{
							nRet = -1;
							//time our error
						}
					}
				}
			}
		}
		if (m_bStopFlag == TRUE)
		{
			nRet = -2;
		}
	}

	m_isMoving = FALSE;
	return nRet;
}

void CSqareOneStageControl::makeDataSet(char *lParam)
{
	int nRet = 0, nDataSetIndex = 0, nBuffIndex = 0;
	CString strTemp = lParam;

	for (nBuffIndex = 0; nBuffIndex < strTemp.GetLength(); nBuffIndex++)
	{
		m_storeBuff[nBuffIndex - nDataSetIndex + m_nRemianBuffIndex] = lParam[nBuffIndex];
		if (lParam[nBuffIndex] == '\r')
		{
			nRet = parseData(m_storeBuff);
			// nRet != unexpected string
			memset(m_storeBuff, '\0', sizeof(char)*(nBuffIndex - nDataSetIndex + m_nRemianBuffIndex + 1));
			nDataSetIndex = nBuffIndex + 1;
			m_nRemianBuffIndex = 0;
		}
	}
	m_nRemianBuffIndex += nBuffIndex - nDataSetIndex;
	return;
}