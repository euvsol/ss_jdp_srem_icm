#pragma once

class CLightControllerDlg :	public CDialogEx , public CLightCtrl
{
	DECLARE_DYNAMIC(CLightControllerDlg)

public:
	CLightControllerDlg(CWnd* pParent = nullptr);
	virtual ~CLightControllerDlg();

	enum { IDD = IDD_LIGHT_CTRL_DIALOG };

protected:
	HICON			m_LedIcon[3];

	CSliderCtrl m_LightValueCtrl;
	CEdit m_CurrentValue;

	virtual void DoDataExchange(CDataExchange* pDX);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();

	void InitializeControls();
	
	DECLARE_MESSAGE_MAP()

	afx_msg void OnDestroy();
	afx_msg void OnBnClickedLightCtrlOnoffButton();
	afx_msg void OnBnClickedLightCtrlSetButton();
	afx_msg void OnNMCustomdrawLightCtrlSlider(NMHDR *pNMHDR, LRESULT *pResult);

public:
	int OpenDevice();
	int LightOn();
	int LightOff();
	int SetLightValueDlg(int nValue);

	BOOL Is_Connected() { return m_bSerialConnected; }
};

