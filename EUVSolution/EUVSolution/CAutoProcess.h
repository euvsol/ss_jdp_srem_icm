/**
 * Auto Process Control Module Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * Description: Loading/Unloading, Align/Measuring, Z axis Process(Interlock설정,Focusing), Auto Recovery, Auto Test Procedure 등
 *              자동 Sequence를 Thread를 이용하여 실행하는 Class, 설비 상태 Data Update도 수행
 *              모든 Sequence는 동시에 진행 불가
 */
#pragma once

class CAutoProcess : public CECommon, public IObserver
{
public:
	CAutoProcess();
	~CAutoProcess();

	/**
	 * Description:  MASK의 현재 위치와 Loading상태인지 Unloading상태인지에 대한 구조체. 추후 Pause/Resume 기능, Recovery 기능 개발
	 * NOTHING						: MASK가 현재 SYSTEM에 없는 상태.
	 * MTSCLOSEPOD					: MASK가 현재 MTS의 POD에 있는 상태. POD는 닫혀있는 상태임
	 * MTSOPENPOD_LOADING			: MASK가 현재 MTS의 POD에 있는 상태. Loading 과정 중에 있으며, POD는 열려있는 상태임
	 * ON_ATRHAND_LOADING				: MASK가 현재 MTS ARM에 있는 상태. Loading 과정 중에 있으며, 회전 전 상태임.
	 * ROTATOR_LOADING				: MASK가 현재 MTS ROTATOR에 있는 상태. Loading 과정 중에 있으며, 회전 전 상태임.
	 * ROTATORAFTERROTATE_LOADING	: MASK가 현재 MTS ROTATOR에 있는 상태.Loading 과정 중에 있으며, 회전 후 상태임.
	 * ATRAFTERROTATE_LOADING		: MASK가 현재 MTS ARM에 있는 상태. Loading 과정 중에 있으며, 회전 후 상태임.
	 * FLIPPER_LOADING				: MASK가 현재 MTS FLIPPER에 있는 상태. Loading 과정 중에 있으며, FLIP 전 상태임.
	 * FLIPPERAFTERFLIP_LOADING		: MASK가 현재 MTS FLIPPER에 있는 상태.Loading 과정 중에 있으며, FLIP 후 상태임.
	 * ATRAFTERFLIP_LOADING			: MASK가 현재 MTS ARM에 있는 상태. Loading 과정 중에 있으며, FLIP 후 상태임.
	 * ON_LLC_VENT_LOADING				: MASK가 현재 LLC에 있는 상태. Loading 과정 중에 있으며, LLC 대기 상태임.
	 * ON_LLC_VACUUM_LOADING				: MASK가 현재 LLC에 있는 상태. Loading 과정 중에 있으며, LLC 펌핑 상태임.
	 * ON_VMTR_LOADING					: MASK가 현재 VMTR에 있는 상태.  Loading 과정 중임.
	 * CHUCK							: MASK가 현재 STAGE에 있는 상태.
	 * VMTR_UNLOADING				: MASK가 현재 VMTR에 있는 상태.  Unloading 과정 중임.
	 * LLKVACUUM_UNLOADING			: MASK가 현재 LLC에 있는 상태. Unloading 과정 중에 있으며, LLC 펌핑 상태임.
	 * LLKVENT_UNLOADING				: MASK가 현재 LLC에 있는 상태. Unloading 과정 중에 있으며, LLC 대기 상태임.
	 * ATRHADN_UNLOADING				: MASK가 현재 MTS ARM에 있는 상태. Unloading 과정 중에 있음.
	 * MTSOPENPOD_UNLOADING			: MASK가 현재 MTS의 POD에 있는 상태. Unloading 과정 중에 있으며, POD는 열려있는 상태임
	 */

	typedef enum {
		NOTHING = 0,
		ON_MTS_POD_LOADING,
		ON_ATRHAND_LOADING,
		ON_FLIPPER_LOADING,
		ON_FLIPPER_AFTERFLIP_LOADING,
		ON_ATR_AFTERFLIP_LOADING,
		ON_ROTATOR_LOADING,
		ON_ROTATOR_AFTERROTATE_LOADING,
		ON_ATR_AFTERROTATE_LOADING,
		ON_LLC_LOADING,
		ON_LLC_VACUUM_LOADING,
		ON_VMTRHAND_LOADING,
		ON_CHUCK_LOADING,
		LOADING_COMPLETE,

		ON_CHUCK,
		ON_VMTR_UNLOADING,
		ON_LLC_UNLOADING,
		ON_LLC_VENT_UNLOADING,
		ON_ATRHAND_UNLOADING,
		ON_LLC_VACUUM_UNLOADING,
		ON_ROTATOR_UNLOADING,
		ON_ROTATOR_AFTERROTATE_UNLOADING,
		ON_ATR_AFTERROTATE_UNLOADING,
		ON_FLIPPER_UNLOADING,
		ON_FLIPPER_AFTERFLIP_UNLOADING,
		ON_ATR_AFTERFLIP_UNLOADING,
		ON_MTS_POD_UNLOADING,
		UNLOADING_COMPLETE,
	} TransferState;

	TransferState		CurrentProcess;	/**Process 진행 중인 Mask의 Position 과 상태 정보*/

	BOOL				m_bThreadStop;			/** Loading Sequence Thread를 종료여부를 확인하기 위한 변수  */
	CWinThread*			m_pAutoThread;			/** Loading/Unloading Sequence Thread 포인터 변수.  */

	/**
	* Description:  프로그램 시작 및 물류 진행 전 관계된 모든 모듈들을 초기화한다. MTS,VMTR,Vacuum Modules
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Initialize();

	/**
	* Description:  MTS,VMTR,Stage,OM,ADAM,Source,Vacuum Modules(TMP,Drypump,Guage,IO) 이상상태를 Check한다.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Is_HWModule_OK();

	/**
	* Description:  진행중인 Sequence를 종료시킴. 기본적으로 하나의 Thread밖에 안돌아가므로 실행되고 있는 Sequence는 종료하게 됨
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		ThreadStop();

	/**
	* Description:  외부 모듈에서 Loading Sequence를 시작 시키는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		MaskLoadingStart(BOOL bUseFlip, BOOL bUseRotate, int nAngle);

	/**
	* Description:  외부 모듈에서 Unloading Sequence를 시작 시키는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		MaskUnloadingStart(BOOL bUseFlip, BOOL bUseRotate, int nAngle);

	int		m_nProcessErrorCode;	/** Processing중 발생한 Error Code */

	void	StopSequence();

	int		Check_STAGE_MovePossible();

private:

	BOOL				m_bAtmState;
	int					m_nRotateAngle;
	BOOL				m_bUseRotate;
	BOOL				m_bUseFlip;
	BOOL				m_bLoadingSeq;

	/**
	* Description:   Mask Loading Thread 함수
	* LPVOID pParam: Thread로 데이터 전달을 위한 포인트 매개변수
	* return:		 static UINT
	*/
	static UINT		MaskAutoThread(LPVOID pParam);

	/**
	* Description:  Mask Loading Start 함수. mask의 현재 위치와 상태에 따라 적절한 Loading 동작을 수행시키는 함수.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Auto_Loop();

	int		Loading_Recovery_Work();
	int		Unloading_Recovery_Work();

	/**
	* Description:  Mask Loading Start 전 준비 함수. HW 상태 점검, Stage를 Loading Position으로 이동, POD 유무 등 준비 작업을 수행한다.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_PreWork();

	/**
	* Description:  POD에서 LLC 앞까지 Mask를 Loading하는 함수(Flip,Rotate도 수행)
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_PODtoATR();

	int		Loading_ATRtoFLIPPER();
	int		Loading_MASK_Flip();
	int		Loading_FLIPPERtoATR();

	int		Loading_ATRtoROTATOR();
	int		Loading_MASK_Rotate(int nAngle);
	int		Loading_ROTATORtoATR();

	/**
	* Description:  MTS Robot에서 LLC로 Mask를 Loading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_ATRtoLLC();

	/**
	* Description:  Loading중 Load Lock Pumping 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_LLC_Rough();

	/**
	* Description:  LLC에서 VMTR로 Mask를 Loading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_LLCtoVMTR();

	/**
	* Description:  VMTR에서 STAGE로 Mask를 Loading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_VMTRtoCHUCK();

	/**
	* Description:  Mask Loading 완료 후 수행되는 함수. mask의 Loading 완료 후 수행해야 하는 것들을 수행시키는 함수.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Loading_Complete();


	/**
	* Description:	Mask Unloading Start 전 준비 함수. Stage를 Loading Position으로 이동하거나, Lamp를 끄는 등 작업을 수행한다.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Unloading_PreWork();

	/**
	* Description:	STAGE에서 VMTR로 Mask를 Unloading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Unloading_CHUCKtoVMTR();

	/**
	* Description:	VMTR에서 LLC로 Mask를 Unloading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Unloading_VMTRtoLLC();

	/**
	* Description:	Unloading중 Load Lock Venting 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Unloading_LLC_Vent();

	/**
	* Description:	LLC에서 MTS POD까지 Mask를 Unloading하는 함수
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/

	int		Unloading_LLCtoATR();
	int		Unloading_LLC_Rough();

	int		Unloading_ATRtoROTATOR();
	int		Unloading_MASK_Rotate(int nAngle);
	int		Unloading_ROTATORtoATR();

	int		Unloading_ATRtoFLIPPER();
	int		Unloading_MASK_Flip();
	int		Unloading_FLIPPERtoATR();

	int		Unloading_ATRtoPOD();

	/**
	* Description:	Mask Unloading 완료 후 수행해야 하는 것들을  수행시키는 함수.
	* return:		이상없으면 0, 그외 Error Code를 Return
	*/
	int		Unloading_Complete();

	void	EmergencyStop();
};

