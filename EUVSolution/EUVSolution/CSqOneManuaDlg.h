﻿#pragma once


// CSqOneManuaDlg 대화 상자

class CSqOneManuaDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSqOneManuaDlg)

public:
	CSqOneManuaDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSqOneManuaDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SQONEMANUAL_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	HICON	m_LedIcon[3];
	HICON	m_ArrowIcon[4];

	HBITMAP	m_SqoneStage[3];
	


	DECLARE_MESSAGE_MAP()
	double m_dSetPositionX;
	double m_dSetPositionY;
	double m_dSetPositionZ;
	double m_dSetPositionRx;
	double m_dSetPositionRy;
	double m_dSetPositionRz;
	double m_dSetPositionCx;
	double m_dSetPositionCy;
	double m_dSetPositionCz;
	double m_dSetPositionIncrement;

public:
	CString m_strHorizontal;
	CString m_strVertical;
	CString m_strBeam;
	CString m_strPitch;
	CString m_strYaw;
	CString m_strRoll;

	virtual BOOL OnInitDialog();

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	void getDlgEditValue(BOOL isMacro);
	void updataUI();

	afx_msg void OnBnClickedBtnSqmanualMove();
	afx_msg void OnBnClickedBtnSqmanualGet();
	afx_msg void OnBnClickedBtnSqonemanuslXplus();
	afx_msg void OnBnClickedBtnSqonemanuslXminus();
	afx_msg void OnBnClickedBtnSqonemanuslYplus();
	afx_msg void OnBnClickedBtnSqonemanuslYminus();
	afx_msg void OnBnClickedBtnSqonemanuslZplus();
	afx_msg void OnBnClickedBtnSqonemanuslZminus();
	afx_msg void OnBnClickedBtnSqonemanuslPitchplus();
	afx_msg void OnBnClickedBtnSqonemanuslPitchminus();
	afx_msg void OnBnClickedBtnSqonemanuslYawplus();
	afx_msg void OnBnClickedBtnSqonemanuslYawminus();
	afx_msg void OnBnClickedBtnSqonemanuslRollplus();
	afx_msg void OnBnClickedBtnSqonemanuslRollminus();
	afx_msg void OnBnClickedBtnSqmanualMove2();
	afx_msg void OnBnClickedCheckSqmanualMacro();

	void setMacroButtonWindow(bool state);
	void setMacroEditWindow(bool state);
	afx_msg void OnBnClickedSqonemanualButtonSave();
};
