﻿// CLoginDatabaseDlg.cpp: 구현 파일
//
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


// CLoginDatabaseDlg 대화 상자

IMPLEMENT_DYNAMIC(CLoginDatabaseDlg, CDialogEx)

CLoginDatabaseDlg::CLoginDatabaseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_LOGINDB_DIALOG, pParent)
{
	m_isLogin = FALSE;
	m_strIDNumber = ""; 
	m_strPassword = "";
	m_strName = "";
	m_strAuthority = "";

	m_strLoginIDNumber = "";
	m_strLoginPassword = "";
}

CLoginDatabaseDlg::~CLoginDatabaseDlg()
{
}

void CLoginDatabaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CLoginDatabaseDlg, CDialogEx)
	ON_BN_CLICKED(IDC_LOGINDB_BUTTON_SAVE, &CLoginDatabaseDlg::OnBnClickedLogindbButtonSave)
	ON_BN_CLICKED(IDC_LOGINDB_BUTTON_LOAD, &CLoginDatabaseDlg::OnBnClickedLogindbButtonLoad)
	ON_BN_CLICKED(IDC_LOGINDB_BUTTON_DELETE, &CLoginDatabaseDlg::OnBnClickedLogindbButtonDelete)
	ON_NOTIFY(NM_RCLICK, IDC_LOGINDB_GRID, OnNMRClickLogindbGrid)
	ON_NOTIFY(NM_DBLCLK, IDC_LOGINDB_GRID, OnNMDblclkLogindbGrid)
	ON_NOTIFY(NM_CLICK, IDC_LOGINDB_GRID, OnNMClickLogindbGrid)
END_MESSAGE_MAP()


// CLoginDatabaseDlg 메시지 처리기


int CLoginDatabaseDlg::loginEqipment()
{
	int nRet = 0;
	if (m_isLogin == FALSE)
	{
		int rc = 0;
		sqlite3 *db = NULL;
		sqlite3_stmt *stmt = NULL;
		char *err_msg = { 0 };
		rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기
		if (SQLITE_OK != rc) {
			MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
			return -1;
		}

		CString tempsql = "", tempstr = "";
		tempsql.Format(_T("select * from LOGINDATA WHERE NUMBER = '%s';"), m_strLoginIDNumber);
		char *sql = (LPSTR)(LPCSTR)tempsql;
		puts(sql);
		if (sqlite3_prepare_v2(db, sql, -1, &stmt, 0) == SQLITE_OK)
		{
			int result = 0;

			result = sqlite3_step(stmt);
			if (result == SQLITE_ROW)
			{
				m_strIDNumber = (char*)sqlite3_column_text(stmt, 0);
				m_strName = (char*)sqlite3_column_text(stmt, 1);
				m_strAuthority = (char*)sqlite3_column_text(stmt, 2);
				m_strPassword = (char*)sqlite3_column_text(stmt, 3);
				puts("");
			}
			else
			{

			}
		}
		rc = sqlite3_finalize(stmt);
		rc = sqlite3_close(db);		//DB 닫기
		if (rc != SQLITE_OK)
		{
			MessageBox("sqlite3_close error", sqlite3_errmsg(db), MB_ICONSTOP);
			return -1;
		}

		if (strcmp(m_strLoginPassword, m_strPassword) == 0)
		{
			m_isLogin = TRUE;
			g_pLoginDB->ShowWindow(SW_HIDE);
			if (strcmp(m_strAuthority, "ADMIN") == 0)
			{
				g_pLoginDB->ShowWindow(SW_SHOW);
			}
		}
		else
		{
			MessageBox("Login 실패 하였습니다.");
			g_pLoginDB->ShowWindow(SW_HIDE);
			m_isLogin = FALSE;
		}
	}
	else
	{
		MessageBox("Logout 성공 하였습니다.");
		g_pLoginDB->ShowWindow(SW_HIDE);
		m_isLogin = FALSE;
	}
	return m_isLogin;
}

void CLoginDatabaseDlg::initailizeDatabase()
{

}

void CLoginDatabaseDlg::OnBnClickedLogindbButtonSave()
{
	int rc = 0;
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;
	char *err_msg = { 0 };
	rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		return;
	}
	CString strTempSQL = "", strIDNumber = "", strName = "", strLevel = "", strPassword = "";
	GetDlgItemTextA(IDC_LOGINDB_EDIT_NUMBER, strIDNumber);
	GetDlgItemTextA(IDC_LOGINDB_EDIT_NAME, strName);
	GetDlgItemTextA(IDC_LOGINDB_COMBO_AUTHORITY, strLevel);
	GetDlgItemTextA(IDC_LOGINDB_EDIT_PASSWORD, strPassword);

	strTempSQL.Format("select * from LOGINDATA WHERE NUMBER = '%s';", strIDNumber);

	if (sqlite3_prepare_v2(db, strTempSQL, -1, &stmt, 0) == SQLITE_OK)
	{
		int result = sqlite3_step(stmt);
		if (result == SQLITE_ROW)
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
			AfxMessageBox("이미 등록되어 있는 Number 입니다.");
			return;
		}
	}

	rc = sqlite3_finalize(stmt);
	rc = sqlite3_close(db);		//DB 닫기}

	addDatatoGridfromDB(LOGIN_FILE_FULLPATH, "LOGINDATA", 4, &m_gridLoginDB, TRUE);
	loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "LOGINDATA", 4, &m_gridLoginDB, TRUE);
}

void CLoginDatabaseDlg::OnBnClickedLogindbButtonLoad()
{
	loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "LOGINDATA", 4, &m_gridLoginDB, TRUE);
}

void CLoginDatabaseDlg::OnBnClickedLogindbButtonDelete()
{
	DeleteDatatoGridfromDB();
}


BOOL CLoginDatabaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//g_pLoginDB->initailizeDatabase();
	initializeDBGrid();
	//loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "LOGINDATA", 4, &m_gridLoginDB,TRUE);
	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CLoginDatabaseDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CLoginDatabaseDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}


void CLoginDatabaseDlg::initializeDBGrid()
{
	CRect rect;
	GetDlgItem(IDC_LOGINDB_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_gridLoginDB.Create(rect, this, IDC_LOGINDB_GRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_gridLoginDB.SetEditable(false);
	m_gridLoginDB.SetListMode(true);
	m_gridLoginDB.SetSingleRowSelection(false);
	m_gridLoginDB.EnableDragAndDrop(false);
	m_gridLoginDB.SetRowCount(1);
	m_gridLoginDB.SetFixedRowCount(1);
	m_gridLoginDB.SetColumnCount(5);
	m_gridLoginDB.SetFixedColumnCount(1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_gridLoginDB.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_gridLoginDB.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_gridLoginDB.SetFont(&Font);
	Font.DeleteObject();

	m_gridLoginDB.SetItemText(0, 0, (LPSTR)(LPCTSTR)_T("Index"));
	m_gridLoginDB.SetItemText(0, 1, (LPSTR)(LPCTSTR)_T("ID Number"));
	m_gridLoginDB.SetItemText(0, 2, (LPSTR)(LPCTSTR)_T("Name"));
	m_gridLoginDB.SetItemText(0, 3, (LPSTR)(LPCTSTR)_T("Athourity"));
	m_gridLoginDB.SetItemText(0, 4, (LPSTR)(LPCTSTR)_T("Passwrod"));
	m_gridLoginDB.SetRowHeight(0, 35);
	m_gridLoginDB.SetColumnWidth(0, 70);
	m_gridLoginDB.SetColumnWidth(1, 120);
	m_gridLoginDB.SetColumnWidth(2, 120);
	m_gridLoginDB.SetColumnWidth(3, 120);
	m_gridLoginDB.SetColumnWidth(4, 120);
	m_gridLoginDB.ExpandColumnsToFit();
}


void CLoginDatabaseDlg::OnNMRClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_gridLoginDB.Refresh();
	*pResult = 0;
}


void CLoginDatabaseDlg::OnNMDblclkLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	m_gridLoginDB.SetRowCount(15);
	*pResult = 0;
}


void CLoginDatabaseDlg::OnNMClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	if (pNMItemActivate->iItem == -1)
	{
		SetDlgItemTextA(IDC_LOGINDB_EDIT_NUMBER, "");
		SetDlgItemTextA(IDC_LOGINDB_EDIT_NAME, "");
		SetDlgItemTextA(IDC_LOGINDB_COMBO_AUTHORITY, "");
		SetDlgItemTextA(IDC_LOGINDB_EDIT_PASSWORD, "");
	}
	else
	{
		SetDlgItemTextA(IDC_LOGINDB_EDIT_NUMBER, m_gridLoginDB.GetItemText(pNMItemActivate->iItem, 1));
		SetDlgItemTextA(IDC_LOGINDB_EDIT_NAME, m_gridLoginDB.GetItemText(pNMItemActivate->iItem, 2));
		SetDlgItemTextA(IDC_LOGINDB_COMBO_AUTHORITY, m_gridLoginDB.GetItemText(pNMItemActivate->iItem, 3));
		SetDlgItemTextA(IDC_LOGINDB_EDIT_PASSWORD, m_gridLoginDB.GetItemText(pNMItemActivate->iItem, 4));
	}
	*pResult = 0;


}

int CLoginDatabaseDlg::loadDatatoGridfromDB(char* filePath, char* tableName, int parameterNum, CGridCtrl* DBGrid, BOOL isIndex)
{
	int nRet = 0;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = 0;
	char *err_msg = { 0 };
	DBGrid->DeleteNonFixedRows();
	rc = sqlite3_open(filePath, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		nRet = -1;
		return nRet;
	}
	CString strLoad = "";
	strLoad.Format(_T("select * from %s;"), tableName);
	sqlite3_prepare_v2(db, strLoad, -1, &stmt, NULL);
	int nItem = 1;
	while (sqlite3_step(stmt) != SQLITE_DONE) 
	{
		if (isIndex == TRUE)
		{
			CString index = "";
			index.Format(_T("%d"), nItem);
			DBGrid->InsertRow(index);
			for (size_t i = 0; i < parameterNum; i++)
			{
				DBGrid->SetItemText(nItem, i + 1, (char *)sqlite3_column_text(stmt, i));
			}
			nItem++;
		}
		else
		{
			DBGrid->InsertRow("");
			for (size_t i = 0; i < parameterNum; i++)
			{
				DBGrid->SetItemText(nItem, i, (char *)sqlite3_column_text(stmt, i));
			}
			nItem++;
		}
	}
	sqlite3_finalize(stmt);

	sqlite3_close(db);
	DBGrid->Refresh();
	return nRet;
}

int CLoginDatabaseDlg::addDatatoGridfromDB(char * filePath, char * tableName, int parameterNum, CGridCtrl * DBGrid, BOOL isIndex)
{
	int rc = 0;
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;
	char *err_msg = { 0 };
	rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		return rc;
	}
	CString tempsql = "", tempmodel = "", tempversion = "", tempmoudule, platform = "", onduty = "", test1 = "", test2 = "";;
	GetDlgItemTextA(IDC_LOGINDB_EDIT_NUMBER, tempmodel);
	GetDlgItemTextA(IDC_LOGINDB_EDIT_NAME, tempversion);
	GetDlgItemTextA(IDC_LOGINDB_COMBO_AUTHORITY, tempmoudule);
	GetDlgItemTextA(IDC_LOGINDB_EDIT_PASSWORD, platform);
	//tempsql.Format(_T("UPDATE  LOGINDATA SET (NAME, AUTHORITY, PW) = ('%s','%s','%s') WHERE NUMBER = '%s';")
	//	, tempversion, tempmoudule, platform, tempmodel);
	tempsql.Format(_T("INSERT INTO LOGINDATA VALUES ('%s','%s','%s','%s');")
		,tempmodel, tempversion, tempmoudule, platform );
	
	char *sql = (LPSTR)(LPCSTR)tempsql;
	puts(sql);
	rc = sqlite3_exec(db, sql, NULL, NULL, &err_msg);

	rc = sqlite3_finalize(stmt);

	rc = sqlite3_close(db);		//DB 닫기}}
	return rc;
}

int CLoginDatabaseDlg::DeleteDatatoGridfromDB()
{
	int row = m_gridLoginDB.GetSelectedRow();
	CString strIDNumber = "";
	GetDlgItemTextA(IDC_LOGINDB_EDIT_NUMBER, strIDNumber);
	sqlite3_stmt *stmt = NULL;

	sqlite3 *db;
	int rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기

	if (rc != SQLITE_OK)
	{
		printf("Failed to open DB\n");
		sqlite3_close(db);
		exit(1);
	}

	char *errmsg = NULL;
	CString sql = "";
	sql.Format("delete from LOGINDATA where NUMBER = '%s';", strIDNumber);

	if (SQLITE_OK != sqlite3_exec(db, sql, NULL, NULL, &errmsg))
	{
		sqlite3_finalize(stmt);
		sqlite3_close(db);
	}
	sqlite3_finalize(stmt);
	
	loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "LOGINDATA", 4, &m_gridLoginDB, TRUE);
	return 0;
}

int CLoginDatabaseDlg::UpdateDatatoGridfromDB()
{
	DeleteDatatoGridfromDB();
	g_pSqOneDB->addDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &g_pSqOneDB->m_gridSQONEDB, TRUE);
	g_pSqOneDB->loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &g_pSqOneDB->m_gridSQONEDB, TRUE);
	return 0;
}

int CLoginDatabaseDlg::returnLevel()
{
	if (m_strAuthority == "")
	{
		return -1;
	}
	else if (m_strAuthority == "Operator")
	{
		return 0;
	}
	else if (m_strAuthority == "Engineer")
	{
		return 1;
	}
	else if (m_strAuthority == "Developer")
	{
		return 2;
	}
	else
	{
		return 3; // ADMIN
	}
	return 0;
}

//int CLoginDatabaseDlg::SearchDatafromDB(char* filePath, char* tableName, char* collum, char* searchString, sqlite3_stmt* result)
//{
//	int nRet = 0;
//	sqlite3 *db;
//	sqlite3_stmt *stmt;
//	int rc = 0;
//	char *err_msg = { 0 };
//	rc = sqlite3_open(filePath, &db);		//DB 열기
//	if (SQLITE_OK != rc) {
//		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
//		nRet = -1;
//		return nRet;
//	}
//	CString strSearch = "";
//	strSearch.Format(_T("select * from %s WHERE %s = '%s';"), tableName, collum, searchString);
//
//	sqlite3_prepare_v2(db, strSearch, -1, &stmt, NULL);
//
//	result = stmt;
//	rc = sqlite3_finalize(stmt);
//	rc = sqlite3_close(db);
//	return 0;	
//}
