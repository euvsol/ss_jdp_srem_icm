﻿/**
 * Input/Output Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once
#include "CIOInputDlg.h"
#include "CIOOutputDlg.h"
#include "CIOInputAnalogDlg.h"
#include "CIOOutputAnalogDlg.h"
#include "CIOPartOutputDlg.h"
#include "CIOPartInputDlg.h"
#include "CIOLinePartInputDlg.h"
#include "CIOLinePartOutputDlg.h"

// CCrevisIODlg 대화 상자

class CIODlg : public CDialogEx , public CCrevisIOCtrl, public CECommon
{
	DECLARE_DYNAMIC(CIODlg)

public:
	CIODlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CIODlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_DIALOG };
//#endif
	

protected:
	BOOL			m_bThreadExitFlag;
	CWinThread*		m_pThread;

	BYTE*			m_pInputData;
	BYTE*			m_pOutputData;

	CDigitalInput*				m_pDlgDi;
	CDigitalOutput*				m_pDlgDo;
	CAnalogInput*				m_pDlgAi;
	CAnalogOutput*				m_pDlgAo;
	CDigitalOutputPart*			m_pDlgDoPart;
	CDigitalInputPart*			m_pDlgDiPart;
	CDigitalInputLinePart*		m_pDlgDiLinePart;
	CDigitalOutputLinePart*		m_pDlgDoLinePart;

	BOOL m_bCrevis_Open_Port;			// CREVI IO 연결 성공 여부 변수

	CTabCtrl		m_TabCtrlDi;
	CTabCtrl		m_TabCtrlDo;

	static UINT	IoCheckThread(LPVOID pParam);
	
	void CreateInputControl(int main_tab);
	void CreateOutputControl(int main_tab);

	void GetInputIoData();
	void GetOutputIoData();
		
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	DECLARE_MESSAGE_MAP()

public:

	HICON	m_LedIcon[3];

	char* Log_str;						// LOG 기록.
	CString return_int;					// Write Return 기록.

	int m_nIOMode;						// OPER, MAINT MODE 선택 변수
	int m_nERROR_MODE;					// ERROR MODE ON, OFF 선택 변수

	

	//DIGITAL INPUT
	int m_nDecimal_Digital_in_Module_1 = 0;
	int m_nDecimal_Digital_in_Module_2 = 0;
	int m_nDecimal_Digital_in_Module_3 = 0;
	int m_nDecimal_Digital_in_Module_4 = 0;

	//ANALOG INPUT 
	int m_nDecimal_Analog_in_ch_1 = 0;
	int m_nDecimal_Analog_in_ch_2 = 0;
	int	m_nDecimal_Analog_in_ch_3 = 0;
	int	m_nDecimal_Analog_in_ch_4 = 0;
	int	m_nDecimal_Analog_in_ch_5 = 0;
	int	m_nDecimal_Analog_in_ch_6 = 0;
	int	m_nDecimal_Analog_in_ch_7 = 0;
	int	m_nDecimal_Analog_in_ch_8 = 0;
	int	m_nDecimal_Analog_in_ch_9 = 0;
	int	m_nDecimal_Analog_in_ch_10 = 0;
	int	m_nDecimal_Analog_in_ch_11 = 0;
	int	m_nDecimal_Analog_in_ch_12 = 0;

	//ANALOG OUTPUT
	int m_nDecimal_Analog_out_ch_1 = 0;
	int m_nDecimal_Analog_out_ch_2 = 0;
	int	m_nDecimal_Analog_out_ch_3 = 0;
	int	m_nDecimal_Analog_out_ch_4 = 0;

	//DIGITAL OUTPUT
	int m_nDecimal_Digital_out_Module_1 = 0;
	int m_nDecimal_Digital_out_Module_2 = 0;
	int m_nDecimal_Digital_out_Module_3 = 0;
	int m_nDecimal_Digital_out_Module_4 = 0;

	int m_nDecimal_Addr_In[ANALOG_INPUT_SIZE + DIGITAL_INPUT_SIZE] = { 0 };
	int m_nDecimal_Addr_Out[ANALOG_OUTPUT_SIZE + DIGITAL_OUTPUT_SIZE] = { 0 };
	int m_nDecimal_Addr_01 = 0;
	int m_nDecimal_Addr_02 = 0;
	int m_nDecimal_Addr_03 = 0;
	int m_nDecimal_Addr_04 = 0;
	int position = 0;
	int position_sub = 32;

	bool m_bDigitalIn[DIGITAL_INPUT_NUMBER];
	bool m_bDigitalOut[DIGITAL_OUTPUT_NUMBER];

	bool m_bCamsensor_Trigger_On; 

	int OpenDevice(CString strIpAddr);

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangingTabDigitalIn(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangeTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnSelchangingTabDigitalOut(NMHDR *pNMHDR, LRESULT *pResult);

	void SetIVNavigatorPgm(int Num);

	BOOL Is_CREVIS_Connected() { return m_bCrevis_Open_Port; }

	//Vacuum Sequence에서 필요로하는 함수
	int Open_LLCGateValve();				//Crevis IO로 정상 명령전달 되었으면 TRUE, 아니면 FALSE 반환
	int Open_TRGateValve();
	int Open_SlowVent_Inlet_Valve1();
	int Open_SlowVent_Outlet_Valve1();
	int Open_SlowVentValve1();
	int Open_SlowVentValve2();
	int Open_FastVent_Inlet_Valve1();
	int Open_FastVent_Outlet_Valve1();
	int Open_FastVentValve();
	int Open_MC_SlowRoughValve();
	int Open_MC_FastRoughValve();
	int Open_MC_TMP1_GateValve();
	int Open_MC_TMP1_ForelineValve();
	int Open_MC_TMP2_GateValve();
	int Open_MC_TMP2_ForelineValve();
	int Open_LLC_SlowRoughValve();
	int Open_LLC_FastRoughValve();
	int Open_LLC_TMP_GateValve();
	int Open_LLC_TMP_ForelineValve();
	int Open_SourceGate_Check();		// Source Gate On 확인. ( output )

	int Close_LLCGateValve();				//Crevis IO로 정상 명령전달 되었으면 TRUE, 아니면 FALSE 반환
	int Close_TRGateValve();
	int Close_SlowVent_Inlet_Valve1();
	int Close_SlowVent_Outlet_Valve1();
	int Close_SlowVentValve1();
	int Close_SlowVentValve2();
	int Close_FastVent_Inlet_Valve();
	int Close_FastVent_Outlet_Valve();
	int Close_FastVentValve();
	int Close_MC_SlowRoughValve();
	int Close_MC_FastRoughValve();
	int Close_MC_TMP1_GateValve();
	int Close_MC_TMP1_ForelineValve();
	int Close_MC_TMP2_GateValve();
	int Close_MC_TMP2_ForelineValve();
	int Close_LLC_SlowRoughValve();
	int Close_LLC_FastRoughValve();
	int Close_LLC_TMP_GateValve();
	int Close_LLC_TMP_ForelineValve();
	int Close_SourceGate_Check();

	int Is_LLCGateValve_Open();				//Valve가 정상 Open되었으면 1, 아니면 0, 잘 모르겠으면 -1 반환 
	int Is_TRGateValve_Open();
	int Is_SlowVent_Inlet_Valve1_Open();
	int Is_SlowVent_Outlet_Valve1_Open();
	int Is_SlowVentValve1_Open();
	int Is_SlowVentValve2_Open();
	int Is_FastVentValve_Open();
	int Is_FastVent_Inlet_Valve1_Open();
	int Is_FastVent_Outlet_Valve1_Open();
	int Is_MC_SlowRoughValve_Open();
	int Is_MC_FastRoughValve_Open();
	int Is_MC_TMP1_GateValve_Open();
	int Is_MC_TMP1_ForelineValve_Open();
	int Is_MC_TMP2_GateValve_Open();
	int Is_MC_TMP2_ForelineValve_Open();
	int Is_LLC_SlowRoughValve_Open();
	int Is_LLC_FastRoughValve_Open();
	int Is_LLC_TMP_GateValve_Open();
	int Is_LLC_TMP_ForelineValve_Open();
	int Is_LaserSource_Open();
	int Is_Water_Valve_Open();				// Water Supply , Return 상태 확인. ( 상태 Run Off시, Dry, tmp 바로 OFF)
	int Is_Mask_OnChuck();					//Chuck위에 Mask가 있으면 TRUE, 아니면 FALSE 반환
	int Is_Mask_OnVMTR();				//VMTR에 Mask가 있으면 TRUE, 아니면 FALSE 반환
	int Is_Mask_OnVMTR_LLC();				//VMTR에 Mask가 있으면 TRUE, 아니면 FALSE 반환
	int Is_Mask_OnLLC();					//LLC에 Mask가 있으면 TRUE, 아니면 FALSE 반환

	int Is_Mask_Check_OnChuck();			//Chuck위에 Mask가 있으면 TRUE, 아니면 FALSE 반환  (기울기 무시, 오로지 마스크만 Check)
	int Is_Mask_Check_OnLLC();				//LLC에 Mask가 있으면 TRUE, 아니면 FALSE 반환 (기울기 무시, 오로지 마스크만 Check)
	
	int Is_Isolator_Up();					//Isolator가 작동 중이면 TRUE, 아니면 FALSE 반환

	int Send_TriggerToCamSensor();			 //Cam Sensor에 트리거 신호 
	int SetTriggerCamSensor(BOOL bTriggerOn);

	int Get_LLC_DryPump_Status();			//LLC 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) DRYPUMP_RUN,DRYPUMP_STOP,DRYPUMP_ERROR,DRYPUMP_WARNING,...
	int Get_MC_DryPump_Status();			//MC 상태를 General_define.h에 정의하고, 현재 상태를 return: ex) DRYPUMP_RUN,DRYPUMP_STOP,DRYPUMP_ERROR,DRYPUMP_WARNING,...
	int Get_LLC_Line_ATM_Status();			// LLC ATM 센서 상태에 따른 LLC GATE OPEN/CLOSE 확인.  Gerneral_define.h 에 정의 ( ATM_STATE 대기상태 : 0 . VACUUM_STATE 진공상태 : 1)
	int Get_Water_Supply_Status();			// Main  Water Supply 상태 확인.
	int Get_Air_Supply_Status();			// Main Air Supply 상태 확인.
	int Get_Leak_Status();					// Water leak Check (LLC, MC 통합)
	int Get_LLC_Leak_Status();				// LLC Water leak Check
	int Get_MC_Leak_Status();				// MC Water leak Check
	int Get_detact_Status();				// Smoke detact Check
	int Water_Valve_Open();					// DRY , TMP 공급 Water Line 확인. (supply, Return 통합)
	int Water_Supply_Valve_Open();			// DRY , TMP 공급 Water Supply 확인.
	int Water_Return_Valve_Open();			// DRY , TMP 공급 Water Return 확인.
	int Is_SourceGate_OpenOn_Check();		// SOURCE GATE OPEN On 확인
	int Get_Loading_Position_Status();		// Stage Loading Position Check

	bool Is_VAC_Robot_Arm_Retract();		// VAC ROBOT ARM 확인  (TRUE = retract)
	bool Is_VAC_Robot_Arm_Extend();			// VAC ROBOT ARM 확인 (TRUE = extend)
	bool Is_ATM_Robot_Arm_Retract();		// ARM ROBOt ARM 확인 (TRUE = extend , FALSE = retract)

	bool Is_LLC_Atm_Check();
	bool Is_MC_Atm_Check();
	bool Is_MC_Vac_Check();

	bool Is_LLC_Mask_Slant_Check();			// LLC MASK 기울기 센서 감지 확인(#1, #2 통합)
	bool Is_MC_Mask_Slant_Check();			// MC MASK 기울기 센서 감지 확인(#1, #2 통합)

	bool Is_LLC_Mask_Slant1_Check();		// LLC MASK1 기울기 센서 감지 확인(개별)
	bool Is_LLC_Mask_Slant2_Check();		// LLC MASK2 기울기 센서 감지 확인(개별)
	bool Is_MC_Mask_Slant1_Check();			// MC MASK1 기울기 센서 감지 확인(개별)
	bool Is_MC_Mask_Slant2_Check();			// MC MASK2 기울기 센서 감지 확인(개별)

	bool Buzzer_on();						// Buzzer On
	bool Buzzer_off();						// Buzzer Off
	bool LLC_Dry_Pump_off();				// LLC Dry Pump Off
	bool MC_Dry_Pump_off();					// MC Dry Pump Off

	bool m_bAPSequenceNormalCloseFlag;

	double Get_MFC2_N2_10sccm()		{ return (double)(m_nDecimal_Analog_in_ch_5) / 1638.0; }
	double Get_MFC1_N2_20000sccm()  { return (double)(m_nDecimal_Analog_in_ch_6) / 1638.0; }

	int Str_ok_Box(CString Log, CString str_nCheck);

	int Valve_Check(bool OPEN, bool CLOSE);

	void Main_Error_Status();
	
	int Error_On(int ErrorCode, char* str);
	int Error_Sequence(int ErrorCode, char* str);

	int Set_DVR_Lamp(int dvr_ch, bool state);

	// DRY PUMP ERROR 시 발생 sequence.
	int MC_Dry_Pump_Error_Sequence();
	int LLC_Dry_Pump_Error_Sequence();

	afx_msg void OnBnClickedBtnAnalog();
	afx_msg void OnBnClickedBtnDigital();
	afx_msg void OnBnClickedBtnMaint();
	afx_msg void OnBnClickedBtnOper();

	clock_t	m_start_time, m_finish_time;

	//EVENT HANDLE
	HANDLE m_LLCGate_WriteEvent;
	HANDLE m_TRGate_WriteEvent;
	HANDLE m_SlowVent_WriteEvent;
	HANDLE m_LLCForelineGate_WriteEvent;
	HANDLE m_LLCTmpGate_WriteEvent;
	HANDLE m_LLCFastRough_WriteEvent;
	HANDLE m_LLCSlowRough_WriteEvent;
	HANDLE m_MCForelineGate_WriteEvent;
	HANDLE m_MCTmpGate_WriteEvent;
	HANDLE m_MCFastRough_WriteEvent;
	HANDLE m_MCSlowRough_WriteEvent;
	HANDLE m_FastVent_WriteEvent;
	HANDLE m_FastVent_Inlet_WriteEvent_Close;
	HANDLE m_SlowVent_Inlet_WriteEvent_Close;
	//HANDLE m_Source_Gate_Open_On;

	HANDLE m_LLCGate_WriteEvent_Open;
	HANDLE m_TRGate_WriteEvent_Open;
	HANDLE m_SlowVent_WriteEvent_Open;
	HANDLE m_LLCForelineGate_WriteEvent_Open;
	HANDLE m_LLCTmpGate_WriteEvent_Open;
	HANDLE m_LLCFastRough_WriteEvent_Open;
	HANDLE m_LLCSlowRough_WriteEvent_Open;
	HANDLE m_MCForelineGate_WriteEvent_Open;
	HANDLE m_MCTmpGate_WriteEvent_Open;
	HANDLE m_MCFastRough_WriteEvent_Open;
	HANDLE m_MCSlowRough_WriteEvent_Open;
	HANDLE m_FastVent_WriteEvent_Open;
	HANDLE m_TRGate_Vauccum_Value_On;
	HANDLE m_FastVent_Inlet_WriteEvent_Open;
	HANDLE m_SlowVent_Inlet_WriteEvent_Open;

	// Digital Output
	typedef enum {
		SIGNAL_TOWER_LED = 0,
		SIGNAL_TOWER_YELLOW,
		SIGNAL_TOWER_GREEN,
		SIGNAL_TOWER_BUZZER,
		DigitalOutput_4,
		LL_GATE_OPEN_WRITE,
		LL_GATE_CLOSE_WRITE,
		LL_SLOW_ROUGH_VV,
		LL_FAST_ROUGH_VV,
		FAST_VENT_INLET,
		SLOW_VENT_INLET,
		MC_SLOW_ROUGH,
		MC_FAST_ROUGH,
		LL_FORELINE,
		CHAMBER_FORELINE,
		DigitalOutput_15,
		DigitalOutput_16,
		DigitalOutput_17,
		DigitalOutput_18,
		FAST_VENT_OUTLET,
		SLOW_VENT_OUTLET,
		DigitalOutput_21,
		LL_TMP_GATE,
		MC_TMP_GATE,
		DigitalOutput_24,
		TR_GATE_OPEN_WRITE,
		TR_GATE_CLOSE_WRITE,
		DigitalOutput_27,
		SHUTTER_1,
		SHUTTER_2,
		SHUTTER_3,
		SHUTTER_4,
		DRY_PUMP1,
		DRY_PUMP2,
		DigitalOutput_34,
		COVER_DOOR,
		LL_CCD_CAMERA,
		DVR1_LAMP,
		DVR2_LAMP,
		DVR3_LAMP,
		DVR4_LAMP,
		DigitalOutput_41,
		SHUTTER_OPEN,
		DigitalOutput_43,
		WATER_SUPPLY_VALVE,
		WATER_RETURN_VALVE,
		DigitalOutput_46,
		CAM_SENSOR_TRIGGER,
	}Digital_Output_Io;

	//Digital_Output_Io Dout;

	// Digital Input
	typedef enum {
		MAIN_AIR = 0,
		MAIN_WATER,
		LL_GATE_OPEN,
		LL_GATE_CLOSE,
		TR_GATE_OPEN,
		TR_GATE_CLOSE,
		LL_ATM_SENSOR,
		EXTERNAL_FAN_ON,
		CB_LEFT_FAN_ON,
		CB_RIGHT_FAN_ON,
		LL_TMP_GATE_OPEN,
		LL_TMP_GATE_CLOSE,
		MC_TMP_GATE_OPEN,
		MC_TMP_GATE_CLOSE,
		m_bDigitalin_14,
		m_bDigitalin_15,
		LL_DRY_PUMP1_ON_STATUS,
		LL_DRY_PUMP1_ALARM,
		LL_DRY_PUMP1_WARNING,
		MC_DRY_PUMP2_ON_STATUS,
		MC_DRY_PUMP2_ALARM,
		MC_DRY_PUMP2_WARNING,
		m_bDigitalin_22,
		WATER_LEAK_SENSOR1_LL_TMP,
		WATER_LEAK_SENSOR2_MC_TMP,
		m_bDigitalin_25,
		SMOKE_DETACT_SENSOR_CB,
		SMOKE_DETACT_SENSOR_VAC_ST,
		m_bDigitalin_28,
		m_bDigitalin_29,
		m_bDigitalin_30,
		m_bDigitalin_31,
		EMO_ON_STATUS_1_CB,
		EMO_ON_STATUS_2_COVER,
		EMO_ON_STATUS_3_COVER,
		EMO_ON_STATUS_4_COVER,
		EMO_ON_STATUS_5_MTS_FRONT,
		EMO_ON_STATUS_6_MTS_LEFT,
		EMO_ON_STATUS_6_MTS_RIGH,
		CP1_ON_MULTI_CONSENT1,
		CP2_ON_MULTI_CONSENT2,
		CP3_ON_SAFETY_PLC_MODULE,
		CP4_ON_CONTROL_DC_IN,
		CP5_ON_LASER_MODULE,
		CP6_ON_MFC_POWER,
		CP7_ON_MTS_PC_FFU,
		CP8_ON_VAC_STG_CONTROLLER,
		CP9_ON_MAINT_LAMP,
		CP10_ON_PI_SCAN_STAGE,
		CP11_ON_ADAM_PC,
		CP12_ON_LL_DRY_PUMP,
		CP13_ON_MC_DRY_PUMP,
		CP14_ON_LL_TMP,
		CP15_ON_MC_TMP,
		CP16_CONSENT3,
		CP17_ON_VAC_ROBOT,
		CP18_ON_MTS,
		CP19_ON_ISOLATION_CON,
		CP20_ON_SPARE,
		CP21_ON_IO_MODULE,
		CP22_ON_SENSOR_PWR,
		CP23_ON_OM_POWER,
		m_bDigitalin_62,
		m_bDigitalin_63,
		COVER_OPEN_STATUS1,
		COVER_OPEN_STATUS2,
		COVER_OPEN_STATUS3,
		COVER_OPEN_STATUS4,
		MC_ATM_SW,
		WATER_RETURN_TEMP_ALARM,
		LL_LINE_PRESSURE_SW,
		MC_LINE_PRESSURE_SW,
		MAINT_MODE,
		VAC_ROBOT_HAND_EXTEND_STATUS_LLC,
		LLC_MASK_ON,
		LLC_MASK_SLANT1,
		LLC_MASK_SLANT2,
		MAIN_MASK_ON,
		MAIN_MASK_SLANT1,
		MAIN_MASK_SLANT2,
		MC_MASK_OK_CAM_SENSOR,
		VAC_ROBOT_HAND_EXTEND_STATUS_MC,
		STAGE_LOADING_POSITION,
		LASER_ALARM_STATUS,
		LASER_VALVE_OPEN_STATUS,
		LASER_VALVE_CLOSE_STATUS,
		LASER_ON_STATUS,
		VAC_ROBOT_ARM_EXTEND_STATUS,
		VAC_ROBOT_ARM_RETRACT_STATUS,
		m_bDigitalin_89,
		ATM_ROBOT_ARM_RETRACT_STATUS,
		COVER_OPEN_STATUS5,
		m_bDigitalin_92,
		m_bDigitalin_93,
		LLC_MASK_OK_CAM_SENSOR,
		m_bDigitalin_95,
		m_bDigitalin_96,
		m_bDigitalin_97,
		m_bDigitalin_98,
		m_bDigitalin_99,
		LL_SLOW_ROUGHING_OPEN,
		LL_FAST_ROUGHING_OPEN,
		LL_ROUGHING_CLOSE,
		MC_SLOW_ROUGHING_OPEN,
		MC_FAST_ROUGHING_OPEN,
		MC_ROUGHING_CLOSE,
		LL_FORELINE_OPEN,
		LL_FORELINE_CLOSE,
		MC_FORELINE_OPEN,
		MC_FORELINE_CLOSE,
		m_bDigitalin_110,
		m_bDigitalin_111,
		MC_GAUGE_SET_POINT,
		LL_GAUGE_SET_POINT,
		m_bDigitalin_114,
		ISOLATOR_INTERLOCK_SENSOR,
		MAIN_LID_AIR_SWITCH,
		ISOLATOR_AIR_SWITCH,
		m_bDigitalin_118,
		m_bDigitalin_119,
		m_bDigitalin_120,
		m_bDigitalin_121,
		m_bDigitalin_122,
		m_bDigitalin_123,
		m_bDigitalin_124,
		m_bDigitalin_125,
		m_bDigitalin_126,
		m_bDigitalin_127,
		m_bDigitalin_128,
	}Digital_Input_Io;

	//Digital_Input_Io Din;



	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedBtnLine();

	void Closedevice_IO();
	int SelXrayCamera();
	int SelCcdCamera();
	BOOL CheckXrayCcdCamera();
};
