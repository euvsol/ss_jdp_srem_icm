﻿// CSQOneDataBaseDlg.cpp: 구현 파일
//
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include "CSqOneConfigDlg.h"
#include "CSQOneDataBaseDlg.h"


// CSQOneDataBaseDlg 대화 상자

IMPLEMENT_DYNAMIC(CSQOneDataBaseDlg, CDialogEx)

CSQOneDataBaseDlg::CSQOneDataBaseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SQONEDB_DIALOG, pParent)
{

	m_isLoaded = FALSE;
}

CSQOneDataBaseDlg::~CSQOneDataBaseDlg()
{
}

void CSQOneDataBaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CSQOneDataBaseDlg, CDialogEx)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_SAVEPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddposition)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_LOADPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonLoadposition)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_DELETEPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonDeleteposition)

	ON_NOTIFY(NM_RCLICK, IDC_SQONEDB_GRID, OnNMRClickLogindbGrid)
	ON_NOTIFY(NM_DBLCLK, IDC_SQONEDB_GRID, OnNMDblclkLogindbGrid)
	ON_NOTIFY(NM_CLICK, IDC_SQONEDB_GRID, OnNMClickLogindbGrid)
	ON_NOTIFY(NM_RCLICK, IDC_SQONEDB_GRID_MEASURE, OnNMRClickMeasureGrid)
	ON_NOTIFY(NM_DBLCLK, IDC_SQONEDB_GRID_MEASURE, OnNMDblclkMeasureGrid)
	ON_NOTIFY(NM_CLICK, IDC_SQONEDB_GRID_MEASURE, OnNMClickMeasureGrid)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_GETCURRENTPOSITION, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonGetcurrentposition)
	ON_BN_CLICKED(IDC_SQONEDB_BUTTON_ADDTOSYSTEMDB, &CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddtosystemdb)
END_MESSAGE_MAP()


// CSQOneDataBaseDlg 메시지 처리기


BOOL CSQOneDataBaseDlg::DestroyWindow()
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::DestroyWindow();
}


BOOL CSQOneDataBaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	initializeDBGrid();
	initializeGridMesure();
	if (g_pSqOneControl->IsFileExist(LOGIN_FILE_FULLPATH) == TRUE)
	{
		//loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &m_gridSQONEDB, TRUE);
		//loadReverseData();
	}
	//loadSQONEStagePosition();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


BOOL CSQOneDataBaseDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddposition()
{

	addDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &m_gridSQONEDB, TRUE);
	loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &m_gridSQONEDB, TRUE);

}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonLoadposition()
{
	if (g_pSqOneControl->IsFileExist(LOGIN_FILE_FULLPATH) == TRUE)
	{
		loadDatatoGridfromDB(LOGIN_FILE_FULLPATH,"SQONEDATA",8,&m_gridSQONEDB,TRUE);
	}
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonDeleteposition()
{
	DeleteDatatoGridfromDB();
}



void CSQOneDataBaseDlg::initializeDBGrid()
{
	CRect rect;
	GetDlgItem(IDC_SQONEDB_GRID)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_gridSQONEDB.Create(rect, this, IDC_SQONEDB_GRID);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_gridSQONEDB.SetEditable(false);
	m_gridSQONEDB.SetListMode(true);
	m_gridSQONEDB.SetSingleRowSelection(false);
	m_gridSQONEDB.EnableDragAndDrop(false);
	m_gridSQONEDB.SetRowCount(1);
	m_gridSQONEDB.SetFixedRowCount(1);
	m_gridSQONEDB.SetColumnCount(9);
	m_gridSQONEDB.SetFixedColumnCount(1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_gridSQONEDB.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_gridSQONEDB.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_gridSQONEDB.SetFont(&Font);
	Font.DeleteObject();

	m_gridSQONEDB.SetItemText(0, 0, (LPSTR)(LPCTSTR)_T("No"));
	m_gridSQONEDB.SetItemText(0, 1, (LPSTR)(LPCTSTR)_T("Name"));
	m_gridSQONEDB.SetItemText(0, 2, (LPSTR)(LPCTSTR)_T("Horizontal X"));
	m_gridSQONEDB.SetItemText(0, 3, (LPSTR)(LPCTSTR)_T("Vertical Y"));
	m_gridSQONEDB.SetItemText(0, 4, (LPSTR)(LPCTSTR)_T("Beam Z"));
	m_gridSQONEDB.SetItemText(0, 5, (LPSTR)(LPCTSTR)_T("Pitch Rx"));
	m_gridSQONEDB.SetItemText(0, 6, (LPSTR)(LPCTSTR)_T("Yaw Ry"));
	m_gridSQONEDB.SetItemText(0, 7, (LPSTR)(LPCTSTR)_T("Roll Rz"));
	m_gridSQONEDB.SetItemText(0, 8, (LPSTR)(LPCTSTR)_T("Revision Date"));
	m_gridSQONEDB.SetRowHeight(0, 35);
	m_gridSQONEDB.SetColumnWidth(0, 40);
	m_gridSQONEDB.SetColumnWidth(1, 130);
	m_gridSQONEDB.SetColumnWidth(2, 80);
	m_gridSQONEDB.SetColumnWidth(3, 80);
	m_gridSQONEDB.SetColumnWidth(4, 80);
	m_gridSQONEDB.SetColumnWidth(5, 80);
	m_gridSQONEDB.SetColumnWidth(6, 80);
	m_gridSQONEDB.SetColumnWidth(7, 80);
	m_gridSQONEDB.SetColumnWidth(8, 130);
	m_gridSQONEDB.ExpandColumnsToFit();
}

void CSQOneDataBaseDlg::initializeGridMesure()
{
	CRect rect;
	GetDlgItem(IDC_SQONEDB_GRID_MEASURE)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_gridMeasureEDB.Create(rect, this, IDC_SQONEDB_GRID_MEASURE);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_gridMeasureEDB.SetEditable(FALSE);
	m_gridMeasureEDB.SetListMode(true);
	m_gridMeasureEDB.SetSingleRowSelection(false);
	m_gridMeasureEDB.EnableDragAndDrop(false);
	m_gridMeasureEDB.SetRowCount(1);
	m_gridMeasureEDB.SetFixedRowCount(1);
	m_gridMeasureEDB.SetColumnCount(9);
	m_gridMeasureEDB.SetFixedColumnCount(1);
	m_gridMeasureEDB.SetSortColumn(8);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_gridMeasureEDB.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_gridMeasureEDB.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_gridMeasureEDB.SetFont(&Font);
	Font.DeleteObject();

	m_gridMeasureEDB.SetItemText(0, 0, (LPSTR)(LPCTSTR)_T("No"));
	m_gridMeasureEDB.SetItemText(0, 1, (LPSTR)(LPCTSTR)_T("Name"));
	m_gridMeasureEDB.SetItemText(0, 2, (LPSTR)(LPCTSTR)_T("Horizontal X"));
	m_gridMeasureEDB.SetItemText(0, 3, (LPSTR)(LPCTSTR)_T("Vertical Y"));
	m_gridMeasureEDB.SetItemText(0, 4, (LPSTR)(LPCTSTR)_T("Beam Z"));
	m_gridMeasureEDB.SetItemText(0, 5, (LPSTR)(LPCTSTR)_T("Pitch Rx"));
	m_gridMeasureEDB.SetItemText(0, 6, (LPSTR)(LPCTSTR)_T("Yaw Ry"));
	m_gridMeasureEDB.SetItemText(0, 7, (LPSTR)(LPCTSTR)_T("Roll Rz"));
	m_gridMeasureEDB.SetItemText(0, 8, (LPSTR)(LPCTSTR)_T("Revision Date"));
	m_gridMeasureEDB.SetRowHeight(0, 35);
	m_gridMeasureEDB.SetColumnWidth(0, 40);
	m_gridMeasureEDB.SetColumnWidth(1, 130);
	m_gridMeasureEDB.SetColumnWidth(2, 80);
	m_gridMeasureEDB.SetColumnWidth(3, 80);
	m_gridMeasureEDB.SetColumnWidth(4, 80);
	m_gridMeasureEDB.SetColumnWidth(5, 80);
	m_gridMeasureEDB.SetColumnWidth(6, 80);
	m_gridMeasureEDB.SetColumnWidth(7, 80);
	m_gridMeasureEDB.SetColumnWidth(8, 130);
	m_gridMeasureEDB.ExpandColumnsToFit();
}

void CSQOneDataBaseDlg::OnNMRClickLogindbGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
}

void CSQOneDataBaseDlg::OnNMDblclkLogindbGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	CString strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "";

	strHorizontal = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 2);
	strVertical = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 3);
	strBeam = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 4);
	strPitch = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 5);
	strYaw = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 6);
	strRoll = m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 7);

	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		int nRet = AfxMessageBox("SQ one stage를 움직이시겠습니까", MB_YESNO);
		if (nRet == IDYES)
		{
			g_pSqOneControl->moveAbsoluteSQ1(_ttof(strHorizontal), _ttof(strVertical), _ttof(strBeam), _ttof(strPitch), _ttof(strYaw), _ttof(strRoll));
			g_pSqOneDB->ShowWindow(SW_HIDE);
			g_pSqOneManual->ShowWindow(SW_SHOW);
		}
		else
		{
			return;
		}
	}
	else
	{
		AfxMessageBox("통신이 연결되지 않았습니다.");
	}
}

void CSQOneDataBaseDlg::OnNMClickLogindbGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (pNMItemActivate->iItem == -1)
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, "");
	}
	else
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 1));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 2));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 3));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 4));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 5));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 6));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, m_gridSQONEDB.GetItemText(pNMItemActivate->iItem, 7));
	}
	*pResult = 0;
}

void CSQOneDataBaseDlg::OnNMRClickMeasureGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (pNMItemActivate->iItem == -1)
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, "");
	}
	else
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 1));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 2));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 3));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 4));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 5));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 6));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 7));
	}

	if (AfxMessageBox(_T("Current Position Data를 삭제하시겠습니까"),MB_YESNO) == IDYES)
	{
		CPasswordDlg pwdlg(this);
		pwdlg.DoModal();
		if (pwdlg.m_strTxt != "srem")
		{
			::AfxMessageBox("Password가 일치 하지 않습니다.");
			return;
		}
		int row = m_gridMeasureEDB.GetSelectedRow();
		CString strSql = "", strName = "", strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "", strDate = "";
		GetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, strName);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, strHorizontal);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, strVertical);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, strBeam);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, strPitch);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, strYaw);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, strRoll);
		strDate = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 8);
		sqlite3_stmt *stmt = NULL;

		sqlite3 *db;
		int rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기

		if (rc != SQLITE_OK)
		{
			printf("Failed to open DB\n");
			sqlite3_close(db);
			exit(1);
		}

		char *errmsg = NULL;
		strSql.Format("DELETE FROM SQMEASUREDATA where name ='%s' and Horizontal='%s'and vertical='%s'and beam='%s'and pitch='%s'and yaw='%s'and roll='%s' and date='%s';"
			, strName, strHorizontal, strVertical, strBeam, strPitch, strYaw, strRoll, strDate);

		if (SQLITE_OK != sqlite3_exec(db, strSql, NULL, NULL, &errmsg))
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
		}
		sqlite3_finalize(stmt);

		loadReverseData();
		//loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQMEASUREDATA", 8, &m_gridSQONEDB, TRUE);
		return;
	}

}

void CSQOneDataBaseDlg::OnNMDblclkMeasureGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	CString strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "";

	strHorizontal = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 2);
	strVertical = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 3);
	strBeam = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 4);
	strPitch = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 5);
	strYaw = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 6);
	strRoll = m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 7);

	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		int nRet = AfxMessageBox("SQ one stage를 움직이시겠습니까", MB_YESNO);
		if (nRet == IDYES)
		{
			g_pSqOneControl->moveAbsoluteSQ1(_ttof(strHorizontal), _ttof(strVertical), _ttof(strBeam), _ttof(strPitch), _ttof(strYaw), _ttof(strRoll));
			g_pSqOneDB->ShowWindow(SW_HIDE);
			g_pSqOneManual->ShowWindow(SW_SHOW);
		}
		else
		{
			return;
		}
	}
	else
	{
		AfxMessageBox("통신이 연결되지 않았습니다.");
	}
}

void CSQOneDataBaseDlg::OnNMClickMeasureGrid(NMHDR * pNMHDR, LRESULT * pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	if (pNMItemActivate->iItem == -1)
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, "");
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, "");
	}
	else
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 1));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 2));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 3));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 4));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 5));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 6));
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, m_gridMeasureEDB.GetItemText(pNMItemActivate->iItem, 7));
	}
	*pResult = 0;
}

int CSQOneDataBaseDlg::loadDatatoGridfromDB(char * filePath, char * tableName, int parameterNum, CGridCtrl * DBGrid, BOOL isIndex)
{
	int nRet = 0;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = 0;
	char *err_msg = { 0 };
	DBGrid->DeleteNonFixedRows();
	rc = sqlite3_open(filePath, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		nRet = -1;
		return nRet;
	}

	CString strLoad = "";
	strLoad.Format(_T("select * from %s;"), tableName);
	sqlite3_prepare_v2(db, strLoad, -1, &stmt, NULL);
	int nItem = 1;
	while (sqlite3_step(stmt) != SQLITE_DONE)
	{
		if (isIndex == TRUE)
		{
			CString index = "";
			index.Format(_T("%d"), nItem);
			DBGrid->InsertRow(index);
			for (size_t i = 0; i < parameterNum; i++)
			{
				DBGrid->SetItemText(nItem, i + 1, (char *)sqlite3_column_text(stmt, i));
			}
			nItem++;
		}
		else
		{
			DBGrid->InsertRow("");
			for (size_t i = 0; i < parameterNum; i++)
			{
				DBGrid->SetItemText(nItem, i, (char *)sqlite3_column_text(stmt, i));
			}
			nItem++;
		}
	}
	sqlite3_finalize(stmt);

	sqlite3_close(db);
	DBGrid->Refresh();
	return nRet;
}

int CSQOneDataBaseDlg::addDatatoGridfromDB(char * filePath, char * tableName, int parameterNum, CGridCtrl * DBGrid, BOOL isIndex)
{
	int rc = 0;
	sqlite3 *db = NULL;
	sqlite3_stmt *stmt = NULL;
	char *err_msg = { 0 };
	rc = sqlite3_open(filePath, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		return rc;
	}
	CString strSql = "", strName = "", strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "",strDate = "";
	GetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, strName);
	CTime tempTime = CTime::GetCurrentTime();
	if (strName == "")
	{
		strName.Format(_T("%02d,%02d,%02d:%02d:%02d"), tempTime.GetMonth(), tempTime.GetDay(), tempTime.GetHour(), tempTime.GetMinute(), tempTime.GetSecond());
		strDate.Format(_T("%02d,%02d,%02d:%02d:%02d"), tempTime.GetMonth(), tempTime.GetDay(), tempTime.GetHour(), tempTime.GetMinute(), tempTime.GetSecond());
	}
	else
	{
		strDate.Format(_T("%02d,%02d,%02d:%02d:%02d"), tempTime.GetMonth(), tempTime.GetDay(), tempTime.GetHour(), tempTime.GetMinute(), tempTime.GetSecond());
	}
	GetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, strHorizontal);
	strHorizontal.Format(_T("%4.4lf"), _ttof(strHorizontal));
	GetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, strVertical);
	strVertical.Format(_T("%4.4lf"), _ttof(strVertical));
	GetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, strBeam);
	strBeam.Format(_T("%4.4lf"), _ttof(strBeam));
	GetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, strPitch);
	strPitch.Format(_T("%4.4lf"), _ttof(strPitch));
	GetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, strYaw);
	strYaw.Format(_T("%4.4lf"), _ttof(strYaw));
	GetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, strRoll);
	strRoll.Format(_T("%4.4lf"), _ttof(strRoll));

	//tempsql.Format(_T("UPDATE  LOGINDATA SET (NAME, AUTHORITY, PW) = ('%s','%s','%s') WHERE NUMBER = '%s';")
	//	, tempversion, tempmoudule, platform, tempmodel);
	strSql.Format(_T("INSERT INTO %s VALUES ('%s','%s','%s','%s','%s','%s','%s','%s');")
		,tableName, strName, strHorizontal, strVertical, strBeam,strPitch,strYaw,strRoll,strDate);

	char *sql = (LPSTR)(LPCSTR)strSql;
	puts(sql);
	rc = sqlite3_exec(db, sql, NULL, NULL, &err_msg);

	rc = sqlite3_finalize(stmt);

	rc = sqlite3_close(db);		//DB 닫기}}
	return rc;
}

int CSQOneDataBaseDlg::DeleteDatatoGridfromDB()
{
	if (AfxMessageBox(_T("삭제하시겠습니까"), MB_YESNO) == IDYES)
	{

		int row = m_gridSQONEDB.GetSelectedRow();
		CString strSql = "", strName = "", strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "", strDate = "";
		GetDlgItemTextA(IDC_SQONEDB_EDIT_NAME, strName);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, strHorizontal);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, strVertical);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, strBeam);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, strPitch);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, strYaw);
		GetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, strRoll);
		sqlite3_stmt *stmt = NULL;

		sqlite3 *db;
		int rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기

		if (rc != SQLITE_OK)
		{
			printf("Failed to open DB\n");
			sqlite3_close(db);
			exit(1);
		}

		char *errmsg = NULL;
		strSql.Format("DELETE FROM SQONEDATA where name ='%s' and Horizontal='%s'and vertical='%s'and beam='%s'and pitch='%s'and yaw='%s'and roll='%s';"
			, strName, strHorizontal, strVertical, strBeam, strPitch, strYaw, strRoll, strDate);

		if (SQLITE_OK != sqlite3_exec(db, strSql, NULL, NULL, &errmsg))
		{
			sqlite3_finalize(stmt);
			sqlite3_close(db);
		}
		sqlite3_finalize(stmt);

		loadDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQONEDATA", 8, &m_gridSQONEDB, TRUE);
		return 0;
	}
	return 0;
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonGetcurrentposition()
{
	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		SetDlgItemTextA(IDC_SQONEDB_EDIT_HORIZONTAL, g_pSqOneManual->m_strHorizontal);
		SetDlgItemTextA(IDC_SQONEDB_EDIT_VERTICAL, g_pSqOneManual->m_strVertical);
		SetDlgItemTextA(IDC_SQONEDB_EDIT_BEAM, g_pSqOneManual->m_strBeam);
		SetDlgItemTextA(IDC_SQONEDB_EDIT_PITCH, g_pSqOneManual->m_strPitch);
		SetDlgItemTextA(IDC_SQONEDB_EDIT_YAW, g_pSqOneManual->m_strYaw);
		SetDlgItemTextA(IDC_SQONEDB_EDIT_ROLL, g_pSqOneManual->m_strRoll);
	}
}


void CSQOneDataBaseDlg::OnBnClickedSqonedbButtonAddtosystemdb()
{
	//SELECT "_rowid_", *FROM "main"."SQMEASUREDATA"  ORDER BY "Date" DESC LIMIT 0, 49999;
	addDatatoGridfromDB(LOGIN_FILE_FULLPATH, "SQMEASUREDATA", 8, &m_gridMeasureEDB, TRUE);
	loadReverseData();
}

int CSQOneDataBaseDlg::MoveSystemPosition()
{
	int nRet = 0;
	CString strHorizontal = "", strVertical = "", strBeam = "", strPitch = "", strYaw = "", strRoll = "";

	strHorizontal = m_gridMeasureEDB.GetItemText(2, 2);
	strVertical = m_gridMeasureEDB.GetItemText(2, 3);
	strBeam = m_gridMeasureEDB.GetItemText(2, 4);
	strPitch = m_gridMeasureEDB.GetItemText(2, 5);
	strYaw = m_gridMeasureEDB.GetItemText(2, 6);
	strRoll = m_gridMeasureEDB.GetItemText(2, 7);

	if (g_pSqOneControl->m_bConnected == TRUE)
	{
		 nRet = AfxMessageBox("SQ one stage를 움직이시겠습니까", MB_YESNO);
		if (nRet == IDYES)
		{
			nRet = g_pSqOneControl->moveAbsoluteSQ1(_ttof(strHorizontal), _ttof(strVertical), _ttof(strBeam), _ttof(strPitch), _ttof(strYaw), _ttof(strRoll));
			if (nRet == 0)
			{
				m_bIsSystemPostion = TRUE;
				g_pSqOneDB->ShowWindow(SW_HIDE);
				g_pSqOneConfig->ShowWindow(SW_HIDE);
				g_pSqOneManual->ShowWindow(SW_SHOW);
			}
			else
			{
				m_bIsSystemPostion = FALSE;
			}
		}
		else
		{
			return nRet;
		}
	}
	else
	{
		AfxMessageBox("통신이 연결되지 않았습니다.");
	}
	return nRet;
}

int CSQOneDataBaseDlg::loadReverseData()
{
	int nRet = 0;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	int rc = 0;
	char *err_msg = { 0 };
	m_gridMeasureEDB.DeleteNonFixedRows();
	rc = sqlite3_open(LOGIN_FILE_FULLPATH, &db);		//DB 열기
	if (SQLITE_OK != rc) {
		MessageBox("sqlite3_open error", sqlite3_errmsg(db), MB_ICONSTOP);
		nRet = -1;
		return nRet;
	}
	CString strLoad = "";
	strLoad.Format(_T("select * from %s ORDER BY \"Date\" DESC LIMIT 0, 49999;"), "SQMEASUREDATA");
	sqlite3_prepare_v2(db, strLoad, -1, &stmt, NULL);
	int nItem = 1;
	while (sqlite3_step(stmt) != SQLITE_DONE)
	{
		CString index = "";
		index.Format(_T("%d"), nItem);
		m_gridMeasureEDB.InsertRow(index);
		for (size_t i = 0; i < 8; i++)
		{
			m_gridMeasureEDB.SetItemText(nItem, i + 1, (char *)sqlite3_column_text(stmt, i));
		}
		nItem++;

	}
	sqlite3_finalize(stmt);

	sqlite3_close(db);
	m_gridMeasureEDB.Refresh();
	return nRet;
}
