﻿// CIOLinePartInputDlg.cpp: 구현 파일
//


#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CDigitalInputLinePart 대화 상자

IMPLEMENT_DYNAMIC(CDigitalInputLinePart, CDialogEx)

CDigitalInputLinePart::CDigitalInputLinePart(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IO_LINE_PART_INPUT_DIALOG, pParent)
{

}

CDigitalInputLinePart::~CDigitalInputLinePart()
{
	m_brush.DeleteObject();
	m_brush2.DeleteObject();
	m_font.DeleteObject();
}

void CDigitalInputLinePart::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDigitalInputLinePart, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CDigitalInputLinePart 메시지 처리기


BOOL CDigitalInputLinePart::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_brush.CreateSolidBrush(LIGHT_GRAY); // Gague 배경 색
	m_brush2.CreateSolidBrush(GRAY); // Gague 배경 색
	m_font.CreateFont(25, 10, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("Arial"));

	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


HBRUSH CDigitalInputLinePart::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if ((pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_LINE_PART_TEXT01) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_LINE_PART_TEXT02) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_LINE_PART_TEXT03) || (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITALIN_LINE_PART_TEXT04))
		{
			pDC->SetBkColor(LIGHT_GRAY);
			//pDC->SetTextColor(RGB(0, 255, 0));
			return m_brush;
		}
		else if (pWnd->GetDlgCtrlID() == IDC_STATIC_DIGITAIN_LINE_PART_TEXT)
		{
			pDC->SetBkColor(GRAY);
			return m_brush2;
		}
	}

	return hbr;
}


void CDigitalInputLinePart::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);

	if (nIDEvent == INPUT_LINE_PART_DIALOG_TIMER)
	{
		//if (g_pIO->m_Crevis_Open_Port == TRUE)
		{
			OnUpdateDigitalInput_Line_Part();
			SetTimer(INPUT_LINE_PART_DIALOG_TIMER, 100, NULL);
		}
	}


	CDialogEx::OnTimer(nIDEvent);
}


void CDigitalInputLinePart::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}


void CDigitalInputLinePart::InitControls_DI_LINE_PART()
{

	CString strTemp;
	int Digital_In_Static_num = 21;

	for (int nIdx = 0; nIdx < Digital_In_Static_num; nIdx++)
	{
		strTemp.Format(_T("X%04d"), nIdx);
		SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_NUM0 + nIdx, strTemp);
		((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y000 + nIdx))->SetIcon(m_LedIcon[0]);

	}

	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y000, "LLC DRY PUMP#1 ON STATUS");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y001, "LLC DRY PUMP#1 ALARM");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y002, "LLC DRY PUMP#1 WARNING");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y003, "MC DRY PUMP#2 ON STATUS");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y004, "MC DRY PUMP#2 ALARM");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y005, "MC DRY PUMP#2 WARNING	");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y006, "MAIN AIR S/W");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y007, "MAIN WATER S/W");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y008, "WATER RETURN TEMP ALARM");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y009, "ISOLATOR INTERLOCK SENSOR");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y010, "MAIN LID AIR SWITCH");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y011, "ISOLATOR AIR SWITCH");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y012, "WATER LEAK SENSOR#1 (LL TMP)");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y013, "WATER LEAK SENSOR#2 (MC TMP#1)");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y014, "SMOKE DETACT SENSOR (CB)");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y015, "SMOKE DETACT SENSOR (VAC ST)");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y016, "COVER OPEN STATUS#1");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y017, "COVER OPEN STATUS#2");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y018, "COVER OPEN STATUS#3");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y019, "COVER OPEN STATUS#4");
	SetDlgItemText(IDC_STATIC_DIGITALIN_LINE_PART_Y020, "COVER OPEN STATUS#5");


		SetTimer(INPUT_LINE_PART_DIALOG_TIMER, 100, NULL);
}


void CDigitalInputLinePart::OnUpdateDigitalInput_Line_Part()
{
	if (g_pIO->Is_CREVIS_Connected() == TRUE)
	{
		if (g_pIO->m_bDigitalIn[g_pIO->LL_DRY_PUMP1_ON_STATUS] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y000))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y000))->SetIcon(m_LedIcon[0]);
		}
		//17 --> 1 정상 구동 상태, 0 이면 알람 발생
		if (g_pIO->m_bDigitalIn[g_pIO->LL_DRY_PUMP1_ALARM] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y001))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y001))->SetIcon(m_LedIcon[2]);
		}
		//18  --> 1 정상 구동 상태, 0 이면 알람 발생
		if (g_pIO->m_bDigitalIn[g_pIO->LL_DRY_PUMP1_WARNING] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y002))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y002))->SetIcon(m_LedIcon[2]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->MC_DRY_PUMP2_ON_STATUS] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y003))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y003))->SetIcon(m_LedIcon[0]);
		}
		//20 --> 1 정상 구동 상태, 0 이면 알람 발생
		if (g_pIO->m_bDigitalIn[g_pIO->MC_DRY_PUMP2_ALARM] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y004))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y004))->SetIcon(m_LedIcon[2]);
		}
		//21 --> 1 정상 구동 상태, 0 이면 알람 발생
		if (g_pIO->m_bDigitalIn[g_pIO->MC_DRY_PUMP2_WARNING] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y005))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y005))->SetIcon(m_LedIcon[2]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->MAIN_AIR] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y006))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y006))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->MAIN_WATER] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y007))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y007))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->WATER_RETURN_TEMP_ALARM] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y008))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y008))->SetIcon(m_LedIcon[2]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->ISOLATOR_INTERLOCK_SENSOR] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y009))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y009))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->MAIN_LID_AIR_SWITCH] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y010))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y010))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->ISOLATOR_AIR_SWITCH] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y011))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y011))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->WATER_LEAK_SENSOR1_LL_TMP] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y012))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y012))->SetIcon(m_LedIcon[2]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->WATER_LEAK_SENSOR2_MC_TMP] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y013))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y013))->SetIcon(m_LedIcon[2]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->SMOKE_DETACT_SENSOR_CB] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y014))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y014))->SetIcon(m_LedIcon[2]);
		}
		//27 ////////////전장 연결 아직 안됨.
		if (g_pIO->m_bDigitalIn[g_pIO->SMOKE_DETACT_SENSOR_VAC_ST] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y015))->SetIcon(m_LedIcon[0]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y015))->SetIcon(m_LedIcon[0]);

		}
		if (g_pIO->m_bDigitalIn[g_pIO->COVER_OPEN_STATUS1] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y016))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y016))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->COVER_OPEN_STATUS2] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y017))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y017))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->COVER_OPEN_STATUS3] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y018))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y018))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->COVER_OPEN_STATUS4] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y019))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y019))->SetIcon(m_LedIcon[0]);
		}
		if (g_pIO->m_bDigitalIn[g_pIO->COVER_OPEN_STATUS5] == 1)
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y020))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_DIGITALIN_LINE_PART_Y020))->SetIcon(m_LedIcon[0]);
		}
	}
}