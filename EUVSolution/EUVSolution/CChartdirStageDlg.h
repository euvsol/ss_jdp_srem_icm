﻿#pragma once

#include "ChartViewer.h"
#include "afxwin.h"

// CChartdirStageDlg 대화 상자


const int sampleSize = 240;
static const int DataInterval = 350;

class CChartdirStageDlg : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CChartdirStageDlg)

public:
	CChartdirStageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CChartdirStageDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_STAGE_CHART_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	HICON	m_LedIcon[3];


	DECLARE_MESSAGE_MAP()
public:
	//CStatic m_ChartViewer;
	virtual BOOL OnInitDialog();

	void drawChart(CChartViewer *viewer);
	CChartViewer m_ChartViewer;
	XYChart *Multiline_chart;

	bool Clear_state;
	bool Long_Run;
	bool Buffer_Run;

	double m_nextDataTime;	// Used by the random number generator to generate real time data.

	double m_timeStamps[sampleSize];	// The timestamps for the data series
	double m_dataSeriesA[sampleSize];	// The values for the data series A
	double m_dataSeriesB[sampleSize];	// The values for the data series B
	double m_dataSeriesC[sampleSize];	// The values for the data series C

	int m_extBgColor;
	int getDefaultBgColor();

	void getData();
	void getDataClear();
	void DataReset();
	void Run();

	int year;
	int mon;
	int day;
	int hour;
	int min;
	int sec;

	double dataA;
	double dataB;
	double dataC;


	CString user_time;
	
	int user_time_int;
	double user_time_double;
	int num;

	CString stage_data_file;
	void Stage_Load_Log_WriteStart();
	void Stage_Load_Stop();
	void Stage_Load_Start();
	void Log_th__Start();

	CWinThread		*m_pStageLoadLogWriteThread;
	static UINT		StageLoadLogWrite_Thread(LPVOID pParam);
	BOOL			m_pStageLoadLogWriteStop;
	BOOL			IsNumeric(CString& csStr);

	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnViewPortChanged();
	CEdit m_edit_user_time;
	afx_msg void OnBnClickedBtnLongrun();
	afx_msg void OnBnClickedBtnStart();
	afx_msg void OnBnClickedBtnClear();
	afx_msg void OnBnClickedBtnStop();
	CComboBox m_UpdatePeriod;
	afx_msg void OnSelchangeUpdatePeriod();
	CEdit m_stage_log;
	afx_msg void OnDestroy();
};
