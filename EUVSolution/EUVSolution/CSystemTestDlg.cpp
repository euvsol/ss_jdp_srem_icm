﻿#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>
#include <math.h>

#define  STAGE_CHECK_DATA_SIZE  50


// CSystemTestDlg 대화 상자

IMPLEMENT_DYNAMIC(CSystemTestDlg, CDialogEx)

CSystemTestDlg::CSystemTestDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SYSTEM_TEST_DIALOG, pParent)
	, m_bTestStop(TRUE)
	, m_nRepeatNo(0)
	, m_nCurrentNo(0)
	, m_bRepeatabilityCheck(FALSE)
	, m_dRepeatPosX(0)
	, m_dRepeatPosY(0)
{
	m_nTestType = 0;

	m_bAutoSequenceProcessing = FALSE;

	//
	m_pSourceTestThreadStop = TRUE;
	m_pSourceTestThread = NULL;

	//
	m_pOffsetThreadStop = TRUE;
	m_pOffsetThread = NULL;


	// Stage 측정 Flag , Thread
	m_stageThreadExitFlag = TRUE;
	m_pstageThread = NULL;
	
	
	// PI Stage 측정 Flag, Thread
	m_PIstageThreadExitFlag = TRUE;
	m_pPIstageThread = NULL;
	
	m_PIstageManualThreadExitFlag = TRUE;
	m_pPIstageManualThread = NULL;
	//test();

	m_Point_Num = 3;
}

CSystemTestDlg::~CSystemTestDlg()
{
	if (m_pSourceTestThread != NULL)
	{
		m_pSourceTestThreadStop = TRUE;
		if (WaitForSingleObject(m_pSourceTestThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pSourceTestThread->m_hThread, 0);
		}
	}

	if (m_pOffsetThread != NULL)
	{
		m_pOffsetThreadStop = TRUE;
		if (WaitForSingleObject(m_pOffsetThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pOffsetThread->m_hThread, 0);
		}
	}

	if (m_pstageThread != NULL)
	{
		m_stageThreadExitFlag = TRUE;
		if (WaitForSingleObject(m_pstageThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pstageThread->m_hThread, 0);
		}
	}

	if (m_pPIstageThread != NULL)
	{
		m_PIstageThreadExitFlag = TRUE;
		if (WaitForSingleObject(m_pPIstageThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pPIstageThread->m_hThread, 0);
		}
	}

	if (m_pPIstageManualThread != NULL)
	{
		m_PIstageManualThreadExitFlag = TRUE;
		if (WaitForSingleObject(m_pPIstageManualThread->m_hThread, 5000) != WAIT_OBJECT_0)
		{
			::TerminateThread(m_pPIstageManualThread->m_hThread, 0);
		}
	}
	
}

void CSystemTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK_TESTEND, m_bTestStop);
	DDX_Text(pDX, IDC_EDIT_TEST_REPEATNO, m_nRepeatNo);
	DDX_Text(pDX, IDC_EDIT_TEST_REPEATNO2, m_nCurrentNo);
	DDX_Control(pDX, IDC_CHECK_TESTEND, m_CheckTestEndCtrl);
	DDX_Control(pDX, IDC_CHECK_MASKSLIPTEST, m_CheckMaskSlipTestCtrl);
	DDX_Control(pDX, IDC_CHECK_EUV_STAGEREPEAT_TEST, m_CheckEUVStageRepeatTestCtrl);
	DDX_Control(pDX, IDC_CHECK_OM_STAGEREPEAT_TEST, m_CheckOMStageRepeatTestCtrl);
	DDX_Control(pDX, IDC_CHECK_STAGELONGRUN_TEST, m_CheckStageLongRunTestCtrl);
	DDX_Control(pDX, IDC_PROGRESS_MASK_FLAT, m_progress_mask_flat);
	DDX_Control(pDX, IDC_PROGRESS_SOURCE_CONTAMINATION, m_progress_source_contamination);
	DDX_Control(pDX, IDC_PROGRESS_STAGE_MEASUREMENT, m_progress_stage_check);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_REF_X, m_ref_pos_x);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_REF_y, m_ref_pos_y);
	DDX_Control(pDX, IDC_EDIT_PI_STAGE_REF_X2, m_point_user_num);
	DDX_Check(pDX, IDC_TEST_LOADING_REPEAT_CHECK, m_bRepeatabilityCheck);
	DDX_Text(pDX, IDC_TEST_NAVI_XPOS_EDIT, m_dRepeatPosX);
	DDX_Text(pDX, IDC_TEST_NAVI_YPOS_EDIT, m_dRepeatPosY);
}


BEGIN_MESSAGE_MAP(CSystemTestDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LOAD_TEST_BUTTON, &CSystemTestDlg::OnBnClickedLoadTestButton)
	ON_BN_CLICKED(IDC_UNLOAD_TEST_BUTTON, &CSystemTestDlg::OnBnClickedUnloadTestButton)
	ON_BN_CLICKED(IDC_SEQ_STOP_BUTTON, &CSystemTestDlg::OnBnClickedSeqStopButton)
	ON_EN_CHANGE(IDC_EDIT_TEST_REPEATNO, &CSystemTestDlg::OnEnChangeEditTestRepeatno)
	ON_BN_CLICKED(IDC_CHECK_STAGELONGRUN_TEST, &CSystemTestDlg::OnBnClickedCheckStagelongrunTest)
	ON_BN_CLICKED(IDC_CHECK_OM_STAGEREPEAT_TEST, &CSystemTestDlg::OnBnClickedCheckOmStagerepeatTest)
	ON_BN_CLICKED(IDC_CHECK_EUV_STAGEREPEAT_TEST, &CSystemTestDlg::OnBnClickedCheckEuvStagerepeatTest)
	ON_BN_CLICKED(IDC_CHECK_MASKSLIPTEST, &CSystemTestDlg::OnBnClickedCheckMasksliptest)
	ON_BN_CLICKED(IDC_CHECK_TESTEND, &CSystemTestDlg::OnBnClickedCheckTestend)
	ON_BN_CLICKED(IDC_LLC_TEST_BUTTON, &CSystemTestDlg::OnBnClickedLlcTestButton)
	ON_BN_CLICKED(IDC_LOAD_TEST_BUTTON3, &CSystemTestDlg::OnBnClickedLoadTestButton3)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT, &CSystemTestDlg::OnBnClickedCheckMaskFlat)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_SET, &CSystemTestDlg::OnBnClickedCheckMaskFlatSet)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_DEFAULT, &CSystemTestDlg::OnBnClickedCheckMaskFlatDefault)
	ON_BN_CLICKED(IDC_CHECK_OFFSET_START, &CSystemTestDlg::OnBnClickedCheckOffsetStart)
	ON_BN_CLICKED(IDC_CHECK_MASK_FLAT_STOP, &CSystemTestDlg::OnBnClickedCheckMaskFlatStop)
	ON_BN_CLICKED(IDC_STAGE_MEASUREMENT_START, &CSystemTestDlg::OnBnClickedStageMeasurementStart)
	ON_BN_CLICKED(IDC_STAGE_MEASUREMENT_STOP, &CSystemTestDlg::OnBnClickedStageMeasurementStop)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_1_MANUAL, &CSystemTestDlg::OnBnClickedCheckPiStagePoint1Manual)
	ON_BN_CLICKED(IDC_CHECK_STAGE_MOVE_POS, &CSystemTestDlg::OnBnClickedCheckStageMovePos)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_2_MANUAL, &CSystemTestDlg::OnBnClickedCheckPiStagePoint2Manual)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_3_MANUAL, &CSystemTestDlg::OnBnClickedCheckPiStagePoint3Manual)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_AUTO, &CSystemTestDlg::OnBnClickedCheckPiStagePointAuto)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_POINT_1_CAL, &CSystemTestDlg::OnBnClickedCheckPiStagePoint1Cal)
	ON_BN_CLICKED(IDC_CHECK_PI_STAGE_READ, &CSystemTestDlg::OnBnClickedCheckPiStageRead)
	ON_BN_CLICKED(IDC_CHECK_STAGE_MOVE_POS2, &CSystemTestDlg::OnBnClickedCheckStageMovePos2)
END_MESSAGE_MAP()


// CSystemTestDlg 메시지 처리기

BOOL CSystemTestDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CSystemTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	CString str;
	str.Empty();
	str.Format("%d", m_Point_Num);
	m_point_user_num.SetWindowTextA(str);

	// ICON
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 24, 24, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[1]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[1]);

	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);

	LLC_Vacuum_Test_State = Pumping_START_State;
	InitPosGrid();

	set_num = 0;
	
	cap_read_value[200] = {0,};

	
	m_progress_mask_flat.SetRange(0, 100);
	m_progress_mask_flat.SetPos(0);

	m_progress_stage_check.SetRange(0, 20);
	m_progress_stage_check.SetPos(0);


	default_x_pos = 204.0;
	default_y_pos = 0.0;

	m_progress_source_contamination.SetRange(0, 300);
	m_progress_source_contamination.SetPos(0);

	

	CString str_defualt_x_pos;
	CString str_defualt_y_pos;

	get_x_pos = default_x_pos;
	get_y_pos = default_y_pos;

	str_defualt_x_pos.Format("%.4f", default_x_pos);
	str_defualt_y_pos.Format("%.4f", default_y_pos);


	SetDlgItemText(IDC_EDIT_TEST_MASK_X_POSITION, str_defualt_x_pos);
	SetDlgItemText(IDC_EDIT_TEST_MASK_Y_POSITION, str_defualt_y_pos);

	CString ini_text;
	ini_text = _T("0.0");
	SetDlgItemText(IDC_EDIT_TEST_MASK_X_VIEW, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_Y_VIEW, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_Z_VIEW, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_TY_VIEW, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_TX_VIEW, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_X_START, ini_text);
	SetDlgItemText(IDC_EDIT_TEST_MASK_Y_START, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_X_POSITION, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Y_POSITION, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Z_POSITION, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CAP, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SEG, ini_text);
	SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CEN, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, ini_text);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_X, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Y, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Z, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TIP, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TILT, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_X, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_Y, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_X, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_Y, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_CAP2, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_CAP2, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_CAP2, ini_text);

	SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS, ini_text);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS, ini_text);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_REF_POS, ini_text);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_REF_POS, ini_text);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP1, ini_text);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP2, ini_text);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP3, ini_text);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP4, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_NVECTOR, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_NVECTOR, ini_text);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_NVECTOR, ini_text);
	
	m_ref_pos_x.SetWindowTextA(ini_text);
	m_ref_pos_y.SetWindowTextA(ini_text);

	if (g_pEUVSource->Is_EUV_On() != FALSE)
	{
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV ON");
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV OFF");
	}

	if (g_pEUVSource->Is_LaserShutter_Opened() != FALSE)
	{
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "OPEN");
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "CLOSE");
	}

	if (g_pScanStage != NULL && g_pNavigationStage != NULL && g_pCamera != NULL
		&& g_pWarning != NULL && g_pAdam != NULL && g_pConfig != NULL)
	{
		//SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	}
	

	//테스트 플래그
	m_bTestStop = TRUE;

	// 각각의 테스트 플래그
	m_Mask_Measrue_Start = TRUE;
	m_Source_Measrue_Start = TRUE;
	m_OMEUV_Measrue_Start = TRUE;
	m_SH_Stage_Measrue_Start = TRUE;
	m_PI_Stage_Measrue_Start = TRUE;


	// PI Stage Check Point 변수 
	check_point = 0;


	// Reference Positon 
	x_pos_ref = 0.0;
	y_pos_ref = 0.0;

	// n1 n2,n3 Vector 
	x1um = 0.0; 
	y1um = 0.0; 
	z1um = 0.0;
	x2um = 0.0;
	y2um = 0.0;
	z2um = 0.0;
	x3um = 0.0;
	y3um = 0.0;
	z3um = 0.0;
	cap1um = 0.0;
	cap2um = 0.0;
	cap3um = 0.0;

	n1_vec = 0.0;
	n2_vec = 0.0;
	n3_vec = 0.0;

	roll = 0.0;
	pitch = 0.0;

	//PI 스테이지 측정 레시피 입력 변수 초기화
	//for (int cnt = 0; cnt < 5; cnt++)
	//{
	//	pi_flat_rcp_y[cnt] = 0.0;
	//	pi_flat_rcp_x[cnt] = 0.0;
	//}

	// 각 Point 측정완료 플래그 
	point_1 = false;
	point_2 = false;
	point_3 = false;

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CSystemTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CSystemTestDlg::OnTimer(UINT_PTR nIDEvent)
{
	KillTimer(nIDEvent);
	switch (nIDEvent)
	{
	case MASK_FLATNESS_MEASUREMENT_TIMER :
		Stage_axis_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	case SOURCE_MEASUREMENT_TIMER :
		Source_measurement_axis_check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	case PI_STAGE_MEASUREMENT_TIMER:
		PI_Stage_Check_Axis_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	case STAGE_MEASUREMENT_TIMER :
		SH_Stage_Check_Axis_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	case REFERENCE_POS_CHECK_TIMER:
		//PI_Stage_Faltness_Monitor_Check();
		SetTimer(nIDEvent, 100, NULL);
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}

void CSystemTestDlg::OnBnClickedLoadTestButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedLoadTestButton() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pTest);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES == AfxMessageBox("Mask를 Loading 하시겠습니까?", MB_YESNO)) MaskLoad();
}

void CSystemTestDlg::OnBnClickedUnloadTestButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedUnloadTestButton() 버튼 클릭!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CAutoMessageDlg MsgBoxAuto(g_pTest);
	if (m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}

	if (IDYES != AfxMessageBox("Mask를 Unloading 하시겠습니까?", MB_YESNO)) return;

	MaskUnload();
}

void CSystemTestDlg::OnBnClickedSeqStopButton()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedSeqStopButton() Button Click!"));

	g_pAP->StopSequence();
	m_bAutoSequenceProcessing = FALSE;
}

void CSystemTestDlg::OnEnChangeEditTestRepeatno()
{
	UpdateData(TRUE);
}

void CSystemTestDlg::OnBnClickedCheckStagelongrunTest()
{
	g_pLog->Display(0, _T("CSystemTestDlg::OnBnClickedCheckStagelongrunTest() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL)
		return;

	UpdateData(TRUE);
	int nRet = AutoRun(m_nRepeatNo);
	if (nRet != 0)
	{
		m_bAutoSequenceProcessing = FALSE;
		g_pWarning->ShowWindow(SW_HIDE);

		CString sTemp;
		sTemp.Format(_T("Fail to long run test (error code : %d)"), nRet);
		AfxMessageBox(sTemp);
	}
}

int CSystemTestDlg::AutoRun(int nRepeatNo)
{
	CString strLog;

	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_nCurrentNo = 0;
	m_bAutoSequenceProcessing = TRUE;
	

	while (m_bAutoSequenceProcessing == TRUE && m_nCurrentNo < nRepeatNo)
	{
		strLog.Format(_T(">>>> Long Run Test %d/%d Start"), m_nCurrentNo, nRepeatNo);
		SaveLogFile("LoadingSequence", strLog);

		WaitSec(2);	//혹시 가동 취소 버튼 누를 시간 확보
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -2;
		}

		g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
		g_pWarning->UpdateData(FALSE);
		if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -3;
		}
		while (TRUE)
		{
			WaitSec(1);
			if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
				break;

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return -4;
			}

			if (g_pAP->m_nProcessErrorCode != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				m_bAutoSequenceProcessing = FALSE;
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return g_pAP->m_nProcessErrorCode;
			}
		}


		if (m_bRepeatabilityCheck == TRUE)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(m_dRepeatPosX, m_dRepeatPosY);
			FindMark();
		}
		
		WaitSec(3);
		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			return -5;
		}

		g_pRecipe->m_bPMSuccess = FALSE;
		g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
		g_pWarning->UpdateData(FALSE);

		if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
			m_bAutoSequenceProcessing = FALSE;
			return -6;
		}
		while (TRUE)
		{
			WaitSec(1);
			if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
				break;

			if (m_bAutoSequenceProcessing == FALSE)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				return -7;
			}

			if (g_pAP->m_nProcessErrorCode != 0)
			{
				g_pWarning->ShowWindow(SW_HIDE);
				MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
				SaveLogFile("LoadingSequence", _T(" Auto Sequence가 중지되었습니다! "));
				m_bAutoSequenceProcessing = FALSE;
				return g_pAP->m_nProcessErrorCode;
			}
		}
		WaitSec(3);

		m_nCurrentNo++;
		UpdateData(FALSE);
	}

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);

	return 0;
}

int CSystemTestDlg::FindMark()
{
	int ret = 0; CString sTemp;

	if (g_pRecipe->m_MilPMModel[PM_TEST_POSITION] > 0)
	{
		MimConvert(g_pCamera->m_CameraWnd.m_MilOMDisplay, g_pRecipe->m_MilGrayModel, M_RGB_TO_L);
		MpatSetAcceptance(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 50L);
		MpatSetCertainty(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 80L);
		MpatSetAccuracy(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_LOW);	//M_LOW: Low accuracy(typically ± 0.20 pixels),M_MEDIUM: Medium accuracy(typically ± 0.10 pixels),M_HIGH: High accuracy(typically ± 0.05 pixels)
		MpatSetSpeed(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_LOW);

		MpatPreprocModel(g_pRecipe->m_MilGrayModel, g_pRecipe->m_MilPMModel[PM_TEST_POSITION], M_DEFAULT);
		MpatSetNumber(g_pRecipe->m_MilPMModel[PM_TEST_POSITION], 1L);

		MpatFindModel(g_pRecipe->m_MilGrayModel, g_pRecipe->m_MilPMModel[PM_TEST_POSITION], g_pRecipe->m_MilResultModel);
		double x = 0., y = 0., angle = 0., Score = 0.;
		int getnum = MpatGetNumber(g_pRecipe->m_MilResultModel, M_NULL);
		if (getnum > 0)
		{
			MpatGetResult(g_pRecipe->m_MilResultModel, M_POSITION_X, &x);
			MpatGetResult(g_pRecipe->m_MilResultModel, M_POSITION_Y, &y);
			MpatGetResult(g_pRecipe->m_MilResultModel, M_SCORE, &Score);

			g_pRecipe->m_dPMResultPosX = x;
			g_pRecipe->m_dPMResultPosY = y;
			g_pRecipe->m_bPMSuccess = TRUE;

			sTemp.Format(_T("Xpos:\t%lf\tYpos:\t%lf"), x, y);
			SaveLogFile("LoadingResult", sTemp);
		}
		else
		{
			g_pRecipe->m_bPMSuccess = FALSE;
			SaveLogFile("LoadingResult", _T("Fail to find the pattern."));
		}
	}
	else
	{
		SaveLogFile("LoadingResult", _T("There is no registered pattern."));
	}

	return 0;
}

void CSystemTestDlg::OnBnClickedCheckOmStagerepeatTest()
{
	CheckTESTTYPE(OM_STAGE_REPEATABILITY_TEST);
}

void CSystemTestDlg::OnBnClickedCheckEuvStagerepeatTest()
{
	CheckTESTTYPE(EUV_STAGE_REPEATABILITY_TEST);
}

void CSystemTestDlg::OnBnClickedCheckMasksliptest()
{
	CheckTESTTYPE(MASK_SLIP_TEST);
}

void CSystemTestDlg::OnBnClickedCheckTestend()
{
	UpdateData(TRUE);
	CheckTESTTYPE(STOP_TEST);
}

void CSystemTestDlg::CheckTESTTYPE(int nTest)
{
	if (m_bTestStop == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		return;
	}
	m_CheckTestEndCtrl.SetCheck(FALSE);
	m_CheckMaskSlipTestCtrl.SetCheck(FALSE);
	m_CheckEUVStageRepeatTestCtrl.SetCheck(FALSE);
	m_CheckOMStageRepeatTestCtrl.SetCheck(FALSE);
	m_CheckStageLongRunTestCtrl.SetCheck(FALSE);

	m_bTestStop = FALSE;
	m_nTestType = nTest;
	switch (nTest)
	{
	case STOP_TEST:
		m_CheckTestEndCtrl.SetCheck(TRUE);
		break;
	case MASK_SLIP_TEST:
		m_CheckMaskSlipTestCtrl.SetCheck(TRUE);
		RunMaskSlipTest();
		break;
	case EUV_STAGE_REPEATABILITY_TEST:
		m_CheckEUVStageRepeatTestCtrl.SetCheck(TRUE);
		break;
	case OM_STAGE_REPEATABILITY_TEST:
		m_CheckOMStageRepeatTestCtrl.SetCheck(TRUE);
		break;
	case STAGE_LONGRUN_TEST:
		m_CheckStageLongRunTestCtrl.SetCheck(TRUE);
		break;
	case LLC_PUMP_VENT_LONGRUN_TEST:
		LLC_long_run_test();
		break;
	default:
		break;
	}

	Invalidate(FALSE);
}

void CSystemTestDlg::RunMaskSlipTest()
{
	int ret = 0;
	int count = 0;
	while (m_bTestStop==FALSE && count < 300)
	{
		count++;
		if (m_bTestStop == TRUE)
			return;

		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);
		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);
		//if (g_pNavigationStage != NULL)
		//	g_pNavigationStage->MoveStageDBPositionNo(99);
		if (m_bTestStop == TRUE)
			return;
		WaitSec(10);

		g_pRecipe->SearchPMModel(0);
	}
}

void CSystemTestDlg::SaveTestResult(int nTestType, double param1, double param2, double param3, double param4)
{
	CString str;
	static int count = 0;
	count = count + 1;

	switch (nTestType)
	{
	case MASK_SLIP_TEST:
		if (count == 1)
			str.Format("No    Pos.X,   Pos.Y,   Angle,   Score \n");
		else
			str.Format("%d   %.6f, %.6f, %.6f,  %.1f  ", count, param1, param2, param3, param4);
		//SaveLogFile();
		break;
	case EUV_STAGE_REPEATABILITY_TEST:
		break;
	case OM_STAGE_REPEATABILITY_TEST:
		break;
	case STAGE_LONGRUN_TEST:
		break;
	}

}

int CSystemTestDlg::MaskLoad()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pTest);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);	//혹시 가동 취소 버튼 누를 시간 확보
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Loading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskLoadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}

	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->LOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Loading이 완료되었습니다! "), 3);
	return 0;
}

int CSystemTestDlg::MaskUnload()
{
	if (g_pAP == NULL || g_pLog == NULL || g_pWarning == NULL)
		return -1;

	CAutoMessageDlg MsgBoxAuto(g_pTest);

	g_pWarning->m_strWarningMessageVal = " Auto Sequence 가동 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	m_bAutoSequenceProcessing = TRUE;

	WaitSec(3);
	if (m_bAutoSequenceProcessing == FALSE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		return -1;
	}

	g_pWarning->m_strWarningMessageVal = " Mask Unloading 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	if (g_pAP->MaskUnloadingStart(g_pConfig->m_bUseFlip, g_pConfig->m_bUseRotate, g_pConfig->m_nRotateAngle) != SYSTEM_OK)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		m_bAutoSequenceProcessing = FALSE;
		return -1;
	}
	while (TRUE)
	{
		//ProcessMessages();
		//Sleep(10);
		WaitSec(1);
		if (g_pAP->m_pAutoThread == NULL && g_pAP->CurrentProcess == g_pAP->UNLOADING_COMPLETE)
			break;

		if (m_bAutoSequenceProcessing == FALSE)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			return -1;
		}

		if (g_pAP->m_nProcessErrorCode != 0)
		{
			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			m_bAutoSequenceProcessing = FALSE;
			return -1;
		}
	}
	WaitSec(3);

	m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	MsgBoxAuto.DoModal(_T(" Mask Unloading이 완료되었습니다! "), 3);
	return 0;
}

void CSystemTestDlg::OnBnClickedLlcTestButton()
{
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		return;

	CheckTESTTYPE(LLC_PUMP_VENT_LONGRUN_TEST);
}

void CSystemTestDlg::LLC_long_run_test()
{
	CString user_num;
	CString Log_str;
	GetDlgItem(IDC_EDIT_TEST_REPEATNO)->GetWindowTextA(user_num);
	num = _ttoi(user_num);


	if (IDYES != AfxMessageBox("LLC VENTING / PUMPING Long Run Test를 시작하시겠습니까?", MB_YESNO)) return;

	if (g_pIO->Is_CREVIS_Connected() != TRUE)
	{
		Log_str = "Crevis Open Fail 이므로 Long Run Test 실행 불가";
		SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(Log_str)));
		g_pLog->Display(0, Log_str);
		return;
	}
	m_pVaccum_Run_Thread = ::AfxBeginThread(LLC_Vacuum_Run_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

UINT CSystemTestDlg::LLC_Vacuum_Run_Thread(LPVOID pParam)
{
	int ret = 0;

	CSystemTestDlg*  g_pRunTest = (CSystemTestDlg*)pParam;
	
	g_pRunTest->m_bVacuum_Run_ThreadStop = FALSE;
	
	while (!g_pRunTest->m_bVacuum_Run_ThreadStop)
	{
		ret = g_pRunTest->LLC_Start_Long_Run_Test();
		g_pRunTest->LLC_Start_Long_Run_Test_View();
		if(ret != 0) g_pRunTest->m_bVacuum_Run_ThreadStop = TRUE;
	}
		
	return ret;
}

int CSystemTestDlg::LLC_Start_Long_Run_Test()
{
	int ret = 0;
	//int set_num = 0;
	CString set_num_str;

	if (set_num != num)
	{
		set_num_str.Format("%d", set_num);
		GetDlgItem(IDC_EDIT_TEST_REPEATNO2)->SetWindowTextA(set_num_str);

		if (LLC_Vacuum_Test_State == Pumping_START_State)
		{
			str = " :: LLC PUMPING START";
			SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
			//ret = g_pVP->LLC_Pumping_Sequence();
			
			if (ret != 0)
			{
				LLC_Vacuum_Test_State = Pumping_START_State;
				str = ":: LLC Pumping Thread Start Fail";
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				g_pLog->Display(0, str);
				Long_Run_Error();

				return -1;
			}
			else
			{
				str = " :: LLC PUMPING END";
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				LLC_Vacuum_Test_State = Venting_START_State;
				g_pLog->Display(0, str);
				WaitSec(10);
			}

		}

		if (LLC_Vacuum_Test_State == Venting_START_State)
		{
			str = " :: LLC VENTING START";
			SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
			//ret = g_pVP->LLC_Venting_Sequence();

			if (ret != 0)
			{
				LLC_Vacuum_Test_State = Venting_START_State;
				str = " :: LLC Venting Thread Start Fail";
				g_pLog->Display(0, str);
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				Long_Run_Error();
				return -1;
			}
			else
			{
				str = " :: LLC VENTING END";
				LLC_Vacuum_Test_State = Pumping_START_State;
				SaveLogFile("LLC_Auto_Long_Run_Test_Log", _T((LPSTR)(LPCTSTR)(set_num_str + str)));
				g_pLog->Display(0, str);
				set_num++;
				WaitSec(10);
			}

		}
	}
	else
	{
		//m_bVacuum_Run_ThreadStop = TRUE;
		Long_Run_Stop();
	}
}

void CSystemTestDlg::LLC_Start_Long_Run_Test_View()
{

	switch (g_pVP->m_nSequence_State)
	{
		case LLC_Venting_PreWork_Error_State :
			break;
		case LLC_Venting_SlowVent_Error_State :
			break;
		case LLC_Venting_FastVent_Error_State :
			break;
		case LLC_Pumping_PreWork_Error_State :
			break;
		case LLC_Pumping_Slow_Rough_Error_State :
			break;
		case LLC_Pumping_Fast_Rough_Error_State :
			break;
		case LLC_Pumping_Tmp_Rough_Error_State :
			break;
		case  Pumping_ERROR_State :
			break;
		case  Venting_ERROR_State :
			break;
		case  State_IDLE_State  :
			break;
		case  Pumping_START_State  :
			break;
		case  Venting_START_State :
			break;
		case  Pumping_SLOWROUGH_State  :
			break;
		case  Pumping_FASTROUGH_State  :
			break;
		case  Pumping_TMPROUGH_State  :
			break;
		case  Pumping_COMPLETE_State  :
			break;
		case  Venting_SLOWVENT_State :
			break;
		case  Venting_FASTVENT_State  :
			break;
		case  Venting_COMPLETE_State  :
			break;
		default:
		break;
	}

}

void CSystemTestDlg::Long_Run_Stop()
{
	int ret = 0;

	if (m_bVacuum_Run_ThreadStop == FALSE)
	{
		m_bVacuum_Run_ThreadStop = TRUE;	// 쓰레드내의 while()에 들어가는 변수 => Thread를 정상 종료시킴
		Sleep(100);
	}

	// Thread Stop
	if (!m_bVacuum_Run_ThreadStop)		// 현재 쓰레드가 종료가 안되었으면
	{
		if (m_pVaccum_Run_Thread != NULL)
		{
			HANDLE threadHandle = m_pVaccum_Run_Thread->m_hThread;
			DWORD dwResult;
			dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);

			if (dwResult == WAIT_TIMEOUT)
			{
				DWORD dwExitCode = STILL_ACTIVE;
				::GetExitCodeThread(threadHandle, &dwExitCode); // *요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
				if (dwExitCode == STILL_ACTIVE)//259
				{
					//AfxEndThread(nExitCode);
					TerminateThread(threadHandle, 0/*dwExitCode*/);
					CloseHandle(threadHandle);
				}
			}
		}
		m_bVacuum_Run_ThreadStop = TRUE;
	}
}

void CSystemTestDlg::Long_Run_Error()
{
	m_bVacuum_Run_ThreadStop = TRUE;
	Long_Run_Stop();
}

//LONG RUN STOP
void CSystemTestDlg::OnBnClickedLoadTestButton3()
{
	Long_Run_Stop();
	set_num = 0;
}

void CSystemTestDlg::OnBnClickedCheckMaskFlat()
{
	GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 가능"));
	//GetDlgItem(IDC_BTN_DIGITAL)->EnableActiveAccessibility();
	GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(false);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(false);

	flatness_measurement_stop = false;



	if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);

		return;
	}


	g_pWarning->m_strWarningMessageVal = " Mask Flatness Measurement 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	if (Mask_Flat() != RUN)
	{
	//	m_progress_mask_flat.SetPos(0);
	//	m_bTestStop = TRUE;
	//	m_Mask_Measrue_Start = TRUE;
		
	//	KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);
		g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
		SetDlgItemTextA(IDC_OM_EUV, "OM");
		WaitSec(2);
		g_pWarning->ShowWindow(SW_HIDE);
		::AfxMessageBox(_T("평탄도 측정에 실패 하였습니다"),MB_ICONERROR);
		GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(_T("0"));
		m_progress_mask_flat.SetPos(0);
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
	
	
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW, _T("0.0"));
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW, _T("0.0"));
		return;
	}

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	SetDlgItemTextA(IDC_OM_EUV, "OM");

	m_progress_mask_flat.SetPos(0);
	GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);

	m_bTestStop = TRUE;
	m_Mask_Measrue_Start = TRUE;
	KillTimer(MASK_FLATNESS_MEASUREMENT_TIMER);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW, _T("0.0"));
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW, _T("0.0"));

	g_pWarning->ShowWindow(SW_HIDE);
	::AfxMessageBox(_T("평탄도 측정이 완료 되었습니다"));

}

int CSystemTestDlg::Mask_Flat()
{

	int cont = 0;

	double cap1;
	double cap2;
	double cap3;
	double cap4;


	double target_x_pos; // 측정 범위 가능 검사 변수
	double target_y_pos;

	double move_x_pos;	// 실제 이동 위치 변수
	double move_y_pos;

	double x_pos; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double y_pos;
	double z_pos;
	double tx_pos;
	double ty_pos;

	CString x_pos_str;
	CString y_pos_str;
	CString z_pos_str;
	CString tx_pos_str;
	CString ty_pos_str;

	CString cap1_str;
	CString cap2_str;
	CString cap3_str;
	CString cap4_str;

	CString position_str;

	double orgin_x_pos;
	double orgin_y_pos;

	int position = 0;
	int x = 0, y = 0;

	int nRet = RUN;


	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return -1;
	

		
	m_progress_mask_flat.SetPos(0);



	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);
	
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	// 측정 중 일 경우 FALSE, 측정 끝나거나 중지되면 TRUE, 
	m_bTestStop = FALSE;
	m_Mask_Measrue_Start = FALSE;
	if (m_Mask_Measrue_Start == FALSE)	SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	////////////////////////////////////
	// MASK SIZE :: 152.4 x 152.4 mm
	////////////////////////////////////


	// 마스크 X축 평탄도 측정 가능 범위 확인 
	//
	// 사용자의 별도 좌표값 없을 경우 Default 로 [210 , 0] 좌표로 시작.
	// 사용자의 값이 입력되어 SET 되면 get_x_pos , get_y_pos 값에 반영됨.
	// default_x_pos = 210.0;
	// default_y_pos = 0.0;

	// 210 기준으로 (15mm 씩 10번 움직임 ) 
	
	target_x_pos = get_x_pos - (10 * 14.5);
	if (target_x_pos < 0)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	// 마스크 Y축 평탄도 측정 가능 범위 확인 
	// mask size 152 mm 이므로 0 기준으로 200 넘어가면 의미 없는 수치 값으로 추정.
	
	target_y_pos = get_y_pos + (10 * 13.5);
	if (target_y_pos > 200)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}
	
	// OM -> EUV 변환 ( Cap 으로 측정되는 값을 확인 하기 위함 )

	g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
	WaitSec(2);
	SetDlgItemTextA(IDC_OM_EUV, "EUV");


	// Z축 0 으로 Setting 후 평탄도 측정을 진행 위함.
	// Z축 위치가 0 되면 Default 값 혹은, 사용자 설정 X, Y 값으로 이동.
	// OM 영역이 아닌, EUV 영역에서 좌표값을 움직인다.
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
	{
		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(get_x_pos, get_y_pos);
	}
	else
	{
		::AfxMessageBox(" Z AXIS 원점이 아님.!");
		return nRet = -1;
	}

	// 설정된 좌표값으로 이동후 , 좌표값 다시 READ 후 범위 측정 확인.
	
	CString start_x_pos_str;
	CString start_y_pos_str;

	orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	start_x_pos_str.Format("%3.7f", orgin_x_pos);
	start_y_pos_str.Format("%3.7f", orgin_y_pos);

	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_START, start_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_START, start_y_pos_str);

	// 읽은 좌표값을 기준으로 측정 가능 범위 재 확인 (X, Y)
	target_x_pos = orgin_x_pos - (10 * 15);
	if (target_x_pos < 0)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	target_y_pos = orgin_y_pos + (10 * 15);
	if (target_y_pos > 200)
	{
		MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
		return nRet = -1;
	}

	if (flatness_measurement_stop == true)
	{
		::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
		position = 0;
		return nRet = -1;
	}

	// 측정 전 Z 축 충돌방지를 위한 0 위치 재확인.
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);

	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != 0)
	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != Z_INITIAL_POS_UM)
	{
		return nRet = -1;
	}

	str = "측정 시작 !";
	SaveLogFile("마스크 평탄도 측정", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	y = 0;
	x = 0;

	position = 0;
	position_str.Format("%d", position);
	
	GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(position_str);
	GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 진행 중"));

	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour(); 
	int min = CTime::GetCurrentTime().GetMinute();

	CString datafile;
	CString datafile_cap;
	CString datafile_4cap;
	CString path;
	path = LOG_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	datafile.Format(_T("%s\\마스크 평탄도_data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile_cap.Format(_T("%s\\cap_data.txt"), path);
	datafile_4cap.Format(_T("%s\\4cap_data.txt"), path);

	std::ofstream flatness_measurement_datafile(datafile);
	std::ofstream flatness_measurement_cap_value(datafile_cap);
	std::ofstream flatness_measurement_4cap_value(datafile_4cap);

	flatness_measurement_datafile << "position" << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "tx_pos" << "\t" << "ty_pos" << "\t" << "z_pos" << "\t" << "cap" << "\n";
	flatness_measurement_4cap_value << "position" << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "tx_pos" << "\t" << "ty_pos" << "\t" << "z_pos" << "\t" << "cap1" << "\t" << "cap2" << "\t" << "cap3" << "\t" << "cap4" << "\t" << "\n";


	if (flatness_measurement_stop == true)
	{
		::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
		return nRet = -1;
	}

	while (y < 10) // Y축 이동
	{
	
		if (y == 0)	y_pos = orgin_y_pos;
		else y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);


		while (x < 10) // X 축 이동
		{
			if (x == 0)	x_pos = orgin_x_pos;
			else x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);


			// SCAN STAGE AXIS 값 READ
			z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			tx_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
			ty_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];
			
			z_pos_str.Format("%3.7f", z_pos);
			x_pos_str.Format("%3.7f", x_pos);
			y_pos_str.Format("%3.7f", y_pos);
			tx_pos_str.Format("%3.7f", tx_pos);
			ty_pos_str.Format("%3.7f", ty_pos);

			position_str.Format("%d", position);

			g_pAdam->Command_ADAMSimpleRun();
			//WaitSec(2);
			

			//WaitSec(4);
			cap1 = g_pAdam->AdamData.m_dCapsensor1;
			cap1_str.Format("%.4f", cap1);
			cap2 = g_pAdam->AdamData.m_dCapsensor2;
			cap2_str.Format("%.4f", cap2);
			cap3 = g_pAdam->AdamData.m_dCapsensor3;
			cap3_str.Format("%.4f", cap3);
			cap4 = g_pAdam->AdamData.m_dCapsensor4;
			cap4_str.Format("%.4f", cap4);
			//g_pAdam->Command_ADAMSimpleRun();

			 cont = position + 1;
			flatness_measurement_datafile << std::setprecision(3) << position_str << "\t"<< x_pos_str << "\t" << y_pos_str << "\t" <<tx_pos_str << "\t" << ty_pos_str << "\t" << z_pos_str << "\t" << cap1_str << "\n";
			flatness_measurement_4cap_value << std::setprecision(3) << position_str << "\t"<< x_pos_str << "\t" << y_pos_str << "\t" <<tx_pos_str << "\t" << ty_pos_str << "\t" << z_pos_str << "\t" << cap1_str << "\t" << cap2_str << "\t" << cap3_str << "\t" << cap4_str << "\n";
			flatness_measurement_cap_value << std::setprecision(3) << cap1_str << "\n";
	

			CString cont_str;
			cont_str.Format("%d", cont);
			GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(cont_str);

			if (flatness_measurement_stop == true)
			{
				::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
				position = 0;
				nRet = -2;
				 break;
			}

			// 10x10 측정을 위한 15 mm 씩 감소하여 이동.
			move_x_pos = x_pos - 15.0;
			move_y_pos = y_pos;

			// Z축 충돌 방지를 위한 0 재 확인 후 이동.
			g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
			WaitSec(2);
			//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
			if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
			}
			else
			{
				::AfxMessageBox(" Z AXIS 원점 아님.!");
				nRet = -1;
				break;
			}



			WaitSec(2);
			position++; // LOG 기록용 
			x++;
			m_progress_mask_flat.OffsetPos(1);
			if (x == 10) break;
		}
		
		
		if (flatness_measurement_stop == true)
		{
			::AfxMessageBox(" 마스크 평탄도 STOP 버튼 누름 ! ");
			position = 0;
			nRet = -1;
			break;
		}
		if (nRet != RUN) break; // x 방향 Error 발생.

		move_x_pos = orgin_x_pos; // X 축 초기 값으로 이동.
		move_y_pos = y_pos + 15.0; // Y 축 15 mm 씩 이동.



		// Z축 충돌 방지를 위한 0 재 확인 후 이동.
		g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
		WaitSec(2);
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
		if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		}
		else
		{
			::AfxMessageBox(" Z AXIS 원점 아님.!");
			nRet = -1;
			break;
		}
	
		WaitSec(2);

		if (nRet != RUN) break; // y 방향 Error 발생.

		x = 0;
		y++;

		if (y == 10) break;
	}


	
	flatness_measurement_datafile << std::endl;
	if (flatness_measurement_datafile.is_open() == true)
	{
		flatness_measurement_datafile.close();
	}

	flatness_measurement_4cap_value << std::endl;
	if (flatness_measurement_4cap_value.is_open() == true)
	{
		flatness_measurement_4cap_value.close();
	}

	flatness_measurement_cap_value << std::endl;
	if (flatness_measurement_cap_value.is_open() == true)
	{
		flatness_measurement_cap_value.close();
	}

	GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 완료"));
	if (nRet != RUN) return nRet; // Error 발생 시

	// 측정 완료 후 다시 OM 영역으로 복귀
	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	SetDlgItemTextA(IDC_OM_EUV, "OM");

	GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
	GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(_T("0"));
	m_progress_mask_flat.SetPos(0);
	position = 0;
	
	return nRet;

}

//if (g_pNavigationStage->MoveRelativePosition(STAGE_X_AXIS, -m_dStageMovingDistance) != XY_NAVISTAGE_OK)
//{
//	::AfxMessageBox(" Stage 동작 불가!");
//}

void CSystemTestDlg::OnBnClickedCheckMaskFlatSet()
{

	CString user_x_str;
	CString user_y_str;
	
	// SET 누름으로 사용자 정의 값 적용
	GetDlgItem(IDC_EDIT_TEST_MASK_X_POSITION)->GetWindowTextA(user_x_str);
	GetDlgItem(IDC_EDIT_TEST_MASK_Y_POSITION)->GetWindowTextA(user_y_str);

	get_x_pos = atof(user_x_str);
	get_y_pos = atof(user_y_str);

}

void CSystemTestDlg::OnBnClickedCheckMaskFlatDefault()
{
	CString default_x_str;
	CString default_y_str;

	// Default 누름으로 처음 Setting 값으로 원복.
	get_x_pos = default_x_pos;
	get_y_pos = default_y_pos;

	default_x_str.Format("%.4f", get_x_pos);
	default_y_str.Format("%.4f", get_y_pos);

	SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_POSITION, default_x_str);
	SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_POSITION, default_y_str);

}

void CSystemTestDlg::Stage_axis_Check()
{
	// 모니터를 위함.
	// 측정 중 Axis 계속 적으로 위치 값 모니터링
	// Mask 측정 중일때만 유효.
	double axis_data = 0.0;
	CString axis_data_str;
	CString axis_data_reset_str;
	axis_data_reset_str = _T("0.0");
	
	if (m_Mask_Measrue_Start == FALSE)
	{
		axis_data = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW, axis_data_str);

		axis_data = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW, axis_data_str);

		axis_data = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW, axis_data_str);


		axis_data = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW, axis_data_str);

		axis_data = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW, axis_data_str);

		//g_pAdam->Command_ADAMSimpleRun();
		//WaitSec(2);
		axis_data = g_pAdam->AdamData.m_dCapsensor1;
		axis_data_str.Format("%.4f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW, axis_data_str);
		//	g_pAdam->Command_ADAMSimpleRun();
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_VIEW, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_VIEW, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_Z_VIEW, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_MASK_CAP1_VIEW, axis_data_reset_str);
	}
}

void CSystemTestDlg::SH_Stage_Check_Axis_Check()
{
	////////////////////////////////////////////////////////
	// 모니터를 위함.
	// 측정 중 Axis 계속 적으로 위치 값 모니터링
	// Stage 측정 중일때만 유효.
	///////////////////////////////////////////////////////

	double axis_data = 0.0;
	CString axis_data_str;
	CString axis_data_reset_str;
	axis_data_reset_str = _T("0.0");

	if (m_SH_Stage_Measrue_Start == FALSE)
	{
		//X POS
		axis_data = abs(g_pNavigationStage->GetPosmm(STAGE_X_AXIS));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, axis_data_str);

		//Y POS
		axis_data = abs(g_pNavigationStage->GetPosmm(STAGE_Y_AXIS));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, axis_data_str);

		//X LOAD
		axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_1"));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, axis_data_str);

		//Y LOAD
		axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("DOUT_0"));
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, axis_data_str);

		//X INPOS
		axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));
		axis_data_str.Format("%3.7e", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, axis_data_str);
		//
		//Y INPOS
		axis_data = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
		axis_data_str.Format("%3.7e", axis_data);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, axis_data_str);
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, axis_data_reset_str);
	}
}

void CSystemTestDlg::PI_Stage_Check_Axis_Check()
{
	////////////////////////////////////////////////////////
	// 모니터를 위함.
	// 측정 중 Axis 계속 적으로 위치 값 모니터링
	// PI Stage 측정 중일때만 유효.
	///////////////////////////////////////////////////////

	double axis_data = 0.0;
	CString axis_data_str;
	CString axis_data_reset_str;
	axis_data_reset_str = _T("0.0");

	if (m_SH_Stage_Measrue_Start == FALSE)
	{
		//X POS
		axis_data = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS, axis_data_str);

		//Y POS
		axis_data = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS, axis_data_str);

		//X REF POS
		axis_data = x_pos_ref;
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS, axis_data_str);

		//Y REF POS
		axis_data = y_pos_ref;
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS, axis_data_str);


		//PI X POS
		axis_data = g_pScanStage->m_dPIStage_MovePos[X_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP1, axis_data_str);

		//PI Y POS
		axis_data = g_pScanStage->m_dPIStage_MovePos[Y_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP2, axis_data_str);

		//PI Z POS
		axis_data = g_pScanStage->m_dPIStage_MovePos[Z_AXIS];
		axis_data_str.Format("%3.7e", axis_data);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP3, axis_data_str);
		
		//PI TIP
		axis_data = g_pScanStage->m_dPIStage_MovePos[TIP_AXIS];
		axis_data_str.Format("%3.7e", axis_data);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP4, axis_data_str);

	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_REF_POS, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_REF_POS, axis_data_reset_str);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP1, axis_data_reset_str);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP2, axis_data_reset_str);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP3, axis_data_reset_str);
		//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP4, axis_data_reset_str);

	}
}

void CSystemTestDlg::Source_measurement_axis_check()
{
	// 모니터를 위함.
	// 측정 중 Axis 계속 적으로 위치 값 모니터링
	// Mask 측정 중일때만 유효.
	double axis_data = 0.0;
	CString axis_data_str;
	CString axis_data_reset_str;
	axis_data_reset_str = _T("0.0");

	if (m_Source_Measrue_Start == FALSE)
	{
		axis_data = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_X_POSITION, axis_data_str);

		axis_data = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Y_POSITION, axis_data_str);

		axis_data = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		axis_data_str.Format("%3.7f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Z_POSITION, axis_data_str);


		//axis_data = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
		//axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_TEST_MASK_TX_VIEW, axis_data_str);
		//
		//axis_data = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];
		//axis_data_str.Format("%3.7f", axis_data);
		//SetDlgItemTextA(IDC_EDIT_TEST_MASK_TY_VIEW, axis_data_str);

		//g_pAdam->Command_ADAMSimpleRun();
		//WaitSec(2);
		axis_data = g_pAdam->AdamData.m_dCapsensor1;
		axis_data_str.Format("%.4f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CAP, axis_data_str);

		axis_data = g_pAdam->AdamData.m_dDetector1;
		axis_data_str.Format("%.4f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SEG, axis_data_str);

		axis_data = g_pAdam->AdamData.m_dDetector2;
		axis_data_str.Format("%.4f", axis_data);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CEN, axis_data_str);

		//	g_pAdam->Command_ADAMSimpleRun();

		if (g_pEUVSource->Is_EUV_On() != FALSE)
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV ON");
		}
		else
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV OFF");
		}

		if (g_pEUVSource->Is_LaserShutter_Opened() != FALSE)
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "OPEN");
		}
		else
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "CLOSE");
		}
	}
	else
	{
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_X_POSITION, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Y_POSITION, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Z_POSITION, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CAP, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SEG, axis_data_reset_str);
		SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CEN, axis_data_reset_str);
		if (g_pEUVSource->Is_EUV_On() != FALSE)
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV ON");
		}
		else
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_EUV, "EUV OFF");
		}

		if (g_pEUVSource->Is_LaserShutter_Opened() != FALSE)
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "OPEN");
		}
		else
		{
			SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SHUTTER, "CLOSE");
		}
	}
}

/*
void CSystemTestDlg::OnBnClickedCheckSource()
{
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(false);

	if (m_bTestStop == FALSE || m_Source_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);

		return;
	}

	//g_pWarning->m_strWarningMessageVal = " Source Contamination Measurement 시작합니다! ";
	//g_pWarning->UpdateData(FALSE);
	//g_pWarning->ShowWindow(SW_SHOW);

	if (m_pSourceTestThread == NULL)
	{
		//m_pSourceTestThread = ::AfxBeginThread(Source_Test_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}

}
*/

UINT CSystemTestDlg::Source_Test_VacuumThread(LPVOID pParam)
{
	int ret = 0;


	CSystemTestDlg*  g_pSourceTest = (CSystemTestDlg*)pParam;

	g_pSourceTest->m_pSourceTestThreadStop = FALSE;
	while (!g_pSourceTest->m_pSourceTestThreadStop)
	{
		ret = g_pSourceTest->Source_Check();
		if (ret != RUN)
		{
			g_pSourceTest->KillTimer(SOURCE_MEASUREMENT_TIMER);
			g_pSourceTest->m_bTestStop = TRUE;
			g_pSourceTest->m_Source_Measrue_Start = TRUE;
			g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
			g_pSourceTest->m_pSourceTestThreadStop = TRUE;
			g_pWarning->ShowWindow(SW_HIDE);
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_X_POSITION, "0.0");
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Y_POSITION, "0.0");
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_Z_POSITION, "0.0");
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CAP, "0.0");
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_SEG, "0.0");
			g_pSourceTest->CWnd::SetDlgItemTextA(IDC_EDIT_TEST_SOURCE_CEN, "0.0");
			::AfxMessageBox(_T("SOURCE TEST 실패 하였습니다"));
		}
		g_pSourceTest->m_pSourceTestThreadStop = TRUE;

	}
	g_pSourceTest->CWnd::GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	g_pSourceTest->CWnd::KillTimer(SOURCE_MEASUREMENT_TIMER);
	g_pSourceTest->m_pSourceTestThread = NULL;
	g_pWarning->ShowWindow(SW_HIDE);

	return ret;
}

int CSystemTestDlg::Source_Check()
{
	int nRet = RUN;


	double Detector1, Detector2, cap1;
	CString Detector1_str, Detector2_str, cap1_str;

	double get_recipe_x_pos, get_recipe_y_pos;
	CString recipe_pos, recipe_x_pos, recipe_y_pos;

	CString Str_Z_Axis;
	double Get_Z_Axis, Set_z_axis;
	double focusposz, m_dZInterlockPos, curpos;
	bool Z_AXIS_SETTING;

	m_progress_source_contamination.SetPos(0);
	m_bTestStop = FALSE;
	m_Source_Measrue_Start = FALSE;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return nRet = -1;
	
	
	
	
	////STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);
	
	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}
	
	if (m_Source_Measrue_Start == FALSE)	SetTimer(SOURCE_MEASUREMENT_TIMER, 100, NULL);

	
	//////////////////////////////////////
	//// MASK SIZE :: 152.4 x 152.4 mm
	//////////////////////////////////////
	

	////////////////////////////////////////////////////////////////////
	//ML 위치로 이동. 저장된 레시피로 이동 ( 미리 ML 위치 저장해야함 ).
	////////////////////////////////////////////////////////////////////

	//recipe_pos = g_pConfig->m_stStagePos[98].chStagePositionString;
	//recipe_x_pos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[98].x);
	//recipe_y_pos.Format(_T("%3.7f"), g_pConfig->m_stStagePos[98].y);
	//
	//get_recipe_x_pos = atof(recipe_x_pos);
	//get_recipe_y_pos = atof(recipe_y_pos);
	//
	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(get_recipe_x_pos, get_recipe_y_pos);
	


	//// OM -> EUV 변환 ( 디텍터 신호를 보기 위함 )

	g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
	WaitSec(2);


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Z축 범위 지정 함수 추가해야함.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	WaitSec(2);

	g_pScanStage->Move_AllAxis_Initial_Position();
	WaitSec(2);

	//double dInc_position;
	//g_pScanStage->MoveZRelative_SlowInterlock(dInc_position, PLUS_DIRECTION);
	//Str_Z_Axis.Format(_T("%10.4f"), g_pScanStage->m_dPIStage_MovePos[Z_AXIS]);
	
	//Get_Z_Axis = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	//Str_Z_Axis.Format("%3.4f", g_pScanStage->m_dPIStage_GetPos[Z_AXIS]);

	g_pAdam->Command_ADAMSimpleRun();
	//WaitSec(2);
	cap1 = g_pAdam->AdamData.m_dCapsensor1;

	////////////////////////////////
	// 지정된 CAP 값이 될때 까지 Z축 조정
	////////////////////////////////

	if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	{
		if (g_pNavigationStage->MoveAbsoluteXY_UntilInposition(g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].x, g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].y) != XY_NAVISTAGE_OK)
		{
			//g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
			return nRet = -1;
		}
		g_pAdam->Command_ADAMSimpleRun();
		//WaitSec(2);
		focusposz = g_pConfig->m_dZdistanceCap1nStage_um - g_pAdam->AdamData.m_dCapsensor1;
	//	focusposz = 313.4;
		CString str;
		//str.Format(_T("focusposz = %.3f, AdamData.m_dCapsensor2 = %.3f"), focusposz, g_pAdam->AdamData.m_dCapsensor2);
		//g_pAdam->Command_ADAMSimpleRun();
	//	m_dZFocusPos = focusposz;
		if (focusposz < 70 || focusposz > 150)
		{
			MsgBoxAuto.DoModal(_T(" Z Interlock 및 Focus 위치 확인 필요. Auto Sequence가 중지되었습니다! "), 2);
			return nRet = -1;
		}

		g_pScanStage->MoveZAbsolute_SlowInterlock(3);
	//	g_pScanStage->MoveZAbsolute_SlowInterlock(focusposz);
		WaitSec(3);
		g_pAdam->Command_ADAMSimpleRun();
		//WaitSec(2);
		cap1 = g_pAdam->AdamData.m_dCapsensor1;
			
	}
	else
	{
		MsgBoxAuto.DoModal(_T(" Auto Sequence가 중지되었습니다! "), 2);
		return nRet = -1;
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	CString cap1_str_;
	if ((cap1 < 312) || (cap1 > 314))
	{
		cap1_str.Format("%f", cap1);
		MsgBoxAuto.DoModal(_T(" Z Focus 위치 확인 필요. Cap1 Sensor 값 확인 필요 [ Focus 범위가 아님 ] :: " + cap1_str), 2);
		return nRet = -1;
	}

	/////////////////////
	//// EUV SOURCE ON
	/////////////////////

	if (g_pLog != NULL)
		g_pLog->Display(0, _T("EUV SOURCE ON"));

	if (g_pEUVSource == NULL) return nRet = -1;


	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return nRet = -1;
	}

	if (g_pEUVSource->Is_EUV_On() != FALSE)
	{
		g_pEUVSource->SetMechShutterOpen(TRUE);
		if (g_pEUVSource->SetEUVSourceOn(TRUE) != 0)
			return -999;
		//g_pEUVSource->SRC_Status();
	}
	else
	{
		AfxMessageBox(_T("EUV 가 ON 되어 있습니다"));
		return nRet = -1;
	}

	//// SOURCE  POWER  CEHCK 
	//// SEGMENT , CENTER

	g_pAdam->Command_ADAMSimpleRun();
	//WaitSec(2);
	Detector1 = g_pAdam->AdamData.m_dDetector1;
	Detector2 = g_pAdam->AdamData.m_dDetector2;
	//g_pAdam->Command_ADAMSimpleRun();

	if (Detector1 <= 0.1)
	{
		AfxMessageBox(_T("ML 위치가 아닙니다. SOURCE 측정 위치로 이동해주세요"));
		g_pEUVSource->SetMechShutterOpen(FALSE);
		g_pEUVSource->SetEUVSourceOn(FALSE);
		//g_pEUVSource->SRC_Status();
		return nRet = -1;
	}

	////////////////////////////////////////////
	//// 5분간 지속 LOG 기록 남길 것.
	////Source Contamination Measurement Start
	////////////////////////////////////////////

	CString source_contamination_data;
	CString source_contamination_redata;

	CString path;
	path = LOG_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour();
	int min = CTime::GetCurrentTime().GetMinute();
	int sec = CTime::GetCurrentTime().GetSecond();

	source_contamination_data.Format(_T("%s\\Source_Contamination_data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	source_contamination_redata.Format(_T("%s\\source_contamination_detect_sensor_data.txt"), path);

	std::ofstream source_contamination(source_contamination_data);
	std::ofstream source_contamination_detect_sensor_data(source_contamination_redata);

	m_start_time = clock();
	m_finish_time = 120;	// 2분
	//m_finish_time = 240;	// 4분
	//m_finish_time = 300;	// 5분
	start = clock();
	clock_t a;

	while ((clock() - m_start_time) / CLOCKS_PER_SEC <= m_finish_time)
	{
		//a = ((clock() - start) / CLOCKS_PER_SEC);
		day = CTime::GetCurrentTime().GetDay();
		hour = CTime::GetCurrentTime().GetHour();
		min = CTime::GetCurrentTime().GetMinute();
		sec = CTime::GetCurrentTime().GetSecond();
		source_contamination << std::setprecision(3) << day << "\t" << hour << "\t" << min << "\t" << sec << "\t" << cap1_str << "\t" << Detector1_str << "\t" << Detector2_str << "\n";
		source_contamination_detect_sensor_data << std::setprecision(3) << cap1_str << "\t" << Detector1_str << "\t" << Detector2_str << "\n";
		start = clock();
		m_progress_source_contamination.OffsetPos(1);
		
		
		//if (((clock() - start) / CLOCKS_PER_SEC) == 1)
		//{
		//	g_pAdam->Command_ADAMSimpleRun();
		//	WaitSec(2);
		//	Detector1 = g_pAdam->AdamData.m_nDetector1;
		//	Detector2 = g_pAdam->AdamData.m_nDetector2;
		//	cap1 = g_pAdam->AdamData.m_dCapsensor1;
		//	Detector1_str.Format("%.4f", Detector1);
		//	Detector2_str.Format("%.4f", Detector2);
		//	cap1_str.Format("%.4f", cap1);
		//	g_pAdam->Command_ADAMSimpleRun();
		//
		//
		//	day = CTime::GetCurrentTime().GetDay();
		//	hour = CTime::GetCurrentTime().GetHour();
		//	min = CTime::GetCurrentTime().GetMinute();
		//	sec = CTime::GetCurrentTime().GetSecond();
		//	source_contamination << std::setprecision(3) << day << "\t" << hour << "\t" << min << "\t" << sec << "\t" << cap1_str << "\t" << Detector1_str << "\t" << Detector2_str << "\n";
		//	source_contamination_detect_sensor_data << std::setprecision(3) << cap1_str << "\t" << Detector1_str << "\t" << Detector2_str << "\n";
		//	start = clock();
		//	m_progress_source_contamination.OffsetPos(1);
		//}
		//
		if (g_pEUVSource->Is_EUV_On() == FALSE)
		{
			g_pLog->Display(0, _T("Source Break!"));
			AfxMessageBox(_T("Source 가 중지 되었습니다."));
			break;
		}

	}

	source_contamination << std::endl;
	if (source_contamination.is_open() == true)
	{
		source_contamination.close();
	}

	source_contamination_detect_sensor_data << std::endl;
	if (source_contamination_detect_sensor_data.is_open() == true)
	{
		source_contamination_detect_sensor_data.close();
	}

	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		g_pLog->Display(0, _T("Source Break!"));
		AfxMessageBox(_T("Source 가 중지 되었습니다."));
		return nRet = -1;
	}



	// EUV SOURCE OFF

	//if (g_pLog != NULL)
	//	g_pLog->Display(0, _T("CEUVSolutionDlg::OnBnClickedButtonMainEuvOff() 버튼 클릭!"));
	//
	if (g_pEUVSource == NULL) return nRet = -1;

	if (g_pEUVSource->Is_SRC_Connected() != TRUE)
	{
		AfxMessageBox(_T("디바이스와 연결이 되지 않았습니다."));
		return nRet = -1;
	}

	g_pEUVSource->SetEUVSourceOn(FALSE);
	g_pEUVSource->SetMechShutterOpen(FALSE);
	//g_pEUVSource->SRC_Status();

	// EUV -> OM 변환 

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	WaitSec(2);
	   
	m_bTestStop = TRUE;
	m_Source_Measrue_Start = TRUE;
	m_pSourceTestThreadStop = TRUE;
}

void CSystemTestDlg::OnBnClickedCheckOffsetStart()
{
	if (m_bTestStop == FALSE || m_OMEUV_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		//GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
		//GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);

		return;
	}

	if (m_pOffsetThread == NULL)
	{
		m_pOffsetThread = ::AfxBeginThread(OffSet_Setting_VacuumThread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	}
	
	
	//KillTimer(OMEUV_OFFSET_TIMER);
}

int CSystemTestDlg::Source_Offset_Check()
{
	int nRet = RUN;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return nRet = -1;

	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	// 측정 중 일 경우 FALSE, 측정 끝나거나 중지되면 TRUE, 
	m_bTestStop = FALSE;
	m_OMEUV_Measrue_Start = FALSE;
	//if (m_OMEUV_Measrue_Start == FALSE)	SetTimer(OMEUV_OFFSET_TIMER, 100, NULL);
	
	
	//LB 1st OM Align
	g_pRecipe->CheckAlignPoint(PM_1STALIGN_POSITION);
	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(FALSE);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	WaitSec(0.5);
	g_pRecipe->SearchPMModel(PM_1STALIGN_POSITION);
	g_pRecipe->m_ButtonTestModelCtrl.EnableWindow(TRUE);
	
	
	//LB 1st align position (X,Y)
	OM_LB_align_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	OM_LB_align_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	
	
	g_pMaskMap->ChangeOMEUVSet(OMTOEUV);
	WaitSec(2);
	
	g_pAdam->m_nEuvImage_Fov = 10000;	//nm
	//g_pAdam->m_nEuvImage_Old_Fov = g_pAdam->m_nEuvImage_Fov;

	g_pAdam->m_nEuvImage_ScanGrid = 80;
	g_pAdam->m_nEuvImage_ScanNumber = 1;
	
	g_pAdam->ADAMRunStart();

	WaitSec(0.2);
	
	SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
	
	// FOV : 10
	// Grid : 1
	// Num : 1
	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, g_pAdam->m_nEuvImage_Fov, g_pAdam->m_nEuvImage_ScanGrid, g_pAdam->m_nEuvImage_ScanNumber);// Scan Stage Run
	


	//LB 1st EUV Align
	g_pAdam->OnBnClickedCornerFindButton();
	EUV_LB_align_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	EUV_LB_align_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	WaitSec(2);


	m_bTestStop = TRUE;
	m_OMEUV_Measrue_Start = TRUE;
	m_pOffsetThreadStop = TRUE;
	return nRet;
}

int CSystemTestDlg::SoureTestThreadStop()
{
	int ret = 0;

	if (m_pSourceTestThreadStop == FALSE)
	{
		m_pSourceTestThreadStop = TRUE;	// 쓰레드내의 while()에 들어가는 변수 => Thread를 정상 종료시킴
		Sleep(100);
	}

	if (!m_pSourceTestThreadStop)		// 현재 쓰레드가 종료가 안되었으면
	{
		if (m_pSourceTestThread != NULL)
		{
			HANDLE threadHandle = m_pSourceTestThread->m_hThread;
			DWORD dwResult;
			dwResult = ::WaitForSingleObject(threadHandle, /*INFINITE*/2000);

			if (dwResult == WAIT_TIMEOUT)
			{
				DWORD dwExitCode = STILL_ACTIVE;
				::GetExitCodeThread(threadHandle, &dwExitCode); // *요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
				if (dwExitCode == STILL_ACTIVE)//259
				{
					TerminateThread(threadHandle, 0/*dwExitCode*/);
					CloseHandle(threadHandle);
				}
			}
		}
		m_pSourceTestThreadStop = TRUE;
	}

	return ret;
}

void CSystemTestDlg::OnBnClickedCheckMaskFlatStop()
{
	flatness_measurement_stop = true;
	m_bTestStop = TRUE;
	m_Mask_Measrue_Start = TRUE;


}

void CSystemTestDlg::OnBnClickedStageMeasurementStart()
{
	CString ini_text;
	ini_text = _T("0.0");
	if (m_SH_Stage_Measrue_Start == FALSE || m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
		return;
	}

	if (g_pNavigationStage->m_bConnect != TRUE)
	{
		AfxMessageBox(_T("Navigation Stage 연결 확인 필요"));
		return;
	}

	g_pNavigationStage->RunBuffer(6);
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);

	///////////////////////////////////////////////////
	// 측정 Thread
	///////////////////////////////////////////////////

	//m_stageThreadExitFlag = FALSE;
	//
	//if (m_pstageThread == NULL)
	//{
	//	m_pstageThread = ::AfxBeginThread(Stage_Check_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
	//}

	g_pWarning->m_strWarningMessageVal = " Navigation Stage (SH) Measurement 시작합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	if (ACS_Stage_Check() != RUN)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		::AfxMessageBox(_T("Navigation Stage 측정에 실패 하였습니다"), MB_ICONERROR);
		m_progress_stage_check.SetPos(0);
		GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
		GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
		KillTimer(STAGE_MEASUREMENT_TIMER);
		m_SH_Stage_Measrue_Start = TRUE;
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, ini_text);
		SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, ini_text);
		return;
	}

	m_progress_stage_check.SetPos(0);
	GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);

	m_SH_Stage_Measrue_Start = TRUE;
	KillTimer(STAGE_MEASUREMENT_TIMER);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XLOAD, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YLOAD, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_XINPOS, ini_text);
	SetDlgItemTextA(IDC_EDIT_STAGE_CHECK_YINPOS, ini_text);

	g_pWarning->ShowWindow(SW_HIDE);
	::AfxMessageBox(_T("Navigation Stage (SH) 측정이 완료 되었습니다"));

}

UINT CSystemTestDlg::OffSet_Setting_VacuumThread(LPVOID pParam)
{
	int ret = 0;

	CSystemTestDlg*  g_pOffset = (CSystemTestDlg*)pParam;

	g_pOffset->m_pOffsetThreadStop = FALSE;
	while (!g_pOffset->m_pOffsetThreadStop)
	{
		ret = g_pOffset->Source_Offset_Check();
		if (ret != RUN)
		{
			g_pOffset->KillTimer(SOURCE_MEASUREMENT_TIMER);
			g_pOffset->m_bTestStop = TRUE;
			g_pOffset->m_OMEUV_Measrue_Start = TRUE;
			g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
			g_pOffset->m_pOffsetThreadStop = TRUE;

			::AfxMessageBox(_T("offset setting check 실패 하였습니다"));
		}
		g_pOffset->m_pOffsetThreadStop = TRUE;

	}

	g_pOffset->m_pOffsetThread = NULL;

	return ret;
}

UINT CSystemTestDlg::Stage_Check_Thread(LPVOID pParam)
{
	CSystemTestDlg*  StageCheck = (CSystemTestDlg*)pParam;

	StageCheck->m_stageThreadExitFlag = FALSE;
	while (!StageCheck->m_stageThreadExitFlag)
	{
		if (StageCheck->ACS_Stage_Check() != RUN)
		{
			return 0;
		}
		Sleep(50);
	}

	return 0;
}

int CSystemTestDlg::ACS_Stage_Check()
{

	m_SH_Stage_Measrue_Start = FALSE;
	int cont = 0;
	int num = 0, x = 0, y = 0;
	int nRet = RUN;

	int x_step_move = 50;
	int y_step_move = 50;


	double get_xLoad_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_yLoad_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_xInPos_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	double get_yInPos_data[STAGE_CHECK_DATA_SIZE] = { 0.0, };
	
	double XLoad = 0.0;
	double YLoad = 0.0;
	double XInPos = 0.0;
	double YInPos = 0.0;

	double Max_XLoad = 0.0;
	double Min_XLoad = 100.0;
	double Max_YLoad = 0.0;
	double Min_YLoad = 100.0;

	double XInposSum = 0.0;
	double YInposSum = 0.0;
	double XInposAvg = 0.0;
	double YInposAvg = 0.0;

	double XInposVariance = 0.0;
	double YInposVariance = 0.0;
	double XInposSTD = 0.0;
	double YInposSTD = 0.0;

	CString x_max_load_str;
	CString x_min_load_str;
	CString y_max_load_str;
	CString y_min_load_str;
	CString x_load_str;
	CString y_load_str;
	CString x_inpos_sub_str;
	CString y_inpos_sub_str;

	CString x_inpos_str;
	CString y_inpos_str;
	CString x_inpos_str_avg;
	CString y_inpos_str_avg;
	CString x_pos_str;
	CString y_pos_str;

	x_max_load_str.Empty();
	x_min_load_str.Empty();
	y_max_load_str.Empty();
	y_min_load_str.Empty();
	x_load_str.Empty();
	y_load_str.Empty();
	x_inpos_sub_str.Empty();
	y_inpos_sub_str.Empty();

	x_inpos_str.Empty();
	y_inpos_str.Empty();
	x_inpos_str_avg.Empty();
	y_inpos_str_avg.Empty();
	x_pos_str.Empty();
	y_pos_str.Empty();

	double orgin_x_pos = 0.0;
	double orgin_y_pos = 0.0;

	double x_pos = 0.0; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double y_pos = 0.0;

	double x_pos_sub = 0.0; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double y_pos_sub = 0.0;
	CString x_pos_sub_str;
	CString y_pos_sub_str;

	double move_x_pos;	// 실제 이동 위치 변수
	double move_y_pos;

	double X_value = 0.0;
	double Y_value = 0.0;

	//double z_pos;
	//double tx_pos;
	//double ty_pos;
	//CString z_pos_str;
	//CString tx_pos_str;
	//CString ty_pos_str;


	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return -1;


	m_progress_stage_check.SetPos(0);

	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	//////////////////////////////////////////////////
	// Stage 0,0 으로 초기화 
	//////////////////////////////////////////////////
	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);


	// 측정 중 일 경우 FALSE, 측정 끝나거나 중지되면 TRUE, 
	m_bTestStop = FALSE;
	m_Mask_Measrue_Start = FALSE;
	if (m_Mask_Measrue_Start == FALSE)	SetTimer(STAGE_MEASUREMENT_TIMER, 100, NULL);

	g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	WaitSec(2);

	///////////////////////////////////////////////////////////////
	// Z 축 0 으로 원점 
	///////////////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
	//WaitSec(2);
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	//{
	//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(0.0, 0.0);
	//}
	//else
	//{
	//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
	//	return nRet = -1;
	//}
	//
	
	//CString start_x_pos_str;
	//CString start_y_pos_str;
	//
	//orgin_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	//orgin_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	//start_x_pos_str.Format("%3.7f", orgin_x_pos);
	//start_y_pos_str.Format("%3.7f", orgin_y_pos);
	//
	//SetDlgItemTextA(IDC_EDIT_TEST_MASK_X_START, start_x_pos_str);
	//SetDlgItemTextA(IDC_EDIT_TEST_MASK_Y_START, start_y_pos_str);

	//// 읽은 좌표값을 기준으로 측정 가능 범위 재 확인 (X, Y)
	//target_x_pos = orgin_x_pos - (10 * 15);
	//if (target_x_pos < 0)
	//{
	//	MsgBoxAuto.DoModal(_T(" 평탄도 x axis 측정 범위 벗어남 ! "), 4);
	//	return nRet = -1;
	//}
	//
	//target_y_pos = orgin_y_pos + (10 * 15);
	//if (target_y_pos > 200)
	//{
	//	MsgBoxAuto.DoModal(_T(" 평탄도 y axis 측정 범위 벗어남 ! "), 4);
	//	return nRet = -1;
	//}

	if (m_SH_Stage_Measrue_Start == TRUE)
	{
		::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
		return nRet = -1;
	}

	/////////////////////////////////////////////////////////////////////////
	// 측정 전 Z 축 충돌방지를 위한 0 위치 재확인.
	/////////////////////////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
	//WaitSec(2);
	//
	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) != 0)
	//{
	//	return nRet = -1;
	//}

	str = "Navigation Stage (SH) 측정 시작 !";
	SaveLogFile("SH_Stage_Check_Log", _T((LPSTR)(LPCTSTR)(str)));
	g_pLog->Display(0, str);


	y = 0;
	x = 0;

	//position = 0;
	//position_str.Format("%d", position);

	//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(position_str);
	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 진행 중"));
	//
	int mon = CTime::GetCurrentTime().GetMonth();
	int day = CTime::GetCurrentTime().GetDay();
	int hour = CTime::GetCurrentTime().GetHour();
	int min = CTime::GetCurrentTime().GetMinute();

	CString datafile_sub;
	CString datafile;
	CString datafile_load;
	CString datafile_inpos;
	CString path;
	CString strThisPath;

	datafile_sub.Empty();
	datafile.Empty();
	datafile_load.Empty();
	datafile_inpos.Empty();
	path.Empty();
	strThisPath.Empty();


	path = LOG_PATH;

	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;
	if (!g_pLog->IsFolderExist(path))
	{
		CreateDirectory(path, NULL);
	}

	strThisPath.Format("%s\\Navigation_Stage_Measurement_Load_Inpos_%d_%d_%d_%d_start.xlsx", path, mon, day, hour, min);
	CXLEzAutomation XL(FALSE);
	XL.SetCellValue(1, 1, "SREM SH Stage Inpostion & Load Measurement Report");
	XL.SetCellValue(5, 2, "Inposition");
	XL.SetCellValue(6, 2, "Position");
	XL.SetCellValue(7, 2, "Axis 0 Inpos");
	XL.SetCellValue(8, 2, "Axis 1 Inpos");
	/////////////////////
	//좌표
	////////////////////
	XL.SetCellValue(6, 3, "(0,0)");
	XL.SetCellValue(6, 4, "(0,50)");
	XL.SetCellValue(6, 5, "(0,100)");
	XL.SetCellValue(6, 6, "(0,150)");
	XL.SetCellValue(6, 7, "(100,0)");
	XL.SetCellValue(6, 8, "(100,50)");
	XL.SetCellValue(6, 9, "(100,100)");
	XL.SetCellValue(6, 10, "(100,150)");
	XL.SetCellValue(6, 11, "(200,0)");
	XL.SetCellValue(6, 12, "(200,50)");
	XL.SetCellValue(6, 13, "(200,100)");
	XL.SetCellValue(6, 14, "(200,150)");
	XL.SetCellValue(6, 15, "(300,0)");
	XL.SetCellValue(6, 16, "(300,50)");
	XL.SetCellValue(6, 17, "(300,100)");
	XL.SetCellValue(6, 17, "(300,150)");

	datafile_sub.Format(_T("%s\\SH_Stage_Measurement_Load_Inpos_Sub_Data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile.Format(_T("%s\\SH_Stage_Measurement_Data_%d_%d_%d_%d.txt"), path, mon, day, hour, min);
	datafile_load.Format(_T("%s\\SH_Stage_load_data.txt"), path);
	datafile_inpos.Format(_T("%s\\SH_Stage_inpos_data.txt"), path);

	std::ofstream sh_stage_measurement_load_Inpos_datafile_sub(datafile_sub);
	std::ofstream sh_stage_measurement_datafile(datafile);
	std::ofstream sh_stage_measurement_load_value(datafile_load);
	std::ofstream sh_stage_measurement_inpos_value(datafile_inpos);

	sh_stage_measurement_load_Inpos_datafile_sub << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad" << "\t" << "yLoad" << "\t" << "xInposition" << "\t" << "yInposition" << "\t" << "\n";
	sh_stage_measurement_datafile << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad_Max" << "\t" << "xLoad_Min" << "\t" << "yLoad_Max" << "\t" << "yLoad_Min" << "\t" << "xInPos" << "\t" << "yInPos" << "\n";
	sh_stage_measurement_load_value << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xLoad_Max" << "\t" << "xLoad_Min" << "\t" << "yLoad_Max" << "\t" << "yLoad_Min" << "\n";
	sh_stage_measurement_inpos_value << "\t" << "x_pos" << "\t" << "y_pos" << "\t" << "xInPos_Avg" << "\t" << "yInPos_Avg" << "\t" << "xInPos_Std" << "\t" << "yInPos_Std" << "\n";


	if (m_SH_Stage_Measrue_Start == TRUE)
	{
		::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");

		XL.SaveFileAs(strThisPath);
		XL.ReleaseExcel();
		
		sh_stage_measurement_datafile << std::endl;
		if (sh_stage_measurement_datafile.is_open() == true)
		{
			sh_stage_measurement_datafile.close();
		}

		sh_stage_measurement_load_value << std::endl;
		if (sh_stage_measurement_load_value.is_open() == true)
		{
			sh_stage_measurement_load_value.close();
		}

		sh_stage_measurement_inpos_value << std::endl;
		if (sh_stage_measurement_inpos_value.is_open() == true)
		{
			sh_stage_measurement_inpos_value.close();
		}

		sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
		if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
		{
			sh_stage_measurement_load_Inpos_datafile_sub.close();
		}
		return nRet = -1;
	}

	while (y <= Y_AXIS_0_SIZE) // Y축 이동
	{
		if (y == 0)	y_pos = orgin_y_pos;
		else y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		while (x <= X_AXIS_1_SIZE) // X 축 이동
		{
			if (x == 0)	x_pos = orgin_x_pos;
			else x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);



			// SCAN STAGE AXIS 값 READ
			//z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
			//tx_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
			//ty_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];
			//z_pos_str.Format("%3.7f", z_pos);
			//tx_pos_str.Format("%3.7f", tx_pos);
			//ty_pos_str.Format("%3.7f", ty_pos);

			for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			{
				get_xLoad_data[i] = 0.0;
				get_yLoad_data[i] = 0.0;
			}
		
			do
			{
				WaitSec(1);
				
				x_pos_sub = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
				y_pos_sub = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
				get_xLoad_data[num] = g_pNavigationStage->GetGlobalRealVariable("DOUT_1");
				get_yLoad_data[num] = g_pNavigationStage->GetGlobalRealVariable("DOUT_0");
				//get_xInPos_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE0"));
				//get_yInPos_data[num] = abs(g_pNavigationStage->GetGlobalRealVariable("PE1"));

				get_xInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE0");
				get_yInPos_data[num] = g_pNavigationStage->GetGlobalRealVariable("PE1");


				x_pos_sub_str.Format("%3.7f", x_pos_sub);
				y_pos_sub_str.Format("%3.7f", y_pos_sub);
				x_load_str.Format("%3.7f", get_xLoad_data[num]);
				y_load_str.Format("%3.7f", get_yLoad_data[num]);
				x_inpos_sub_str.Format("%3.3e", get_xInPos_data[num]);
				y_inpos_sub_str.Format("%3.3e", get_yInPos_data[num]);
				num += 1;

				sh_stage_measurement_load_Inpos_datafile_sub << std::setprecision(3) << "\t" << x_pos_sub_str << "\t" << y_pos_sub_str << "\t" << x_load_str << "\t" << y_load_str << "\t" << x_inpos_sub_str <<"\t"<< y_inpos_sub_str <<"\n";

			} while (num < STAGE_CHECK_DATA_SIZE);
			num = 0;

			for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			{
				if (get_xLoad_data[i] > Max_XLoad) Max_XLoad = get_xLoad_data[i];
				if (get_yLoad_data[i] > Max_YLoad) Max_YLoad = get_yLoad_data[i];
				if (get_xLoad_data[i] < Min_XLoad) Min_XLoad = get_xLoad_data[i];
				if (get_yLoad_data[i] < Min_YLoad) Min_YLoad = get_yLoad_data[i];
			}

			for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			{
				XInposSum += get_xInPos_data[i];
				YInposSum += get_yInPos_data[i];
			}

			XInposAvg = (XInposSum / STAGE_CHECK_DATA_SIZE);
			YInposAvg = (YInposSum / STAGE_CHECK_DATA_SIZE);

			XInposSum = 0.0;
			YInposSum = 0.0;
			
			x_inpos_str_avg.Format("%3.4e", XInposAvg);
			y_inpos_str_avg.Format("%3.4e", YInposAvg);


			for (int i = 0; i < STAGE_CHECK_DATA_SIZE; i++)
			{
				X_value += pow((get_xInPos_data[i] - XInposAvg), 2);
				Y_value += pow((get_yInPos_data[i] - YInposAvg), 2);
			}
			
			XInposVariance = (X_value / STAGE_CHECK_DATA_SIZE);
			YInposVariance = (Y_value / STAGE_CHECK_DATA_SIZE);

			X_value = 0.0;
			Y_value = 0.0;

			XInposSTD = sqrt(XInposVariance);
			YInposSTD = sqrt(YInposVariance);

			x_pos_str.Format("%3.7f", x_pos);
			y_pos_str.Format("%3.7f", y_pos);

			x_max_load_str.Format("%3.7f", Max_XLoad);
			x_min_load_str.Format("%3.7f", Min_XLoad);
			y_max_load_str.Format("%3.7f", Max_YLoad);
			y_min_load_str.Format("%3.7f", Min_YLoad);
			x_inpos_str.Format("%3.4e", XInposSTD);
			y_inpos_str.Format("%3.4e", YInposSTD);

			sh_stage_measurement_datafile << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_max_load_str << "\t\t" << x_min_load_str << "\t\t" << y_max_load_str << "\t\t" << y_min_load_str << "\t\t" << x_inpos_str << "\t\t" << y_inpos_str << "\n";
			sh_stage_measurement_load_value << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_max_load_str << "\t\t" << x_min_load_str << "\t\t" << y_max_load_str << "\t\t" << y_min_load_str << "\n";
			sh_stage_measurement_inpos_value << std::setprecision(3) << "\t\t" << x_pos_str << "\t\t" << y_pos_str << "\t\t" << x_inpos_str_avg << "\t\t" << y_inpos_str_avg <<  "\t\t" << x_inpos_str << "\t\t" << y_inpos_str << "\n";

			// X = 0
			//if (-1.0 < x_pos < 1.0)
			if ( abs(x_pos) < 2.0)
			{
				//Y = 0 (0,0)
				//if (-1.0 < y_pos < 1.0)
				if ( abs(y_pos) <= 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_0_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_0_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 3, x_inpos_str);
					XL.SetCellValue(8, 3, y_inpos_str);
				}
				// Y = 50 (0,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_50_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_50_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 4, x_inpos_str);
					XL.SetCellValue(8, 4, y_inpos_str);
				}
				// Y = 100 (0,100)
				else if ((98.0 < y_pos) && (y_pos < 101.0))
				{
					XL.SetCellValue(7, 5, x_inpos_str);
					XL.SetCellValue(8, 5, y_inpos_str);
				}
				// Y = 150 (0,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_0_150_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_0_150_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 6, x_inpos_str);
					XL.SetCellValue(8, 6, y_inpos_str);
				}
			}
			else if ((98.0 < x_pos) && (x_pos < 102.0))
			{
				//Y = 0 (100,0)
				//if (-1.0 < y_pos < 1.0)
				if (y_pos <= 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_0_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_0_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 7, x_inpos_str);
					XL.SetCellValue(8, 7, y_inpos_str);
				}
				// Y = 50 (100,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_50_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_50_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 8, x_inpos_str);
					XL.SetCellValue(8, 8, y_inpos_str);
				}
				// Y = 100 (100,100)
				else if ((198.0 < y_pos) && (y_pos < 101.0))
				{
					XL.SetCellValue(7, 9, x_inpos_str);
					XL.SetCellValue(8, 9, y_inpos_str);
				}
				// Y = 150 (100,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_100_150_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_100_150_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 10, x_inpos_str);
					XL.SetCellValue(8, 10, y_inpos_str);
				}
			}
			else if ((198.0 < x_pos) && (x_pos < 202.0))
			{
				//Y = 0 (200,0)
				//if (-1.0 < y_pos < 1.0)
				if (y_pos < 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_0_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_0_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 11, x_inpos_str);
					XL.SetCellValue(8, 11, y_inpos_str);
				}
				// Y = 50 (200,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_50_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_50_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 12, x_inpos_str);
					XL.SetCellValue(8, 12, y_inpos_str);
				}
				// Y = 100 (200,100)
				else if ((98.0 < y_pos) && (y_pos < 101.0))
				{
					XL.SetCellValue(7, 13, x_inpos_str);
					XL.SetCellValue(8, 13, y_inpos_str);
				}
				// Y = 150 (200,150)
				else if ((148.0 < y_pos) && (y_pos < 152.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_200_150_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_200_150_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 14, x_inpos_str);
					XL.SetCellValue(8, 14, y_inpos_str);
				}
			}
			else if ((298.0 < x_pos) && (x_pos < 302.0))
			{				//Y = 0 (300,0)
				//if (-1.0 < y_pos < 1.0)
				if (y_pos < 2.0)
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_0_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_0_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 15, x_inpos_str);
					XL.SetCellValue(8, 15, y_inpos_str);
				}
				// Y = 50 (300,50)
				else if ((48.0 < y_pos) && (y_pos < 52.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_50_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_50_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 16, x_inpos_str);
					XL.SetCellValue(8, 16, y_inpos_str);
				}
				// Y = 100 (300,100)
				else if ((98.0 < y_pos) && (y_pos < 102.0))
				{
					XL.SetCellValue(7, 17, x_inpos_str);
					XL.SetCellValue(8, 17, y_inpos_str);
				}
				// Y = 150 (100,150)
				else if ((149.0 < y_pos) && (y_pos < 151.0))
				{
					SetDlgItemTextA(IDC_EDIT_POS_300_150_X_LOAD, x_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_Y_LOAD, y_max_load_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_X_INPOS, x_inpos_str);
					SetDlgItemTextA(IDC_EDIT_POS_300_150_Y_INPOS, y_inpos_str);
					XL.SetCellValue(7, 18, x_inpos_str);
					XL.SetCellValue(8, 18, y_inpos_str);
				}
			}
			//CString cont_str;
			//cont_str.Format("%d", cont);
			//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(cont_str);

			if (m_SH_Stage_Measrue_Start == TRUE)
			{

				::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
				nRet = -2;
				
				XL.SaveFileAs(strThisPath);
				XL.ReleaseExcel();
				sh_stage_measurement_datafile << std::endl;
				if (sh_stage_measurement_datafile.is_open() == true)
				{
					sh_stage_measurement_datafile.close();
				}

				sh_stage_measurement_load_value << std::endl;
				if (sh_stage_measurement_load_value.is_open() == true)
				{
					sh_stage_measurement_load_value.close();
				}

				sh_stage_measurement_inpos_value << std::endl;
				if (sh_stage_measurement_inpos_value.is_open() == true)
				{
					sh_stage_measurement_inpos_value.close();
				}

				sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
				if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
				{
					sh_stage_measurement_load_Inpos_datafile_sub.close();
				}

				break;
			}
			
			Max_XLoad = 0.0;
			Max_YLoad = 0.0;
			Min_XLoad = 0.0;
			Min_YLoad = 0.0;

			///////////////////////////////////////////////
			//Y stroke 170, X stroke 350 측정을 위함.
			///////////////////////////////////////////////
			move_x_pos = x_pos + x_step_move;
			move_y_pos = y_pos;

			// Z축 충돌 방지를 위한 0 재 확인 후 이동.
			//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
			//WaitSec(2);
			//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
			//{
			//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
			//}
			//else
			//{
			//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
			//	nRet = -1;
			//	break;
			//}

			x = move_x_pos;
			if (x >= X_AXIS_1_SIZE) break;

			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
			WaitSec(10);

			//m_progress_stage_check.OffsetPos(1);
			// m_progress_stage_check.OffsetPos(1);
			cont++;
			m_progress_stage_check.SetPos(cont);
		
		}

		if (m_SH_Stage_Measrue_Start == TRUE)
		{
			::AfxMessageBox(" Navigation Stage 측정 STOP 버튼 누름 ! ");
			nRet = -2;
			XL.SaveFileAs(strThisPath);
			XL.ReleaseExcel();
			sh_stage_measurement_datafile << std::endl;
			if (sh_stage_measurement_datafile.is_open() == true)
			{
				sh_stage_measurement_datafile.close();
			}

			sh_stage_measurement_load_value << std::endl;
			if (sh_stage_measurement_load_value.is_open() == true)
			{
				sh_stage_measurement_load_value.close();
			}

			sh_stage_measurement_inpos_value << std::endl;
			if (sh_stage_measurement_inpos_value.is_open() == true)
			{
				sh_stage_measurement_inpos_value.close();
			}

			sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
			if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
			{
				sh_stage_measurement_load_Inpos_datafile_sub.close();
			}
			break;
		}

		if (nRet != RUN) break; // x 방향 Error 발생.

		move_x_pos = orgin_x_pos;		  // X 축 초기 값으로 이동.
		move_y_pos = y_pos + y_step_move; // Y 축 50 mm 씩 이동.

		x = 0;
		y = move_y_pos;
		if (y >= Y_AXIS_0_SIZE) break;

		g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);

		// Z축 충돌 방지를 위한 0 재 확인 후 이동.
		//g_pScanStage->MoveZAbsolute_SlowInterlock(0.0);
		//WaitSec(2);
		//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
		//{
		//	g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//}
		//else
		//{
		//	::AfxMessageBox(" Z AXIS 0.0 아님.!");
		//	nRet = -1;
		//	break;
		//}
		//
		//WaitSec(2);

		if (nRet != RUN) break; // y 방향 Error 발생.
	}

	XL.SaveFileAs(strThisPath);
	XL.ReleaseExcel();

	sh_stage_measurement_datafile << std::endl;
	if (sh_stage_measurement_datafile.is_open() == true)
	{
		sh_stage_measurement_datafile.close();
	}

	sh_stage_measurement_load_value << std::endl;
	if (sh_stage_measurement_load_value.is_open() == true)
	{
		sh_stage_measurement_load_value.close();
	}

	sh_stage_measurement_inpos_value << std::endl;
	if (sh_stage_measurement_inpos_value.is_open() == true)
	{
		sh_stage_measurement_inpos_value.close();
	}

	sh_stage_measurement_load_Inpos_datafile_sub << std::endl;
	if (sh_stage_measurement_load_Inpos_datafile_sub.is_open() == true)
	{
		sh_stage_measurement_load_Inpos_datafile_sub.close();
	}


	//GetDlgItem(IDC_MASK_PRO)->SetWindowTextA(_T("측정 완료"));
	if (nRet != RUN) return nRet; // Error 발생 시

	// 측정 완료 후 다시 OM 영역으로 복귀
	//g_pMaskMap->ChangeOMEUVSet(EUVTOOM);
	//SetDlgItemTextA(IDC_OM_EUV, "OM");
	//
	//GetDlgItem(IDC_CHECK_MASK_FLAT_SET)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT_DEFAULT)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_SOURCE)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_MASK_FLAT)->EnableWindow(true);
	//GetDlgItem(IDC_EDIT_TEST_MASK_FLAT_NUM)->SetWindowTextA(_T("0"));
	//m_progress_stage_check.SetPos(0);
	cont = 0;

	m_SH_Stage_Measrue_Start = TRUE;
	return nRet;

}

void CSystemTestDlg::OnBnClickedStageMeasurementStop()
{
	m_SH_Stage_Measrue_Start = TRUE;
	m_bTestStop = TRUE;
	m_Mask_Measrue_Start = TRUE;

}

CSystemTestDlg::vectors vec(double x1, double y1, double z1, double x2, double y2, double z2)
{
	CSystemTestDlg::vectors vector;

	vector.a = (x2 - x1);
	vector.b = (y2 - y1);
	vector.c = (z2 - z1);

	return vector;
}

void CSystemTestDlg::test()
{
	double n1, n2, n3;
	double roll, pitch;

	roll = 0.0;
	pitch = 0.0;
	n1 = 0.0;
	n2 = 0.0;
	n3 = 0.0;

	double navigatation_x_pos, navigatation_y_pos;
	double scan_x_pos, scan_y_pos, scan_z_pos, scan_tip_pos, scan_tilt_pos;
	double cap1, cap2, cap3, cap4;
	

	vectors vec1, vec2;
	navigatation_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	navigatation_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	g_pAdam->Command_AverageRunAfterTimeout();
	cap1 = g_pAdam->AdamData.m_dCapsensor1;
	cap2 = g_pAdam->AdamData.m_dCapsensor2;
	cap3 = g_pAdam->AdamData.m_dCapsensor3;
	cap4 = g_pAdam->AdamData.m_dCapsensor4;
	
	
	scan_x_pos = g_pScanStage->m_dPIStage_MovePos[X_AXIS];
	scan_y_pos = g_pScanStage->m_dPIStage_MovePos[Y_AXIS];
	scan_z_pos = g_pScanStage->m_dPIStage_MovePos[Z_AXIS];
	scan_tip_pos = g_pScanStage->m_dPIStage_MovePos[TIP_AXIS];
	scan_tilt_pos = g_pScanStage->m_dPIStage_MovePos[TILT_AXIS];
	
	
	vec1 = vec(2, 3, 4, 5, 6, 7);
	vec2 = vec(10, 11, 12, 3, 3, 3);

	n1 = ((vec1.b * vec2.c) - (vec1.c * vec2.b));
	n2 = ((vec1.c * vec2.a) - (vec1.a * vec2.c));
	n3 = ((vec1.a * vec2.b) - (vec1.b * vec2.a));

	
	roll = atan((n1 / n3));
	pitch = asin(-n2);
}

void CSystemTestDlg::PI_Stage_Faltness_Monitor_Check()
{
	double navigatation_x_pos, navigatation_y_pos;
	double mask_center_x_pos, mask_center_y_pos;
	double mask_lb_x_pos, mask_lb_y_pos;
	double scan_x_pos, scan_y_pos, scan_z_pos, scan_tip_pos, scan_tilt_pos;
	double cap1, cap2, cap3, cap4;

	navigatation_x_pos = 0.0;
	navigatation_y_pos = 0.0;
	mask_center_x_pos = 0.0;
	mask_center_y_pos = 0.0;
	mask_lb_x_pos = 0.0;
	mask_lb_y_pos = 0.0;
	scan_x_pos = 0.0; 
	scan_y_pos = 0.0; 
	scan_z_pos = 0.0; 
	scan_tip_pos = 0.0; 
	scan_tilt_pos = 0.0;
	cap1 = 0.0;
	cap2 = 0.0; 
	cap3 = 0.0; 
	cap4 = 0.0;

	CString navigatation_x_pos_str, navigatation_y_pos_str , mask_center_x_pos_str, mask_center_y_pos_str, mask_lb_x_pos_str, mask_lb_y_pos_str;
	CString scan_x_pos_str, scan_y_pos_str, scan_z_pos_str, scan_tip_pos_str, scan_tilt_pos_str;
	CString cap1_str, cap2_str, cap3_str, cap4_str;

	navigatation_x_pos_str.Empty();
	navigatation_y_pos_str.Empty();
	mask_center_x_pos_str.Empty();
	mask_center_y_pos_str.Empty();
	mask_lb_x_pos_str.Empty();
	mask_lb_y_pos_str.Empty();
	scan_x_pos_str.Empty();
	scan_y_pos_str.Empty();
	scan_z_pos_str.Empty(); 
	scan_tip_pos_str.Empty();
	scan_tilt_pos_str.Empty();

	cap1_str.Empty(); 
	cap2_str.Empty(); 
	cap3_str.Empty(); 
	cap4_str.Empty();

	////////////////////////////////////////////////////////
	//Navigation Stage 기준
	////////////////////////////////////////////////////////
	navigatation_x_pos = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	navigatation_y_pos = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	////////////////////////////////////////////////////////
	// Mask (LB0,0)기준
	////////////////////////////////////////////////////////
	mask_lb_x_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - navigatation_x_pos;
	mask_lb_x_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - navigatation_y_pos;

	////////////////////////////////////////////////////////
	// Mask Center 0,0 기준
	////////////////////////////////////////////////////////
	mask_center_x_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - navigatation_x_pos;
	mask_center_y_pos = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - navigatation_y_pos;

	////////////////////////////////////////////////////////
	// Adam Cap Read Command
	////////////////////////////////////////////////////////
	//if (g_pAdam->Command_CapReadTimeout()) //return 0면 정상.. 아니면 error사항.. 
	//{
	//	::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
	//	cap1 = -1.111111;
	//	cap2 = -1.111111;
	//	cap3 = -1.111111;
	//	cap4 = -1.111111;
	//}
	//else
	//{
	//	cap1 = g_pAdam->AdamData.m_dCapsensor1;
	//	cap2 = g_pAdam->AdamData.m_dCapsensor2;
	//	cap3 = g_pAdam->AdamData.m_dCapsensor3;
	//	cap4 = g_pAdam->AdamData.m_dCapsensor4;
	//}
	//
	////////////////////////////////////////////////////////
	// Scan Stage Axis Read Command
	////////////////////////////////////////////////////////
	scan_x_pos = g_pScanStage->m_dPIStage_GetPos[X_AXIS];
	scan_y_pos = g_pScanStage->m_dPIStage_GetPos[Y_AXIS];
	scan_z_pos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	scan_tip_pos = g_pScanStage->m_dPIStage_GetPos[TIP_AXIS];
	scan_tilt_pos = g_pScanStage->m_dPIStage_GetPos[TILT_AXIS];


	navigatation_x_pos_str.Format("%3.7f", navigatation_x_pos);
	navigatation_y_pos_str.Format("%3.7f", navigatation_y_pos);
	mask_lb_x_pos_str.Format("%3.7f", mask_lb_x_pos);
	mask_lb_y_pos_str.Format("%3.7f", mask_lb_y_pos);
	mask_center_x_pos_str.Format("%3.7f", mask_center_x_pos);
	mask_center_y_pos_str.Format("%3.7f", mask_center_y_pos);

	cap1_str.Format("%3.7f", cap1);
	cap2_str.Format("%3.7f", cap2);
	cap3_str.Format("%3.7f", cap3);
	cap4_str.Format("%3.7f", cap4);

	scan_x_pos_str.Format("%3.7f", scan_x_pos);
	scan_y_pos_str.Format("%3.7f", scan_y_pos);
	scan_z_pos_str.Format("%3.7f", scan_z_pos);
	scan_tip_pos_str.Format("%3.7f", scan_tip_pos);
	scan_tilt_pos_str.Format("%3.7f", scan_tilt_pos);

	SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_POS, navigatation_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_POS, navigatation_y_pos_str);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_LB_POS, mask_lb_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_LB_POS, mask_lb_y_pos_str);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_X_CENTER_POS, mask_center_x_pos_str);
	SetDlgItemTextA(IDC_EDIT_SH_STAGE_Y_CENTER_POS, mask_center_y_pos_str);


	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Z, scan_z_pos_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TIP, scan_tip_pos_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TILT, scan_tilt_pos_str);

	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP1, cap1_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP2, cap2_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP3, cap3_str);
	//SetDlgItemTextA(IDC_EDIT_PI_STAGE_CAP4, cap4_str);

}


UINT CSystemTestDlg::PI_Stage_Check_Thread(LPVOID pParam)
{
	CSystemTestDlg*  PIStageCheck = (CSystemTestDlg*)pParam;

	PIStageCheck->m_PIstageThreadExitFlag = FALSE;
	
	while (!PIStageCheck->m_PIstageThreadExitFlag)
	{
		if (PIStageCheck->PI_Stage_Check() != RUN)
		{
			return 0;
		}
		Sleep(50);
	}

	return 0;

}

int CSystemTestDlg::PI_Stage_Check()
{
	vectors vec1, vec2;

	int cont = 0;

	double nvector1, nvector2, nvector3;
	double roll, pitch;
	double cap1, cap2, cap3, cap4;
	double move_x_pos, move_y_pos;	// 실제 이동 위치 변수
	double x_pos_abs1, y_pos_abs1, x_pos_rel1, y_pos_rel1, z_pos1, tip_pos1, tilt_pos1; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double x_pos_abs2, y_pos_abs2, x_pos_rel2, y_pos_rel2, z_pos2, tip_pos2, tilt_pos2; // 현재 위치 값 읽어 상태를 전달 하는 변수
	double x_pos_abs3, y_pos_abs3, x_pos_rel3, y_pos_rel3, z_pos3, tip_pos3, tilt_pos3; // 현재 위치 값 읽어 상태를 전달 하는 변수
	

	roll = 0.0;
	pitch = 0.0;
	
	nvector1 = 0.0;
	nvector2 = 0.0;
	nvector3 = 0.0;
	
	cap1 = 0.0;
	cap2 = 0.0;
	cap3 = 0.0;
	cap4 = 0.0;

	move_x_pos = 0.0;
	move_y_pos = 0.0;

	x_pos_abs1 = 0.0;
	y_pos_abs1 = 0.0;
	x_pos_rel1 = 0.0;
	y_pos_rel1 = 0.0;
	z_pos1 = 0.0;
	tip_pos1 = 0.0;
	tilt_pos1 = 0.0;


	x_pos_abs2 = 0.0;
	y_pos_abs2 = 0.0;
	x_pos_rel2 = 0.0;
	y_pos_rel2 = 0.0;
	z_pos2 = 0.0;
	tip_pos2 = 0.0;
	tilt_pos2 = 0.0;


	x_pos_abs3 = 0.0;
	y_pos_abs3 = 0.0;
	x_pos_rel3 = 0.0;
	y_pos_rel3 = 0.0;
	z_pos3 = 0.0;
	tip_pos3 = 0.0;
	tilt_pos3 = 0.0;


	CString x_pos_abs1_str, y_pos_abs1_str, x_pos_rel1_str, y_pos_rel1_str, z_pos1_str, tip_pos1_str, tilt_pos1_str;
	CString x_pos_abs2_str, y_pos_abs2_str, x_pos_rel2_str, y_pos_rel2_str, z_pos2_str, tip_pos2_str, tilt_pos2_str;
	CString x_pos_abs3_str, y_pos_abs3_str, x_pos_rel3_str, y_pos_rel3_str, z_pos3_str, tip_pos3_str, tilt_pos3_str;
	CString cap1_str, cap2_str, cap3_str, cap4_str;
	CString position_str;
	CString roll_str, pitch_str;

	x_pos_abs1_str.Empty();
	y_pos_abs1_str.Empty();
	x_pos_rel1_str.Empty();
	y_pos_rel1_str.Empty();
	z_pos1_str.Empty();
	tip_pos1_str.Empty();
	tilt_pos1_str.Empty();

	x_pos_abs2_str.Empty();
	y_pos_abs2_str.Empty();
	x_pos_rel2_str.Empty();
	y_pos_rel2_str.Empty();
	z_pos2_str.Empty();
	tip_pos2_str.Empty();
	tilt_pos2_str.Empty();

	x_pos_abs3_str.Empty();
	y_pos_abs3_str.Empty();
	x_pos_rel3_str.Empty();
	y_pos_rel3_str.Empty();
	z_pos3_str.Empty();
	tip_pos3_str.Empty();
	tilt_pos3_str.Empty();

	cap1_str.Empty();
	cap2_str.Empty();
	cap3_str.Empty();
	cap4_str.Empty();

	position_str.Empty();

	roll_str.Empty();
	pitch_str.Empty();

	double orgin_x_pos;
	double orgin_y_pos;

	int position = 0;
	int x = 0, y = 0;

	int nRet = RUN;

	m_SH_Stage_Measrue_Start = FALSE;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pCamera == NULL || g_pWarning == NULL || g_pAdam == NULL || g_pConfig == NULL)	return -1;

	//STAGE 움직임 가능 여부 확인. 
	CAutoMessageDlg MsgBoxAuto(this);

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		MsgBoxAuto.DoModal(_T(" Stage Moving Impossible! "), 4);
		return nRet = -1;
	}

	// 측정 중 일 경우 FALSE, 측정 끝나거나 중지되면 TRUE, 
	m_bTestStop = FALSE;
	m_Mask_Measrue_Start = FALSE;
	if (m_Mask_Measrue_Start == FALSE)	SetTimer(MASK_FLATNESS_MEASUREMENT_TIMER, 100, NULL);
	////////////////////////////////////
	// MASK SIZE :: 152.4 x 152.4 mm
	////////////////////////////////////




	////////////////////////////////////////////////////
	//// point 1 로 이동 하는 함수 필요
	/////////////////////////////////////////////////////

	x_pos_abs1 = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	y_pos_abs1 = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	z_pos1 = g_pScanStage->m_dPIStage_MovePos[Z_AXIS];
	tip_pos1 = g_pScanStage->m_dPIStage_MovePos[TIP_AXIS];
	tilt_pos1 = g_pScanStage->m_dPIStage_MovePos[TILT_AXIS];
	cap1 = g_pAdam->AdamData.m_dCapsensor2;

	x_pos_rel1 = x_pos_abs1 - x_pos_ref;
	y_pos_rel1 = y_pos_abs1 - y_pos_ref;

	x_pos_abs1_str.Format("%3.7f", x_pos_abs1);
	y_pos_abs1_str.Format("%3.7f", y_pos_abs1);
	x_pos_rel1_str.Format("%3.7f", x_pos_rel1);
	y_pos_rel1_str.Format("%3.7f", y_pos_rel1);
	z_pos1_str.Format("%3.7f", z_pos1);
	tip_pos1_str.Format("%3.7f", tip_pos1);
	tilt_pos1_str.Format("%3.7f", tilt_pos1);
	cap1_str.Format("%3.7f", cap1);


	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_X, x_pos_rel1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Y, y_pos_rel1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Z, z_pos1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TIP, tip_pos1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_TILT, tilt_pos1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_CAP2, cap1_str);


	////////////////////////////////////////////////////
	//// point 2 로 이동 하는 함수 필요
	/////////////////////////////////////////////////////

	x_pos_abs2 = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	y_pos_abs2 = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	z_pos2 = g_pScanStage->m_dPIStage_MovePos[Z_AXIS];
	tip_pos2 = g_pScanStage->m_dPIStage_MovePos[TIP_AXIS];
	tilt_pos2 = g_pScanStage->m_dPIStage_MovePos[TILT_AXIS];
	cap2 = g_pAdam->AdamData.m_dCapsensor2;

	x_pos_rel2 = x_pos_abs2 - x_pos_ref;
	y_pos_rel2 = y_pos_abs2 - y_pos_ref;

	x_pos_abs2_str.Format("%3.7f", x_pos_abs2);
	y_pos_abs2_str.Format("%3.7f", y_pos_abs2);
	x_pos_rel2_str.Format("%3.7f", x_pos_rel2);
	y_pos_rel2_str.Format("%3.7f", y_pos_rel2);
	z_pos2_str.Format("%3.7f", z_pos2);
	tip_pos2_str.Format("%3.7f", tip_pos2);
	tilt_pos2_str.Format("%3.7f", tilt_pos2);
	cap2_str.Format("%3.7f", cap2);


	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_X, x_pos_rel2_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_Y, y_pos_rel2_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_CAP2, cap2_str);


	////////////////////////////////////////////////////
	//// point 3 로 이동 하는 함수 필요
	/////////////////////////////////////////////////////

	x_pos_abs3 = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	y_pos_abs3 = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);
	z_pos3 = g_pScanStage->m_dPIStage_MovePos[Z_AXIS];
	tip_pos3 = g_pScanStage->m_dPIStage_MovePos[TIP_AXIS];
	tilt_pos3 = g_pScanStage->m_dPIStage_MovePos[TILT_AXIS];
	cap3 = g_pAdam->AdamData.m_dCapsensor2;

	x_pos_rel3 = x_pos_abs3- x_pos_ref;
	y_pos_rel3 = y_pos_abs3 - y_pos_ref;

	x_pos_abs3_str.Format("%3.7f", x_pos_abs3);
	y_pos_abs3_str.Format("%3.7f", y_pos_abs3);
	x_pos_rel3_str.Format("%3.7f", x_pos_rel3);
	y_pos_rel3_str.Format("%3.7f", y_pos_rel3);
	z_pos3_str.Format("%3.7f", z_pos3);
	tip_pos3_str.Format("%3.7f", tip_pos3);
	tilt_pos3_str.Format("%3.7f", tilt_pos3);
	cap3_str.Format("%3.7f", cap3);


	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_X, x_pos_rel3_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_Y, y_pos_rel3_str);


	vec1 = vec(x_pos_rel1, y_pos_rel1, cap1, x_pos_rel2, y_pos_rel2, cap2);
	vec2 = vec(x_pos_rel1, y_pos_rel1, cap1, x_pos_rel3, y_pos_rel3, cap3);

	nvector1 = ((vec1.b * vec2.c) - (vec1.c * vec2.b));
	nvector2 = ((vec1.c * vec2.a) - (vec1.a * vec2.c));
	nvector3 = ((vec1.a * vec2.b) - (vec1.b * vec2.a));

	roll = atan((nvector1 / nvector3));
	pitch = asin(-nvector2);

	roll_str.Format("%3.7f", roll);
	pitch_str.Format("%3.7f", pitch);
	
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1, roll_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2, pitch_str);
	
	//KillTimer(REFERENCE_POS_CHECK_TIMER);
	return 0;
}

int CSystemTestDlg::PI_Stage_Check_Manual(int point)
{
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(true);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(true);

	double move_x_pos, move_y_pos;	// 실제 이동 위치 변수
	double x_pos_abs, y_pos_abs;	//Absolute x, y
	double x_pos_rel, y_pos_rel;	// relative x, y
	double x_pos_mask, y_pos_mask;	// Mask x, y
	double x_pos_mask_center, y_pos_mask_center; // Mask Center x,y

	double z_pos, tip_pos, tilt_pos, cap;

	vectors vec1, vec2;
	
	CString n1_str, n2_str, n3_str;
	CString move_x_pos_str, move_y_pos_str;	// 실제 이동 위치 변수
	CString x_pos_ref_str, y_pos_ref_str;	//Reference x, y 
	CString x_pos_abs_str, y_pos_abs_str;	//Absolute x, y
	CString x_pos_rel_str, y_pos_rel_str;	// relative x, y
	CString x_pos_mask_str, y_pos_mask_str;	// Mask x, y
	CString x_pos_mask_center_str, y_pos_mask_center_str;	// Mask Center x, y
	CString z_pos_str, tip_pos_str, tilt_pos_str, cap_str;


	z_pos = 0.0; 
	tip_pos = 0.0; 
	tilt_pos = 0.0; 
	cap = 0.0;

	/////////////////////////
	//기준 좌표
	/////////////////////////
	x_pos_ref = 0.0; 
	y_pos_ref = 0.0;

	//////////////////////////////////////
	//SREM 좌표계 기준으로의 이동좌표
	//////////////////////////////////////
	x_pos_abs = 0.0;
	y_pos_abs = 0.0;
	
	//////////////////////////////////////
	//기준 좌표계 기준으로의 이동 좌표.
	//////////////////////////////////////
	x_pos_rel = 0.0;
	y_pos_rel = 0.0;

	//////////////////////////////////////
	//mask 좌표.
	//////////////////////////////////////
	x_pos_mask = 0.0;
	y_pos_mask = 0.0;

	x_pos_ref_str.Empty();
	y_pos_ref_str.Empty();
	x_pos_abs_str.Empty();
	y_pos_abs_str.Empty();
	x_pos_rel_str.Empty();
	y_pos_rel_str.Empty();
	x_pos_mask_str.Empty();
	y_pos_mask_str.Empty();
	x_pos_mask_center_str.Empty();
	y_pos_mask_center_str.Empty();
	z_pos_str.Empty();
	tip_pos_str.Empty();
	tilt_pos_str.Empty();
	cap_str.Empty();
	n1_str.Empty();
	n2_str.Empty();
	n3_str.Empty();


	CAutoMessageDlg MsgBoxAuto(this);

	/////////////////////////////////////////////////////
	// PI Stage Z 축 원점
	/////////////////////////////////////////////////////

	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//MsgBoxAuto.DoModal(_T(" PI Stage Z 축 원점 진행 중 ! "), 5);


	//SetTimer(REFERENCE_POS_CHECK_TIMER, 100, NULL);

	switch (point)
	{
	case 1:
		point_1 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[1]);
		////////////////////////////////////////////////////
		//// point 1 로 이동 하는 함수
		/////////////////////////////////////////////////////

		/////////////////////////////////////////////////////
		//레시피 X, Y READ (um)
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[point - 1];
		move_y_pos = pi_flat_rcp_y[point - 1];

		/////////////////////////////////////////////////////
		//Stage Mask Center 기준(0,0)으로 이동.
		/////////////////////////////////////////////////////

		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 1] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 1] PI Stage Moving ! "), 5);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		////	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		////	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}

		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);
		
		cap_str.Format("%3.7f", cap);

		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		x1um = x_pos_mask_center*1000;
		y1um = y_pos_mask_center*1000;
		cap1um = cap;


		////////////////////////////////////////////////////////
		// Viewer
		////////////////////////////////////////////////////////
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_Y, y_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_CAP2, cap_str);


		/////////////////////////////////////////////////////////////////
		// Point 1 에서 스테이지 이동 및 x,y,cap 값 리드 완료 플래그 ON
		/////////////////////////////////////////////////////////////////
		((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
		point_1 = true;
		
		if (point_1 && point_2 && point_3)
		{
			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
			//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);
		}
		else
		{
			//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);
		}
		break;
	case 2:

		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);

		////////////////////////////////////////////////////
		//// point 2 로 이동 하는 함수
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[point - 1];
		move_y_pos = pi_flat_rcp_y[point - 1];

		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 2] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 2] PI Stage Moving ! "), 10);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		//	//	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}

		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);
		cap_str.Format("%3.7f", cap);
		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		x2um = x_pos_mask_center*1000;
		y2um = y_pos_mask_center*1000;
		cap2um = cap;



		////////////////////////////////////////////////////////
		// Viewer
		////////////////////////////////////////////////////////
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_Y, y_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_CAP2, cap_str);


		/////////////////////////////////////////////////////////////////
		// Point 2 에서 스테이지 이동 및 x,y,cap 값 리드 완료 플래그 ON
		/////////////////////////////////////////////////////////////////
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
		point_2 = true;

		if (point_1 && point_2 && point_3)
		{
			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
		//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);

		}
		else
		{
		//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);

		}
		break;
	case 3:
		point_3 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[1]);
		////////////////////////////////////////////////////
		//// point 3 로 이동 하는 함수
		/////////////////////////////////////////////////////
		move_x_pos = pi_flat_rcp_x[point - 1];
		move_y_pos = pi_flat_rcp_y[point - 1];
	
		g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		MsgBoxAuto.DoModal(_T(" [Point 3] Stage Moving ! "), 7);

		//if ((int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) >= (Z_INITIAL_POS_UM - 2.0)) && (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) <= (Z_INITIAL_POS_UM + 2.0)))
		//{
		//	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(move_x_pos, move_y_pos);
		//	g_pNavigationStage->MoveToMaskCenterCoordinate(move_x_pos, move_y_pos);
		//	MsgBoxAuto.DoModal(_T(" [Point 3] PI Stage Moving ! "), 5);
		//}
		//else
		//{
		//	m_PIstageManualThreadExitFlag = TRUE;
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
		//	//	GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);
		//	//	KillTimer(REFERENCE_POS_CHECK_TIMER);
		//	::AfxMessageBox(" Z AXIS 원점이 아님.!");
		//	return -1;
		//}
		//
		////////////////////////////////////////////////////////
		//Navigation Stage 기준
		////////////////////////////////////////////////////////
		x_pos_abs = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		y_pos_abs = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		////////////////////////////////////////////////////////
		// Mask (LB0,0)기준
		////////////////////////////////////////////////////////
		x_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - x_pos_abs;
		y_pos_mask = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - y_pos_abs;

		////////////////////////////////////////////////////////
		// Mask Center 0,0 기준
		////////////////////////////////////////////////////////
		x_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].x - MaskSize_mm / 2 - x_pos_abs;
		y_pos_mask_center = g_pConfig->m_stStagePos[SYSTEM_OM_MASKLEFTDOWN_POSITION].y - MaskSize_mm / 2 - y_pos_abs;

		////////////////////////////////////////////////////////
		// Adam Cap Read Command
		////////////////////////////////////////////////////////
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) //return 0면 정상.. 아니면 error사항.. 
		{
			::AfxMessageBox(" Adam Command_CapReadTimeOut() Error 발생!");
			return -1;
		}
		cap = g_pAdam->AdamData.m_dCapsensor2;


		x_pos_mask_center_str.Format("%3.7f", x_pos_mask_center);
		y_pos_mask_center_str.Format("%3.7f", y_pos_mask_center);
		cap_str.Format("%3.7f", cap);
		////////////////////////////////////////////////////////
		// 실질적 법선벡터 사용 변수에 대입
		////////////////////////////////////////////////////////
		x3um = x_pos_mask_center*1000;
		y3um = y_pos_mask_center*1000;
		cap3um = cap;

		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_X, x_pos_mask_center_str);
		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_Y, y_pos_mask_center_str);

		SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_CAP2, cap_str);

		((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);

		point_3 = true;
		if (point_1 && point_2 && point_3)
		{

			((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[0]);
			((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[0]);
			GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(true);

		}
		else
		{
			GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_CAL)->EnableWindow(false);

		}
		break;
	case 4:
		break;
	default:
		break;
	}

	
	m_PIstageManualThreadExitFlag = TRUE;

	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_1_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_2_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_3_MANUAL)->EnableWindow(false);
	//GetDlgItem(IDC_CHECK_PI_STAGE_POINT_AUTO)->EnableWindow(false);

	//KillTimer(REFERENCE_POS_CHECK_TIMER);
	return 0;
}

UINT CSystemTestDlg::PI_Stage_Check_Manual_Thread(LPVOID pParam)
{
	CSystemTestDlg*  PIStageCheckManual = (CSystemTestDlg*)pParam;

	PIStageCheckManual->m_PIstageManualThreadExitFlag = FALSE;


	while (!PIStageCheckManual->m_PIstageManualThreadExitFlag)
	{
		if (PIStageCheckManual->PI_Stage_Check_Manual(PIStageCheckManual->check_point) != RUN)
		{
			return 0;
		}
		Sleep(50);
	}
	PIStageCheckManual->m_pPIstageManualThread = NULL;
	PIStageCheckManual->m_PIstageManualThreadExitFlag = TRUE;
	return 0;

}

void CSystemTestDlg::OnBnClickedCheckStageMovePos()
{
	ReSetting_Pos_Recipy();
}

void CSystemTestDlg::ReSetting_Pos_Recipy()
{
	CString strAppName, strString;
	strAppName.Empty();
	strString.Empty();
	

	CString x_pos_ref_str, y_pos_ref_str;
	x_pos_ref_str.Empty();
	y_pos_ref_str.Empty();

	CString strX, strY;
	strX.Empty();
	strY.Empty();

	double x_pos = 0.0, y_pos = 0.0;


	int row = 0;
	row = m_StagePosGrid.GetSelectedRow();

	if (row > m_Point_Num)
	{
		AfxMessageBox(_T("범위가 잘못 되었습니다"),MB_ICONERROR);
		return;
	}
	m_ref_pos_x.GetWindowTextA(x_pos_ref_str);
	m_ref_pos_y.GetWindowTextA(y_pos_ref_str);

	x_pos = atof(x_pos_ref_str);
	y_pos = atof(y_pos_ref_str);

	g_pConfig->m_stStagePos[row - 1].x = x_pos;
	g_pConfig->m_stStagePos[row - 1].y = y_pos;

	strX.Format(_T("%6.6f"), g_pConfig->m_stStagePos[row - 1].x);
	m_StagePosGrid.SetItemText(row, 1, strX);
	strY.Format(_T("%6.6f"), g_pConfig->m_stStagePos[row - 1].y);
	m_StagePosGrid.SetItemText(row, 2, strY);

	m_StagePosGrid.Invalidate(FALSE);

	strAppName.Format("POINT_%02d", row);
	WritePrivateProfileString(strAppName, "X(um)", x_pos_ref_str, PISTAGE_TEST_POSITION_FILE_FULLPATH);
	WritePrivateProfileString(strAppName, "Y(um)", y_pos_ref_str, PISTAGE_TEST_POSITION_FILE_FULLPATH);

}

void CSystemTestDlg::OnBnClickedCheckPiStagePoint1Manual()
{

	if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		return;
	}
	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pAdam == NULL || g_pConfig == NULL)
	{
		AfxMessageBox(_T("Stage 구동 Error"));
		return;
	}
	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	check_point = 1;
	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);

}

void CSystemTestDlg::OnBnClickedCheckPiStagePoint2Manual()
{
	if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
		return;
	}

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		point_2 = false;
		((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
		return;
	}
	
	check_point = 2;
	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

void CSystemTestDlg::OnBnClickedCheckPiStagePoint3Manual()
{
	if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		return;
	}

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}
	check_point = 3;

	m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);
}

void CSystemTestDlg::OnBnClickedCheckPiStagePointAuto()
{
	point_1 = false;
	point_2 = false;
	point_3 = false;

	((CStatic*)GetDlgItem(IDC_ICON_POINT_1))->SetIcon(m_LedIcon[1]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_2))->SetIcon(m_LedIcon[1]);
	((CStatic*)GetDlgItem(IDC_ICON_POINT_3))->SetIcon(m_LedIcon[1]);

	if (m_bTestStop == FALSE || m_Mask_Measrue_Start == FALSE || m_PI_Stage_Measrue_Start == FALSE)
	{
		::AfxMessageBox(_T("이미 테스트가 진행중입니다. 테스트 완료 후 실행해 주세요."));
		return;
	}

	if (!g_pNavigationStage->Is_NAVI_Stage_Connected())
	{
		AfxMessageBox(_T("Navigation Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	if (g_pAP->Check_STAGE_MovePossible() != XY_NAVISTAGE_OK)
	{
		AfxMessageBox(_T("Navigation Stage Moving Impossible!"), MB_ICONERROR);
		return;
	}

	if (!g_pScanStage->Is_SCAN_Stage_Connected())
	{
		AfxMessageBox(_T("Scan Stage 가 연결되지 않았습니다"), MB_ICONERROR);
		return;
	}

	//check_point = 4;
	//m_pPIstageManualThread = ::AfxBeginThread(PI_Stage_Check_Manual_Thread, this, THREAD_PRIORITY_NORMAL, 0, 0);

	// KJH 순차 진행 테스트 필요.
	//OnBnClickedCheckPiStagePoint1Manual();
	//OnBnClickedCheckPiStagePoint2Manual();
	//OnBnClickedCheckPiStagePoint3Manual();

	if (point_1 && point_2 && point_3)
	{
		OnBnClickedCheckPiStagePoint1Cal();
	}
}

void CSystemTestDlg::InitPosGrid()
{
	CRect rect;
	GetDlgItem(IDC_TEXT_STAGEPOSGRID_PI_STAGE_CHECK)->GetWindowRect(rect);
	ScreenToClient(&rect);
	m_StagePosGrid.Create(rect, this, IDC_TEXT_STAGEPOSGRID_PI_STAGE_CHECK);

	int nGap, col, row;
	col = 0;
	row = 0;
	nGap = 8;

	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(3);
	m_StagePosGrid.SetRowCount(m_Point_Num+1);
	//m_StagePosGrid.SetBkColor(RGB(192, 192, 192));
	m_StagePosGrid.GetDefaultCell(FALSE, FALSE)->SetBackClr(WHITE);

	LOGFONT lf;
	CFont Font;
	m_StagePosGrid.GetFont()->GetLogFont(&lf);
	lf.lfWeight = FW_BOLD;
	//lf.lfHeight=10;
	//strcpy(lf.lfFaceName,"Verdana");
	Font.CreateFontIndirect(&lf);
	m_StagePosGrid.SetFont(&Font);
	Font.DeleteObject();

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(um)");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(um)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);
	//str.Format("%s", "Y(mm)");
	//m_StagePosGrid.SetItemText(0, 3, (LPSTR)(LPCTSTR)str);

	m_StagePosGrid.SetRowHeight(0, 35);
	m_StagePosGrid.SetColumnWidth(0, 30);
	m_StagePosGrid.SetColumnWidth(1, 65);
	m_StagePosGrid.SetColumnWidth(2, 65);
	//m_StagePosGrid.SetColumnWidth(3, 200);
	m_StagePosGrid.ExpandColumnsToFit();
	//
	CString strNo, strX, strY;
	for (row = 1; row < m_Point_Num + 1; row++)
	{
		if (row == 4)
		{
			m_StagePosGrid.SetItemText(row, 0, _T("Ref"));
		}
		else
		{
			strNo.Format("%d", row);
			m_StagePosGrid.SetItemText(row, 0, strNo);
		}
		//m_StagePosGrid.SetItemText(row, 1, g_pConfig->m_stStagePos[row - 1].chStagePositionString);
		pi_flat_rcp_x[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].x;
		pi_flat_rcp_y[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].y;
		
		strX.Format("%4.6f", g_pConfig->m_stStageumPos_PI[row - 1].x);
		m_StagePosGrid.SetItemText(row, 1, strX);
		strY.Format("%4.6f", g_pConfig->m_stStageumPos_PI[row - 1].y);
		m_StagePosGrid.SetItemText(row, 2, strY);
	}
	//m_StagePosGrid.SetSelectedRow(1);
}

void CSystemTestDlg::OnBnClickedCheckPiStagePoint1Cal()
{
	vectors vec1, vec2;
	roll = 0.0;
	pitch = 0.0;

	CString roll_str, pitch_str;
	CString roll_str_revision, pitch_str_revisiton;

	roll_str_revision.Empty();
	pitch_str_revisiton.Empty();
	roll_str.Empty();
	pitch_str.Empty();
	
	CString user_value;
	user_value.Empty();

	CString n1_str, n2_str, n3_str;
	n1_str.Empty();
	n2_str.Empty();
	n3_str.Empty();

	double user_point1_cap = 0.0;
	double user_point2_cap = 0.0;
	double user_point3_cap = 0.0;


	Calculate_Vector();
	
	n1_str.Format("%3.9f", n1_vec);
	n2_str.Format("%3.9f", n2_vec);
	n3_str.Format("%3.9f", n3_vec);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_1_NVECTOR, n1_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_2_NVECTOR, n2_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_POINT_3_NVECTOR, n3_str);

	roll_str.Format("%3.9f", roll_urad);
	pitch_str.Format("%3.9f", pitch_urad);


	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_1, roll_str);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_2, pitch_str);

	roll_revision_urad = -(roll_urad);
	pitch_revision_urad = (pitch_urad);

	roll_str_revision.Format("%3.9f", roll_revision_urad);
	pitch_str_revisiton.Format("%3.9f", pitch_revision_urad);

	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TY, roll_str_revision);
	SetDlgItemTextA(IDC_EDIT_PI_STAGE_RESULT_TX, pitch_str_revisiton);

}

void CSystemTestDlg::Calculate_Vector()
{
	vectors vec1, vec2;

	CString user_value;
	user_value.Empty();


	double user_point1_cap = 0.0;
	double user_point2_cap = 0.0;
	double user_point3_cap = 0.0;

	GetDlgItem(IDC_EDIT_PI_STAGE_POINT_1_CAP2)->GetWindowTextA(user_value);
	cap1um = atof(user_value);

	GetDlgItem(IDC_EDIT_PI_STAGE_POINT_2_CAP2)->GetWindowTextA(user_value);
	cap2um = atof(user_value);

	GetDlgItem(IDC_EDIT_PI_STAGE_POINT_3_CAP2)->GetWindowTextA(user_value);
	cap3um = atof(user_value);

	//TEST
	//vec1 = vec(0, 0, 0, 10, 10, 40);
	//vec2 = vec(0, 0, 0, 5, 5, 40);

	//////////////////////////////////////////////////////////////////
	// 법선 벡터 계산
	//////////////////////////////////////////////////////////////////


	vec1 = vec(x1um, y1um, cap1um, x2um, y2um, cap2um);
	vec2 = vec(x1um, y1um, cap1um, x3um, y3um, cap3um);
	
	n1_vec = vec1.b * vec2.c - vec1.c * vec2.b;
	n2_vec = vec1.c * vec2.a - vec1.a * vec2.c;
	n3_vec = vec1.a * vec2.b - vec1.b * vec2.a;

	double lenth = sqrt( n1_vec * n1_vec + n2_vec * n2_vec + n3_vec * n3_vec);

	n1_vec = n1_vec / lenth;
	n2_vec = n2_vec / lenth;
	n3_vec = n3_vec / lenth;

	//n1_str.Format("%3.7f", n1);
	//vec1 = vec(x1, y1, z1, x2, y2, z2);
	//vec2 = vec(x1, y1, z1, x3, y3, z3);
	//n2_str.Format("%3.7f", n2);
	//vec1 = vec(x1, y1, z1, x2, y2, z2);
	//vec2 = vec(x1, y1, z1, x3, y3, z3);
	//n3_str.Format("%3.7f", n3);

	//roll = atan((n1_vec / n3_vec));

	roll = atan2(n1_vec, n3_vec);
	pitch = asin(-n2_vec);

	roll_urad = roll * 1000000;
	pitch_urad = pitch * 1000000;

}

void CSystemTestDlg::ReadPosGrid()
{
	m_StagePosGrid.SetEditable(false);
	m_StagePosGrid.SetListMode(true);
	m_StagePosGrid.SetSingleRowSelection(false);
	m_StagePosGrid.EnableDragAndDrop(false);
	m_StagePosGrid.SetFixedRowCount(1);
	m_StagePosGrid.SetFixedColumnCount(1);
	m_StagePosGrid.SetColumnCount(3);
	m_StagePosGrid.SetRowCount(m_Point_Num+1);

	CString str;
	str.Format("%s", "No");
	m_StagePosGrid.SetItemText(0, 0, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "X(um)");
	m_StagePosGrid.SetItemText(0, 1, (LPSTR)(LPCTSTR)str);
	str.Format("%s", "Y(um)");
	m_StagePosGrid.SetItemText(0, 2, (LPSTR)(LPCTSTR)str);

	CString strNo, strX, strY;
	for (int row = 1; row < m_Point_Num + 1; row++)
	{
		strNo.Format("%d", row);
		m_StagePosGrid.SetItemText(row, 0, strNo);
		pi_flat_rcp_x[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].x;
		pi_flat_rcp_y[row - 1] = g_pConfig->m_stStageumPos_PI[row - 1].y;

		strX.Format("%4.6f", g_pConfig->m_stStageumPos_PI[row - 1].x);
		m_StagePosGrid.SetItemText(row, 1, strX);
		strY.Format("%4.6f", g_pConfig->m_stStageumPos_PI[row - 1].y);
		m_StagePosGrid.SetItemText(row, 2, strY);
	}
}

void CSystemTestDlg::OnBnClickedCheckPiStageRead()
{
	g_pConfig->ReadPIStageTestPositionFromDBum();
	ReadPosGrid();
}

void CSystemTestDlg::OnBnClickedCheckStageMovePos2()
{
	CString point_num_str;
	point_num_str.Empty();

	int point_n = 0;

	m_point_user_num.GetWindowTextA(point_num_str);
	point_n = atof(point_num_str);

	m_Point_Num = point_n;
	point_num_str.Format("%d", m_Point_Num);
	m_point_user_num.SetWindowTextA(point_num_str);

	g_pConfig->ReadPIStageTestPositionFromDBum();
	ReadPosGrid();

}
