﻿#pragma once


// CSQOneDataBaseDlg 대화 상자

class CSQOneDataBaseDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSQOneDataBaseDlg)

public:
	CSQOneDataBaseDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSQOneDataBaseDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SQONEDB_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	virtual BOOL DestroyWindow();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	afx_msg void OnBnClickedSqonedbButtonAddposition();
	afx_msg void OnBnClickedSqonedbButtonLoadposition();
	afx_msg void OnBnClickedSqonedbButtonDeleteposition();
	BOOL m_isLoaded;

	CGridCtrl m_gridSQONEDB;
	CGridCtrl m_gridMeasureEDB;
	void initializeDBGrid();
	void initializeGridMesure();
	afx_msg void OnNMRClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMRClickMeasureGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkMeasureGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickMeasureGrid(NMHDR *pNMHDR, LRESULT *pResult);


	int loadDatatoGridfromDB(char* filePath, char* tableName, int parameterNum, CGridCtrl* DBGrid, BOOL isIndex = TRUE);
	int addDatatoGridfromDB(char* filePath, char* tableName, int parameterNum, CGridCtrl* DBGrid, BOOL isIndex = TRUE);
	int DeleteDatatoGridfromDB();
	afx_msg void OnBnClickedSqonedbButtonGetcurrentposition();
	afx_msg void OnBnClickedSqonedbButtonAddtosystemdb();

	BOOL IsSystemPosition();
	BOOL m_bIsSystemPostion;
	int MoveSystemPosition();

	int loadReverseData();
};
