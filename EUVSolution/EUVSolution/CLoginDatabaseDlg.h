﻿#pragma once


// CLoginDatabaseDlg 대화 상자

class CLoginDatabaseDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLoginDatabaseDlg)

public:
	CLoginDatabaseDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CLoginDatabaseDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_LOGINDB_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	BOOL m_isLogin;

	CString m_strLoginIDNumber;
	CString m_strLoginPassword;

	CString m_strIDNumber;
	CString m_strPassword;
	CString m_strName;
	CString m_strAuthority;

	int loginEqipment();
	void initailizeDatabase();

	afx_msg void OnBnClickedLogindbButtonSave();
	afx_msg void OnBnClickedLogindbButtonLoad();
	afx_msg void OnBnClickedLogindbButtonDelete();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL DestroyWindow();

	CGridCtrl m_gridLoginDB;
	void initializeDBGrid();
	afx_msg void OnNMRClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMDblclkLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnNMClickLogindbGrid(NMHDR *pNMHDR, LRESULT *pResult);

	int loadDatatoGridfromDB(char* filePath, char* tableName,int parameterNum,CGridCtrl* DBGrid, BOOL isIndex = TRUE);
	int addDatatoGridfromDB(char* filePath, char* tableName,int parameterNum,CGridCtrl* DBGrid, BOOL isIndex = TRUE);
	int DeleteDatatoGridfromDB();
	int UpdateDatatoGridfromDB();
	int returnLevel();
};
