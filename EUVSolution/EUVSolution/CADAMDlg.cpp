﻿#pragma once

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"
#include <iomanip>
#include "..\..\CommonModule\DllADAM\CADAMInterpolationAlgorithm.h"		
#include "..\..\CommonModule\DllADAM\CADAMInterpolationAlgorithm.cpp"
#include <string>


using namespace std;

IMPLEMENT_DYNAMIC(CADAMDlg, CDialogEx)

CADAMDlg* CADAMDlg::m_pAdamDlgStaticInst = NULL;

CADAMDlg::CADAMDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ADAM_DIALOG, pParent)
	, m_bEUVCrossLineDisplay(FALSE)
	, m_bEUVMouseMove(FALSE)
	, m_AverageCount(20000)
	, IsDisplayNormalizedImage(FALSE)
	, bIsUse4ptAlign(FALSE)
	, m_IsUseRegridRawImage(TRUE)
	, m_IsCrossStep(TRUE)
	, m_Sq1GetString(_T("Get"))
	, m_Sq1SetString(_T("Set"))
{
	m_pAdamDlgStaticInst = this;

	//m_strRawImageFileName = "";
	m_strRawImageBMPFileName = "";
	m_strRegirdImageFileName = "";
	m_strAveragedImageFileName = "";
	//m_strFilteredImageFileName = "";

	//m_pNormalized_Image_buffer = new BYTE[ADAM_MAX_DATA];					//단순 화면 Display를 위해서 만들어 놓은 메모리
	//memset(m_pNormalized_Image_buffer, 0, sizeof(byte) * ADAM_MAX_DATA);	//CADAMCtrl에서는 그냥 메모리만 잡았는데, 요거는 0으로 초기화 함

	//m_pNormalized_RegridImage_buffer = new BYTE[ADAM_MAX_WIDTH_REGRID*ADAM_MAX_HEIGHT_REGRID];					// 2 um 를 1 nm 로 interpolatin 했다고 가정하고 임의의 상수로 잡음
	//memset(m_pNormalized_RegridImage_buffer, 0, sizeof(byte) * ADAM_MAX_WIDTH_REGRID*ADAM_MAX_HEIGHT_REGRID);


	m_8URawImage_bufferForDisp = new UINT8[ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT];
	memset(m_8URawImage_bufferForDisp, 0, sizeof(UINT8) * ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT);

	m_8URegridImage_bufferForDisp = new UINT8[ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID];
	memset(m_8URegridImage_bufferForDisp, 0, sizeof(UINT8) * ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID);

	m_nDisplayImageWidth = 0;
	m_nDisplayImageHeight = 0;

	m_nUpperCutPixelNum = 0;
	m_nLeftCutPixelNum = 0;

	// FOV 사이즈 초기화	
	m_nEuvImage_Fov = 3000;  // nm
	//m_nEuvImage_Old_Fov = m_nEuvImage_Fov;	//nm
	m_nEuvImage_ScanGrid = 10; // nm
	m_nEuvImage_ScanNumber = 1; // single scan

	// Interpolation 알고리즘 초기화
	m_nEUVImage_InterpolationGrid = 2; // nm scale
	m_nEUVImage_Detectorselection = 1; // Detector center

	CornerThreshold = 0.02;		//corner threshold initial value

	m_bEUVMouseMoveLineDisplay = FALSE;
	m_bEUVEdgeFindSuccess = FALSE;
	m_nEdgeFindResultX = 0;
	m_nEdgeFindResultY = 0;
}

CADAMDlg::~CADAMDlg()
{
	//if (m_pNormalized_Image_buffer != NULL)
	//	delete[] m_pNormalized_Image_buffer;

	//if (m_pNormalized_RegridImage_buffer != NULL)
	//	delete[]  m_pNormalized_RegridImage_buffer;

	if (m_8URawImage_bufferForDisp != NULL)
		delete[]  m_8URawImage_bufferForDisp;

	if (m_8URegridImage_bufferForDisp != NULL)
		delete[] m_8URegridImage_bufferForDisp;
}

void CADAMDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO_FOV_SELECT, m_comboFovSelect);
	DDX_Control(pDX, IDC_COMBO_SCANGRID_SELECT, m_comboScanGridSelect);
	DDX_Control(pDX, IDC_COMBO_SCAN_NUM, m_comboScanNumber);
	//DDX_Control(pDX, IDC_STATIC_TCPSOCKET, m_chTcpipViewer);
	DDX_Control(pDX, IDC_COMBO_INTERPOLATION_GRID, m_comboInterpolationFridSelect);
	DDX_Control(pDX, IDC_COMBO_DETOCTOR_SELECTION, m_comboDetectorSelection);
	DDX_Check(pDX, IDC_CHECK_EUV_CROSSLINE, m_bEUVCrossLineDisplay);
	DDX_Check(pDX, IDC_CHECK_EUV_MOUSEMOVE, m_bEUVMouseMove);
	DDX_Control(pDX, IDC_EDIT_CORNER_THRESHOLD, m_EditCornerThreshold);
	DDX_Control(pDX, IDC_PROGRESS_EUVIMAGING, m_ProgressEUVImagingCtrl);
	//DDX_Text(pDX, IDC_EDIT_ADAM_CAP1, m_dCurrentCapsensorValue1);
	//DDX_Text(pDX, IDC_EDIT_ADAM_CAP2, m_dCurrentCapsensorValue2);
	//DDX_Text(pDX, IDC_EDIT_ADAM_CAP3, m_dCurrentCapsensorValue3);
	//DDX_Text(pDX, IDC_EDIT_ADAM_CAP4, m_dCurrentCapsensorValue4);
	DDX_Control(pDX, IDC_EDIT_ADAM_CAP1, m_editCtrlCap1);
	DDX_Control(pDX, IDC_EDIT_ADAM_CAP2, m_editCtrlCap2);
	DDX_Control(pDX, IDC_EDIT_ADAM_CAP3, m_editCtrlCap3);
	DDX_Control(pDX, IDC_EDIT_ADAM_CAP4, m_editCtrlCap4);
	DDX_Control(pDX, IDC_CHECK_CAPSENSOR_AUTO_READ, m_ctrlCapSensorAutoRead);
	DDX_Text(pDX, IDC_EDIT_ADAM_AVERAGE_COUNT, m_AverageCount);
	DDX_Control(pDX, IDC_EDIT_ADAM_LIF_X, m_editCtrlLifX);
	DDX_Control(pDX, IDC_EDIT_ADAM_LIF_Y, m_editCtrlLifY);
	DDX_Check(pDX, IDC_CHECK_ADAM_SAVE_EVERY_RAW_IMAGE, m_bSaveEveryScanRawImage);
	DDX_Check(pDX, IDC_CHECK_DISP_NORMALIZED, IsDisplayNormalizedImage);
	DDX_Check(pDX, IDC_CHECK_ADAM_SAVE_I0_NORMALIZED_IMAGE, bSaveNormalizedImage);
	DDX_Check(pDX, IDC_CHECK_ADAM_USE_4PT_ALIGN, bIsUse4ptAlign);
	DDX_Control(pDX, IDC_EDIT_ADAM_CURRENT_SCAN_NUM, m_editCtrlCurrentScanNum);
	DDX_Control(pDX, IDC_EDIT_ADAM_MEASURE_STATUS, m_ctrlMeasureStatus);
	DDX_Control(pDX, IDC_EDIT_ADAM_THROUGH_FOCUS, m_ctrlThroughFocusStatus);
	DDX_Check(pDX, IDC_CHECK_ADAM_REGRID_RAW_IMAGE, m_IsUseRegridRawImage);

	DDX_Text(pDX, IDC_EDIT_ADAM_I0_REFERENCE, m_I0Reference);
	DDX_Text(pDX, IDC_EDIT_ADAM_LOW_PASS_FILTER_SIGMA, m_LowPassFilterSigma);

	DDX_Text(pDX, IDC_EDIT_I0_FILTER_WINDOW_SIZE, m_I0FilterWindowSize);

	DDX_Check(pDX, IDC_CHECK_ADAM_HIGH_FOV_INTERPOLATION, m_bIsUseHighFovInterpolation);
	DDX_Control(pDX, IDC_CHECK_ADAM_HIGH_FOV_INTERPOLATION, m_checkIsUseHighFovInterpolation);
	DDX_Check(pDX, IDC_CHECK_ADAM_THROUGH_FOCUS_CROSS_STEP, m_IsCrossStep);
	DDX_Check(pDX, IDC_CHECK_3UM_FOV, m_IsUse3umFOV);
	DDX_Check(pDX, IDC_CHECK_RESET_START_POS, m_IsResetStartPos);
	DDX_Text(pDX, IDC_EDIT2, m_Sq1GetString);
	DDX_Text(pDX, IDC_EDIT3, m_Sq1SetString);
}

BEGIN_MESSAGE_MAP(CADAMDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_ADAM_RUN_BUTTON, &CADAMDlg::OnBnClickedAdamRunButton)
	ON_BN_CLICKED(IDC_ADAM_STOP_BUTTON, &CADAMDlg::OnBnClickedAdamStopButton)
	ON_BN_CLICKED(IDC_IMAGE_LOAD_BUTTON, &CADAMDlg::OnBnClickedImageLoadButton)
	ON_WM_MOUSEMOVE()

	ON_BN_CLICKED(IDC_IMAGE_SAVE_BUTTON, &CADAMDlg::OnBnClickedImageSaveButton)
	ON_CBN_CLOSEUP(IDC_COMBO_FOV_SELECT, &CADAMDlg::OnCbnCloseupComboFovSelect)
	ON_CBN_CLOSEUP(IDC_COMBO_SCANGRID_SELECT, &CADAMDlg::OnCbnCloseupComboScangridSelect)
	ON_BN_CLICKED(IDC_GRAPH_ON_BUTTON, &CADAMDlg::OnBnClickedGraphOnButton)
	ON_CBN_CLOSEUP(IDC_COMBO_SCAN_NUM, &CADAMDlg::OnCbnCloseupComboScanNum)
	ON_BN_CLICKED(IDC_ADAM_LIFRESET_BUTTON, &CADAMDlg::OnBnClickedAdamLifresetButton)
	//ON_BN_CLICKED(IDC_INTERPOLATION_TEST_BUTTON, &CADAMDlg::OnBnClickedInterpolationTestButton)
	ON_CBN_CLOSEUP(IDC_COMBO_INTERPOLATION_GRID, &CADAMDlg::OnCbnCloseupComboInterpolationGrid)
	ON_CBN_CLOSEUP(IDC_COMBO_DETOCTOR_SELECTION, &CADAMDlg::OnCbnCloseupComboDetoctorSelection)
	ON_BN_CLICKED(IDC_PY_INTERPOLATION_BUTTON, &CADAMDlg::OnBnClickedCornerFindButton)
	ON_BN_CLICKED(IDC_CHECK_EUV_CROSSLINE, &CADAMDlg::OnBnClickedCheckEuvCrossline)
	ON_BN_CLICKED(IDC_CHECK_EUV_MOUSEMOVE, &CADAMDlg::OnBnClickedCheckEuvMousemove)
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_AVERAGE_DATA_SAVE_BUTTON, &CADAMDlg::OnBnClickedAverageDataSaveButton)
	//ON_EN_CHANGE(IDC_EDIT_CORNER_THRESHOLD, &CADAMDlg::OnEnChangeEditCornerThreshold)
	ON_BN_CLICKED(IDC_ADAM_ORESET_BUTTON, &CADAMDlg::OnBnClickedAdamOresetButton)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_TEST, &CADAMDlg::OnBnClickedButtonAdamTest)
	ON_BN_CLICKED(IDC_ADAM_CAP_READ_BUTTON, &CADAMDlg::OnBnClickedAdamCapReadButton)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_PORT_REOPEN, &CADAMDlg::OnBnClickedButtonAdamPortReopen)
	ON_BN_CLICKED(IDC_BUTTON3, &CADAMDlg::OnBnClickedButton3)
	ON_CBN_SELCHANGE(IDC_COMBO_SCANGRID_SELECT, &CADAMDlg::OnCbnSelchangeComboScangridSelect)
	ON_BN_CLICKED(IDC_RAW_IMAGE_SAVE_BUTTON, &CADAMDlg::OnBnClickedRawImageSaveButton)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_PORT_CLOSE, &CADAMDlg::OnBnClickedButtonAdamPortClose)
	ON_BN_CLICKED(IDC_CHECK_CAPSENSOR_AUTO_READ, &CADAMDlg::OnBnClickedCheckCapsensorAutoRead)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_AVERAGE_COUNT, &CADAMDlg::OnBnClickedButtonAdamAverageCount)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_FIT, &CADAMDlg::OnBnClickedButtonAdamFit)
	ON_BN_CLICKED(IDC_ADAM_SCAN, &CADAMDlg::OnBnClickedAdamScan)
	ON_BN_CLICKED(IDC_FILTERED_DATA_SAVE_BUTTON, &CADAMDlg::OnBnClickedFilteredDataSaveButton)
	ON_BN_CLICKED(IDC_CHECK_ADAM_SAVE_EVERY_RAW_IMAGE, &CADAMDlg::OnBnClickedCheckAdamSaveEveryRawImage)
	ON_BN_CLICKED(IDC_CHECK_DISP_NORMALIZED, &CADAMDlg::OnBnClickedCheckDispNormalized)
	ON_BN_CLICKED(IDC_CHECK_ADAM_SAVE_I0_NORMALIZED_IMAGE, &CADAMDlg::OnBnClickedCheckAdamSaveI0NormalizedImage)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_INIT_EUV_ALIGN, &CADAMDlg::OnBnClickedButtonAdamInitEuvAlign)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_APPLY_ALIGN, &CADAMDlg::OnBnClickedButtonAdamApplyAlign)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_ALGIN_LT, &CADAMDlg::OnBnClickedButtonAdamAlginLt)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_ALGIN_RT, &CADAMDlg::OnBnClickedButtonAdamAlginRt)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_ALGIN_LB, &CADAMDlg::OnBnClickedButtonAdamAlginLb)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_ALGIN_RB, &CADAMDlg::OnBnClickedButtonAdamAlginRb)
	ON_BN_CLICKED(IDC_CHECK_ADAM_USE_4PT_ALIGN, &CADAMDlg::OnBnClickedCheckAdamUse4ptAlign)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_MOVE_Z_FOCUS_POS, &CADAMDlg::OnBnClickedButtonAdamMoveZFocusPos)
	ON_BN_CLICKED(IDC_BUTTON5, &CADAMDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_MOVE_Z_FOCUS_POS_AND_SET_INTERLOCK, &CADAMDlg::OnBnClickedButtonAdamMoveZFocusPosAndSetInterlock)
	ON_BN_CLICKED(IDC_BUTTON7, &CADAMDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON_BEAM_OPEN, &CADAMDlg::OnBnClickedButtonBeamOpen)
	ON_BN_CLICKED(IDC_BUTTON_BEAM_CLOSE, &CADAMDlg::OnBnClickedButtonBeamClose)
	ON_BN_CLICKED(IDC_BUTTON_BEAM_REOPEN, &CADAMDlg::OnBnClickedButtonBeamReopen)
	//ON_STN_CLICKED(IDC_EUV_DISPLAY, &CADAMDlg::OnStnClickedEuvDisplay)
	ON_BN_CLICKED(IDC_CHECK_ADAM_REGRID_RAW_IMAGE, &CADAMDlg::OnBnClickedCheckAdamRegridRawImage)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_I0_APPLY, &CADAMDlg::OnBnClickedButtonAdamI0Apply)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_I0_SAVE, &CADAMDlg::OnBnClickedButtonAdamI0Save)
	ON_BN_CLICKED(IDC_BUTTON_ADAM_I0_RESTORE, &CADAMDlg::OnBnClickedButtonAdamI0Restore)
	ON_BN_CLICKED(IDC_CHECK_ADAM_HIGH_FOV_INTERPOLATION, &CADAMDlg::OnBnClickedCheckAdamHighFovInterpolation)
	ON_BN_CLICKED(IDC_CHECK_ADAM_THROUGH_FOCUS_CROSS_STEP, &CADAMDlg::OnClickedCheckAdamThroughFocusCrossStep)
	ON_BN_CLICKED(IDC_BUTTON4, &CADAMDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON6, &CADAMDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON8, &CADAMDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON11, &CADAMDlg::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON10, &CADAMDlg::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON9, &CADAMDlg::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON1, &CADAMDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_CHECK_3UM_FOV, &CADAMDlg::OnBnClickedCheck3umFov)
	ON_BN_CLICKED(IDC_BUTTON12, &CADAMDlg::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_CHECK_RESET_START_POS, &CADAMDlg::OnBnClickedCheckResetStartPos)
	ON_BN_CLICKED(IDC_BUTTON13, &CADAMDlg::OnBnClickedButton13)
END_MESSAGE_MAP()

// CADAMDlg 메시지 처리기

void CADAMDlg::OnDestroy()		//ADAM DLG는 따로 종료 버튼이 없는데 어디서 OnDestroy()를 호출하는 거지? 메모리느 해제 되는거 같음.. KYD
{

	m_bRawImageDisplayThreadExitFlag = TRUE;

	if (m_RawImageDisplayThread != NULL)
	{
		HANDLE threadHandleRawDisp = m_RawImageDisplayThread->m_hThread;
		DWORD dwResult;
		dwResult = ::WaitForSingleObject(threadHandleRawDisp, 1000); /*INFINITE*/
		if (dwResult == WAIT_TIMEOUT)
		{
			DWORD dwExitCode = STILL_ACTIVE;
			::GetExitCodeThread(threadHandleRawDisp, &dwExitCode); // 요기서 무한루프 => dwCode가 계속 STILL_ACTIVE가 됩니다.
			if (dwExitCode == STILL_ACTIVE)	//259
			{
				TerminateThread(threadHandleRawDisp, 0/*dwExitCode*/);
				CloseHandle(threadHandleRawDisp);
			}
		}
		m_RawImageDisplayThread = NULL;
	}


	//m_bCapAutoRead = FALSE; 
	CloseTcpIpSocket(); // 종료시 에러 디버그 (Dialog가 Detroy되었는데 virtual 함수로 UI 업데이트 시도해서 발생, 종료시 CloseTcpIpSocket()을 수행해서 receive 안되도록 막음)
	//Cap ReadThread 종료
	if (m_pCapReadThread != NULL)
	{
		HANDLE threadHandle = m_pCapReadThread->m_hThread;
		m_bCapReadThreadExitFlag = TRUE;
		DWORD dwResult;

		dwResult = WaitForSingleObject(threadHandle, 10000);

		if (dwResult == WAIT_OBJECT_0)
		{
			//정상 종료됨
			int test = 0;
		}
		else
		{
			if (dwResult == WAIT_TIMEOUT)
			{
				//Timeout
				DWORD dwExitCode = NULL;
				::GetExitCodeThread(threadHandle, &dwExitCode);
				if (dwExitCode == STILL_ACTIVE)	//259
				{
					TerminateThread(threadHandle, 0);
					CloseHandle(threadHandle);
				}
			}
		}
		m_pCapReadThread = NULL;
	}

	MilDestroy();

	//if (m_ImageBufferForSave != NULL)
	//{
	//	for (int a = 0; a < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; a++)
	//	{
	//		for (int b = 0; b < KIND_OF_REGRIDDATA; b++)
	//		{
	//			delete[] * (*(m_ImageBufferForSave + a) + b);
	//		}
	//		delete[] * (m_ImageBufferForSave + a);
	//	}
	//	delete[] m_ImageBufferForSave;
	//}
	//

	if (m_ImageBufferForSave != NULL)
	{
		for (int i = 0; i < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; i++)
		{
			for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
			{
				delete[] m_ImageBufferForSave[i][j];
			}

			delete[] m_ImageBufferForSave[i];
		}
		delete[] m_ImageBufferForSave;
	}



	//if (m_MilSystem != M_NULL)
	//{
	//	MsysFree(m_MilSystem);
	//	m_MilSystem = M_NULL;
	//}

	//if (m_MilApplication != M_NULL)
	//{
	//	MappFree(m_MilApplication);
	//	m_MilApplication = M_NULL;
	//}

	CDialogEx::OnDestroy();
}

BOOL CADAMDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}

BOOL CADAMDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();


	// Combo box의 텍스트 값을 초기화 함
	m_comboFovSelect.AddString(_T(" 1000 nm"));
	m_comboFovSelect.AddString(_T(" 1500 nm")); //획득할 이미지의 FOV를 Combo box로 나타냅니다.
	m_comboFovSelect.AddString(_T(" 2000 nm"));
	m_comboFovSelect.AddString(_T(" 3000 nm"));
	m_comboFovSelect.AddString(_T(" 5000 nm"));
	m_comboFovSelect.AddString(_T("10000 nm"));
	m_comboFovSelect.SetCurSel(2); //1500 nm

	m_comboScanGridSelect.AddString(_T(" 5 nm"));
	m_comboScanGridSelect.AddString(_T("10 nm"));
	m_comboScanGridSelect.AddString(_T("20 nm"));
	m_comboScanGridSelect.AddString(_T("40 nm"));
	m_comboScanGridSelect.AddString(_T("80 nm"));
	//m_comboScanGridSelect.AddString(_T("160 nm"));
	m_comboScanGridSelect.SetCurSel(2);

	m_comboScanNumber.AddString(_T("  1"));			// Scan number 인데...
	m_comboScanNumber.AddString(_T("  2"));
	m_comboScanNumber.AddString(_T("  3"));
	m_comboScanNumber.AddString(_T("  4"));
	m_comboScanNumber.AddString(_T("  5"));
	m_comboScanNumber.AddString(_T("  6"));
	m_comboScanNumber.AddString(_T("  7"));
	m_comboScanNumber.AddString(_T("  8"));
	m_comboScanNumber.AddString(_T("  9"));
	m_comboScanNumber.AddString(_T(" 10"));
	m_comboScanNumber.AddString(_T(" 12"));
	m_comboScanNumber.AddString(_T(" 16"));
	m_comboScanNumber.AddString(_T(" 24"));
	m_comboScanNumber.AddString(_T(" 50"));
	m_comboScanNumber.AddString(_T(" 100"));
	m_comboScanNumber.SetCurSel(0);

	m_comboInterpolationFridSelect.AddString(_T(" 1"));
	m_comboInterpolationFridSelect.AddString(_T(" 2"));
	m_comboInterpolationFridSelect.AddString(_T(" 5"));
	m_comboInterpolationFridSelect.AddString(_T("10"));
	m_comboInterpolationFridSelect.SetCurSel(1);

	m_comboDetectorSelection.AddString(_T("1 Det"));
	m_comboDetectorSelection.AddString(_T("2 Det"));
	m_comboDetectorSelection.SetCurSel(0);

	CString strThreshold;
	strThreshold.Format("%f", CornerThreshold);
	m_EditCornerThreshold.SetWindowTextA(strThreshold);

	// Graph Display 화면 초기화
	//CRect m_Rect, m_GraphRect;
	//GetClientRect(m_Rect);

	//m_GraphRect.left = m_Rect.left;
	//m_GraphRect.top = m_Rect.top + 1760;		// 1610 에서 1750으로 변경_KYD_20191217_Stream data 소켓 내용을 보기위해 공간을 마련함
	//m_GraphRect.right = m_Rect.right;
	//m_GraphRect.bottom = m_Rect.bottom - 10;
	//m_GraphWnd.CreateGraphWnd(_T(""), m_GraphRect, WS_CHILD | WS_VISIBLE, this, 1);
	//m_GraphWnd.SetTitle("Detector(Segment1:Blue, Segment2:Red) Intensity Graph");
	//m_GraphWnd.SetGraphPointColor(0, RGB(255, 0, 0));
	//m_GraphWnd.SetGraphPointColor(1, RGB(0, 0, 255));


	// Config Data Update
	m_I0Reference = g_pConfig->m_I0Reference;
	m_I0FilterWindowSize = g_pConfig->m_I0FilterWindowSize;
	m_LowPassFilterSigma = g_pConfig->m_LowPassFilterSigma;

	ADAM_MAX_WIDTH = g_pConfig->m_AdamMaxWidth;
	ADAM_MAX_HEIGHT = g_pConfig->m_AdamMaxHeight;
	ADAM_MAX_WIDTH_REGRID = g_pConfig->m_AdamMaxWidthRegrid;
	ADAM_MAX_HEIGHT_REGRID = g_pConfig->m_AdamMaxHeightRegrid;

	//Adam Memory Alloc
	MemoryAllocation();

	MilInitialize();

	DisplayRawImage();

	m_ProgressEUVImagingCtrl.SetRange(0, 100);
	m_ProgressEUVImagingCtrl.SetStep(1);
	m_ProgressEUVImagingCtrl.SetPos(1);
	m_ProgressEUVImagingCtrl.SetColor(PURPLE);


	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[2] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONRED), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_ICON_ADAM_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_ICON_SQ_CONNECT))->SetIcon(m_LedIcon[0]);



	SetTimer(ADAMDATA_CONNECTION_TIMER, 1000, NULL);

	//Cap sensor ReadThread
	m_ctrlCapSensorAutoRead.SetCheck(m_bCapAutoRead); //UI 연동
	m_pCapReadThread = AfxBeginThread(CapReadThread, this, THREAD_PRIORITY_NORMAL, 0, 0);


	m_ImageSaveThreadCount = 0;

	/*m_ImageBufferForSave = new double**[IMAGE_BUFFER_SIZE_FOR_FILE_SAVE];
	for (int i = 0; i < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; i++)
	{
		*(m_ImageBufferForSave + i) = new double*[KIND_OF_REGRIDDATA];
	}

	for (int i = 0; i < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; i++)
	{
		for (int k = 0; k < KIND_OF_REGRIDDATA; k++)
		{
			*(*(m_ImageBufferForSave + i) + k) = new double[ADAM_MAX_DATA_REGRID];
		}
	}

	for (int a = 0; a < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; a++)
	{
		for (int b = 0; b < KIND_OF_REGRIDDATA; b++)
		{
			memset(m_ImageBufferForSave[a][b], 0, sizeof(double)*ADAM_MAX_DATA_REGRID);
		}
	}
*/

	m_ImageBufferForSave = new double**[IMAGE_BUFFER_SIZE_FOR_FILE_SAVE]; //메모리 소요 많음 18GBYTE 정도
	for (int i = 0; i < IMAGE_BUFFER_SIZE_FOR_FILE_SAVE; i++)
	{
		m_ImageBufferForSave[i] = new double*[KIND_OF_REGRIDDATA];

		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
		{
			m_ImageBufferForSave[i][j] = new double[ADAM_MAX_DATA_REGRID];
			memset(m_ImageBufferForSave[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
		}
	}




	m_RawImageDisplayThread = AfxBeginThread(RawImageDisplayThread, (LPVOID)this, THREAD_PRIORITY_NORMAL);


	UpdateData(FALSE);


	BeamOpen();


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CADAMDlg::MilInitialize()
{
	m_MilSystem = g_milSystemHost;

	//MbufAlloc2d(m_MilSystem, ADAM_MAX_WIDTH_REGRID, ADAM_MAX_HEIGHT_REGRID, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilDisplayGray);
	//MbufAllocColor(m_MilSystem, 3, ADAM_MAX_WIDTH_REGRID, ADAM_MAX_HEIGHT_REGRID, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilDisplayColor);
	//MbufControl(m_MilDisplayGray, M_DC_ALLOC, M_DEFAULT);
	//MbufClear(m_MilDisplayGray, 0);
	//MappControl(M_ERROR, M_PRINT_ENABLE);
	//m_MilDisplayColor = M_NULL;
	m_MilSaveBmpImage = M_NULL;


	MdispAlloc(m_MilSystem, M_DEFAULT, M_DISPLAY_SETUP, M_DEFAULT, &m_MilDisplay);

	MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
	MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);
	//MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_DISABLE);
	//MdispControl(m_MilDisplay, M_KEYBOARD_USE, M_ENABLE);

	MdispControl(m_MilDisplay, M_BACKGROUND_COLOR, M_COLOR_BLACK);
	MdispControl(m_MilDisplay, M_CENTER_DISPLAY, M_ENABLE);

	MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE, MilMouseMoveFct, (void*)this);
	MdispHookFunction(m_MilDisplay, M_MOUSE_LEFT_DOUBLE_CLICK, MilMouseLeftButtonDoubleClickFct, (void*)this);


	//LUT
	m_MilColorLut = MbufAllocColor(m_MilSystem, 3, m_constLutMax + 1, 1, 8 + M_UNSIGNED, M_LUT, M_NULL);

	MakeRainbowColorMap();

	//MgenLutFunction(m_MilColorLut, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);

	MdispLut(m_MilDisplay, m_MilColorLut);
	MdispControl(m_MilDisplay, M_UPDATE, M_ENABLE);

	MgraAlloc(m_MilSystem, &m_MilGraphContext);
	MgraAllocList(m_MilSystem, M_DEFAULT, &m_MilGraphListCross);
	MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, m_MilGraphListCross); // 이미지 확대 축소시 깨지지 않게 하기 위해 추가	

	m_pWndDisplay = (CWnd *)(GetDlgItem(IDC_IMAGE_VIEW_ADAM));

	//MdispControl(m_MilDisplay, M_OVERLAY, M_ENABLE);
	//CrossLine Setting

	// 임시
	/*m_MilRawImageWidth = m_nRawImage_PixelWidth;
	m_MilRawImageHeight = m_nRawImage_PixelHeight;

	MbufAlloc2d(m_MilSystem, m_MilRawImageWidth, m_MilRawImageHeight, 16 + M_UNSIGNED, M_IMAGE + M_DISP + M_PROC, &m_MilRawImage);
	MbufClear(m_MilRawImage, 0);

	MbufAlloc2d(m_MilSystem, m_MilRawImageWidth * 2, m_MilRawImageHeight * 2, 16 + M_UNSIGNED, M_IMAGE + M_DISP + M_PROC, &m_MilRegridImage);
	MbufClear(m_MilRegridImage, m_constLutMax);

	SelectDisp(m_MilRawImage);*/

	MbufCreate2d(m_MilSystem, ADAM_MAX_WIDTH, ADAM_MAX_HEIGHT, 8 + M_UNSIGNED, M_IMAGE + M_DISP, M_DEFAULT, M_DEFAULT, m_8URawImage_bufferForDisp, &m_MilRawImageForDisp);
	m_MilRawImageForDispWidth = ADAM_MAX_WIDTH;
	m_MilRawImageForDispHeight = ADAM_MAX_HEIGHT;

	MbufCreate2d(m_MilSystem, ADAM_MAX_WIDTH_REGRID, ADAM_MAX_HEIGHT_REGRID, 8 + M_UNSIGNED, M_IMAGE + M_DISP, M_DEFAULT, M_DEFAULT, m_8URegridImage_bufferForDisp, &m_MilRegridImageForDisp);
	m_MilRegridImageForDispWidth = ADAM_MAX_WIDTH_REGRID;
	m_MilRegridImageForDispHeight = ADAM_MAX_HEIGHT_REGRID;

	// 초기 이미지 세팅
	MbufChild2d(m_MilRawImageForDisp, 0, 0, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight, &m_MilRawImageForDispChild);
	SelectDisp(m_MilRawImageForDispChild);
	m_DispType = DISP_RAW_IMAGE;

	MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);

	//	if (m_IsDisplayChange)
	   //{
	   //	// MIL로 변경
	   //	((CStatic*)GetDlgItem(IDC_EUV_DISPLAY))->ShowWindow(SW_HIDE);
	   //	((CStatic*)GetDlgItem(IDC_ADAM_STATIC))->ShowWindow(SW_HIDE);


	   //	((CStatic*)GetDlgItem(IDC_IMAGE_VIEW_ADAM))->ShowWindow(SW_SHOW);
	   //	((CStatic*)GetDlgItem(IDC_STATIC_ADAM_EUV_POSITION))->ShowWindow(SW_SHOW);
	   //}
	   //else
	   //{
	   //	// 원복
	   //	((CStatic*)GetDlgItem(IDC_EUV_DISPLAY))->ShowWindow(SW_SHOW);
	   //	((CStatic*)GetDlgItem(IDC_ADAM_STATIC))->ShowWindow(SW_SHOW);

	   //	((CStatic*)GetDlgItem(IDC_IMAGE_VIEW_ADAM))->ShowWindow(SW_HIDE);
	   //	((CStatic*)GetDlgItem(IDC_STATIC_ADAM_EUV_POSITION))->ShowWindow(SW_HIDE);
	   //}

}

void CADAMDlg::MakeRainbowColorMap()
{
	//MgenLutFunction(m_MilColorLut, M_COLORMAP_JET, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT);
	//MbufGetColor(m_MilColorLut, M_PLANAR, M_ALL_BANDS, LutJetArray);

	MIL_UINT8 LutJetArray[3][256];

	float dividemax = 255 / 4.;
	for (int i = 0; i <= m_constLutMax; i++)
	{
		if (i < dividemax)
		{
			LutJetArray[0][i] = 0;
			LutJetArray[1][i] = 0;
			LutJetArray[2][i] = 150 + (unsigned char)((255 - 150) * i / dividemax);
		}
		else if (i < 2 * dividemax)
		{
			LutJetArray[0][i] = 0;
			LutJetArray[1][i] = (unsigned char)(255 * (i - dividemax) / dividemax);
			LutJetArray[2][i] = 255;
		}
		else if (i < 3 * dividemax)
		{
			LutJetArray[0][i] = (unsigned char)(255 * (i - 2 * dividemax) / dividemax);
			LutJetArray[1][i] = 255;
			LutJetArray[2][i] = 255 - LutJetArray[0][i];
		}
		else if (i < 255)
		{
			LutJetArray[0][i] = 255;
			LutJetArray[1][i] = (unsigned char)(255 - 255 * (i - 3 * dividemax) / dividemax);
			LutJetArray[2][i] = 0;
		}
		else
		{
			LutJetArray[0][i] = 255;
			LutJetArray[1][i] = 0;
			LutJetArray[2][i] = 0;
		}
	}

	MbufPutColor(m_MilColorLut, M_PLANAR, M_ALL_BANDS, LutJetArray);
}



void CADAMDlg::SelectDisp(MIL_ID milImage)
{
	if (milImage != M_NULL)
	{
		//MdispControl(m_MilDisplay, M_OVERLAY_SHOW, M_DISABLE);
		MdispSelectWindow(m_MilDisplay, milImage, m_pWndDisplay->m_hWnd);
		MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);

		MdispControl(m_MilDisplay, M_UPDATE, M_NOW);

		MbufInquire(milImage, M_SIZE_X, &m_MilDispWidth);
		MbufInquire(milImage, M_SIZE_Y, &m_MilDispHeight);

		//Cross Line 초기 설정

		MIL_DOUBLE startX[2];
		MIL_DOUBLE startY[2];
		MIL_DOUBLE endX[2];
		MIL_DOUBLE endY[2];

		if (m_CrossLabelCenter == -1)
		{
			MgraColor(m_MilGraphContext, M_COLOR_WHITE);

			startX[0] = (m_MilDispWidth - 1.0) / 2.0;
			startY[0] = -0.5;
			endX[0] = (m_MilDispWidth - 1.0) / 2.0;
			endY[0] = m_MilDispHeight - 0.5;

			startX[1] = -0.5;
			startY[1] = (m_MilDispHeight - 1.0) / 2.0;
			endX[1] = m_MilDispWidth - 0.5;
			endY[1] = (m_MilDispHeight - 1.0) / 2.0;


			MgraLines(m_MilGraphContext, m_MilGraphListCross, 2, startX, startY, endX, endY, M_DEFAULT);
			MgraInquireList(m_MilGraphListCross, M_LIST, M_DEFAULT, M_LAST_LABEL, &m_CrossLabelCenter);
		}
		else //재설정
		{
			startX[0] = (m_MilDispWidth - 1.0) / 2.0;
			startY[0] = -0.5;
			endX[0] = (m_MilDispWidth - 1.0) / 2.0;
			endY[0] = m_MilDispHeight - 0.5;

			startX[1] = -0.5;
			startY[1] = (m_MilDispHeight - 1.0) / 2.0;
			endX[1] = m_MilDispWidth - 0.5;
			endY[1] = (m_MilDispHeight - 1.0) / 2.0;

			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 0, M_POSITION_X, startX[0]);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 0, M_POSITION_Y, startY[0]);

			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 1, M_POSITION_X, endX[0]);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 1, M_POSITION_Y, endY[0]);

			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 2, M_POSITION_X, startX[1]);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 2, M_POSITION_Y, startY[1]);

			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 3, M_POSITION_X, endX[1]);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), 3, M_POSITION_Y, endY[1]);

		}

		if (m_bEUVCrossLineDisplay)
		{
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), M_DEFAULT, M_VISIBLE, M_TRUE);
		}
		else
		{
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), M_DEFAULT, M_VISIBLE, M_FALSE);
		}

		//Mouse Move Line 설정
		//Cross Line 초기 설정
		if (m_CrossLabelMove == -1)
		{
			MgraColor(m_MilGraphContext, M_RGB888(155, 0, 200));

			MIL_DOUBLE startX[2];
			MIL_DOUBLE startY[2];
			MIL_DOUBLE endX[2];
			MIL_DOUBLE endY[2];

			startX[0] = m_MilDispWidth / 2;
			startY[0] = 0;
			endX[0] = m_MilDispWidth / 2;
			endY[0] = m_MilDispHeight - 1;

			startX[1] = 0;
			startY[1] = m_MilDispHeight / 2;
			endX[1] = m_MilDispWidth - 1;
			endY[1] = m_MilDispHeight / 2;


			MgraLines(m_MilGraphContext, m_MilGraphListCross, 2, startX, startY, endX, endY, M_DEFAULT);
			MgraInquireList(m_MilGraphListCross, M_LIST, M_DEFAULT, M_LAST_LABEL, &m_CrossLabelMove);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		}

		if (m_MatchRectLabelCenter == -1)
		{
			MgraColor(m_MilGraphContext, M_RGB888(0, 255, 0));

			double crossRectSize = 2;
			double startX = 0, startY = 0;

			MgraRect(m_MilGraphContext, m_MilGraphListCross, startX, startY, startX + crossRectSize, startY + crossRectSize);
			MgraInquireList(m_MilGraphListCross, M_LIST, M_DEFAULT, M_LAST_LABEL, &m_MatchRectLabelCenter);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_VISIBLE, M_FALSE);
		}
		if (m_MatchCrossLabelCenter == -1)
		{
			MgraColor(m_MilGraphContext, M_RGB888(0, 255, 0));
			
			double CrossSize = 2;
			MIL_DOUBLE startX[2] = { 0,0 };
			MIL_DOUBLE startY[2] = { 0,0 };
			MIL_DOUBLE endX[2] = { 0,0 };
			MIL_DOUBLE endY[2] = { 0,0 };

			endX[0] = startX[0] + CrossSize;
			endX[1] = startX[1] + CrossSize;

			MgraLines(m_MilGraphContext, m_MilGraphListCross, 2, startX, startY, endX, endY, M_DEFAULT);
			MgraInquireList(m_MilGraphListCross, M_LIST, M_DEFAULT, M_LAST_LABEL, &m_MatchCrossLabelCenter);
			MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), M_DEFAULT, M_VISIBLE, M_FALSE);
		}

	

	}

}

int MFTYPE CADAMDlg::IsMouseInImage(MOUSEPOSITION* mousePos, MIL_ID EventID)
{
	int nRet = 0;

	MdispGetHookInfo(EventID, M_MOUSE_POSITION_X, &mousePos->m_DisplayPositionX);
	MdispGetHookInfo(EventID, M_MOUSE_POSITION_Y, &mousePos->m_DisplayPositionY);
	MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_X, &mousePos->m_BufferPositionX);
	MdispGetHookInfo(EventID, M_MOUSE_POSITION_BUFFER_Y, &mousePos->m_BufferPositionY);

	if (mousePos->m_BufferPositionX >= -0.5 && mousePos->m_BufferPositionX < (m_pAdamDlgStaticInst->m_MilDispWidth - 0.5) &&
		mousePos->m_BufferPositionY >= -0.5 && mousePos->m_BufferPositionY < (m_pAdamDlgStaticInst->m_MilDispHeight - 0.5))
	{
		nRet = 0;
	}
	else
	{
		nRet = -1;
	}

	return nRet;
}


MIL_INT MFTYPE CADAMDlg::MilMouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr)
{
	CADAMDlg* pCurrentView = (CADAMDlg*)UserDataPtr;

	if (pCurrentView)
	{
		MOUSEPOSITION MousePosition;

		if (IsMouseInImage(&MousePosition, EventID) == 0)
		{
			pCurrentView->DispMousePosition(MousePosition);

			//if ((GetAsyncKeyState(VK_LBUTTON) & 0x8000) && pCurrentView->m_bEUVMouseMove == TRUE)			
			if (pCurrentView->m_bEUVMouseMove == TRUE)
			{
				pCurrentView->EUVMoveLineDraw(MousePosition.m_BufferPositionX, MousePosition.m_BufferPositionY);
			}
		}
	}
	return 0;
}

MIL_INT MFTYPE CADAMDlg::MilMouseLeftButtonDoubleClickFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr)
{
	CADAMDlg* pCurrentView = (CADAMDlg*)UserDataPtr;

	if (pCurrentView)
	{
		MOUSEPOSITION MousePosition;
		if (IsMouseInImage(&MousePosition, EventID) == 0)
		{
			if (pCurrentView->m_bEUVMouseMove == TRUE)
			{
				//pCurrentView->m_bEUVMouseMoveLineDisplay = FALSE;

				double pixel_x = MousePosition.m_BufferPositionX;
				double pixel_y = -1 * MousePosition.m_BufferPositionY + (pCurrentView->m_MilDispHeight - 1.0);

				double center_pixel_x = pixel_x - (pCurrentView->m_MilDispWidth - 1.0) / 2.0;
				double center_pixel_y = pixel_y - (pCurrentView->m_MilDispHeight - 1.0) / 2.0;

				double scanGrid_nm;

				// Scan Grid  가져오는것으로 수정
				if (pCurrentView->m_DispType == DISP_RAW_IMAGE)
				{
					scanGrid_nm = pCurrentView->m_nEuvImage_ScanGrid;
				}
				else if (pCurrentView->m_DispType == DISP_REGRID_IMAGE)
				{
					scanGrid_nm = pCurrentView->m_nEUVImage_InterpolationGrid;
				}

				double nCenter_DistanceX_mm = scanGrid_nm / 1000000.0 * center_pixel_x;
				double nCenter_DistanceY_mm = scanGrid_nm / 1000000.0 * center_pixel_y;

				double current_posx_mm = 0.0, current_posy_mm = 0.0;
				current_posx_mm = g_pNavigationStage->GetTargetPosmm(STAGE_X_AXIS);
				current_posy_mm = g_pNavigationStage->GetTargetPosmm(STAGE_Y_AXIS);

				double target_posx_mm = 0.0, target_posy_mm = 0.0;
				target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
				target_posy_mm = current_posy_mm - nCenter_DistanceY_mm;

				if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
				{
					//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
					g_pNavigationStage->SetLaserMode();
				}
				if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE || g_pMaskMap->m_bManualAlign == TRUE)
					g_pNavigationStage->MoveAbsoluteXY_OnTheFly(target_posx_mm, target_posy_mm);

				// Center Cross line 재설정  
				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 0, M_POSITION_X, -0.5);
				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 0, M_POSITION_Y, MousePosition.m_BufferPositionY);

				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 1, M_POSITION_X, m_pAdamDlgStaticInst->m_MilDispWidth);
				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 1, M_POSITION_Y, MousePosition.m_BufferPositionY);

				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 2, M_POSITION_X, MousePosition.m_BufferPositionX);
				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 2, M_POSITION_Y, -0.5);

				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 3, M_POSITION_X, MousePosition.m_BufferPositionX);
				MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelCenter), 3, M_POSITION_Y, m_pAdamDlgStaticInst->m_MilDispHeight);

			}
		}
	}

	pCurrentView->m_bEUVMouseMove = FALSE;
	MgraControlList(pCurrentView->m_MilGraphListCross, M_GRAPHIC_LABEL(pCurrentView->m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
	MdispControl(pCurrentView->m_MilDisplay, M_MOUSE_USE, M_ENABLE);
	MdispControl(pCurrentView->m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);

	pCurrentView->UpdateData(FALSE);

	return 0;
}

void CADAMDlg::DispMousePosition(MOUSEPOSITION MousePosition)
{
	double pixel_x = MousePosition.m_BufferPositionX;
	double pixel_y = -1 * MousePosition.m_BufferPositionY + (m_MilDispHeight - 1.0);

	double center_pixel_x = pixel_x - (m_MilDispWidth - 1.0) / 2.0;
	double center_pixel_y = pixel_y - (m_MilDispHeight - 1.0) / 2.0;

	double nDistanceX_nm, nDistanceY_nm, nCenter_DistanceX_nm, nCenter_DistanceY_nm;

	//double scale = 1;


	double pixelValue = 0.0;

	int x = MousePosition.m_BufferPositionX + 0.5;
	int y = MousePosition.m_BufferPositionY + 0.5;

	double scanGrid_nm;


	if (m_DispType == DISP_RAW_IMAGE)
	{
		//scale = 1;
		pixelValue = m_dRawImageForDisplay[DETECTOR_INTENSITY_1][y*m_nRawImage_PixelWidth + x];
		scanGrid_nm = m_nEuvImage_ScanGrid;
	}
	else if (m_DispType == DISP_REGRID_IMAGE)
	{
		//scale = (double)m_nEUVImage_InterpolationGrid / m_nEuvImage_ScanGrid;
		pixelValue = m_dFilteredImage_Data3D[0][D1_REGRID][y*m_nReGridImage_PixelWidth + x];

		scanGrid_nm = m_nEUVImage_InterpolationGrid;
	}

	nDistanceX_nm = scanGrid_nm * pixel_x;
	nDistanceY_nm = scanGrid_nm * pixel_y;

	nCenter_DistanceX_nm = scanGrid_nm * center_pixel_x;
	nCenter_DistanceY_nm = scanGrid_nm * center_pixel_y;

	CString strMousePoint;
	strMousePoint.Format("point_x=%.1f, point_y=%.1f, distance_x=%.1f nm, distance_y=%.1f nm,  center_point_x=%.1f, center_point_y=%.1f, center_distance_x=%.1f nm, center_distance_y=%.1f nm   Value =%f", pixel_x, pixel_y, nDistanceX_nm, nDistanceY_nm, center_pixel_x, center_pixel_y, nCenter_DistanceX_nm, nCenter_DistanceY_nm, pixelValue);
	GetDlgItem(IDC_STATIC_ADAM_EUV_POSITION)->SetWindowTextA(strMousePoint);

}

void CADAMDlg::MilDestroy()
{
	if (m_MilSaveBmpImage != M_NULL)
	{
		MbufFree(m_MilSaveBmpImage);
		m_MilSaveBmpImage = M_NULL;
	}

	//if (m_MilDisplayGray != M_NULL)
	//{
	//	MbufFree(m_MilDisplayGray);
	//	m_MilDisplayGray = M_NULL;
	//}

	//if (m_MilDisplayColor != M_NULL)
	//{
	//	MbufFree(m_MilDisplayColor);
	//	m_MilDisplayColor = M_NULL;
	//}

	if (m_MilDisplay != M_NULL)
	{
		MdispHookFunction(m_MilDisplay, M_MOUSE_MOVE + M_UNHOOK, MilMouseMoveFct, (void*)this);
		MdispHookFunction(m_MilDisplay, M_MOUSE_LEFT_DOUBLE_CLICK + M_UNHOOK, MilMouseLeftButtonDoubleClickFct, (void*)this);
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispControl(m_MilDisplay, M_ASSOCIATED_GRAPHIC_LIST_ID, M_NULL);
	}
	if (m_MilGraphListCross)
	{
		MgraFree(m_MilGraphListCross);
		m_MilGraphListCross = M_NULL;
	}
	if (m_MilGraphContext)
	{
		MgraFree(m_MilGraphContext);
		m_MilGraphContext = M_NULL;
	}
	if (m_MilDisplay != M_NULL)
	{
		MdispFree(m_MilDisplay);
		m_MilDisplay = M_NULL;
	}

	if (m_MilColorLut != M_NULL)
	{
		MbufFree(m_MilColorLut);
		m_MilColorLut = M_NULL;
	}

	if (m_MilRawImageForDispChild != M_NULL)
	{
		MbufFree(m_MilRawImageForDispChild);
		m_MilRawImageForDispChild = M_NULL;
	}

	if (m_MilRegridImageForDispChild != M_NULL)
	{
		MbufFree(m_MilRegridImageForDispChild);
		m_MilRegridImageForDispChild = M_NULL;
	}

	if (m_MilRawImageForDisp != M_NULL)
	{
		MbufFree(m_MilRawImageForDisp);
		m_MilRawImageForDisp = M_NULL;
	}

	if (m_MilRegridImageForDisp != M_NULL)
	{
		MbufFree(m_MilRegridImageForDisp);
		m_MilRegridImageForDisp = M_NULL;
	}
}

void CADAMDlg::OnTimer(UINT_PTR nIDEvent)
{
	double value = 0.0, totalNo = 0.0, progressedNo = 0.0;
	static BOOL bPreviousScaningOn = FALSE;
	static double old_value = 0.0;

	switch (nIDEvent)
	{

	case ADAMDATA_DISPLAY_TIMER:	//1초(1000ea data)마다 data 추이를 graph로 갱신해주는 타이머
		totalNo = (double)m_nEuvImage_ScanNumber * (double)m_nRawImage_PixelWidth * (double)m_nRawImage_PixelHeight;
		progressedNo = (double)m_nFovDataCnt + ((double)m_nRawImage_PixelWidth * (double)m_nRawImage_PixelHeight)*g_pScanStage->m_nScanNum;
		//progressedNo = (double)m_nFovDataCnt + ((double)m_nRawImage_PixelWidth * (double)m_nRawImage_PixelHeight)*m_nImageScanCnt;		
		value = progressedNo / totalNo;
		if (value == 0 || value > old_value)
			m_ProgressEUVImagingCtrl.SetPos((int)(value * 100));
		old_value = value;

		//value = (double)(rand() % 50);
		//m_GraphWnd.AddPoint(0, CTime::GetCurrentTime(), value);
		//value = 50.0 + (double)(rand() % 50) - 1;
		//m_GraphWnd.AddPoint(1, CTime::GetCurrentTime(), value);
		//m_GraphWnd.Redraw();
		break;

	case ADAMDATA_CONNECTION_TIMER:
		if (m_bConnected)
		{
			((CStatic*)GetDlgItem(IDC_ICON_ADAM_CONNECT))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_ADAM_CONNECT))->SetIcon(m_LedIcon[2]);

			if (g_pConfig->m_nEquipmentMode == ONLINE)
			{
				// 재접속?, ADAM 연결 없을 때 이거 있으면 에러 발생
				//CloseTcpIpSocket();				
				//OpenTcpIpSocketForAdam(g_pConfig->m_chIP[ETHERNET_ADAM], g_pConfig->m_nPORT[ETHERNET_ADAM], FALSE, ADAM_RECEIVE_BUFFER_SIZE);
			}
		}
		
		if (m_Sq1.m_bConnected)
		{
			((CStatic*)GetDlgItem(IDC_ICON_SQ_CONNECT))->SetIcon(m_LedIcon[1]);
		}
		else
		{
			((CStatic*)GetDlgItem(IDC_ICON_SQ_CONNECT))->SetIcon(m_LedIcon[2]);
		}

		break;
	default:
		break;
	}

	__super::OnTimer(nIDEvent);
}

int CADAMDlg::DisplayRawImage()
{
	int ret = 0, i = 0;
	double min = 0.0, max = 0.0;

	int displayedimage = 4;

	// Detector 선택에 따라 이미지 Display를 다르게 해주기 위해 작성한 코드_KYD_20200126
	if (m_nEUVImage_Detectorselection == 1)
	{
		displayedimage = DETECTOR_INTENSITY_1;
	}
	else if (m_nEUVImage_Detectorselection == 2)
	{
		displayedimage = DETECTOR_INTENSITY_2;
	}

	// Regrid Disp Image
	double *imageBuf;

	if (m_IsUseRegridRawImage)
	{
		imageBuf = m_dRawImageDataFor1DRegrid;

		int currentLineCount;

		if (m_nScanType == UNI_DIRECTION)
		{
			currentLineCount = m_nFovDataCnt / m_nRawImage_PixelWidth - 1;//왜인지는 모르겠음
		}
		else if (m_nScanType == BI_DIRECTION)
		{
			currentLineCount = m_nFovDataCnt_Bi / m_nRawImage_PixelWidth - 1;//왜인지는 모르겠음
		}
		if (currentLineCount > 0)
		{
			if (currentLineCount > m_nRawImage_PixelHeight) currentLineCount = m_nRawImage_PixelHeight;
			double *x_out = new double[m_nRawImage_PixelWidth];

			for (int i = 0; i < m_nRawImage_PixelWidth; i++)
			{
				x_out[i] = m_dAcelDistance_X + m_nEuvImage_ScanGrid * i;

			}

			for (int i = previousLintCountInterpolation + 1; i <= currentLineCount; i++)
			{
				// i번 데이터 
				double  *x = m_dRawImageForDisplay[LASERINTERFEROMETER_X] + (i - 1) * m_nRawImage_PixelWidth;
				double  *y = m_dRawImageForDisplay[displayedimage] + (i - 1) * m_nRawImage_PixelWidth;
				double  *y_out = m_dRawImageDataFor1DRegrid + (i - 1) * m_nRawImage_PixelWidth;

				InterpolationPchip1D(x, y, m_nRawImage_PixelWidth, x_out, y_out, m_nRawImage_PixelWidth);

				// 2D 인터폴레이션으로 구현..)현재 들어온 데이터 바로 아랫줄까지 수행 (연산속도 계선
			}

			delete[] x_out;
			previousLintCountInterpolation = currentLineCount;
		}
	}
	else
	{
		imageBuf = m_dRawImageForDisplay[displayedimage];
	}

	int pitch = m_MilRawImageForDispWidth;

	min = 0.0, max = 0.0;
	for (i = 0; i < m_nRawImage_PixelWidth * m_nRawImage_PixelHeight; i++)
	{
		if (imageBuf[i] < 0)					// 마이너스 값 Zeor padding
			imageBuf[i] = 0.0;

		if (min > imageBuf[i])                // 1D array에서 min, max를 구하는 로직임. 작동함
			min = imageBuf[i];

		if (max < imageBuf[i])
			max = imageBuf[i];
	}

	for (int i = 0; i < m_nRawImage_PixelHeight; i++)
	{
		for (int j = 0; j < m_nRawImage_PixelWidth; j++)
		{
			//m_8URawImage_bufferForDisp[pitch*i + j] = (UINT8)((m_dRawImage_Data[displayedimage][i*m_nRawImage_PixelWidth + j] - min) / (max - min) * (double)m_constLutMax);
			m_8URawImage_bufferForDisp[pitch*i + j] = (UINT8)((imageBuf[i*m_nRawImage_PixelWidth + j] - min) / (max - min) * (double)m_constLutMax);
		}
	}
	SelectDisp(m_MilRawImageForDispChild);
	m_DispType = DISP_RAW_IMAGE;

	return ret;
}
//
//int CADAMDlg::DisplayReGridImage()		//수정 필요함  최종 Averaging 된걸로 Display 함
//{
//	int ret = 0, i = 0;
//	double min = 0.0, max = 0.0;
//	//int m_nRawImage_PixelWidth = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;
//	//int m_nRawImage_PixelHeight = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;
//
//	//double** m_dReconstructedImage_Data;		//우선 넣었는데 향후 Global로 변경 필요함
//
//	//1.Received data를 0~255로 Normalization
//	for (i = 0; i < m_nRawImage_PixelWidth * m_nRawImage_PixelHeight; i++)
//	{
//		if (m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i] < 0)
//			m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i] = 0.0;
//
//		if (min > m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i])                // 1D array에서 min, max를 구하는 로직임. 작동함
//			min = m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i];
//
//		if (max < m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i])
//			max = m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i];
//	}
//	for (i = 0; i < m_nRawImage_PixelWidth * m_nRawImage_PixelHeight; i++)
//	{
//		m_pNormalized_RegridImage_buffer[i] = (BYTE)((m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i] - min) / (max - min) * 255.0);	//data normalization
//	}
//
//	//2.MIL Buffer로 Copy
//	MbufPut2d(m_MilDisplayGray, 0L, 0L, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight, m_pNormalized_RegridImage_buffer);
//
//	// buffer limit에 맞지 않는다는 에러가 뜸... 어떻게 고쳐야 하지? KYD
//
//	//3.Short Rainbow Color로 변환
//	if (g_pMaskMap != NULL)
//		g_pMaskMap->m_MaskMapWnd.ConvertToShortRainbowColor(m_MilDisplayGray, m_MilDisplayColor);
//
//	//4.이미지 Display
//	//m_nDisplayImageWidth = 820;
//	//m_nDisplayImageHeight = 1000;		// 시작점 윗줄에 Null이 있는 것 수정 필요함
//	m_nDisplayImageWidth = m_nRawImage_PixelWidth;
//	m_nDisplayImageHeight = m_nRawImage_PixelHeight;
//
//	m_nUpperCutPixelNum = 0;
//	m_nLeftCutPixelNum = 0;
//
//	CRect rect;
//	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);
//	InvalidateRect(rect, FALSE);
//
//	return ret;
//}
//
//int CADAMDlg::DisplayAveragedImage()		//수정 필요함  최종 Averaging 된걸로 Display 함
//{
//	int ret = 0, i = 0;
//	double min = 0.0, max = 0.0;
//	//int m_nReGridImage_PixelWidth = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;
//	//int m_nReGridImage_PixelHeight = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;
//
//	//double** m_dReconstructedImage_Data;		//우선 넣었는데 향후 Global로 변경 필요함
//
//	//1.Received data를 0~255로 Normalization
//	for (i = 0; i < m_nReGridImage_PixelWidth * m_nReGridImage_PixelHeight; i++)
//	{
//		if (m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i] < 0)					// 마이너스 값 Zeor padding  --> 여기서 막힘...
//			m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i] = 0.0;
//
//		if (min > m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i])                // 1D array에서 min, max를 구하는 로직임. 작동함
//			min = m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i];
//
//		if (max < m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i])
//			max = m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i];
//	}
//	for (i = 0; i < m_nReGridImage_PixelWidth * m_nReGridImage_PixelHeight; i++)
//	{
//		m_pNormalized_RegridImage_buffer[i] = (BYTE)((m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i] - min) / (max - min) * 255.0);	//data normalization 255에서 120으로 변경
//	}
//
//	//2.MIL Buffer로 Copy
//	MbufPut2d(m_MilDisplayGray, 0L, 0L, m_nReGridImage_PixelWidth, m_nReGridImage_PixelHeight, m_pNormalized_RegridImage_buffer);
//
//	// buffer limit에 맞지 않는다는 에러가 뜸... 어떻게 고쳐야 하지? KYD
//
//	//3.Short Rainbow Color로 변환
//	if (g_pMaskMap != NULL)
//		g_pMaskMap->m_MaskMapWnd.ConvertToShortRainbowColor(m_MilDisplayGray, m_MilDisplayColor);
//
//	//4.이미지 Display
//	//m_nDisplayImageWidth = 820;
//	//m_nDisplayImageHeight = 1000;		// 시작점 윗줄에 Null이 있는 것 수정 필요함
//	m_nDisplayImageWidth = m_nReGridImage_PixelWidth;
//	m_nDisplayImageHeight = m_nReGridImage_PixelHeight;
//
//	m_nUpperCutPixelNum = 0;		//Interpolation 위에 영상 짤라내기
//	m_nLeftCutPixelNum = 0;
//
//	CRect rect;
//	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);
//	InvalidateRect(rect, FALSE);
//
//	return ret;
//}


//---------------------------------------
int CADAMDlg::DisplayFilteredImage(double* image)		//수정 필요함  최종 Averaging 된걸로 Display 함
{
	int ret = 0, i = 0;
	double min = 0.0, max = 0.0;
	//int m_nReGridImage_PixelWidth = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;
	//int m_nReGridImage_PixelHeight = m_nEuvImage_Fov * 1000 / m_nEUVImage_InterpolationGrid;

	//double** m_dReconstructedImage_Data;		//우선 넣었는데 향후 Global로 변경 필요함

	//1.Received data를 0~255로 Normalization
	for (i = 0; i < m_nReGridImage_PixelWidth * m_nReGridImage_PixelHeight; i++)
	{
		if (image[i] < 0)					// 마이너스 값 Zeor padding  --> 여기서 막힘...
			image[i] = 0.0;

		if (min > image[i])                // 1D array에서 min, max를 구하는 로직임. 작동함
			min = image[i];

		if (max < image[i])
			max = image[i];
	}


#if false
	for (i = 0; i < m_nReGridImage_PixelWidth * m_nReGridImage_PixelHeight; i++)
	{
		m_pNormalized_RegridImage_buffer[i] = (BYTE)((image[i] - min) / (max - min) * 255.0);	//data normalization
	}

	//2.MIL Buffer로 Copy
	MbufPut2d(m_MilDisplayGray, 0L, 0L, m_nReGridImage_PixelWidth, m_nReGridImage_PixelHeight, m_pNormalized_RegridImage_buffer);

	// buffer limit에 맞지 않는다는 에러가 뜸... 어떻게 고쳐야 하지? KYD

	//3.Short Rainbow Color로 변환
	if (g_pMaskMap != NULL)
		g_pMaskMap->m_MaskMapWnd.ConvertToShortRainbowColor(m_MilDisplayGray, m_MilDisplayColor);

	//4.이미지 Display

	m_nDisplayImageWidth = m_nReGridImage_PixelWidth - 30;
	m_nDisplayImageHeight = m_nReGridImage_PixelHeight - 30;

	//m_nDisplayImageWidth = m_nReGridImage_PixelWidth;
	//m_nDisplayImageHeight = m_nReGridImage_PixelHeight;


	m_nUpperCutPixelNum = 0;
	m_nLeftCutPixelNum = 0;

	//CRect rect;
	//GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);
	//InvalidateRect(rect, FALSE);

#endif
	//For new display
	int pitch = m_MilRegridImageForDispWidth;

	for (int i = 0; i < m_nReGridImage_PixelHeight; i++)
	{
		for (int j = 0; j < m_nReGridImage_PixelWidth; j++)
		{
			m_8URegridImage_bufferForDisp[pitch*i + j] = (UINT8)((image[i*m_nReGridImage_PixelWidth + j] - min) / (max - min) * (double)m_constLutMax);
		}
	}
	SelectDisp(m_MilRegridImageForDispChild);
	m_DispType = DISP_REGRID_IMAGE;

	return ret;
}


#if false
void CADAMDlg::OnPaint() // 삭제예정 ihlee
{
	CPaintDC dc(this); // device context for painting

	SetBkMode(dc.m_hDC, TRANSPARENT);

	CRect rect;
	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);

	if (GetDlgItem(IDC_EUV_DISPLAY)->IsWindowVisible())
	{
		if (m_MilDisplayColor == M_NULL)
			return;

		HDC hDC;
		MbufControl(m_MilDisplayColor, M_DC_ALLOC, M_DEFAULT);
		hDC = (HDC)MbufInquire(m_MilDisplayColor, M_DC_HANDLE, M_NULL);


		::StretchBlt(dc.GetSafeHdc(), rect.left, rect.top, rect.Width(), rect.Height(), hDC, m_nLeftCutPixelNum, m_nUpperCutPixelNum, m_nDisplayImageWidth - m_nLeftCutPixelNum, m_nDisplayImageHeight - m_nUpperCutPixelNum, SRCCOPY);

		if (m_bEUVCrossLineDisplay)
		{
			CPen hpen, *hpenOld;
			hpen.CreatePen(PS_DOT, 1, WHITE);
			hpenOld = dc.SelectObject(&hpen);
			dc.MoveTo(0, rect.Height() / 2);
			dc.LineTo(rect.Width(), rect.Height() / 2);
			dc.MoveTo(rect.Width() / 2, 0);
			dc.LineTo(rect.Width() / 2, rect.Height());
			dc.SelectObject(hpenOld);
			hpen.DeleteObject();
		}
		if (m_bEUVMouseMoveLineDisplay)
		{
			CPen hpen, *hpenOld;
			hpen.CreatePen(PS_DOT, 1, PURPLE);
			hpenOld = dc.SelectObject(&hpen);
			dc.MoveTo(0, m_ptEUVLeftButtonDownPoint.y);
			dc.LineTo(rect.Width(), m_ptEUVLeftButtonDownPoint.y);
			dc.MoveTo(m_ptEUVLeftButtonDownPoint.x, 0);
			dc.LineTo(m_ptEUVLeftButtonDownPoint.x, rect.Height());
			dc.SelectObject(hpenOld);
			hpen.DeleteObject();
		}
		if (m_bEUVEdgeFindSuccess)
		{
			int nCrossSize = 15;
			int nCrossRectSize = 20;
			CPen hpen, *hpenOld;
			hpen.CreatePen(PS_SOLID, 3, RED);
			hpenOld = dc.SelectObject(&hpen);
			double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
			pixelvalue_x = (double)m_nRawImage_PixelWidth / rect.Width();
			pixelvalue_y = (double)m_nRawImage_PixelWidth / rect.Height();
			dc.MoveTo(m_nEdgeFindResultX / pixelvalue_x - nCrossSize, m_nEdgeFindResultY / pixelvalue_y);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x + nCrossSize, m_nEdgeFindResultY / pixelvalue_y);
			dc.MoveTo(m_nEdgeFindResultX / pixelvalue_x, m_nEdgeFindResultY / pixelvalue_y - nCrossSize);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x, m_nEdgeFindResultY / pixelvalue_y + nCrossSize);
			dc.MoveTo(m_nEdgeFindResultX / pixelvalue_x - nCrossRectSize, m_nEdgeFindResultY / pixelvalue_y - nCrossRectSize);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x - nCrossRectSize, m_nEdgeFindResultY / pixelvalue_y + nCrossRectSize);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x + nCrossRectSize, m_nEdgeFindResultY / pixelvalue_y + nCrossRectSize);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x + nCrossRectSize, m_nEdgeFindResultY / pixelvalue_y - nCrossRectSize);
			dc.LineTo(m_nEdgeFindResultX / pixelvalue_x - nCrossRectSize, m_nEdgeFindResultY / pixelvalue_y - nCrossRectSize);
			dc.SelectObject(hpenOld);
			hpen.DeleteObject();
		}

		MbufControl(m_MilDisplayColor, M_DC_FREE, M_DEFAULT);
	}
}
#endif

//m_dRawImage_Data를 인자로 받게 함수화 ihlee
//int CADAMDlg::RawImageFileSave()
//{
//	int ret = 0;
//	//int i = 0;
//
//	//ADAM에서 날아온 데이터를 저장용 버퍼에 Copy해두자. 
//	memcpy(m_dSaveImage_DataNum[LASERINTERFEROMETER_X], m_dRawImage_Data[LASERINTERFEROMETER_X], sizeof(double) * m_nRawImage_PixelWidth*m_nRawImage_PixelHeight);
//	memcpy(m_dSaveImage_DataNum[LASERINTERFEROMETER_Y], m_dRawImage_Data[LASERINTERFEROMETER_Y], sizeof(double) * m_nRawImage_PixelWidth*m_nRawImage_PixelHeight);
//
//	memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_1], m_dRawImage_Data[DETECTOR_INTENSITY_1], sizeof(double) * m_nRawImage_PixelWidth*m_nRawImage_PixelHeight);
//	memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_2], m_dRawImage_Data[DETECTOR_INTENSITY_2], sizeof(double) * m_nRawImage_PixelWidth*m_nRawImage_PixelHeight);
//	memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_3], m_dRawImage_Data[DETECTOR_INTENSITY_3], sizeof(double) * m_nRawImage_PixelWidth*m_nRawImage_PixelHeight);
//
//
//	//1.저장할 폴더 날짜별로 생성
//	int year, month, day, hour, minute, second;
//	year = CTime::GetCurrentTime().GetYear();
//	month = CTime::GetCurrentTime().GetMonth();
//	day = CTime::GetCurrentTime().GetDay();
//	hour = CTime::GetCurrentTime().GetHour();
//	minute = CTime::GetCurrentTime().GetMinute();
//	second = CTime::GetCurrentTime().GetSecond();
//
//	CString dir;
//	dir = IMAGEFILE_PATH;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString strDate, strTime;
//	GetSystemDateTime(&strDate, &strTime);
//	dir += "\\";
//	dir += strDate;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//
//	time_t start, end;
//	start = clock();
//	double FileSaveTime1, FileSaveTime2, FileSaveTime3;
//
//	m_strRawImageFileName.Format(_T("%s\\RawImageData_%d um_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
//
//	////////////// 11111111111111111111111111111111111
//	//2.Raw Image File Open 및 Header(Image종류,FOV,Pixel개수,...) 기록
//	
//
////	ofstream rawImageFile(m_strRawImageFileName.GetBuffer(0));
////	rawImageFile.precision(8);
////	rawImageFile.setf(ios_base::fixed, ios_base::floatfield);
////	rawImageFile << _T("[Header]") << "\n";
////	rawImageFile << _T("FILE TYPE: RAW: (1)") << "\n";
////	rawImageFile << m_nRawImage_PixelWidth << "\n";
////	rawImageFile << m_nRawImage_PixelHeight << "\n";
////
////	//3.Raw Image FOV 크기만큼 파일에 기록
////	rawImageFile << _T("[Image]") << "\n";	
////	for (int i=0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
////		rawImageFile << setprecision(3) << m_dSaveImage_DataNum[LASERINTERFEROMETER_X][i] << "\t" << m_dSaveImage_DataNum[LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dSaveImage_DataNum[DETECTOR_INTENSITY_1][i] << "\t" << m_dSaveImage_DataNum[DETECTOR_INTENSITY_2][i] << "\n";	
////
//////4.File Close -> 저장완료
////	rawImageFile << endl;
////	rawImageFile.close();
////	end = clock();
////
////	FileSaveTime1 = (double)(end - start);
//
//	////// 2///////////////////
//	char buf[1000];
//	int len = 0;
//
//	//m_strRawImageFileName = m_strRawImageFileName ;
//	start = clock();
//	HANDLE	hFile;
//	hFile = CreateFile(m_strRawImageFileName, GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
//
//	// FILE_ATTRIBUTE_NORMAL| FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING
//
//	//DWORD  flags = FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING;
//
//	sprintf(buf, "[Header]\r\n");
//	WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	sprintf(buf, "FILE TYPE: RAW: (1)\r\n");
//	WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	sprintf(buf, "%d\r\n", m_nRawImage_PixelWidth);
//	WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	sprintf(buf, "%d\r\n", m_nRawImage_PixelHeight);
//	WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	sprintf(buf, "[Image]\r\n", m_nRawImage_PixelHeight);
//	WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	for (int i=0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
//	{
//		sprintf(buf, "%.3f\t%.3f\t%.8f\t%.8f\r\n", m_dSaveImage_DataNum[LASERINTERFEROMETER_X][i], m_dSaveImage_DataNum[LASERINTERFEROMETER_Y][i], m_dSaveImage_DataNum[DETECTOR_INTENSITY_1][i], m_dSaveImage_DataNum[DETECTOR_INTENSITY_2][i]);
//		WriteFile(hFile, buf, strlen(buf), NULL, NULL);
//
//	}
//
//	CloseHandle(hFile);
//	end = clock();
//	FileSaveTime2 = (double)(end - start);
//	///////////////////////////////
//
//	return ret;
//}

int CADAMDlg::RawImageFileSave()
{
	int ret = 0;
	CString m_strRawImageFileName;
	CString m_strRawImageFileName_Nor_I0;
	CString m_strRawImageFileName_Nor_I0_Filter;

	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString dir;
	dir = IMAGEFILE_PATH;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	m_strRawImageFileName.Format(_T("%s\\RawImageData_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
	m_strRawImageFileName_Nor_I0.Format(_T("%s\\RawImageData_I0_Nor1_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
	m_strRawImageFileName_Nor_I0_Filter.Format(_T("%s\\RawImageData_I0_Nor2_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);


	FILE *fp;
	errno_t err;
	err = fopen_s(&fp, m_strRawImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: RAW: (1)\n");
	fprintf(fp, "%d\n", m_nRawImage_PixelWidth);
	fprintf(fp, "%d\n", m_nRawImage_PixelHeight);
	fprintf(fp, "[Image]\n", m_nRawImage_PixelHeight);

	for (int i = 0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
	{
		fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImageForDisplay[LASERINTERFEROMETER_X][i], m_dRawImageForDisplay[LASERINTERFEROMETER_Y][i], m_dRawImageForDisplay[DETECTOR_INTENSITY_1][i], m_dRawImageForDisplay[DETECTOR_INTENSITY_2][i]);
	}

	fclose(fp);


#if FALSE
	FILE *fp_I0;
	FILE *fp_I0_Average;

	if (bSaveNormalizedImage)
	{

		err = fopen_s(&fp_I0, m_strRawImageFileName_Nor_I0, "w");

		fprintf(fp_I0, "[Header]\n");
		fprintf(fp_I0, "FILE TYPE: RAW: (1)\n");
		fprintf(fp_I0, "%d\n", m_nRawImage_PixelWidth);
		fprintf(fp_I0, "%d\n", m_nRawImage_PixelHeight);
		fprintf(fp_I0, "[Image]\n", m_nRawImage_PixelHeight);


		err = fopen_s(&fp_I0_Average, m_strRawImageFileName_Nor_I0_Filter, "w");

		fprintf(fp_I0_Average, "[Header]\n");
		fprintf(fp_I0_Average, "FILE TYPE: RAW: (1)\n");
		fprintf(fp_I0_Average, "%d\n", m_nRawImage_PixelWidth);
		fprintf(fp_I0_Average, "%d\n", m_nRawImage_PixelHeight);
		fprintf(fp_I0_Average, "[Image]\n", m_nRawImage_PixelHeight);

		for (int i = 0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
		{
			fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImageForDisplay[LASERINTERFEROMETER_X][i], m_dRawImageForDisplay[LASERINTERFEROMETER_Y][i], m_dRawImageForDisplay[D_NOR_I0][i], m_dRawImageForDisplay[I0][i]);
			fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImageForDisplay[LASERINTERFEROMETER_X][i], m_dRawImageForDisplay[LASERINTERFEROMETER_Y][i], m_dRawImageForDisplay[D_NOR_I0_FILTER][i], m_dRawImageForDisplay[I0_FILTERED][i]);
		}

		fclose(fp_I0);
		fclose(fp_I0_Average);
	}

#endif
	return ret;
}

int CADAMDlg::RawImageFileSave3D(int scanBufIndex)
{
	int ret = 0;
	CString m_strRawImageFileName;
	CString m_strRawImageFileName_Nor_I0;
	CString m_strRawImageFileName_Nor_I0_Filter;

	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString dir;
	dir = IMAGEFILE_PATH;
	if (!g_pLog->IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!g_pLog->IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	//time_t start, end;
	//start = clock();
	//double FileSaveTime1, FileSaveTime2, FileSaveTime3;

	if (scanBufIndex == -1)
	{
		m_strRawImageFileName.Format(_T("%s\\RawImageData_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
		m_strRawImageFileName_Nor_I0.Format(_T("%s\\RawImageData_I0_Nor1_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
		m_strRawImageFileName_Nor_I0_Filter.Format(_T("%s\\RawImageData_I0_Nor2_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
	}
	else
	{
		m_strRawImageFileName.Format(_T("%s\\RawImageData_ScanNo_%03d_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, scanBufIndex, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
		m_strRawImageFileName_Nor_I0.Format(_T("%s\\RawImageData_I0_Nor1_ScanNo_%03d_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, scanBufIndex, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
		m_strRawImageFileName_Nor_I0_Filter.Format(_T("%s\\RawImageData_I0_Nor2_ScanNo_%03d_%d nm_%d nm_%d_%d_%d_%d_%d_%d.txt"), dir, scanBufIndex, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, year, month, day, hour, minute, second);
	}

	FILE *fp;
	errno_t err;
	err = fopen_s(&fp, m_strRawImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: RAW: (1)\n");
	fprintf(fp, "%d\n", m_nRawImage_PixelWidth);
	fprintf(fp, "%d\n", m_nRawImage_PixelHeight);
	fprintf(fp, "[Image]\n", m_nRawImage_PixelHeight);


	for (int i = 0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
	{
		fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_X][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_Y][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][DETECTOR_INTENSITY_1][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][DETECTOR_INTENSITY_2][i]);
	}

	fclose(fp);

	FILE *fp_I0;
	FILE *fp_I0_Average;

	if (bSaveNormalizedImage)
	{

		err = fopen_s(&fp_I0, m_strRawImageFileName_Nor_I0, "w");

		fprintf(fp_I0, "[Header]\n");
		fprintf(fp_I0, "FILE TYPE: RAW: (1)\n");
		fprintf(fp_I0, "%d\n", m_nRawImage_PixelWidth);
		fprintf(fp_I0, "%d\n", m_nRawImage_PixelHeight);
		fprintf(fp_I0, "[Image]\n", m_nRawImage_PixelHeight);

		err = fopen_s(&fp_I0_Average, m_strRawImageFileName_Nor_I0_Filter, "w");

		fprintf(fp_I0_Average, "[Header]\n");
		fprintf(fp_I0_Average, "FILE TYPE: RAW: (1)\n");
		fprintf(fp_I0_Average, "%d\n", m_nRawImage_PixelWidth);
		fprintf(fp_I0_Average, "%d\n", m_nRawImage_PixelHeight);
		fprintf(fp_I0_Average, "[Image]\n", m_nRawImage_PixelHeight);

		for (int i = 0; i < m_nRawImage_PixelWidth*m_nRawImage_PixelHeight; i++)
		{
			fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_X][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_Y][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][D_NOR_I0][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][I0][i]);
			fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_X][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][LASERINTERFEROMETER_Y][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][D_NOR_I0_FILTER][i], m_dRawImage_Data3D[scanBufIndex % MAX_SCAN_MEMORY_NUMBER][I0_FILTERED][i]);
		}

		fclose(fp_I0);
		fclose(fp_I0_Average);
	}

	return ret;
}

//
//int CADAMDlg::RegridImageFileSave()
//{
//	int ret = 0;
//	int i = 0;
//
//	// Interpolated image pixel number 구하는 부분
//	double FOV = m_nEuvImage_Fov * 1000;										//field of view
//	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
//	int pixelsize = int(FOV / grid_step);
//
//	// Interpolation 된 데이터 저장하자...
//	//memcpy(m_dSaveImage_DataNum[LASERINTERFEROMETER_X], m_dReconstructedImage_Data[LASERINTERFEROMETER_X], sizeof(double) * pixelsize * pixelsize);			// 수정필요... KYD
//	//memcpy(m_dSaveImage_DataNum[LASERINTERFEROMETER_Y], m_dReconstructedImage_Data[LASERINTERFEROMETER_Y], sizeof(double) * pixelsize * pixelsize);			// 수정필요... KYD
//	//memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_1], m_dReconstructedImage_Data[DETECTOR_INTENSITY_1], sizeof(double) * pixelsize * pixelsize);			// 수정필요... KYD
//	//memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_1], m_dReconstructedImage_Data[DETECTOR_INTENSITY_1], sizeof(double) * pixelsize * pixelsize);			// 수정필요... KYD
//	//memcpy(m_dSaveImage_DataNum[DETECTOR_INTENSITY_2], m_dReconstructedImage_Data[DETECTOR_INTENSITY_2], sizeof(double) * pixelsize * pixelsize);			// 수정필요... KYD
//
//	//1.저장할 폴더 날짜별로 생성
//	int year, month, day, hour, minute, second;
//	year = CTime::GetCurrentTime().GetYear();
//	month = CTime::GetCurrentTime().GetMonth();
//	day = CTime::GetCurrentTime().GetDay();
//	hour = CTime::GetCurrentTime().GetHour();
//	minute = CTime::GetCurrentTime().GetMinute();
//	second = CTime::GetCurrentTime().GetSecond();
//
//	CString dir;
//	dir = IMAGEFILE_PATH;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString strDate, strTime;
//	GetSystemDateTime(&strDate, &strTime);
//	dir += "\\";
//	dir += strDate;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//
//	//2.Raw Image File Open 및 Header(Image종류,FOV,Pixel개수,...) 기록
//	m_strRegirdImageFileName.Format(_T("%s\\RegridImageData_%d um_Grid %d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEUVImage_InterpolationGrid, year, month, day, hour, minute, second);
//	ofstream RegirdImageFile(m_strRegirdImageFileName.GetBuffer(0));
//	RegirdImageFile.precision(8);
//	RegirdImageFile.setf(ios_base::fixed, ios_base::floatfield);
//	RegirdImageFile << _T("[Header]") << "\n";
//	RegirdImageFile << _T("FILE TYPE: Regrid: (2)") << "\n";
//	RegirdImageFile << pixelsize << "\n";
//	RegirdImageFile << pixelsize << "\n";
//
//	//3.Raw Image FOV 크기만큼 파일에 기록
//	RegirdImageFile << _T("[Image]") << "\n";
//	//rawImageFile << _T("X") << "\t" << _T("Y") << "\t" << _T("INTENSITY_1") << "\n";  //Image load 함수에 맞춰서 그냥 제거 했음 나중에 수정 필요
//	//rawImageFile << _T("-------------------------------------------------------------------") << "\n";  // 그냥 한번 넣어봤음... KYD
//	for (i; i < pixelsize*pixelsize; i++)		// 수정필요... KYD
//	{
//		RegirdImageFile << setprecision(3) << m_dReconstructedImage_Data[LASERINTERFEROMETER_X][i] << "\t" << m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i] << "\t" << m_dReconstructedImage_Data[DETECTOR_INTENSITY_2][i] << "\n";
//		//RegirdImageFile << setprecision(3) << m_dReconstructedImage_Data[LASERINTERFEROMETER_X][i] << "\t" << m_dReconstructedImage_Data[LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dReconstructedImage_Data[DETECTOR_INTENSITY_1][i] << "\n";
//		//RegirdImageFile << setprecision(3) << m_dSaveImage_DataNum[LASERINTERFEROMETER_X][i] << "\t" << m_dSaveImage_DataNum[LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dSaveImage_DataNum[DETECTOR_INTENSITY_1][i] << "\t" << m_dSaveImage_DataNum[DETECTOR_INTENSITY_2][i] << "\n";
//	}
//	//4.File Close -> 저장완료
//	RegirdImageFile << endl;
//	RegirdImageFile.close();
//
//
//	return ret;
//}
//
//int CADAMDlg::AveragedImageFileSave()
//{
//	int ret = 0;
//	int i = 0;
//
//	// Interpolated image pixel number 구하는 부분
//	double FOV = m_nEuvImage_Fov * 1000;										//field of view
//	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
//	int pixelsize = int(FOV / grid_step);
//
//	//1.저장할 폴더 날짜별로 생성
//	int year, month, day, hour, minute, second;
//	year = CTime::GetCurrentTime().GetYear();
//	month = CTime::GetCurrentTime().GetMonth();
//	day = CTime::GetCurrentTime().GetDay();
//	hour = CTime::GetCurrentTime().GetHour();
//	minute = CTime::GetCurrentTime().GetMinute();
//	second = CTime::GetCurrentTime().GetSecond();
//
//	CString dir;
//	dir = IMAGEFILE_PATH;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString strDate, strTime;
//	GetSystemDateTime(&strDate, &strTime);
//	dir += "\\";
//	dir += strDate;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//
//	//2.Raw Image File Open 및 Header(Image종류,FOV,Pixel개수,...) 기록
//	m_strAveragedImageFileName.Format(_T("%s\\AveragedImageData_%d um_Grid %d nm_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEUVImage_InterpolationGrid, year, month, day, hour, minute, second);
//	ofstream AverageImageFile(m_strAveragedImageFileName.GetBuffer(0));
//	AverageImageFile.precision(8);
//	AverageImageFile.setf(ios_base::fixed, ios_base::floatfield);
//	AverageImageFile << _T("[Header]") << "\n";
//	AverageImageFile << _T("FILE TYPE: Averaged: (2)") << "\n";
//	AverageImageFile << pixelsize << "\n";
//	AverageImageFile << pixelsize << "\n";
//
//	//3.Raw Image FOV 크기만큼 파일에 기록
//	AverageImageFile << _T("[Image]") << "\n";
//
//	for (i; i < pixelsize*pixelsize; i++)		// 수정필요... KYD
//	{
//		AverageImageFile << setprecision(3) << m_dAveragedImage_Data[LASERINTERFEROMETER_X][i] << "\t" << m_dAveragedImage_Data[LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dAveragedImage_Data[DETECTOR_INTENSITY_1][i] << "\t" << m_dAveragedImage_Data[DETECTOR_INTENSITY_2][i] << "\n";
//	}
//	//4.File Close -> 저장완료
//	AverageImageFile << endl;
//	AverageImageFile.close();
//
//	return ret;
//}
////
////-------------
//int CADAMDlg::FilteredImageFileSave(int nDefectNo)
//{
//	int ret = 0;
//	CString m_strFilteredImageFileName;
//
//	int i = 0;
//
//	// Interpolated image pixel number 구하는 부분
//	double FOV = m_nEuvImage_Fov;// *1000;										//field of view
//	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
//	int pixelsize = int(FOV / grid_step);
//
//	//1.저장할 폴더 날짜별로 생성
//	int year, month, day, hour, minute, second;
//	year = CTime::GetCurrentTime().GetYear();
//	month = CTime::GetCurrentTime().GetMonth();
//	day = CTime::GetCurrentTime().GetDay();
//	hour = CTime::GetCurrentTime().GetHour();
//	minute = CTime::GetCurrentTime().GetMinute();
//	second = CTime::GetCurrentTime().GetSecond();
//
//	CString dir;
//	dir = IMAGEFILE_PATH;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString strDate, strTime;
//	GetSystemDateTime(&strDate, &strTime);
//	dir += "\\";
//	dir += strDate;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//
//	//2.Raw Image File Open 및 Header(Image종류,FOV,Pixel개수,...) 기록
//	//m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_%d nm_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, m_nEuvImage_Fov, m_nEUVImage_InterpolationGrid, m_dCurrentImagePosition_Cap1, year, month, day, hour, minute, second);
//	//m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No"":"" %d_%d nm_Grid %d nm_Cap1Val"":"" %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, m_nEuvImage_Fov, m_nEUVImage_InterpolationGrid, m_dCurrentImagePosition_Cap2, year, month, day, hour, minute, second);
//	m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No %d_%d nm_Grid %d nm_Cap2Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, m_nEuvImage_Fov, m_nEUVImage_InterpolationGrid, m_dCurrentImagePosition_Cap2, year, month, day, hour, minute, second);
//
//	ofstream FilteredImageFile(m_strFilteredImageFileName.GetBuffer(0));
//	FilteredImageFile.precision(8);
//	FilteredImageFile.setf(ios_base::fixed, ios_base::floatfield);
//	FilteredImageFile << _T("[Header]") << "\n";
//	FilteredImageFile << _T("FILE TYPE: Filtered: (2)") << "\n";
//	FilteredImageFile << pixelsize << "\n";
//	FilteredImageFile << pixelsize << "\n";
//
//	//3.Raw Image FOV 크기만큼 파일에 기록
//	FilteredImageFile << _T("[Image]") << "\n";
//
//	for (i; i < pixelsize*pixelsize; i++)		// 수정필요... KYD
//	{
//		FilteredImageFile << setprecision(3) << m_dFilteredImage_Data[][LASERINTERFEROMETER_X][i] << "\t" << m_dFilteredImage_Data[][LASERINTERFEROMETER_Y][i] << "\t" << setprecision(8) << m_dFilteredImage_Data[][DETECTOR_INTENSITY_1][i] << "\t" << m_dFilteredImage_Data[][DETECTOR_INTENSITY_2][i] << "\n";
//	}
//	//4.File Close -> 저장완료
//	FilteredImageFile << endl;
//	FilteredImageFile.close();
//
//	return ret;
//}

//  filtered image를 사이즈 조정하여 저장하는 코드
//int CADAMDlg::FilteredImageFileSave_Resize(int removeEdge, int nDefectNo)
//{	
//	int ret = 0;
//	CString m_strFilteredImageFileName;
//	CString m_strFilteredImageFileName_IO_Normalized;
//
//	// Interpolated image pixel number 구하는 부분
//	double FOV = m_nEuvImage_Fov;// * 1000;										//field of view
//	double Re_EUV_FOV = double(m_nEuvImage_Fov/1000.) - 0.1;
//
//
//	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
//	int pixelsize = int(FOV / grid_step);
//
//	int i = 0;
//	int k = 0;
//	int pixelsize_R = pixelsize - removeEdge;
//
//	//1.저장할 폴더 날짜별로 생성
//	int year, month, day, hour, minute, second;
//	year = CTime::GetCurrentTime().GetYear();
//	month = CTime::GetCurrentTime().GetMonth();
//	day = CTime::GetCurrentTime().GetDay();
//	hour = CTime::GetCurrentTime().GetHour();
//	minute = CTime::GetCurrentTime().GetMinute();
//	second = CTime::GetCurrentTime().GetSecond();
//
//	CString dir;
//	dir = IMAGEFILE_PATH;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//	CString strDate, strTime;
//	GetSystemDateTime(&strDate, &strTime);
//	dir += "\\";
//	dir += strDate;
//	if (!g_pLog->IsFolderExist(dir))
//	{
//		CreateDirectory(dir, NULL);
//	}
//
//	m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No %d_%0.1f um_Grid %d nm_Cap2Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, AdamData.m_dCapsensor2, year, month, day, hour, minute, second);
//	m_strFilteredImageFileName_IO_Normalized.Format(_T("%s\\I0_FilteredImageData_No %d_%0.1f um_Grid %d nm_Cap2Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, AdamData.m_dCapsensor2, year, month, day, hour, minute, second);
//	
//	FILE *fp;
//	errno_t err;
//	err = fopen_s(&fp, m_strFilteredImageFileName, "w");
//
//	fprintf(fp, "[Header]\n");
//	fprintf(fp, "FILE TYPE: Filtered: (2)\n");	
//	fprintf(fp, "%d\n", pixelsize_R);	
//	fprintf(fp, "%d\n", pixelsize_R);
//	fprintf(fp, "[Image]\n");
//	
//	for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
//	{
//		for (k = 0; k < pixelsize_R; k++)
//		{
//			fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dFilteredImage_Data[][LASERINTERFEROMETER_X][i*pixelsize + k], m_dFilteredImage_Data[][LASERINTERFEROMETER_Y][i*pixelsize + k], m_dFilteredImage_Data[][DETECTOR_INTENSITY_1][i*pixelsize + k], m_dFilteredImage_Data[][DETECTOR_INTENSITY_2][i*pixelsize + k]);			
//		}
//	}
//
//	fclose(fp);		
//	////// I0 Normalized Image Save
//	if (bSaveNormalizedImage)
//	{
//
//		err = fopen_s(&fp, m_strFilteredImageFileName_IO_Normalized, "w");
//
//		fprintf(fp, "[Header]\n");
//		fprintf(fp, "FILE TYPE: Filtered: (2)\n");
//		fprintf(fp, "%d\n", pixelsize_R);
//		fprintf(fp, "%d\n", pixelsize_R);
//		fprintf(fp, "[Image]\n");
//
//		for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
//		{
//			for (k = 0; k < pixelsize_R; k++)
//			{
//				fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", m_dFilteredImage_Data[][LASERINTERFEROMETER_X][i*pixelsize + k], m_dFilteredImage_Data[][LASERINTERFEROMETER_Y][i*pixelsize + k], m_dFilteredImage_Data[][DETECTOR_INTENSITY_1_NORMALIZED_REGRID][i*pixelsize + k], m_dFilteredImage_Data[][DETECTOR_INTENSITY_1_NORMALIZED_REGRID][i*pixelsize + k]);
//			
//}
//		}
//
//		fclose(fp);		
//	}
//		
//	return ret;
//}

#if FALSE

void CADAMDlg::FilteredImageFileSave_Resize(int removeEdge, int nDefectNo, double **ImageBuffer, double capPos)
{
	CString m_strFilteredImageFileName;
	CString m_strFilteredImageFileName_Nor_I0;
	CString m_strFilteredImageFileName_Nor_I0_Filter;
	//CString m_strFilteredImageFileName_IO_Normalized;

	// Interpolated image pixel number 구하는 부분
	double FOV = m_nEuvImage_Fov;// *1000;										//field of view
	double Re_EUV_FOV = double(m_nEuvImage_Fov / 1000.) - 0.1;


	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
	int pixelsize = int(FOV / grid_step);

	int i = 0;
	int k = 0;
	int pixelsize_R = pixelsize - removeEdge;

	//1.저장할 폴더 날짜별로 생성
	int year, month, day, hour, minute, second;
	year = CTime::GetCurrentTime().GetYear();
	month = CTime::GetCurrentTime().GetMonth();
	day = CTime::GetCurrentTime().GetDay();
	hour = CTime::GetCurrentTime().GetHour();
	minute = CTime::GetCurrentTime().GetMinute();
	second = CTime::GetCurrentTime().GetSecond();

	CString dir;
	dir = IMAGEFILE_PATH;
	if (!g_pLog->IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}
	CString strDate, strTime;
	GetSystemDateTime(&strDate, &strTime);
	dir += "\\";
	dir += strDate;
	if (!g_pLog->IsFolderExist(dir))
	{
		CreateDirectory(dir, NULL);
	}

	m_strFilteredImageFileName.Format(_T("%s\\FilteredImageData_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);
	m_strFilteredImageFileName_Nor_I0.Format(_T("%s\\FilteredImageData_I0_Nor1_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);
	m_strFilteredImageFileName_Nor_I0_Filter.Format(_T("%s\\FilteredImageData_I0_Nor2_No_%03d_%0.1f um_Grid %d nm_Cap1Val %0.2f_%d_%d_%d_%d_%d_%d.txt"), dir, nDefectNo, Re_EUV_FOV, m_nEUVImage_InterpolationGrid, capPos, year, month, day, hour, minute, second);

	FILE *fp;
	errno_t err;
	err = fopen_s(&fp, m_strFilteredImageFileName, "w");

	fprintf(fp, "[Header]\n");
	fprintf(fp, "FILE TYPE: Filtered: (2)\n");
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "%d\n", pixelsize_R);
	fprintf(fp, "[Image]\n");

	for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
	{
		for (k = 0; k < pixelsize_R; k++)
		{
			//fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
			fprintf(fp, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D1_REGRID][i*pixelsize + k], 0);
		}
	}

	fclose(fp);
	////// I0 Normalized Image Save

	FILE *fp_I0;
	FILE *fp_I0_Average;
	if (bSaveNormalizedImage)
	{

		err = fopen_s(&fp_I0, m_strFilteredImageFileName_Nor_I0, "w");

		fprintf(fp_I0, "[Header]\n");
		fprintf(fp_I0, "FILE TYPE: Filtered: (2)\n");
		fprintf(fp_I0, "%d\n", pixelsize_R);
		fprintf(fp_I0, "%d\n", pixelsize_R);
		fprintf(fp_I0, "[Image]\n");


		err = fopen_s(&fp_I0_Average, m_strFilteredImageFileName_Nor_I0_Filter, "w");

		fprintf(fp_I0_Average, "[Header]\n");
		fprintf(fp_I0_Average, "FILE TYPE: Filtered: (2)\n");
		fprintf(fp_I0_Average, "%d\n", pixelsize_R);
		fprintf(fp_I0_Average, "%d\n", pixelsize_R);
		fprintf(fp_I0_Average, "[Image]\n");

		for (i = removeEdge; i < pixelsize; i++)		// 수정필요... KYD
		{
			for (k = 0; k < pixelsize_R; k++)
			{
				//fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
				//fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k]);
				fprintf(fp_I0, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_REGRID][i*pixelsize + k], 0);
				fprintf(fp_I0_Average, "%.3f\t%.3f\t%.8f\t%.8f\n", ImageBuffer[X_REGRID][i*pixelsize + k], ImageBuffer[Y_REGRID][i*pixelsize + k], ImageBuffer[D_NOR_I0_FILTER_REGRID][i*pixelsize + k], 0);
			}
		}

		fclose(fp_I0);
		fclose(fp_I0_Average);
	}

}
//
#endif
//---------------------------------------------
// 뭔가 꼬여있음.. 수정해야함.. 김영덕_ 20191204
int CADAMDlg::RawImageBMPFileSave()		// Normalized 된 이미지 데이터를 BMP로 저장하도록 함
{
	int ret = 0;

	//	if (m_MilDisplayGray == M_NULL)
	//		return -1;
	//

	//	int i = 0;
	//
	//	//1.저장할 폴더 날짜별로 생성
	//	int year, month, day, hour, minute, second;
	//	year = CTime::GetCurrentTime().GetYear();
	//	month = CTime::GetCurrentTime().GetMonth();
	//	day = CTime::GetCurrentTime().GetDay();
	//	hour = CTime::GetCurrentTime().GetHour();
	//	minute = CTime::GetCurrentTime().GetMinute();
	//	second = CTime::GetCurrentTime().GetSecond();
	//
	//	CString dir;
	//	dir = IMAGEFILE_PATH;
	//	if (!g_pLog->IsFolderExist(dir))
	//	{
	//		CreateDirectory(dir, NULL);
	//	}
	//	CString strDate, strTime;
	//	GetSystemDateTime(&strDate, &strTime);
	//	dir += "\\";
	//	dir += strDate;
	//	if (!g_pLog->IsFolderExist(dir))
	//	{
	//		CreateDirectory(dir, NULL);
	//	}
	//
	//	////2.Raw Image BMP File Open 및 BMP 파일로 저장
	//	//m_strRawImageBMPFileName.Format(_T("%s\\TestImage_%d_%d_%d_%d_%d_%d.bmp"), dir, year, month, day, hour, minute, second);
	//	////MbufAllocColor(m_MilSystem, 3, width, height, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilDisplayColor);
	//	//MbufAlloc2d(m_MilSystem, m_nRawImage_PixelHeight, m_nRawImage_PixelWidth, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilDisplayColor);
	//	//MbufPut2d(m_MilDisplayColor, 0L, 0L, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight, m_pNormalized_Image_buffer);				// 이부분은 나중에 수정 필요함
	//	//MbufExport(m_strRawImageBMPFileName.GetBuffer(0), M_BMP, m_MilDisplayColor);
	//
	//	//2.Raw Image BMP File Open 및 BMP 파일로 저장
	//	m_strRawImageBMPFileName.Format(_T("%s\\TestImage_%d_%d_%d_%d_%d_%d.bmp"), dir, year, month, day, hour, minute, second);
	//	MbufAllocColor(m_MilSystem, 3, 2000, 2000, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilSaveBmpImage);
	//	//MbufAllocColor(m_MilSystem, 3, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight, 8L + M_UNSIGNED, M_IMAGE + M_PROC + M_DIB + M_GDI + M_DISP, &m_MilSaveBmpImage);
	//	MbufCopy(m_MilDisplayColor, m_MilSaveBmpImage);
	//	//MbufCopyClip(m_MilDisplayColor, m_MilSaveBmpImage, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight);
	//	MbufExport(m_strRawImageBMPFileName.GetBuffer(0), M_BMP, m_MilSaveBmpImage);
	//
	//	MbufFree(m_MilSaveBmpImage);
	//	m_MilSaveBmpImage = M_NULL;
	//
	//	CAutoMessageDlg MsgBoxAuto(g_pAdam);
	//	MsgBoxAuto.DoModal(_T(" BMP Image가 생성되었습니다! "), 2);

	return ret;
}

//
//BOOL threadEnd = FALSE;
//void CADAMDlg::ManualScanThreadFun()
//{
//	deque<std::thread> PointMeasureThreadQue;
//
//	// ADAM Scan Run 
//	g_pAdam->AdamUIControl(FALSE);
//	g_pAdam->m_IsMeasureComplete = FALSE;
//	g_pAdam->m_bAbort = FALSE;
//
//	int TotalScanNum = m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
//	int PointMeasureBufIndex;
//	vector <int> vecScanBufIndex;
//	vecScanBufIndex.reserve(TotalScanNum);
//	MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);
//
//	g_pAdam->m_bIsScaningOn = FALSE; //Adam 시작후 스캔 스테이지 움직여야함
//
//	PointMeasureThreadQue.emplace_back([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, 0, 1, TotalScanNum, 0, 1); });
//
//	SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
//
//	// Adam Start 기다림
//	{
//		unique_lock<mutex> lock(mutexAdamStart);
//		cvAdamStart.wait(lock, [this] { return IsAdamStart; });
//		IsAdamStart = FALSE;
//	}
//
//	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// Scan Stage Run
//
//	// Adam Stop 기다림
//	{
//		unique_lock<mutex> lock(mutexAdamEnd);
//		cvAdamEnd.wait(lock, [this] { return IsAdamEnd; });
//		IsAdamEnd = FALSE; // 초기화 어디서? 
//	}
//
//	//제거
//	/*while (!g_pAdam->m_IsMeasureComplete)
//	{
//		WaitSec(1);
//	}
//*/
//	// Thread 종료 기다림
//	for (thread& pointMeasureThread : PointMeasureThreadQue)
//	{
//		pointMeasureThread.join();
//	}
//
//	g_pAdam->AdamUIControl(TRUE);
//
//	threadEnd = TRUE;
//}


void CADAMDlg::OnBnClickedAdamRunButton()
{
	g_pLog->Display(0, _T("CADAMDlg::OnBnClickedAdamRunButton() Button Click!"));

	if (g_pConfig->m_nEquipmentMode == OFFLINE && !m_bConnected)
		return;

#if FALSE
	// New version
	if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV)
	{
		//threadEnd = FALSE;
		//thread ManualScanThread = thread([=] {ManualScanThreadFun();});
		//	
		////ManualScanThread.detach();

		//while (!threadEnd)
		//{
		//	WaitSec(1);
		//}


		deque<std::thread> PointMeasureThreadQue;

		// ADAM Scan Run 
		g_pAdam->AdamUIControl(FALSE);
		g_pAdam->m_IsMeasureComplete = FALSE;
		g_pAdam->m_bAbort = FALSE;

		int TotalScanNum = m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
		int PointMeasureBufIndex;
		vector <int> vecScanBufIndex;
		vecScanBufIndex.reserve(TotalScanNum);
		MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

		g_pAdam->m_bIsScaningOn = FALSE; //Adam 시작후 스캔 스테이지 움직여야함

		PointMeasureThreadQue.emplace_back([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, 0, 1, TotalScanNum, 0, 1); });
		//thread PointMeasureThread = thread([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, 0, 1, TotalScanNum, 0, 1); });

		SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);

		//Scanning (Scan Stage) //Adam 시작후 스캔 스테이지 움직여야함

		// Adam 시작된거 확인후..스캔 시작해야함..
		{
			unique_lock<mutex> lock(mutexAdamStart);
			cvAdamStart.wait(lock, [this] { return IsAdamStart; });			
			IsAdamStart = FALSE; 
		}

		//while (!g_pAdam->m_bIsScaningOn)
		//{
		//	//WaitSec(1);
		//}
		//WaitSec(0.1);

		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// Scan Stage Run

		int test = 0;

		while (!threadEnd)
		{
			WaitSec(1);
		}

		//11. Scan 종료(Adam에서 데이터 받아짐, Adam Stop. 기다림 삭제 해야함)
		{
			unique_lock<mutex> lock(mutexAdamEnd);
			cvAdamEnd.wait(lock, [this] { return IsAdamEnd; });			
			IsAdamEnd = FALSE; // 초기화 어디서? 
		}

		/*while (g_pAdam->m_bIsScaningOn)
		{
			WaitSec(1);
		}*/
		
		
	

		while (!g_pAdam->m_IsMeasureComplete)
		{
			WaitSec(1);
		}
		while (!threadEnd)
		{
			WaitSec(1);
		}
		//PointMeasureThread.join();
		for (thread& pointMeasureThread : PointMeasureThreadQue)
		{
			pointMeasureThread.join();
		}


		g_pAdam->AdamUIControl(TRUE);

		g_pAdam->Invalidate(FALSE);

	}
	// Old version
	else
#endif
	{
		
		
		// ADAM Scan Run 	
		ADAMRunStart();
		SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);
		// Scan Stage Run	if (g_pScanStage != NULL)	
		g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// Scan Stage Run
	}
}

void CADAMDlg::OnBnClickedAdamStopButton()
{
	//Scan Stage 정지
	g_pScanStage->Stop_Scan();

	//ADAM 통신 정지 및 Laser Interferometer Reset
	ADAMAbort();


	GetDlgItem(IDC_ADAM_SCAN)->EnableWindow(TRUE);

	AfxMessageBox("ADAM이 강제 종료되었습니다");
}

void CADAMDlg::OnBnClickedAdamLifresetButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	// LIF 리셋 함수를 추가해야함_ KYD, 이전에 팀장님 코드에서 버튼 막혀있는거 지우고 새로추가_20191206
	//AfxMessageBox("LIF reset button clicked");
	Command_LIFReset();
	// LIF 리셋 함수는 CADAMCtrl에 구현되어 있음

	//int i = 0;
	//for (i = 0; i < KINDS_OF_ADAMDATA; i++)
	//	memset(m_dRawImage_Data[i], 0, sizeof(double) * ADAM_MAX_DATA);

}

void CADAMDlg::OnBnClickedAdamOresetButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	Command_OpticSensorReset();
}


// 로직이 어떻게 되는거지?
void CADAMDlg::OnBnClickedImageLoadButton()
{
	CString strPath = _T("");

#if FALSE
	string strHeader = _T("");
	string strFileType = _T("");
	string strImageHeader = _T("");
	ifstream fileopen;
	string readStr;
	int i = 0, j = 0;
	string strTypeRaw = _T("RAW");
	string strTypeFiltered = _T("Filtered");

#endif
	CFileDialog FileDlg(TRUE, _T("*.*"), _T("*.txt"), OFN_FILEMUSTEXIST | OFN_HIDEREADONLY, _T("*.txt"), this);

	int fileType = -1;
	int width;
	int height;

	char strHeader[20];
	char strFileType[30];
	char strImageHeader[20];


	if (FileDlg.DoModal() == IDOK)
	{
		strPath = FileDlg.GetPathName();

		FILE* fp = fopen(strPath, "r");

		fgets(strHeader, 20, fp);
		fgets(strFileType, 30, fp);

		fscanf(fp, "%d", &width); // 한줄이 안넘어감
		fscanf(fp, "%d", &height);// 한줄이 안넘어감
		fscanf(fp, "%s", strHeader);

		if (strstr(strFileType, "RAW") != NULL)
		{
			fileType = DISP_RAW_IMAGE;

			m_nRawImage_PixelWidth = width;
			m_nRawImage_PixelHeight = height;

			for (int i = 0; i < m_nRawImage_PixelHeight*m_nRawImage_PixelWidth; i++)
			{
				fscanf(fp, "%lf", &m_dRawImageForDisplay[LASERINTERFEROMETER_X][i]);
				fscanf(fp, "%lf", &m_dRawImageForDisplay[LASERINTERFEROMETER_Y][i]);
				fscanf(fp, "%lf", &m_dRawImageForDisplay[DETECTOR_INTENSITY_1][i]);
				fscanf(fp, "%lf", &m_dRawImageForDisplay[DETECTOR_INTENSITY_2][i]);
			}
		}

		else if (strstr(strFileType, "Filtered") != NULL)
		{
			fileType = DISP_REGRID_IMAGE;

			m_nReGridImage_PixelWidth = width;
			m_nReGridImage_PixelHeight = height;

			for (int i = 0; i < m_nReGridImage_PixelWidth*m_nReGridImage_PixelHeight; i++)
			{
				fscanf(fp, "%lf", &m_dFilteredImage_Data3D[0][X_REGRID][i]);
				fscanf(fp, "%lf", &m_dFilteredImage_Data3D[0][Y_REGRID][i]);
				fscanf(fp, "%lf", &m_dFilteredImage_Data3D[0][D1_REGRID][i]);
				fscanf(fp, "%lf", &m_dFilteredImage_Data3D[0][D2_REGRID][i]);
			}
		}
		fclose(fp);
		
		if (g_pConfig->m_nEquipmentMode == OFFLINE)
			InitAdamDlgForScan();

		if (fileType == DISP_RAW_IMAGE)
		{
			DisplayRawImage();
		}
		else if (fileType == DISP_REGRID_IMAGE)
		{
			DisplayFilteredImage(m_dFilteredImage_Data3D[0][D1_REGRID]);
		}


#if FALSE

		fileopen.open(strPath.GetBuffer(0));

		getline(fileopen, strHeader);
		getline(fileopen, strFileType);

		getline(fileopen, readStr);
		width = atoi(readStr.c_str());
		getline(fileopen, readStr);
		height = atoi(readStr.c_str());

		getline(fileopen, strImageHeader);

		int test = strFileType.find(strTypeRaw);
		int test2 = strFileType.find(strTypeFiltered);

		if ((int)strFileType.find(strTypeRaw) >= 0)
		{
			fileType = DISP_RAW_IMAGE;

			m_nRawImage_PixelWidth = width;
			m_nRawImage_PixelHeight = height;

			for (i = 0; i < m_nRawImage_PixelHeight*m_nRawImage_PixelWidth; i++)
			{
				fileopen >> m_dRawImage_Data[LASERINTERFEROMETER_X][i];
				fileopen >> m_dRawImage_Data[LASERINTERFEROMETER_Y][i];
				fileopen >> m_dRawImage_Data[DETECTOR_INTENSITY_1][i];
				fileopen >> m_dRawImage_Data[DETECTOR_INTENSITY_2][i];
			}
		}
		else if ((int)strFileType.find(strTypeFiltered) >= 0)
		{
			fileType = DISP_REGRID_IMAGE;

			m_nReGridImage_PixelWidth = width;
			m_nReGridImage_PixelHeight = height;

			for (i = 0; i < m_nReGridImage_PixelWidth*m_nReGridImage_PixelHeight; i++)
			{
				fileopen >> m_dFilteredImage_Data[][LASERINTERFEROMETER_X][i];
				fileopen >> m_dFilteredImage_Data[][LASERINTERFEROMETER_Y][i];
				fileopen >> m_dFilteredImage_Data[][DETECTOR_INTENSITY_1][i];
				fileopen >> m_dFilteredImage_Data[][DETECTOR_INTENSITY_2][i];
			}
		}

		fileopen.close();

		if (fileType == DISP_RAW_IMAGE)
		{
			DisplayRawImage();
		}
		else if (fileType == DISP_REGRID_IMAGE)
		{
			DisplayFilteredImage(m_dFilteredImage_Data[][DETECTOR_INTENSITY_1]);
		}
#endif
	}
}

void CADAMDlg::OnBnClickedImageSaveButton()
{
	RawImageBMPFileSave();
}

//int CADAMDlg::TestImageLoading()
//{
//	//int jump = 20;//nm
//	//float cd = 88.0;//88nm L&S
//	//int height = 3000;//3um
//	//int width = 3000;//3um
//	//int height_data_no = height / jump;//150ea
//	//int width_data_no = width / jump;//150ea
//	//float pi = 3.141592;//PI
//
//	//float *raw_image = (float*)malloc(height_data_no*width_data_no * sizeof(float));
//	//float *heightval = (float*)malloc(height_data_no*width_data_no * sizeof(float));
//	//float *widthval = (float*)malloc(height_data_no*width_data_no * sizeof(float));
//
//	//FILE *fp = NULL;
//	//char tmpath[512];
//	//sprintf(tmpath, "c:\\EUVSolution\\TestData.txt");
//	//fp = fopen(tmpath, "r");
//
//	//int i = 0;
//	//int j = 0;
//	//for (i = 0; i < height_data_no * width_data_no; i++)
//	//	fscanf(fp, "%lf\t%lf\t%lf\n", &heightval[i], &widthval[i], &raw_image[i]);
//
//	//fclose(fp);
//
//	//FILE *fp2 = NULL;
//	//sprintf(tmpath, "c:\\EUVSolution\\TestData2.txt");
//	//fp2 = fopen(tmpath, "w");
//
//	//for (i = 0; i < height_data_no; i++) {
//	//	for (j = 0; j < width_data_no; j++) {
//	//		int k = i * width_data_no + j;
//	//		fprintf(fp2, "%lf\t%lf\t%lf\n", heightval[k], widthval[k], raw_image[k]);
//	//	}
//	//}
//	//fclose(fp2);	
//
//	//free(raw_image);
//	//free(heightval);
//	//free(widthval);
//}


// 아래의 것들은 FOV, Scan Grid 및 Scan number를 설정하기 위한 버튼클릭 함수임
//--------------------------------------------------------------------------------------------------
void CADAMDlg::OnCbnCloseupComboFovSelect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strFovData;
	m_comboFovSelect.GetLBText(m_comboFovSelect.GetCurSel(), strFovData);
	int m = atoi(strFovData);

	int nEuvImage_Old_Fov = m_nEuvImage_Fov;
	m_nEuvImage_Fov = m;

	//아래는 FOV Change시마다 Par Center 보정하는 부분 by smchoi
	MoveParCenter_ForFOVChange(nEuvImage_Old_Fov, m_nEuvImage_Fov);
	//m_nEuvImage_Old_Fov = m_nEuvImage_Fov;
}

void CADAMDlg::OnCbnCloseupComboScangridSelect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strScanGridData;
	m_comboScanGridSelect.GetLBText(m_comboScanGridSelect.GetCurSel(), strScanGridData);
	int m = atoi(strScanGridData);
	m_nEuvImage_ScanGrid = m;
}

void CADAMDlg::OnCbnCloseupComboScanNum()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strScanNumData;
	int selIndex = m_comboScanNumber.GetCurSel();

	m_comboScanNumber.GetLBText(selIndex, strScanNumData);
	int m = atoi(strScanNumData);
	m_nEuvImage_ScanNumber = m;
	UpdateData(FALSE);
}
//--------------------------------------------------------------------------------------------------

void CADAMDlg::OnCbnCloseupComboInterpolationGrid()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strInterpolatoinGridSelect;
	m_comboInterpolationFridSelect.GetLBText(m_comboInterpolationFridSelect.GetCurSel(), strInterpolatoinGridSelect);
	int m = atoi(strInterpolatoinGridSelect);
	m_nEUVImage_InterpolationGrid = m;

}

void CADAMDlg::OnCbnCloseupComboDetoctorSelection()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CString strDetectorSelect;
	m_comboDetectorSelection.GetLBText(m_comboDetectorSelection.GetCurSel(), strDetectorSelect);
	int m = atoi(strDetectorSelect);
	m_nEUVImage_Detectorselection = m;
}

//------------------------------------------------------------여기서 부터 수정----------------------------------------------

//void CADAMDlg::OnBnClickedInterpolationTestButton()		//이름은 Interpolation 이지만 이미지 필터링 하는 함수임... 예전에 Interpolation 이었으며, 바꿔야 하는데 아직 안바꿈. 버튼 캡션은 수정 KYD
//{
//	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//
//	//m_dFilteredImage_Data[] = m_dAveragedImage_Data;
//
//	//RunFilter();
//
//	//time_t start, end;
//	//double ParsingTime;
//	//CString msg;
//	//start = clock();
//
//	//DisplayFilteredImage();
//
//	//end = clock();
//	//ParsingTime = (double)(end - start);
//	//msg.Format("필터 Display에 걸린 시간은 : %.2f 입니다.", ParsingTime);
//	//AfxMessageBox(msg);
//
//
//	//Sleep(1000);
//	//AfxMessageBox("필터를 적용하였습니다. ");
//
//	//FilteredImageFileSave(0);	
//	FilteredImageFileSave_Resize(50, 0);
//
//	
//	
//
//	AfxMessageBox("filtered 이미지 저장이 완료되었습니다. ");
//	
//}
// 로만 원래 코드... 수정에 참고 할것

void CADAMDlg::OnBnClickedAverageDataSaveButton()		//임시로 Filter image file save thread run 으로 사용함
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
//	AveragedImageFileSave();
//	AfxMessageBox("Averaged 이미지 저장이 완료되었습니다. ");

	//FilteredImageFileSave();	
	//AfxMessageBox("filtered 이미지 저장이 완료되었습니다. ");

}

void CADAMDlg::OnBnClickedCornerFindButton()
{
	GetCornerPixel(&m_nEdgeFindResultX, &m_nEdgeFindResultY);
	Invalidate(FALSE);
	if (m_bEUVEdgeFindSuccess == TRUE /* && 물류동작 == FALSE && 마스크있음 == TRUE*/)
	{
		if (g_pWarning != NULL)
		{
			g_pWarning->m_strWarningMessageVal = "Align Point Center 조정 중입니다. 잠시 기다려 주세요!";
			g_pWarning->UpdateData(FALSE);
			g_pWarning->ShowWindow(SW_SHOW);
		}
		int center_pixel_x = (int)(m_nEdgeFindResultX - m_nRawImage_PixelWidth / 2);
		int center_pixel_y = (int)(m_nEdgeFindResultY - m_nRawImage_PixelHeight / 2);
		double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
		double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);

		double current_posx_mm = 0.0, current_posy_mm = 0.0;
		current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
		current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

		double target_posx_mm = 0.0, target_posy_mm = 0.0;
		target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
		target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;

		if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
		{
			//g_pAdam->GetCurrentLIFPosValue();
			//g_pNavigationStage->m_bLaserValueAfterSwitchingX = g_pAdam->m_dCurrentXposition;
			//g_pNavigationStage->m_bLaserValueAfterSwitchingY = g_pAdam->m_dCurrentYposition;
			//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
			g_pNavigationStage->SetLaserMode();
			WaitSec(0.1);
		}
		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
			g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);

		m_bEUVEdgeFindSuccess = FALSE;

		if (g_pWarning != NULL)
			g_pWarning->ShowWindow(SW_HIDE);
	}
}

int CADAMDlg::GetCornerPixel(int* result_pixelx, int* result_pixely)
{
	int nRet = 0;

	CString strThreshold;
	m_EditCornerThreshold.GetWindowTextA(strThreshold);
	double m = atof(strThreshold);
	CornerThreshold = m;

	int image_size_x, image_size_y;
	double FOV = m_nEuvImage_Fov;// *1000;
	image_size_x = image_size_y = int(FOV / m_nEuvImage_ScanGrid);
	int result_x = 0, result_y = 0;
	m_bEUVEdgeFindSuccess = FindCorner(m_dRawImageForDisplay, image_size_x, image_size_y, CornerThreshold, &result_x, &result_y);
	*result_pixelx = result_x;
	*result_pixely = result_y;

	return nRet;
}

int CADAMDlg::MoveParCenter_ForFOVChange(int nOldFov, int nNewFov)
{
	double dDifference = 0.0;

	if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
	{
		//g_pAdam->GetCurrentLIFPosValue();
		//g_pNavigationStage->m_bLaserValueAfterSwitchingX = m_dCurrentXposition;
		//g_pNavigationStage->m_bLaserValueAfterSwitchingY = m_dCurrentYposition;
		//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
		g_pNavigationStage->SetLaserMode();
		WaitSec(0.1);
	}

	dDifference = (nOldFov - nNewFov) / 2;
	g_pNavigationStage->MoveRelativeOnTheFly(STAGE_X_AXIS, -dDifference / 1000000.);
	g_pNavigationStage->MoveRelativeOnTheFly(STAGE_Y_AXIS, dDifference / 1000000.);
	WaitSec(0.5);

	return 0;
}

#if FALSE
int CADAMDlg::RunFilter()
{
	int ret = 0;

	// Roman High NA Filter에서 사용하는 변수_나중에 위치 및 순서 수정 필요_KYD
	double threshold = 0.3;
	double averaging_size = 50;

	double FOV = m_nEuvImage_Fov;// *1000;			// in nm scale
	double grid_step = m_nEUVImage_InterpolationGrid;							//output grid step
	int output_size = int(FOV / grid_step);


	time_t start, end;
	double ParsingTime;
	CString msg;
	start = clock();

	//#ifdef _DEBUG

	//vector<vector<double>> filtered_image;
	//FilterImage(m_dFilteredImage_Data[], filtered_image, output_size, output_size, 0, conventional, 0.33 / 13.5, 0.8, 0, 0, grid_step, m_hWnd);

	//#else

	LowPassFilter(m_pyFuncFilterImage, this->m_dFilteredImage_Data[], output_size, output_size, conventional, 0.35 / 13.5, 0.8, 0.0, 0.0, grid_step);		//python filter
	//AfxMessageBox("Python lowpass filter ddd 적용 됨");

	//#endif 

	end = clock();
	ParsingTime = (double)(end - start);
	msg.Format("필터 적용 시간은 : %.2f 입니다.", ParsingTime);
	AfxMessageBox(msg);

	return ret;
}
#endif

void CADAMDlg::OnBnClickedCheckEuvCrossline()
{
	UpdateData(TRUE);

	if (m_bEUVCrossLineDisplay)
	{
		MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), M_DEFAULT, M_VISIBLE, M_TRUE);
	}
	else
	{
		MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), M_DEFAULT, M_VISIBLE, M_FALSE);
	}

	//Invalidate(FALSE);//OnPaint 호출? 
}

void CADAMDlg::OnBnClickedCheckEuvMousemove()
{
	UpdateData(TRUE);

	m_bEUVCrossLineDisplay = TRUE;
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelCenter), M_DEFAULT, M_VISIBLE, M_TRUE);

	UpdateData(FALSE);

	if (m_bEUVMouseMove)
	{
		//MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
		MdispControl(m_MilDisplay, M_MOUSE_USE, M_DISABLE);
		MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_DISABLE);
		//MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_TRUE);
		//MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_DISABLE);
	}
	else
	{
		//MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
		//MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);

		MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
		MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
		MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);

	}

	//
}

#if false
void CADAMDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	if (g_pNavigationStage == NULL || g_pScanStage == NULL)
		return;

	CRect rect;
	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);

	GetDlgItem(IDC_EUV_DISPLAY)->IsWindowVisible();

	if (rect.PtInRect(point) && GetDlgItem(IDC_EUV_DISPLAY)->IsWindowVisible())
	{
		if (m_bEUVMouseMove && ((nFlags & MK_LBUTTON) == MK_LBUTTON))
		{
			m_ptEUVLeftButtonDownPoint = point;
			Invalidate(FALSE);
		}
		//m_ptEUVMousePoint = point;

		double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
		pixelvalue_x = (double)m_nRawImage_PixelWidth / rect.Width();
		pixelvalue_y = (double)m_nRawImage_PixelWidth / rect.Height();
		int center_pixel_x = (int)((double)(abs(point.x - rect.Width() / 2) * pixelvalue_x));
		int center_pixel_y = (int)((double)(abs(point.y - rect.Height() / 2) * pixelvalue_y));
		//int center_pixel_x = (int)((double)((point.x - rect.Width() / 2) * pixelvalue_x));
		//int center_pixel_y = (int)((double)((point.y - rect.Height() / 2) * pixelvalue_y));
		int nCenter_DistanceX_nm = (int)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] * 1000 * center_pixel_x);
		int nCenter_DistanceY_nm = (int)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] * 1000 * center_pixel_y);

		int pixel_x = (int)((double)point.x * pixelvalue_x);
		int pixel_y = (int)((double)point.y * pixelvalue_y);
		int nDistanceX_nm = (int)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] * 1000 * pixel_x);
		int nDistanceY_nm = (int)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] * 1000 * pixel_y);

		CString strMousePoint;
		strMousePoint.Format("point_x=%d, point_y=%d, distance_x=%d nm, distance_y=%d nm,            center_point_x=%d, center_point_y=%d, center_distance_x=%d nm, center_distance_y=%d nm", pixel_x, pixel_y, nDistanceX_nm, nDistanceY_nm, center_pixel_x, center_pixel_y, nCenter_DistanceX_nm, nCenter_DistanceY_nm);
		GetDlgItem(IDC_ADAM_STATIC)->SetWindowTextA(strMousePoint);
		//GetDlgItem(IDC_STATIC_TCPSOCKET)->SetWindowTextA(m_strReceivedMsg);		//마우스 움직일때 볼려고... 될려나?
	}


	__super::OnMouseMove(nFlags, point);
}


void CADAMDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (g_pNavigationStage == NULL || g_pScanStage == NULL)
		return;

	CRect rect;
	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);

	if (rect.PtInRect(point) && GetDlgItem(IDC_EUV_DISPLAY)->IsWindowVisible())
	{
		m_bEUVMouseMoveLineDisplay = FALSE;
		if (m_bEUVMouseMove == TRUE /* && 물류동작 == FALSE && 마스크있음 == TRUE*/)
		{
			double pixelvalue_x = 0.0, pixelvalue_y = 0.0;
			pixelvalue_x = (double)m_nRawImage_PixelWidth / rect.Width();
			pixelvalue_y = (double)m_nRawImage_PixelWidth / rect.Height();
			int center_pixel_x = (int)((point.x - rect.Width() / 2) * pixelvalue_x);
			int center_pixel_y = (int)((point.y - rect.Height() / 2) * pixelvalue_y);
			double nCenter_DistanceX_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_x);
			double nCenter_DistanceY_mm = (double)(g_pScanStage->m_pdAmplitudeOfLineWave[g_pScanStage->m_nStrokeNo] / 1000. * center_pixel_y);

			double current_posx_mm = 0.0, current_posy_mm = 0.0;
			current_posx_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			current_posy_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			double target_posx_mm = 0.0, target_posy_mm = 0.0;
			target_posx_mm = current_posx_mm - nCenter_DistanceX_mm;
			target_posy_mm = current_posy_mm + nCenter_DistanceY_mm;

			if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == FALSE)
			{
				//g_pAdam->GetCurrentLIFPosValue();
				//g_pNavigationStage->m_bLaserValueAfterSwitchingX = g_pAdam->m_dCurrentXposition;
				//g_pNavigationStage->m_bLaserValueAfterSwitchingY = g_pAdam->m_dCurrentYposition;
				//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_LASER);
				g_pNavigationStage->SetLaserMode();
				WaitSec(0.1);
			}
			if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE || g_pMaskMap->m_bManualAlign == TRUE)
				g_pNavigationStage->MoveAbsoluteXY_OnTheFly(target_posx_mm, target_posy_mm);
		}
	}
	else
	{
		m_bEUVMouseMoveLineDisplay = FALSE;
	}

	m_bEUVMouseMove = FALSE;
	UpdateData(FALSE);
	Invalidate(FALSE);

	/*MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_FALSE);
	MdispControl(m_MilDisplay, M_MOUSE_USE, M_ENABLE);
	MdispControl(m_MilDisplay, M_MOUSE_CURSOR_CHANGE, M_ENABLE);*/

	__super::OnLButtonUp(nFlags, point);
}

void CADAMDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CRect rect;
	GetDlgItem(IDC_EUV_DISPLAY)->GetClientRect(&rect);

	if (rect.PtInRect(point) && GetDlgItem(IDC_EUV_DISPLAY)->IsWindowVisible())
	{
		if (m_bEUVMouseMove)
		{
			m_bEUVMouseMoveLineDisplay = TRUE;
			m_ptEUVLeftButtonDownPoint = point;
			Invalidate(FALSE);
		}
	}


	__super::OnLButtonDown(nFlags, point);
}
#endif

void CADAMDlg::OnBnClickedAdamCapReadButton()
{
	GetDlgItem(IDC_ADAM_CAP_READ_BUTTON)->EnableWindow(false);
	Command_AverageRunAfterTimeout();
	GetDlgItem(IDC_ADAM_CAP_READ_BUTTON)->EnableWindow(true);
}

void CADAMDlg::OnBnClickedButtonAdamPortReopen()
{
	int nRet = 0;

	CloseTcpIpSocket();
	nRet = OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_ADAM], g_pConfig->m_nPORT[ETHERNET_ADAM], FALSE, ADAM_RECEIVE_BUFFER_SIZE);
	Command_SetAverageCount(m_AverageCount);
}

//void CADAMDlg::EUVMoveLineDraw(MOUSEPOSITION MousePos)
void CADAMDlg::EUVMoveLineDraw(double posX, double posY)
{
	//MgraClear(m_MilGraphContext, m_MilGraphListCross);
	//MgraColor(m_MilGraphContext, M_RGB888(155, 0, 200)); //PURPLE

	//MgraLine(m_MilGraphContext, m_MilGraphListCross, 0, MousePos.m_BufferPositionY, m_pAdamDlgStaticInst->m_MilDispWidth, MousePos.m_BufferPositionY);
	//MgraLine(m_MilGraphContext, m_MilGraphListCross, MousePos.m_BufferPositionX, 0, MousePos.m_BufferPositionX, m_pAdamDlgStaticInst->m_MilDispHeight);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 0, M_POSITION_X, -0.5);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 0, M_POSITION_Y, posY);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 1, M_POSITION_X, m_pAdamDlgStaticInst->m_MilDispWidth);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 1, M_POSITION_Y, posY);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 2, M_POSITION_X, posX);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 2, M_POSITION_Y, -0.5);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 3, M_POSITION_X, posX);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), 3, M_POSITION_Y, m_pAdamDlgStaticInst->m_MilDispHeight);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_CrossLabelMove), M_DEFAULT, M_VISIBLE, M_TRUE);

}

void CADAMDlg::EUVMatchPointDraw(double posX, double posY)
{

	//MgraColor(m_MilGraphContext, M_RGB888(0, 255, 0));


	double crossRectSize = 2;	
	
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_POSITION_X, posX - crossRectSize);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_POSITION_Y, posY - crossRectSize);

	//MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), 1, M_POSITION_X, posX + 2*crossRectSize);
	//MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), 1, M_POSITION_Y, posY + 2*crossRectSize);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_RECTANGLE_HEIGHT,2*crossRectSize);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_RECTANGLE_WIDTH, 2*crossRectSize);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchRectLabelCenter), M_DEFAULT, M_VISIBLE, M_TRUE);

	//MgraColor(m_MilGraphContext, M_RGB888(255, 0, 0));

	double CrossSize = 2;

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 0, M_POSITION_X, posX - CrossSize);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 0, M_POSITION_Y, posY);
	
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 1, M_POSITION_X, posX + CrossSize);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 1, M_POSITION_Y, posY);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 2, M_POSITION_X, posX);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 2, M_POSITION_Y, posY - CrossSize);

	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 3, M_POSITION_X, posX);
	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), 3, M_POSITION_Y, posY + CrossSize);


	MgraControlList(m_MilGraphListCross, M_GRAPHIC_LABEL(m_MatchCrossLabelCenter), M_DEFAULT, M_VISIBLE, M_TRUE);

}




void CADAMDlg::AdamUIControl(BOOL isEnable)
{
	GetDlgItem(IDC_ADAM_RUN_BUTTON)->EnableWindow(isEnable);
}

void CADAMDlg::OnBnClickedButtonAdamTest()
{
	AdamUIControl(TRUE);
}

void CADAMDlg::OnBnClickedButton3()
{
	//int nRet = Command_GetValueBefore();//nRet =0 면 정상, -99면 timeout, local host 일때 70이면 안정적

	int nRet = Command_AverageRunTimeout();

	if (nRet != 0)
	{
		int test = 0;
	}

	//Command_GetValueBefore(1000);
}

void CADAMDlg::OnCbnSelchangeComboScangridSelect()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.	
}

void CADAMDlg::OnBnClickedRawImageSaveButton()
{
	RawImageFileSave();
	AfxMessageBox("Raw 이미지 저장이 완료되었습니다. ");
}


void CADAMDlg::CapSensorDataUiUpdate()
{
	CString str;

	str.Format("%.3f", AdamData.m_dCapsensor1);
	m_editCtrlCap1.SetWindowTextA(str);

	str.Format("%.3f", AdamData.m_dCapsensor2);
	m_editCtrlCap2.SetWindowTextA(str);

	str.Format("%.3f", AdamData.m_dCapsensor3);
	m_editCtrlCap3.SetWindowTextA(str);

	str.Format("%.3f", AdamData.m_dCapsensor4);
	m_editCtrlCap4.SetWindowTextA(str);

	str.Format("%.2f", AdamData.m_dX_position);
	m_editCtrlLifX.SetWindowTextA(str);

	str.Format("%.2f", AdamData.m_dY_position);
	m_editCtrlLifY.SetWindowTextA(str);

}

void CADAMDlg::CurrentScanNumUiUpdate()
{
	CString str;

	str.Format("%d", m_nImageScanCnt + 1);
	m_editCtrlCurrentScanNum.SetWindowTextA(str);

}

void CADAMDlg::SetAdamFovInfoUi()
{
	if (g_pAdam->m_nEuvImage_Fov == 1000)
		g_pAdam->m_comboFovSelect.SetCurSel(0);
	else if (g_pAdam->m_nEuvImage_Fov == 1500)
		g_pAdam->m_comboFovSelect.SetCurSel(1);
	else if (g_pAdam->m_nEuvImage_Fov == 2000)
		g_pAdam->m_comboFovSelect.SetCurSel(2);
	else if (g_pAdam->m_nEuvImage_Fov == 3000)
		g_pAdam->m_comboFovSelect.SetCurSel(3);
	else if (g_pAdam->m_nEuvImage_Fov == 5000)
		g_pAdam->m_comboFovSelect.SetCurSel(4);
	else if (g_pAdam->m_nEuvImage_Fov == 10000)
		g_pAdam->m_comboFovSelect.SetCurSel(5);

	if (g_pAdam->m_nEuvImage_ScanGrid == 10)
		g_pAdam->m_comboScanGridSelect.SetCurSel(1);
	else if (g_pAdam->m_nEuvImage_ScanGrid == 20)
		g_pAdam->m_comboScanGridSelect.SetCurSel(2);
	else if (g_pAdam->m_nEuvImage_ScanGrid == 40)
		g_pAdam->m_comboScanGridSelect.SetCurSel(3);
	else if (g_pAdam->m_nEuvImage_ScanGrid == 80)
		g_pAdam->m_comboScanGridSelect.SetCurSel(4);

	if (g_pAdam->m_nEuvImage_ScanNumber >= 1 || g_pAdam->m_nEuvImage_ScanNumber <= 10)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(g_pAdam->m_nEuvImage_ScanNumber - 1);
	}
	else if (g_pAdam->m_nEuvImage_ScanNumber == 12)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(10);
	}
	else if (g_pAdam->m_nEuvImage_ScanNumber == 16)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(11);
	}
	else if (g_pAdam->m_nEuvImage_ScanNumber == 24)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(12);
	}
	else if (g_pAdam->m_nEuvImage_ScanNumber == 50)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(13);
	}
	else if (g_pAdam->m_nEuvImage_ScanNumber == 100)
	{
		g_pAdam->m_comboScanNumber.SetCurSel(14);
	}

	m_bIsUseHighFovInterpolationOld = m_bIsUseHighFovInterpolation;
	m_bIsUseHighFovInterpolation = FALSE;
	m_checkIsUseHighFovInterpolation.SetCheck(m_bIsUseHighFovInterpolation);
}

void CADAMDlg::OnBnClickedButtonAdamPortClose()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	CloseTcpIpSocket();
}

void CADAMDlg::OnBnClickedCheckCapsensorAutoRead()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다

	if (m_ctrlCapSensorAutoRead.GetCheck())
	{
		m_bCapAutoRead = TRUE;
	}
	else
	{
		m_bCapAutoRead = FALSE;
	}

	//IDC_CHECK_CAPSENSOR_AUTO_READ
}

void CADAMDlg::OnBnClickedButtonAdamAverageCount()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
	Command_SetAverageCount(m_AverageCount);

}

void CADAMDlg::OnBnClickedButtonAdamFit()
{
	MdispControl(m_MilDisplay, M_SCALE_DISPLAY, M_ENABLE);
}

void CADAMDlg::InitAdamDlgForScan()
{
	if (m_MilRawImageForDispChild != NULL) MbufFree(m_MilRawImageForDispChild);
	MbufChild2d(m_MilRawImageForDisp, 0, 0, m_nRawImage_PixelWidth, m_nRawImage_PixelHeight, &m_MilRawImageForDispChild);

	//if (m_nEuvImage_Fov < 4000)
	if (m_bIsUseInterpolation)
	{
		if (m_MilRegridImageForDispChild != NULL) MbufFree(m_MilRegridImageForDispChild);
		MbufChild2d(m_MilRegridImageForDisp, 0, 0, m_nReGridImage_PixelWidth, m_nReGridImage_PixelHeight, &m_MilRegridImageForDispChild);
	}

	((CButton*)GetDlgItem(IDC_CHECK_DISP_NORMALIZED))->SetCheck(FALSE);
}

UINT CADAMDlg::CapReadThread(LPVOID pParam)
{
	CADAMDlg* pAdamDlg = (CADAMDlg*)pParam;
	int nRet;

	while (!pAdamDlg->m_bCapReadThreadExitFlag)
	{
		if (pAdamDlg->m_bConnected && !pAdamDlg->m_bIsScaningOn && pAdamDlg->m_bCapAutoRead)
		{
			//Scan 끝나고  Cap 자동 Read시 지연추가, 프로그램 시작시 지연
			if (pAdamDlg->m_bIsScaningOnPrevious == TRUE)
			{
				for (int i = 0; i < 10; i++)
				{
					if (pAdamDlg->m_bCapReadThreadExitFlag) break;
					Sleep(200);
				}

				if (pAdamDlg->m_bCapReadThreadExitFlag) break;
			}
			nRet = pAdamDlg->Command_AverageRunTimeout();//nRet =0 면 정상, -99면 timeout, local host 일때 70이면 안정적

			if (nRet != 0)
			{
				for (int i = 0; i < 10; i++)
				{
					if (pAdamDlg->m_bCapReadThreadExitFlag) break;
					Sleep(200);
				}

				if (pAdamDlg->m_bCapReadThreadExitFlag) break;
			}

		}

		pAdamDlg->m_bIsScaningOnPrevious = pAdamDlg->m_bIsScaningOn;

		Sleep(100);
	}

	pAdamDlg->m_pCapReadThread = NULL;

	return 0;
}



void CADAMDlg::OnBnClickedAdamScan()
{
	g_pLog->Display(0, _T("CADAMDlg::OnBnClickedAdamScan() Button Click!"));

	//smchoi: g_pScanStage->Start_Scan() 안에서 진행하도록 변경 20210219
	//if (g_pNavigationStage->m_bLaserSwichingModeFlag == TRUE && g_pNavigationStage->m_bLaserFeedbackFlag == TRUE)
	//{
	//	//g_pAdam->GetCurrentLIFPosValue();
	//	//g_pNavigationStage->m_bLaserValueBeforeSwitchingX = m_dCurrentXposition;
	//	//g_pNavigationStage->m_bLaserValueBeforeSwitchingY = m_dCurrentYposition;
	//	g_pNavigationStage->SetEncoderMode();
	//	//g_pNavigationStage->SetFeedbackTypeDlg(FEEDBACK_ENCODER);

	//}

	// ADAM Scan Run 	
	//g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// Scan Stage Run

	GetDlgItem(IDC_ADAM_SCAN)->EnableWindow(FALSE);

	g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// 21.09.27 BeamSearch Test

	GetDlgItem(IDC_ADAM_SCAN)->EnableWindow(TRUE);
}

void CADAMDlg::OnBnClickedFilteredDataSaveButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	g_pLog->Display(0, _T("CADAMDlg::OnBnClickedFilteredDataSaveButton() Button Click!"));

	FilteredImageFileSave_Resize(50, 0, m_dFilteredImage_Data3D[0], AdamData.m_dCapsensor2);

	AfxMessageBox("Filtered 이미지 저장이 완료되었습니다. ");
}


void CADAMDlg::OnBnClickedCheckAdamSaveEveryRawImage()
{
	UpdateData(TRUE);
}

void CADAMDlg::OnBnClickedCheckDispNormalized()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);

	if (IsDisplayNormalizedImage)
	{
		DisplayFilteredImage(m_dFilteredImage_Data3D[0][D_NOR_I0_FILTER_REGRID]);
	}
	else
	{
		DisplayFilteredImage(m_dFilteredImage_Data3D[0][D1_REGRID]);
	}

}

void CADAMDlg::OnBnClickedCheckAdamSaveI0NormalizedImage()
{
	UpdateData(TRUE);
}

void CADAMDlg::OnBnClickedButtonAdamInitEuvAlign()
{
	/// Design 좌표 slot4
	XAlignDesign_um[LB] = -64950;
	XAlignDesign_um[LT] = -64950;
	XAlignDesign_um[RT] = 64950;
	XAlignDesign_um[RB] = 64950;

	YAlignDesign_um[LB] = -51450;
	YAlignDesign_um[LT] = 51550;
	YAlignDesign_um[RT] = 51550;
	YAlignDesign_um[RB] = -51450;
}

void CADAMDlg::OnBnClickedButtonAdamApplyAlign()
{

	// Homography Matrix 계산
	g_pPhase->Homograpy(H_StageToMask, 4, XAlignMeasure_um, YAlignMeasure_um, XAlignDesign_um, YAlignDesign_um);
	g_pPhase->Homograpy(H_MaskToStage, 4, XAlignDesign_um, YAlignDesign_um, XAlignMeasure_um, YAlignMeasure_um);


	//체크 

	double x_stage[4];
	double y_stage[4];

	double x_mask[4];
	double y_mask[4];

	//Mask To Stage Test
	g_pPhase->HomographyTransform(XAlignDesign_um[0], YAlignDesign_um[0], H_MaskToStage, x_stage[0], y_stage[0]);
	g_pPhase->HomographyTransform(XAlignDesign_um[1], YAlignDesign_um[1], H_MaskToStage, x_stage[1], y_stage[1]);
	g_pPhase->HomographyTransform(XAlignDesign_um[2], YAlignDesign_um[2], H_MaskToStage, x_stage[2], y_stage[2]);
	g_pPhase->HomographyTransform(XAlignDesign_um[3], YAlignDesign_um[3], H_MaskToStage, x_stage[3], y_stage[3]);

	//Stage To Mask Test
	g_pPhase->HomographyTransform(XAlignMeasure_um[0], YAlignMeasure_um[0], H_StageToMask, x_mask[0], y_mask[0]);
	g_pPhase->HomographyTransform(XAlignMeasure_um[1], YAlignMeasure_um[1], H_StageToMask, x_mask[1], y_mask[1]);
	g_pPhase->HomographyTransform(XAlignMeasure_um[2], YAlignMeasure_um[2], H_StageToMask, x_mask[2], y_mask[2]);
	g_pPhase->HomographyTransform(XAlignMeasure_um[3], YAlignMeasure_um[3], H_StageToMask, x_mask[3], y_mask[3]);


	////Mask To Stage Test
	//x_mask[0] = -66258.285;
	//x_mask[1] = -52566.542;
	//x_mask[2] = -49272.339;
	//x_mask[3] = -45397.110;

	//y_mask[0] = 24596.746;
	//y_mask[1] = -1746.166;
	//y_mask[2] = 44864.810;
	//y_mask[3] = -18809.248;


	//g_pPhase->HomographyTransform(x_mask[0], y_mask[0], H_MaskToStage, x_stage[0], y_stage[0]);
	//g_pPhase->HomographyTransform(x_mask[1], y_mask[1], H_MaskToStage, x_stage[1], y_stage[1]);
	//g_pPhase->HomographyTransform(x_mask[2], y_mask[2], H_MaskToStage, x_stage[2], y_stage[2]);
	//g_pPhase->HomographyTransform(x_mask[3], y_mask[3], H_MaskToStage, x_stage[3], y_stage[3]);

	//int ste = 0;
}

void CADAMDlg::OnBnClickedButtonAdamAlginLb()
{
	XAlignMeasure_um[LB] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS) * 1000;
	YAlignMeasure_um[LB] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) * 1000;
}

void CADAMDlg::OnBnClickedButtonAdamAlginLt()
{
	XAlignMeasure_um[LT] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS) * 1000;;
	YAlignMeasure_um[LT] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) * 1000;;
}

void CADAMDlg::OnBnClickedButtonAdamAlginRt()
{
	XAlignMeasure_um[RT] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS) * 1000;;
	YAlignMeasure_um[RT] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) * 1000;;
}

void CADAMDlg::OnBnClickedButtonAdamAlginRb()
{
	XAlignMeasure_um[RB] = g_pNavigationStage->GetPosmm(STAGE_X_AXIS) * 1000;;
	YAlignMeasure_um[RB] = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) * 1000;;
}

void CADAMDlg::OnBnClickedCheckAdamUse4ptAlign()
{
	UpdateData(TRUE);

}

void CADAMDlg::FilteredImageFileSaveByThread(int _removeEdge, int _DefectNo, int _ThroughFocusNo, double capPos_um, int refcap, double focusPos)
{
	ImageSaveThreadParam *imageSaveParam = new ImageSaveThreadParam; // 이거 New로 전달해야하나? ????
	imageSaveParam->pAdam = this;
	imageSaveParam->removeEdge = _removeEdge;
	imageSaveParam->defectNo = _DefectNo;
	imageSaveParam->throughFocusNo = _ThroughFocusNo;
	imageSaveParam->threadCount = m_ImageSaveThreadCount;
	imageSaveParam->capPos = capPos_um;
	imageSaveParam->refCap = refcap;
	imageSaveParam->focusPos = focusPos;

	

	m_ImageSaveThreadCount++;
	if (m_ImageSaveThreadCount >= IMAGE_BUFFER_SIZE_FOR_FILE_SAVE) m_ImageSaveThreadCount = 0;

	AfxBeginThread(g_pAdam->ImageSaveThreadFun, (LPVOID)imageSaveParam, THREAD_PRIORITY_NORMAL, 0, 0);
}

UINT CADAMDlg::ImageSaveThreadFun(LPVOID pParam)
{
	CString strLog;

	ImageSaveThreadParam* imageSaveParam = (ImageSaveThreadParam*)pParam;

	CADAMDlg *pAdam = imageSaveParam->pAdam;
	int threadCount = imageSaveParam->threadCount;

	strLog.Format("Begin(Image_Save): Point:%d Through_Focus:%d", imageSaveParam->defectNo, imageSaveParam->throughFocusNo);
	pAdam->SaveLogFile("ProcessLogAdam ", strLog);

	//Memcopy
	for (int i = 0; i < KIND_OF_REGRIDDATA; i++)
	{
		memcpy(pAdam->m_ImageBufferForSave[threadCount][i], pAdam->m_dFilteredImage_Data3D[0][i], sizeof(double) * pAdam->m_nReGridImage_PixelWidth * pAdam->m_nReGridImage_PixelHeight);
	}
	
	pAdam->FilteredImageFileSave_Resize(imageSaveParam->removeEdge, imageSaveParam->defectNo, pAdam->m_ImageBufferForSave[threadCount], imageSaveParam->capPos, imageSaveParam->refCap, 0, imageSaveParam->focusPos);

	strLog.Format("End(Image_Save): Point:%d Through_Focus:%d", imageSaveParam->defectNo, imageSaveParam->throughFocusNo);
	pAdam->SaveLogFile("ProcessLogAdam ", strLog);

	delete imageSaveParam; // 이렇게 하는거 맞음??

	return 0;
}


// 21.02.09 인터락 이동.. .나중에 변경 필요 ihlee
int CADAMDlg::MoveZCapsensor(double targetPos_um, int maxTry, int capNum, double *measureCapPos)
{
	/*  단위는 um */
	int nRet = 0;

	double cap = 0;
	//double cap_old1 = 0;
	//double cap_old2 = 0;

	double stage = 0;
	double error;
	double stageSet;
	double stageFinalEstimated;
	double Zinterlock; //Stage 기준
	int tryCount = 0;

	double delZ = 0;

	if (capNum < CAP_SENSOR1 || capNum > CAP_SENSOR4) return -1;

	/* Interlock limit Update */
	//g_pMaskMap->m_dZInterlockPos g_pConfig->m_dZInterlock_um g_pScanStage->m_dZUpperLimit		
	Zinterlock = g_pMaskMap->m_dZInterlockPos;

	//Cap & Stage Read, Error 계산
	if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

	switch (capNum)
	{
	case CAP_SENSOR1:
		cap = g_pAdam->AdamData.m_dCapsensor1;
		break;
	case CAP_SENSOR2:
		cap = g_pAdam->AdamData.m_dCapsensor2;
		break;
	case CAP_SENSOR3:
		cap = g_pAdam->AdamData.m_dCapsensor3;
		break;
	case CAP_SENSOR4:
		cap = g_pAdam->AdamData.m_dCapsensor4;
		break;
	default:
		return -4;
		break;
	}


	//if (cap < -150) { return -2; }//마스크 없을때 또는 OM 위치

	g_pScanStage->GetPosAxesData();
	stage = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	error = targetPos_um - cap;

	// Interlock 체크 임시로 제거, ihlee cap3 쪽이 내려갈때 쳐져있음..
	stageFinalEstimated = stage + error;
	//if (stageFinalEstimated > Zinterlock) return -3;

	while (TRUE)
	{
		// Error check 
		//if (abs(error) < 0.005) { nRet = 0; break; }// 정상 종료			
		if (abs(error) < 0.02) { nRet = 0; break; }// 정상 종료			

		if (tryCount >= maxTry)
		{
			return -5;
		}//

		// Z축 제어 1차로 30um 까지 2차: 10um 3차: 5um 4차이후: error 만큼 이동
		if (abs(error) > 31)
		{
			delZ = (error > 0) ? error - 30 : error + 30;
		}
		else if (abs(error) > 11)
		{
			delZ = (error > 0) ? error - 10 : error + 10;
		}
		else if (abs(error) > 6)
		{
			delZ = (error > 0) ? error - 5 : error + 5;
		}
		else
		{
			delZ = error;
		}

		stageSet = stage + delZ;

		// Interlock 체크
		if (stageSet > Zinterlock) return -3;
		g_pScanStage->MoveZAbsolute_SlowInterlock(stageSet);

		//Cap & Stage Read, Error 계산
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		switch (capNum)
		{
		case CAP_SENSOR1:
			cap = g_pAdam->AdamData.m_dCapsensor1;
			break;
		case CAP_SENSOR2:
			cap = g_pAdam->AdamData.m_dCapsensor2;
			break;
		case CAP_SENSOR3:
			cap = g_pAdam->AdamData.m_dCapsensor3;
			break;
		case CAP_SENSOR4:
			cap = g_pAdam->AdamData.m_dCapsensor4;
			break;
		default:
			return -4;
			break;
		}

		////Cap stabilization test1
		//if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		//switch (capNum)
		//{
		//case CAP_SENSOR1:
		//	cap_old1 = g_pAdam->AdamData.m_dCapsensor1;
		//	break;
		//case CAP_SENSOR2:
		//	cap_old1 = g_pAdam->AdamData.m_dCapsensor2;
		//	break;
		//case CAP_SENSOR3:
		//	cap_old1 = g_pAdam->AdamData.m_dCapsensor3;
		//	break;
		//case CAP_SENSOR4:
		//	cap_old1 = g_pAdam->AdamData.m_dCapsensor4;
		//	break;
		//default:
		//	return -4;
		//	break;
		//}

		////Cap stabilization test2
		//if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		//switch (capNum)
		//{
		//case CAP_SENSOR1:
		//	cap_old2 = g_pAdam->AdamData.m_dCapsensor1;
		//	break;
		//case CAP_SENSOR2:
		//	cap_old2 = g_pAdam->AdamData.m_dCapsensor2;
		//	break;
		//case CAP_SENSOR3:
		//	cap_old2 = g_pAdam->AdamData.m_dCapsensor3;
		//	break;
		//case CAP_SENSOR4:
		//	cap_old2 = g_pAdam->AdamData.m_dCapsensor4;
		//	break;
		//default:
		//	return -4;
		//	break;
		//}


		g_pScanStage->GetPosAxesData();
		stage = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		error = targetPos_um - cap;


		tryCount++;
	}

	if (measureCapPos != NULL)
	{
		*measureCapPos = cap;
	}

	return 0;
}

#if FALSE  // 21.02.09 이전꺼

int CADAMDlg::MoveZCapsensor(double targetPos_um, int maxTry, int capNum)
{
	/*  단위는 um */
	int nRet = 0;

	double cap = 0;
	double cap_old1 = 0;
	double cap_old2 = 0;

	double stage = 0;
	double error;
	double stageSet;
	double stageFinalEstimated;
	double Zinterlock; //Stage 기준
	int tryCount = 0;

	double delZ = 0;

	if (capNum < CAP_SENSOR1 || capNum > CAP_SENSOR4) return -1;

	/* Interlock limit Update */
	//g_pMaskMap->m_dZInterlockPos g_pConfig->m_dZInterlock_um g_pScanStage->m_dZUpperLimit		
	Zinterlock = g_pMaskMap->m_dZInterlockPos;

	//Cap & Stage Read, Error 계산
	if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

	switch (capNum)
	{
	case CAP_SENSOR1:
		cap = g_pAdam->AdamData.m_dCapsensor1;
		break;
	case CAP_SENSOR2:
		cap = g_pAdam->AdamData.m_dCapsensor2;
		break;
	case CAP_SENSOR3:
		cap = g_pAdam->AdamData.m_dCapsensor3;
		break;
	case CAP_SENSOR4:
		cap = g_pAdam->AdamData.m_dCapsensor4;
		break;
	default:
		return -4;
		break;
	}


	if (cap < -150) { return -2; }//마스크 없을때 또는 OM 위치

	g_pScanStage->GetPosAxesData();
	stage = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	error = targetPos_um - cap;

	// Interlock 체크
	stageFinalEstimated = stage + error;
	if (stageFinalEstimated > Zinterlock) return -3;

	while (TRUE)
	{
		// Error check 
		if (abs(error) < 0.02) { nRet = 0; break; }// 정상 종료			

		if (tryCount >= maxTry) { return -5; }//

		// Z축 제어 1차로 30um 까지 2차: 10um 3차: 5um 4차이후: error 만큼 이동
		if (abs(error) > 31)
		{
			delZ = (error > 0) ? error - 30 : error + 30;
		}
		else if (abs(error) > 11)
		{
			delZ = (error > 0) ? error - 10 : error + 10;
		}
		else if (abs(error) > 6)
		{
			delZ = (error > 0) ? error - 5 : error + 5;
		}
		else
		{
			delZ = error;
		}

		stageSet = stage + delZ;

		// Interlock 체크
		if (stageSet > Zinterlock) return -3;
		g_pScanStage->MoveZAbsolute_SlowInterlock(stageSet);

		//Cap & Stage Read, Error 계산
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		switch (capNum)
		{
		case CAP_SENSOR1:
			cap = g_pAdam->AdamData.m_dCapsensor1;
			break;
		case CAP_SENSOR2:
			cap = g_pAdam->AdamData.m_dCapsensor2;
			break;
		case CAP_SENSOR3:
			cap = g_pAdam->AdamData.m_dCapsensor3;
			break;
		case CAP_SENSOR4:
			cap = g_pAdam->AdamData.m_dCapsensor4;
			break;
		default:
			return -4;
			break;
		}

		//Cap stabilization test1
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		switch (capNum)
		{
		case CAP_SENSOR1:
			cap_old1 = g_pAdam->AdamData.m_dCapsensor1;
			break;
		case CAP_SENSOR2:
			cap_old1 = g_pAdam->AdamData.m_dCapsensor2;
			break;
		case CAP_SENSOR3:
			cap_old1 = g_pAdam->AdamData.m_dCapsensor3;
			break;
		case CAP_SENSOR4:
			cap_old1 = g_pAdam->AdamData.m_dCapsensor4;
			break;
		default:
			return -4;
			break;
		}

		//Cap stabilization test2
		if (g_pAdam->Command_AverageRunAfterTimeout() != 0) return -2;

		switch (capNum)
		{
		case CAP_SENSOR1:
			cap_old2 = g_pAdam->AdamData.m_dCapsensor1;
			break;
		case CAP_SENSOR2:
			cap_old2 = g_pAdam->AdamData.m_dCapsensor2;
			break;
		case CAP_SENSOR3:
			cap_old2 = g_pAdam->AdamData.m_dCapsensor3;
			break;
		case CAP_SENSOR4:
			cap_old2 = g_pAdam->AdamData.m_dCapsensor4;
			break;
		default:
			return -4;
			break;
		}


		g_pScanStage->GetPosAxesData();
		stage = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		error = targetPos_um - cap;


		tryCount++;
	}

	return 0;
}
#endif




int CADAMDlg::MoveZToGapPosition(double targetGag_um)
{

	// Test 필요
	
#if FALSE
	BOOL isSetInterlock = FALSE;
	BOOL isSetFocusPos = FALSE;
	double interlockMargin_um = 0;

	int refCap;
	double refCapPos;
	double measureCapPos;

	double delta_target = m_refGap_um - targetGag_um;

	delta_target = 0;

	// (m_refGap_um) 이상으로는 못 올라감
	if (delta_target > 0)
	{
		delta_target = 0;
	}

	MoveZCapsensorFocusPosition(isSetFocusPos, isSetInterlock, interlockMargin_um, &refCap, &refCapPos, &measureCapPos, delta_target);

#endif

	return 0;

}


int CADAMDlg::MoveZCapsensorFastAlignFocusPosition(BOOL isSetFocusPos, BOOL isSetInterlock, double interlockMargin_um, int *refCap, double *refCapPos, double *measureCapPos, double delta_target)
{
	return MoveZCapsensorFocusPosition(isSetFocusPos, isSetInterlock, interlockMargin_um, refCap, refCapPos,  measureCapPos, delta_target);
}

int CADAMDlg::MoveZCapsensorFocusPosition(BOOL isSetFocusPos, BOOL isSetInterlock, double interlockMargin_um, int *refCap, double *refCapPos, double *measureCapPos, double delta_target)
{
	int nRet = 0;

	int nRefCapSencor = -1;
	double dTargetFocus;

	// Mask center 좌표계
	double x_stage_mm;
	double y_stage_mm;

	double x_mm;
	double y_mm;


	double Zinterlock_old = g_pMaskMap->m_dZInterlockPos;

	if (isSetInterlock)
	{
		g_pMaskMap->m_dZInterlockPos = 400; //인터락 해제..
		g_pScanStage->m_dZUpperLimit = 400;
		g_pMaskMap->UpdateData(FALSE);

	}

	x_stage_mm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
	y_stage_mm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

	switch (g_pConfig->m_nEquipmentType)
	{
	case ELITHO:
		x_stage_mm = x_stage_mm + g_pConfig->m_dGlobalOffsetX_mm;
		y_stage_mm = y_stage_mm - g_pConfig->m_dGlobalOffsetY_mm;
		g_pNavigationStage->ConvertToWaferFromStageELitho(x_stage_mm, y_stage_mm, x_mm, y_mm);
		break;
	default:
		g_pNavigationStage->ConvertToMaskFromStage(x_stage_mm, y_stage_mm, x_mm, y_mm);
		break;

	}	


	// Coarse Stage 통신 끊긴경우
	if (x_stage_mm==0 || y_stage_mm==0)
	{
		nRet = -1;
		return nRet;

	}

	//사분면 기준으로 사용할 capsensor 선정
	if (x_mm >= 0 && y_mm <= 0) { nRefCapSencor = CAP_SENSOR3; dTargetFocus = g_pConfig->m_dZdistanceCap3nStage_um + delta_target; }
	else if (x_mm >= 0 && y_mm >= 0) { nRefCapSencor = CAP_SENSOR4; dTargetFocus = g_pConfig->m_dZdistanceCap4nStage_um + delta_target; }
	else if (x_mm <= 0 && y_mm >= 0) { nRefCapSencor = CAP_SENSOR1; dTargetFocus = g_pConfig->m_dZdistanceCap1nStage_um + delta_target; }
	else if (x_mm <= 0 && y_mm <= 0) { nRefCapSencor = CAP_SENSOR2; dTargetFocus = g_pConfig->m_dZdistanceCap2nStage_um + delta_target; }
	else
	{
		nRet = -2;
		//인터락 복구
		if (isSetInterlock)
		{
			g_pMaskMap->m_dZInterlockPos = Zinterlock_old;
			g_pScanStage->m_dZUpperLimit = Zinterlock_old;
			g_pMaskMap->UpdateData(FALSE);
		}
		return nRet;
	}

	if (refCap != NULL && refCapPos != NULL)
	{
		*refCap = nRefCapSencor;
		*refCapPos = dTargetFocus;
	}
	double measureCapPossition = 0;

	if (MoveZCapsensor(dTargetFocus, 15, nRefCapSencor, &measureCapPossition) != 0)
	{
		nRet = -3;
		//인터락 복구
		if (isSetInterlock)
		{
			g_pMaskMap->m_dZInterlockPos = Zinterlock_old;
			g_pScanStage->m_dZUpperLimit = Zinterlock_old;
			g_pMaskMap->UpdateData(FALSE);
		}

		return nRet;
	}

	if (isSetFocusPos)
	{
		g_pScanStage->GetPosAxesData();
		g_pMaskMap->m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
		g_pConfig->m_dZFocusPos_um = g_pMaskMap->m_dZFocusPos;

		if (isSetInterlock)
		{
			g_pMaskMap->m_dZInterlockPos = g_pMaskMap->m_dZFocusPos + interlockMargin_um;
			g_pConfig->m_dZInterlock_um = g_pMaskMap->m_dZInterlockPos;
			g_pScanStage->m_dZUpperLimit = g_pMaskMap->m_dZInterlockPos;
			//g_pMaskMap->UpdateData(FALSE);
		}
		g_pConfig->SaveRecoveryData();
	}


	if (measureCapPos != NULL)
	{
		*measureCapPos = measureCapPossition;
	}
			
	g_pMaskMap->UpdateData(FALSE);

	return nRet;
}

void CADAMDlg::OnBnClickedButtonAdamMoveZFocusPos()
{
	MoveZCapsensorFocusPosition();
}

void CADAMDlg::OnBnClickedButtonAdamMoveZFocusPosAndSetInterlock()
{
	MoveZCapsensorFocusPosition(TRUE, TRUE, 5);
}



void CADAMDlg::OnBnClickedButton5()

{
	if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV)
	{
		// ADAM Scan Run 
		g_pAdam->AdamUIControl(FALSE);
		g_pAdam->m_IsMeasureComplete = FALSE;
		g_pAdam->m_bAbort = FALSE;

		int TotalScanNum = m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
		int PointMeasureBufIndex;
		vector <int> vecScanBufIndex;
		vecScanBufIndex.reserve(TotalScanNum);
		MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

		thread PointMeasureThread = thread([=] {g_pAdam->PointMeasureThreadFun(PointMeasureBufIndex, vecScanBufIndex, 0, 1, TotalScanNum, 0, 1); });

		SetTimer(ADAMDATA_DISPLAY_TIMER, 1000, NULL);

		//g_pScanStage->Start_Scan(SCAN_MODE_IMAGING, m_nEuvImage_Fov, m_nEuvImage_ScanGrid, m_nEuvImage_ScanNumber);// Scan Stage Run

		while (!g_pAdam->m_IsMeasureComplete)
		{
			WaitSec(1);
		}

		PointMeasureThread.join();
		g_pAdam->AdamUIControl(TRUE);
	}
	else
	{
		ADAMRunStart();
	}
}

void CADAMDlg::OnBnClickedButton7()
{
	Command_ADAMStop();
}

UINT CADAMDlg::RawImageDisplayThread(LPVOID pParam)
{
	CADAMDlg* pAdam = (CADAMDlg*)pParam;
	DWORD dwResult;

	while (!pAdam->m_bRawImageDisplayThreadExitFlag)
	{
		dwResult = WaitForSingleObject(pAdam->m_hDisplayRawImage, 500); //ACQUISITION_TIME_OUT_MS

		if (dwResult == WAIT_OBJECT_0)
		{
			if (pAdam->m_bIsScaningOn == TRUE)
			{
				pAdam->DisplayRawImage();
			}

			//ResetEvent(pAdam->m_hDisplayRawImage); 자동리셋 이벤트임
		}
		else
		{
			int test = 0;
		}
	}
	return 0;
}

//int splitString(CString str, CString var, CStringArray &strs)
//{
//	int count = 0;
//
//	CString tempStr = str;
//
//	int length = str.GetLength();
//
//	while (length)
//	{
//		int find = tempStr.Find(var);
//		if (find != -1)
//		{
//			CString temp = tempStr.Left(find);
//			int varLen = _tcslen(var);
//			tempStr = tempStr.Mid(find + varLen);
//			count++;
//
//			strs.Add(temp);
//		}
//		else
//		{
//			strs.Add(tempStr);
//			count++;
//			break;
//		}
//	}
//	return count;
//}

void CADAMDlg::BeamOpen()
{
	if (g_pConfig->m_nEquipmentType == SREM033)
	{
		//if (!g_pBeamCon->OpenDevice())
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[1]);
		//}
		//else
		//{
		//	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[2]);
		//}
	}
}

void CADAMDlg::OnBnClickedButtonBeamOpen()
{
	//BeamOpen();
}

void CADAMDlg::OnBnClickedButtonBeamClose()
{
	//g_pBeamCon->CloseDevice();
	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[2]);
}

void CADAMDlg::UpdateBeamReceiveData(char* data)
{
	//if (g_pBeamCon->m_beam_bConnected)
	//{
	//	((CStatic*)GetDlgItem(IDC_EDIT_BEAM))->SetWindowTextA(data);
	//}
	//else
	//{
	//	((CStatic*)GetDlgItem(IDC_EDIT_BEAM))->SetWindowTextA(_T("-"));
	//}
}

void CADAMDlg::OnBnClickedButtonBeamReopen()
{
	//g_pBeamCon->CloseDevice();
	//((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[2]);
	//if (!g_pBeamCon->OpenDevice())
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[1]);
	//}
	//else
	//{
	//	((CStatic*)GetDlgItem(IDC_ICON_BEAM_CONNECT))->SetIcon(m_LedIcon[2]);
	//}
}

void CADAMDlg::OnStnClickedEuvDisplay()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
}

void CADAMDlg::OnBnClickedCheckAdamRegridRawImage()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	UpdateData(TRUE);
}

void CADAMDlg::OnBnClickedButtonAdamI0Apply()
{
	UpdateData(TRUE);
}

void CADAMDlg::OnBnClickedButtonAdamI0Save()
{
	UpdateData(TRUE);
	g_pConfig->m_I0Reference = m_I0Reference;
	g_pConfig->m_I0FilterWindowSize = m_I0FilterWindowSize;
	g_pConfig->m_LowPassFilterSigma = m_LowPassFilterSigma;

	g_pConfig->SaveAdamInfo();
}

void CADAMDlg::OnBnClickedButtonAdamI0Restore()
{
	g_pConfig->ReadAdamInfo();
	m_I0Reference = g_pConfig->m_I0Reference;
	m_I0FilterWindowSize = g_pConfig->m_I0FilterWindowSize;
	m_LowPassFilterSigma = g_pConfig->m_LowPassFilterSigma;

	UpdateData(FALSE);
}

void CADAMDlg::OnBnClickedCheckAdamHighFovInterpolation()
{
	UpdateData(TRUE);
}

void CADAMDlg::OnClickedCheckAdamThroughFocusCrossStep()
{
	UpdateData(TRUE);
}

void CADAMDlg::OnBnClickedButton4()
{
	m_Sq1.Close();
}



void CADAMDlg::OnBnClickedButton12()
{
	g_pNavigationStage->SetEncoderModeWithDriftReset();
}



void CADAMDlg::OnBnClickedGraphOnButton()
{
	if (m_Sq1.GetUntilReceiveData() == 0)
	{

	}	

	m_Sq1GetString = m_Sq1.m_ReceiveString;
	UpdateData(FALSE);

}

void CADAMDlg::OnBnClickedButton8()
{
	if (m_Sq1.m_IsInitailsed)
	{		
		if (m_Sq1.SetUntilInposition(m_Sq1.InitialData.X, m_Sq1.InitialData.Y, m_Sq1.InitialData.Z, m_Sq1.InitialData.Rx, m_Sq1.InitialData.Ry, m_Sq1.InitialData.Rz, m_Sq1.InitialData.Cx, m_Sq1.InitialData.Cy, m_Sq1.InitialData.Cz) == 0)
		{
			
		}	
		m_Sq1SetString = m_Sq1.m_SendString;
		UpdateData(FALSE);
	}
}



bool dataReady = FALSE;
condition_variable cvTest;
mutex mutexTest;

void cvTestFun()
{

	////wait
	//unique_lock<mutex> lock(mutexTest);
	//cvTest.wait(lock, [] {return dataReady; });
	////cvTest.wait(lock);
	//int test = 0;
	//notify
	while(1)
	{
		int test = 0;
	}
	//notify
	int test2 = 0;
	{
		unique_lock<mutex> lock(mutexTest);
		dataReady = TRUE;
	}
	cvTest.notify_one();
}

void cvTestFun2()
{
	dataReady = FALSE;
	{
		unique_lock<mutex> lock(mutexTest);
		cvTest.wait(lock, [] {return dataReady; });
		dataReady = FALSE;
	}
}

void cvTestFun3()
{
	thread test2 = thread([=] {cvTestFun2(); });

	test2.join();
}

//wait
void CADAMDlg::OnBnClickedButton10()
{
	thread test3 = thread([=] {cvTestFun3(); });

	test3.join();
	


	thread test = thread([=] {cvTestFun(); });

	//wait
	dataReady = FALSE;
	{
		unique_lock<mutex> lock(mutexTest);
		cvTest.wait(lock, [] {return dataReady; });
		dataReady = FALSE;
	}
	//cvTest.wait(lock);

	test.detach();

}

void CADAMDlg::OnBnClickedButton9()
{
	if (m_Sq1.GetUntilReceiveData() == 0)
	{
		m_Sq1.m_IsInitailsed = TRUE;
		m_Sq1.InitialData = m_Sq1.SqData; // DATA가 COPY 되는지 확인 필요, 확인됨
		
		//int test = 0;
	}

	
	
}

//notifiy
void CADAMDlg::OnBnClickedButton11()
{

	//notify
	{
		unique_lock<mutex> lock(mutexTest);
		dataReady = TRUE;
	}
	cvTest.notify_one();

	while (1)
	{
		int test = 0;
	}

	int test2 = 0;

}



void CADAMDlg::OnBnClickedButton6()
{
	m_Sq1.Open();
}


//Thread Test
void CADAMDlg::OnBnClickedButton1()
{
	AdamUIControl(FALSE);

	//1. 초기화(1)

	int TotalPointNum = 7;
	int TotalScanNum = 6;
	int TotalThroughFocusNum = 4;

	CString strLog;
	strLog.Format("Begin(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess", strLog);

	m_IsMeasureComplete = FALSE;
	m_bAbort = FALSE;


	deque<std::thread> PointMeasureThreadQue;
	deque<std::thread> ScanDataReceiveThreadQue;

	for (int pointNum = 0; pointNum < TotalPointNum; pointNum++)
	{
		//2. 초기화(2) - Measure Point	
		strLog.Format("Begin(MoveToSelectedPointNo): Point:%d", pointNum);
		SaveLogFile("ProcessLogMaskImagingProcess", strLog);

		//3. Measure Point로  Stage 이동
		WaitSec(1);

		strLog.Format("End(MoveToSelectedPointNo): Point:%d", pointNum);
		SaveLogFile("ProcessLogMaskImagingProcess", strLog);

		//4. 기준 Focus Position 이동

		for (int throughFocusNum = 0; throughFocusNum < TotalThroughFocusNum; throughFocusNum++)
		{
			//5. Focus Position Z축 이동
			int PointMeasureBufIndex;
			vector <int> vecScanBufIndex;
			vecScanBufIndex.reserve(TotalScanNum);

			if (!m_bAbort)
			{

				strLog.Format("Begin(MemoryAcquireAndInitForPointMeasure): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

				//6. Point Measure 메모리 요청(메모리 확보 될때까지 기다림)
				MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

				strLog.Format("End(MemoryAcquireAndInitForPointMeasure): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

				strLog.Format("Begin(PointMeasureThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

				//7. Point Measure 시작후 바로 리턴 ==> 정상종료 알아야함..				
				InitAdamDlgForScan();//
				PointMeasureThreadQue.emplace_back([=] {PointMeasureThreadFunForTest(PointMeasureBufIndex, vecScanBufIndex, pointNum, TotalPointNum, TotalScanNum, throughFocusNum, TotalThroughFocusNum); });
				//thread PointMeasureThread = thread([=] {PointMeasureThreadFunForTest(PointMeasureBufIndex, ScanBufIndex, pointNum, TotalPointNum, TotalScanNum, throughFocusNum, TotalThroughFocusNum);});					
				//PointMeasureThreadQue.push_back(PointMeasureThread);
				//PointMeasureThread.detach();

				strLog.Format("End(PointMeasureThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

			}

			if (!m_bAbort)
			{
				//Adam Data Simulation(time) Start(test)
				strLog.Format("Begin(ScanDataReceiveThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

				//this_thread::sleep_for(chrono::milliseconds(10));
				WaitSec(0.01);

				ScanDataReceiveThreadQue.emplace_back([=] {ScanDataReceiveThreadFunForTest(TotalScanNum, vecScanBufIndex, pointNum, throughFocusNum); });
				//thread ScanDataReceiveThread = thread([=] {ScanDataReceiveThreadFunForTest(TotalScanNum, ScanBufIndex, pointNum, throughFocusNum); });
				//ScanDataReceiveThread.detach();

				strLog.Format("End(ScanDataReceiveThread): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);
			}

			if (!m_bAbort)
			{

				strLog.Format("Begin(Scan): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);

				//8. Scan Start
				WaitSec(1);

				strLog.Format("End(Scan): Point:%d Through_Focus:%d", pointNum, throughFocusNum);
				SaveLogFile("ProcessLogMaskImagingProcess", strLog);
			}



		}
	}

	//9. 모든 측정 Point 완료, 10번항목 대체 가능
	while (!m_IsMeasureComplete) //있어야 하나? 
	{
		WaitSec(1);
	}

	//10. 모든 쓰레드 종료
	for (thread& pointMeasureThread : PointMeasureThreadQue)
	{
		pointMeasureThread.join();
	}
	for (thread& scanDataThread : ScanDataReceiveThreadQue)
	{
		scanDataThread.join();
	}

	// 11. 종료 프로세스
	AdamUIControl(TRUE);
	g_pAdam->ADAMAbort();

	strLog.Format("End(MaskImagingProcess)");
	SaveLogFile("ProcessLogMaskImagingProcess", strLog);
}

void CADAMDlg::OnBnClickedCheck3umFov()
{
	UpdateData(TRUE);
}


void CADAMDlg::OnBnClickedCheckResetStartPos()
{
	UpdateData(TRUE);
}


void CADAMDlg::OnBnClickedButton13()
{
	m_Sq1.Stop();
}
