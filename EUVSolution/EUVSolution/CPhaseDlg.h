﻿/**
 * Phase Measurement Dialog Class
 *
 * Copyright 2020 by E-SOL, Inc.,
 *
 */
#pragma once

// CPhaseDlg 대화 상자
class CPhaseDlg : public CDialogEx, public CPhaseAlgorithm
{
	DECLARE_DYNAMIC(CPhaseDlg)

public:
	CPhaseDlg(CWnd* pParent = nullptr); // 표준 생성자입니다.
	CListBox m_listPhaseResult;

	virtual ~CPhaseDlg();

// 대화 상자 데이터입니다.

	enum { IDD = IDD_PHASE_DIALOG };


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
	//afx_msg void OnViewPortChanged();

public:
	virtual BOOL OnInitDialog();	

	//CPhaseAlgorithm phaseAlgorithm;
	void chartAlignUpdate(double* pos, double* intensity, int N, double posStart, double posEnd, double refAbs, double refMl, int isFitted =0, double* intensity_fit =NULL, double targetPos=0, double targetIntensity=0, int preResultMLIndex=-1, int preResultABSIndex=-1);

	CChartViewer m_chartPhaseAlign;
	virtual BOOL PreTranslateMessage(MSG* pMsg);	
	afx_msg void OnBnClickedButtonCoarseAlign();
	afx_msg void OnBnClickedButtonFineAlign();
	int m_AlignDirection;	
	CComboBox m_comboAlignDirection;
	int m_Slope;
	double m_RefelectanceMl;
	double m_RefelectanceAbs;
	double m_ResultPostionX_mm;
	double m_ResultPostionY_mm;
	int m_CapSensorNumber;
	double m_CapSensorSetValue;
	double m_CapSensorGetValue;
	BOOL m_IsMouseMoveClick;


	tuple<int, double, int, double, double> EuvPhaseCoarseAlign(double posX_mm, double posY_mm, int nDir, int coarsAlignType, double LsHalfPitch_um = 0.0, BOOL isFastMode = FALSE);	//nScopeType: 0(EUV), 1(SCOPE_OM4X), 2(SCOPE_OM100X), 3(SCOPE_ALL)	

	tuple<int, double> EuvPhaseFineAlign(double posX_mm, double posY_mm, int nDir, int slope, double refAbs, double refMl, BOOL isFastMode =FALSE, BOOL isMoveResultPos= FALSE);//nScopeType: 0(EUV), 1(SCOPE_OM4X), 2(SCOPE_OM100X), 3(SCOPE_ALL)	nDir: X_DIRECTION, X_DIRECTION
	int EuvPhaseFineAlign2(double posX_mm, double posY_mm, double &result_pos_mm, int nDir, int slope, double refAbs, double refMl);	//nScopeType: 0(EUV), 1(SCOPE_OM4X), 2(SCOPE_OM100X), 3(SCOPE_ALL)			

	//임시 검사용 좌표
	vector<double> m_LeftX_mm;
	vector<double> m_RightX_mm;
	vector<double> m_LeftY_mm;
	vector<double> m_RightY_mm;

	
	//int MoveZCapsensor(double targetPos_um, int maxTry, bool resetInterlock, int nRefCapSencor);
	//int SetZInterlockPhase();

private:
	void MouseChartClickMove();

	void CopyListToClipboard(CListBox* pListCtrl);
	BOOL isUpdatedCwData = TRUE;

public:	
	afx_msg void OnBnClickedCheckMouseClickMove();
	afx_msg void OnBnClickedButtonReadCapsensor();
	
	double m_MeasPt1PosX_mm;
	double m_MeasPt1PosY_mm;
	double m_MeasPt2PosX_mm;
	double m_MeasPt2PosY_mm;
	
	afx_msg void OnBnClickedButtonSetMeasurePos1FromAlignResult();
	afx_msg void OnBnClickedButtonSetMeasurePos2FromAlignResult();
	int m_AlignMarkType;
	CComboBox m_comboAlignMarkType;
	double m_distanceBetweenMeasurePoints_um;
	afx_msg void OnBnClickedButtonPhaseEuvAlign();
	int m_NumOfPhaseMeasureRepeat;
	afx_msg void OnBnClickedButtonPhaseMeasure();
	afx_msg void OnBnClickedButtonGetCenterFrequency();
	double m_PhaseResult;
	double m_PhasePoint1;
	double m_PhasePoint2;
	afx_msg void OnBnClickedButtonPhaseAlignX();

	double PhaseMeasure(double posX_mm, double posY_mm, BOOL isMaskCoordinate = FALSE);
	afx_msg void OnBnClickedButtonMovePos1();
	afx_msg void OnBnClickedButtonMovePos2();



	void PreTranslateMessagePhaseResultListBox(MSG* pMsg);
	void PreTranslateMessageCwEdit(MSG* pMsg);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnEnChangeEditContinuousWave();
	afx_msg void OnEnChangeEditCwDate();
	afx_msg void OnBnClickedButtonSetCw();
	afx_msg void OnBnClickedButtonResetCw();
	//double m_CenterWaveLengthFromCw;
	CComboBox m_comboCoarseAlignType;
	int m_CoarseAlignType;
	afx_msg void OnBnClickedButtonResetMeasurePosition();
	int SetCenterFrequency();	

	afx_msg void OnBnClickedButtonPhaseStop();

	BOOL m_IsStop = FALSE;
	double m_MeaPt1MaskCoordX;
	double m_MeaPt1MaskCoordY;	
	double m_MeaPt2MaskCoordX;
	double m_MeaPt2MaskCoordY;

	afx_msg void OnBnClickedButtonGetCurrentY();
	afx_msg void OnBnClickedButtonGetCurrentY2();
	double m_LsHalfPitch_um;
	afx_msg void OnBnClickedButtonMoveAndSetZInterlock();
	void PhasePositionUpdate(double PosX, double PosY);

	double m_dRefPosX = 0;
	double m_dRefPosY = 0;
	double m_dAbsPosX = 0;
	double m_dAbsPosY = 0;
	double m_dRelPosX = 0;
	double m_dRelPosY = 0;

	CEdit m_ctrlPosX;
	CEdit m_ctrlPosY;
	CEdit m_ctrlRelPosX;
	CEdit m_ctrlRelPosY;
	CString m_strRefPosX;
	CString m_strRefPosY;
	afx_msg void OnBnClickedButtonGetRefPos();
	void InitEditBox();

	afx_msg void OnEnChangeEditRefPosX();
	afx_msg void OnEnChangeEditRefPosY();
	afx_msg void OnBnClickedButtonTestException();
	void ExceptionTest1();
	void ExceptionTest2();
	afx_msg void OnBnClickedButtonPhaseApply();
	double m_ScanStep;
	int m_ScanNum;
	afx_msg void OnBnClickedButtonPhaseScan();
	double m_InitialFocusPos;
	int m_NumFocus;
	double m_ZStep_um;
	afx_msg void OnBnClickedButtonPhaseMeasureFocus();
	afx_msg void OnBnClickedButton1();
	double m_Zoffset;
	double m_Xshfit_um;
	BOOL m_UseAlign;
	BOOL m_checkFastAlign;

	double m_FastAlignZdistanceFromFocus_um = -20.0;
	afx_msg void OnBnClickedButtonBgUpdateMeasure();

	void BackGroundSet(double posX_mm, double posY_mm);

	//afx_msg void OnBnClickedButtonPhaseSetPoints();
	//afx_msg void OnBnClickedButtonPhaseProcess();
	//afx_msg void OnBnClickedButtonPhaseEuvManualAlign();
	afx_msg void OnBnClickedButtonPhaseEuvManualAlign();
	afx_msg void OnBnClickedButtonPhaseSetPoints();
	afx_msg void OnBnClickedButtonPhaseProcess();
};

class PhaseException : public exception
{
public:
	PhaseException(char const* const _Message) : exception(_Message)
	{
	}
	PhaseException(char const* const _Message, char* _filename, char* _fucntionName, int _line) : exception(_Message)
	{
		message.Format("%s", _Message);
		//fullPosition.Format("%s %s %d", _filename, _fucntionName, _line);
		fileName.Format("%s", _filename);
		fucntionName.Format("%s", _fucntionName);
		line.Format("%d", _line);
		//log->Display(0, fileName + " " + fucntionName + " " + line + " " + message);
	}

	PhaseException(char const* const _Message, char* _filename, char* _fucntionName, int _line, CLogDisplayDlg* _log) : exception(_Message)
	{
		message.Format("%s", _Message);
		//fullPosition.Format("%s %s %d", _filename, _fucntionName, _line);
		fileName.Format("%s", _filename);
		fucntionName.Format("%s", _fucntionName);
		line.Format("%d", _line);
		_log->Display(0, fileName + " " + fucntionName + " " + line + " " + message);
	}

	void Catched(char* _filename, char* _fucntionName, int _line, CLogDisplayDlg* _log)
	{	

		CString logMessage;
		logMessage.Format("%s %s %d %s %s", _filename, _fucntionName, _line, message, "-Catched");
		_log->Display(0, logMessage);
	}

	void CatchedAndThrow(char* _filename, char* _fucntionName, int _line, CLogDisplayDlg* _log)
	{
		CString logMessage;
		logMessage.Format("%s %s %d %s %s", _filename, _fucntionName, _line, message, "-Catched And Throw");
		_log->Display(0, logMessage);		
	}

	CString message;
	CString fileName;
	CString fucntionName;
	CString line;
	//CString fullPosition;
	CLogDisplayDlg* log;
	//g_pLog->Display(0, _T("CMaskMapDlg::OnStagePosGridDblClick !"));
};
