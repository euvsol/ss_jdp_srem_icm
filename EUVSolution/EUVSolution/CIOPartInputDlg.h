﻿#pragma once


// CDigitalInputPart 대화 상자

class CDigitalInputPart : public CDialogEx
{
	DECLARE_DYNAMIC(CDigitalInputPart)

public:
	CDigitalInputPart(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalInputPart();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_PART_INPUT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();

	HICON	m_LedIcon[3];

	CBrush  m_brush;
	CBrush  m_brush2;
	CFont	m_font;

	void InitControls_DI_PART();
	void OnUpdateDigitalInput_Part();

};
