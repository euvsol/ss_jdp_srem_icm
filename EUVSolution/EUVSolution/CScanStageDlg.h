﻿/**
 * Scan Stage Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


 // CScanStageDlg 대화 상자

class CScanStageDlg : public CDialogEx, public CPIE712Ctrl, public IObserver
{
	DECLARE_DYNAMIC(CScanStageDlg)

public:
	CScanStageDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CScanStageDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SCAN_STAGE_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	
	HICON m_LedIcon[2];

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();

	afx_msg void OnBnClickedButtonPimoveZero();
	afx_msg void OnBnClickedButtonPimoveOrigin();
	afx_msg void OnBnClickedButtonPiddlInit();
	afx_msg void OnBnClickedButtonPiStagescan();
	afx_msg void OnBnClickedButtonPiAbsoluteMove();
	afx_msg void OnBnClickedButtonPiXplus();
	afx_msg void OnBnClickedButtonPiXminus();
	afx_msg void OnBnClickedButtonPiYplus();
	afx_msg void OnBnClickedButtonPiYminus();
	afx_msg void OnBnClickedButtonPistageXyorigin();
	afx_msg void OnBnClickedButtonPiZplus();
	afx_msg void OnBnClickedButtonPiZminus();
	afx_msg void OnBnClickedButtonPistageZorigin();
	afx_msg void OnBnClickedButtonPiTipminus();
	afx_msg void OnBnClickedButtonPiTipplus();
	afx_msg void OnBnClickedButtonPiTiltplus();
	afx_msg void OnBnClickedButtonPiTiltminus();
	afx_msg void OnBnClickedButtonPistageXythetaorigin();
	CEdit m_editXmovestepCtrl;
	CEdit m_editYmovestepCtrl;
	CEdit m_editZmovestepCtrl;
	CEdit m_editTipmovestepCtrl;
	CEdit m_editTiltmovestepCtrl;
	CEdit m_editXAbsolutePosCtrl;
	CEdit m_editYAbsolutePosCtrl;
	CEdit m_editZAbsolutePosCtrl;
	CEdit m_editTipAbsolutePosCtrl;
	CEdit m_editTiltAbsolutePosCtrl;
	CEdit m_editPIStatusCtrl;
	CStatic m_StaticPIStatusGridCtrl;
	afx_msg void OnDeltaposSpinPiX(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinPiY(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinPiZ(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinPiXtheta(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinPiYtheta(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedButtonPiScanrecord();

	CGridCtrl m_PIStageGrid;
	void InitPIStageGrid();
	void UpdateDataScanStage();					// UI상의 grid에 feedback position을 뿌려준다.
	BOOL m_bZHoldOn;							// ELitho에서 Z Hold 상태 중인지 판단하는 Flag
	double m_ElithoGap_um = 7;
	void SetElithoGap(double gap_um);

	int MoveRelative(int axis, double pos_um);
	void MoveThroughfocusZ();					//Throughfocus용으로 z axis을 내림

	//External Module에서 사용하는 함수(ex:물류 Sequence 모듈)
	int Start_Scan(int nScanMode, int nFov, int nStepSize, int nRepeatNo);
	int Start_Scan_For_Test(int nScanMode, int nFov, int nStepSize, int nRepeatNo); //BeamFinding

	int Stop_Scan();
	int Move_Origin();

	BOOL Is_SCAN_Stage_Connected() { return m_bConnect; }

	afx_msg void OnBnClickedCheckServoOnoff();
	afx_msg void OnBnClickedReConnectionScanStage();
	afx_msg void OnBnClickedDisconnectionScanStage();

	void Stage_Connected_Check();
	CButton m_PIbtnYPlusCtrl;
	CButton m_PIbtnYMinusCtrl;
	CButton m_PIbtnXPlusCtrl;
	CButton m_PIbtnXMinusCtrl;
	CButton m_PIbtnZPlusCtrl;
	CButton m_PIbtnZMinusCtrl;
	CButton m_PIbtnTYPlusCtrl;
	CButton m_PIbtnTYMinusCtrl;
	CButton m_PIbtnTXPlusCtrl;
	CButton m_PIbtnTXMinusCtrl;

	void	EmergencyStop();
};
