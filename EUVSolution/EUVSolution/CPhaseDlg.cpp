﻿// CPhaseDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

#include "EUVSolution.h"
#include "CPhaseDlg.h"
#include "afxdialogex.h"

#include <vector>
#include <tuple>
#define _USE_MATH_DEFINES
#include <cmath>
#include <algorithm>

using namespace std;

// CPhaseDlg 대화 상자
IMPLEMENT_DYNAMIC(CPhaseDlg, CDialogEx)

CPhaseDlg::CPhaseDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PHASE_DIALOG, pParent)
	, m_AlignDirection(0)
	, m_Slope(0)
	, m_RefelectanceMl(0)
	, m_RefelectanceAbs(0)
	, m_CapSensorNumber(0)
	, m_CapSensorSetValue(0)
	, m_CapSensorGetValue(0)
	, m_IsMouseMoveClick(FALSE)
	, m_ResultPostionX_mm(0)
	, m_ResultPostionY_mm(0)
	, m_distanceBetweenMeasurePoints_um(5)
	, m_MeasPt1PosX_mm(0)
	, m_MeasPt1PosY_mm(0)
	, m_MeasPt2PosX_mm(0)
	, m_MeasPt2PosY_mm(0)
	, m_AlignMarkType(0)
	, m_NumOfPhaseMeasureRepeat(3)
	, m_PhaseResult(0)
	, m_PhasePoint1(0)
	, m_PhasePoint2(0)
	, m_CoarseAlignType(0)
	, m_MeaPt1MaskCoordX(0)
	, m_MeaPt1MaskCoordY(0)
	, m_LsHalfPitch_um(6)
	, m_strRefPosX(_T(""))
	, m_strRefPosY(_T(""))
	, m_ScanStep(5)
	, m_ScanNum(5)
	, m_InitialFocusPos(5)
	, m_NumFocus(14)
	, m_ZStep_um(-1)
	, m_Zoffset(0)
	, m_Xshfit_um(0.1)
	, m_UseAlign(FALSE)
	, m_checkFastAlign(TRUE)
{

}

CPhaseDlg::~CPhaseDlg()
{
	XYChart *c = (XYChart *)m_chartPhaseAlign.getChart();
	if (c != NULL)
		delete c;
}

void CPhaseDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHART_PHASE_ALIGN, m_chartPhaseAlign);
	DDX_CBIndex(pDX, IDC_COMBO_ALIGN_DIRECTION, m_AlignDirection);
	DDX_Control(pDX, IDC_COMBO_ALIGN_DIRECTION, m_comboAlignDirection);
	DDX_Text(pDX, IDC_EDIT_SLOPE, m_Slope);
	DDX_Text(pDX, IDC_EDIT_REFLECTANCE_ML, m_RefelectanceMl);
	DDX_Text(pDX, IDC_EDIT_REFLECTANCE_ABS, m_RefelectanceAbs);
	DDX_Text(pDX, IDC_EDIT_CAPSENSOR_NUMBER, m_CapSensorNumber);
	DDX_Text(pDX, IDC_EDIT_CAPSENSOR_SET_VALUE, m_CapSensorSetValue);
	DDX_Text(pDX, IDC_EDIT_CAPSENSOR_GET_VALUE, m_CapSensorGetValue);
	DDX_Check(pDX, IDC_CHECK_MOUSE_CLICK_MOVE, m_IsMouseMoveClick);
	DDX_Text(pDX, IDC_EDIT_CAP1, g_pAdam->AdamData.m_dCapsensor1);
	DDX_Text(pDX, IDC_EDIT_CAP2, g_pAdam->AdamData.m_dCapsensor2);
	DDX_Text(pDX, IDC_EDIT_CAP3, g_pAdam->AdamData.m_dCapsensor3);
	DDX_Text(pDX, IDC_EDIT_CAP4, g_pAdam->AdamData.m_dCapsensor4);
	DDX_Text(pDX, IDC_EDIT_RESULT_POSITION_X, m_ResultPostionX_mm);
	DDX_Text(pDX, IDC_EDIT_RESULT_POSITION_Y, m_ResultPostionY_mm);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT1_POS_X, m_MeasPt1PosX_mm);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT1_POS_Y, m_MeasPt1PosY_mm);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT2_POS_X, m_MeasPt2PosX_mm);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT2_POS_Y, m_MeasPt2PosY_mm);
	DDX_CBIndex(pDX, IDC_COMBO_SELECT_ALIGN_MARK_TYPE, m_AlignMarkType);
	DDX_Control(pDX, IDC_COMBO_SELECT_ALIGN_MARK_TYPE, m_comboAlignMarkType);
	DDX_Text(pDX, IDC_EDIT_NUMBER_OF_PHASE_MEASURE_REPEAT, m_NumOfPhaseMeasureRepeat);

	DDX_Text(pDX, IDC_EDIT_CENTER_FREQUENCY_INDEX, m_CenterFreqIndex);
	DDX_Text(pDX, IDC_EDIT_CENTER_WAVE_LENTH_FROM_IMAGE, m_CenterWaveLengthFromFft);

	DDX_Text(pDX, IDC_EDIT_REFERENCE_PHASE, m_ReferencePhase);
	DDX_Text(pDX, IDC_EDIT_PHASE_RESULT, m_PhaseResult);
	DDX_Text(pDX, IDC_EDIT_PHASE_POINT1, m_PhasePoint1);
	DDX_Text(pDX, IDC_EDIT_PHASE_POINT2, m_PhasePoint2);
	DDX_Control(pDX, IDC_LIST_PHASE_RESULT, m_listPhaseResult);
	DDX_Text(pDX, IDC_EDIT_CONTINUOUS_WAVE, g_pConfig->m_dContinuousWave);
	DDX_Text(pDX, IDC_EDIT_CW_DATE, g_pConfig->m_nContinuousWaveDate);
	DDX_Text(pDX, IDC_EDIT_WAVE_LENTH_FROM_CW, m_CenterWaveLengthFromCw);
	DDX_Control(pDX, IDC_COMBO_COASE_ALIGN_TYPE, m_comboCoarseAlignType);
	DDX_CBIndex(pDX, IDC_COMBO_COASE_ALIGN_TYPE, m_CoarseAlignType);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT1_MASK_COORD_POS_X, m_MeaPt1MaskCoordX);
	DDX_Text(pDX, IDC_EDIT_MEAS_PT1_MASK_COORD_POS_Y, m_MeaPt1MaskCoordY);
	DDX_Text(pDX, IDC_EDIT_LS_HALF_PITCH, m_LsHalfPitch_um);
	DDX_Control(pDX, IDC_EDIT_STAGE_POS_X, m_ctrlPosX);
	DDX_Control(pDX, IDC_EDIT_STAGE_POS_Y, m_ctrlPosY);
	DDX_Control(pDX, IDC_EDIT_REL_POS_X, m_ctrlRelPosX);
	DDX_Control(pDX, IDC_EDIT_REL_POS_Y, m_ctrlRelPosY);
	DDX_Text(pDX, IDC_EDIT_REF_POS_X, m_strRefPosX);
	DDX_Text(pDX, IDC_EDIT_REF_POS_Y, m_strRefPosY);
	DDX_Text(pDX, IDC_EDIT_PHASE_SCAN_STEP, m_ScanStep);
	DDX_Text(pDX, IDC_EDIT_SCAN_NUM, m_ScanNum);
	DDX_Text(pDX, IDC_EDIT_PHASE_INIT_FOCUS, m_InitialFocusPos);
	DDX_Text(pDX, IDC_EDIT_PHASE_ITER_NUM, m_NumFocus);
	DDX_Text(pDX, IDC_EDIT_PHASE_FOCUS_STEP, m_ZStep_um);
	DDX_Text(pDX, IDC_EDIT_PHASE_Z_OFFSET, m_Zoffset);
	DDX_Text(pDX, IDC_EDIT_PHASE_Y_SHIFT, m_Xshfit_um);
	DDX_Check(pDX, IDC_CHECK_PHASE_USE_ALIGN, m_UseAlign);
	DDX_Check(pDX, IDC_CHECK_PHASE_FAST_ALIGN, m_checkFastAlign);
}


BEGIN_MESSAGE_MAP(CPhaseDlg, CDialogEx)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_COARSE_ALIGN, &CPhaseDlg::OnBnClickedButtonCoarseAlign)
	ON_BN_CLICKED(IDC_BUTTON_FINE_ALIGN, &CPhaseDlg::OnBnClickedButtonFineAlign)
	ON_BN_CLICKED(IDC_CHECK_MOUSE_CLICK_MOVE, &CPhaseDlg::OnBnClickedCheckMouseClickMove)
	ON_BN_CLICKED(IDC_BUTTON_READ_CAPSENSOR, &CPhaseDlg::OnBnClickedButtonReadCapsensor)
	ON_BN_CLICKED(IDC_BUTTON_SET_MEASURE_POS1_FROM_ALIGN_RESULT, &CPhaseDlg::OnBnClickedButtonSetMeasurePos1FromAlignResult)
	ON_BN_CLICKED(IDC_BUTTON_SET_MEASURE_POS2_FROM_ALIGN_RESULT, &CPhaseDlg::OnBnClickedButtonSetMeasurePos2FromAlignResult)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_EUV_ALIGN, &CPhaseDlg::OnBnClickedButtonPhaseEuvAlign)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_MEASURE, &CPhaseDlg::OnBnClickedButtonPhaseMeasure)
	ON_BN_CLICKED(IDC_BUTTON_GET_CENTER_FREQUENCY, &CPhaseDlg::OnBnClickedButtonGetCenterFrequency)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_ALIGN_X, &CPhaseDlg::OnBnClickedButtonPhaseAlignX)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_POS1, &CPhaseDlg::OnBnClickedButtonMovePos1)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_POS2, &CPhaseDlg::OnBnClickedButtonMovePos2)
	ON_WM_KEYDOWN()
	ON_WM_CTLCOLOR()
	ON_EN_CHANGE(IDC_EDIT_CONTINUOUS_WAVE, &CPhaseDlg::OnEnChangeEditContinuousWave)
	ON_EN_CHANGE(IDC_EDIT_CW_DATE, &CPhaseDlg::OnEnChangeEditCwDate)
	ON_BN_CLICKED(IDC_BUTTON_SET_CW, &CPhaseDlg::OnBnClickedButtonSetCw)
	ON_BN_CLICKED(IDC_BUTTON_RESET_CW, &CPhaseDlg::OnBnClickedButtonResetCw)
	ON_BN_CLICKED(IDC_BUTTON_RESET_MEASURE_POSITION, &CPhaseDlg::OnBnClickedButtonResetMeasurePosition)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_STOP, &CPhaseDlg::OnBnClickedButtonPhaseStop)
	ON_BN_CLICKED(IDC_BUTTON_GET_CURRENT_Y, &CPhaseDlg::OnBnClickedButtonGetCurrentY)
	ON_BN_CLICKED(IDC_BUTTON_GET_CURRENT_Y2, &CPhaseDlg::OnBnClickedButtonGetCurrentY2)
	ON_BN_CLICKED(IDC_BUTTON_MOVE_AND_SET_Z_INTERLOCK, &CPhaseDlg::OnBnClickedButtonMoveAndSetZInterlock)
	ON_BN_CLICKED(IDC_BUTTON_GET_REF_POS, &CPhaseDlg::OnBnClickedButtonGetRefPos)
	ON_EN_CHANGE(IDC_EDIT_REF_POS_X, &CPhaseDlg::OnEnChangeEditRefPosX)
	ON_EN_CHANGE(IDC_EDIT_REF_POS_Y, &CPhaseDlg::OnEnChangeEditRefPosY)
	ON_BN_CLICKED(IDC_BUTTON_TEST_EXCEPTION, &CPhaseDlg::OnBnClickedButtonTestException)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_APPLY, &CPhaseDlg::OnBnClickedButtonPhaseApply)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_SCAN, &CPhaseDlg::OnBnClickedButtonPhaseScan)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_MEASURE_FOCUS, &CPhaseDlg::OnBnClickedButtonPhaseMeasureFocus)
	ON_BN_CLICKED(IDC_BUTTON1, &CPhaseDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_BG_UPDATE_MEASURE, &CPhaseDlg::OnBnClickedButtonBgUpdateMeasure)

	//ON_BN_CLICKED(IDC_BUTTON_PHASE_SET_POINTS, &CPhaseDlg::OnBnClickedButtonPhaseSetPoints)
	//ON_BN_CLICKED(IDC_BUTTON_PHASE_PROCESS, &CPhaseDlg::OnBnClickedButtonPhaseProcess)
	//ON_BN_CLICKED(IDC_BUTTON_PHASE_EUV_MANUAL_ALIGN, &CPhaseDlg::OnBnClickedButtonPhaseEuvManualAlign)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_EUV_MANUAL_ALIGN, &CPhaseDlg::OnBnClickedButtonPhaseEuvManualAlign)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_SET_POINTS, &CPhaseDlg::OnBnClickedButtonPhaseSetPoints)
	ON_BN_CLICKED(IDC_BUTTON_PHASE_PROCESS, &CPhaseDlg::OnBnClickedButtonPhaseProcess)
END_MESSAGE_MAP()


// CPhaseDlg 메시지 처리기


BOOL CPhaseDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  여기에 추가 초기화 작업을 추가합니다.

	//MIL initailize


	MilInitialize(g_milSystemHost);
	SetLogPath(PHASE_RESULT_PATH);

	InitEditBox();

	// Set initial mouse usage for ChartViewer
	m_chartPhaseAlign.setMouseUsage(Chart::MouseUsageDefault);
	//m_chartPhaseAlign.setScrollDirection(Chart::DirectionHorizontalVertical);
	//m_chartPhaseAlign.setZoomDirection(Chart::DirectionHorizontalVertical);

	// Trigger the ViewPortChanged event to draw the chart
	//m_chartPhaseAlign.updateViewPort(true, true);

	// Enable mouse wheel zooming by setting the zoom ratio to 1.1 per wheel event
	//m_chartPhaseAlign.setMouseWheelZoomRatio(1.1);
	chartAlignUpdate(NULL, NULL, 0, 0, 0, 0, 0, 0);

	m_comboAlignDirection.AddString(_T("X_DIRECTION"));//0
	m_comboAlignDirection.AddString(_T("Y_DIRECTION"));//1
	m_comboAlignDirection.SetCurSel(X_DIRECTION);

	m_comboAlignMarkType.AddString(_T("DOWN_RIGHT ┏━"));
	m_comboAlignMarkType.AddString(_T("DOWN_LEFT ━┓"));
	m_comboAlignMarkType.AddString(_T("UP_RIGHT ┗━"));
	m_comboAlignMarkType.AddString(_T("UP_LEFT ━┛"));
	m_comboAlignMarkType.SetCurSel(UP_RIGHT);

	m_comboCoarseAlignType.AddString(_T("ALIGN_PAD"));//0
	m_comboCoarseAlignType.AddString(_T("ALIGN_LS"));//1
	m_comboCoarseAlignType.SetCurSel(COARSE_ALIGN_PAD);


	//┏ down right ┓down left
	//┗(up right   ┛up left
	UpdateData(TRUE);  // combox 선택된것 변수로 업데이트

	m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}


void CPhaseDlg::chartAlignUpdate(double* pos, double* intensity, int N, double posStart, double posEnd, double refAbs, double refMl, int isFitted, double* intensity_fit, double targetPos, double targetIntensity, int preResultMLIndex, int preResultABSIndex)
{
	// Create a XYChart object of fsize 450 x 450 pixels
	//delete m_chartPhaseAlign.getChart();
	XYChart *c = (XYChart *)m_chartPhaseAlign.getChart();

	if (c != NULL)
		delete c;

	c = new XYChart(675, 375);

	// Set the plotarea at (55, 65) and of size 350 x 300 pixels, with white background and a light
	// grey border (0xc0c0c0). Turn on both horizontal and vertical grid lines with light grey color
	// (0xc0c0c0)
	c->setPlotArea(65, 65, 500, 250, 0xffffff, -1, 0xc0c0c0, 0xc0c0c0, -1);

	c->addLegend(50, 30, false, "arial.ttf", 12)->setBackground(Chart::Transparent);

	c->addTitle("EUV Alignment", "arial.ttf", 18);
	c->yAxis()->setTitle("Intensity(au)", "arial.ttf", 12);
	c->yAxis()->setWidth(2);
	c->xAxis()->setTitle("Position(mm)", "arial.ttf", 12);
	c->xAxis()->setWidth(2);

	double min = 0, max = 0;
	if (N > 0)
	{
		min = intensity[0];
		max = intensity[0];
		for (int i = 0; i < N; i++)
		{
			if (intensity[i] < min)
			{
				min = intensity[i];
			}
			else if (intensity[i] > max)
			{
				max = intensity[i];
			}
		}
	}


	double xAxisMargin = (posEnd - posStart)*0.015;
	double yAxisMargin = (refMl - refAbs)*0.03;

	double ylim_min = refAbs - yAxisMargin;
	double ylim_max = refMl + yAxisMargin;

	if (min < ylim_min)
		ylim_min = min;
	if (max > ylim_max)
		ylim_max = max;

	c->xAxis()->setLinearScale(posStart - xAxisMargin, posEnd + xAxisMargin, 0, 0);
	c->yAxis()->setLinearScale(ylim_min, ylim_max, 0, 0);

	ScatterLayer *scatter1 = c->addScatterLayer(DoubleArray(pos, N), DoubleArray(intensity, N), "Measure Data", Chart::TriangleSymbol, 8, 0x008800);


	//0: Not end 1: coase Ended 2:Fine Ended 3:Fine1 4:Coarse prealign end
	if (isFitted == 1)
	{
		double* pos_sorted = new double[N];
		double* intensity_sorted = new double[N];

		vector <pair<double, double>> v_pair;
		v_pair.reserve(N);

		for (int i = 0; i < N; i++)
		{
			v_pair.push_back(pair<double, double>(pos[i], intensity[i]));
		}

		//sort(v_pair.begin(), v_pair.end(),less<>());//greater
		sort(v_pair.begin(), v_pair.end());

		for (int i = 0; i < v_pair.size(); i++)
		{
			pos_sorted[i] = v_pair[i].first;
			intensity_sorted[i] = v_pair[i].second;
		}

		//LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity_sorted, N), 0x3333ff, "Measure line");
		LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity_sorted, N), 0x3333ff, "Fitted Data");
		layer1->setXData(DoubleArray(pos_sorted, N));
		layer1->setLineWidth(2);

		delete[] pos_sorted;
		delete[] intensity_sorted;

		ScatterLayer *scatter2 = c->addScatterLayer(DoubleArray(&pos[N - 1], 1), DoubleArray(&intensity[N - 1], 1), "Center Point", Chart::DiamondSymbol, 15, 0xff0000);

	}
	else if (isFitted == 2)//
	{
		LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity_fit, N), 0x3333ff, "Fitted Data");
		layer1->setXData(DoubleArray(pos, N));
		layer1->setLineWidth(2);

		ScatterLayer *scatter2 = c->addScatterLayer(DoubleArray(&targetPos, 1), DoubleArray(&targetIntensity, 1), "Center Point", Chart::DiamondSymbol, 15, 0xff0000);
	}
	else if (isFitted == 3)  // fine align1
	{
		LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity, N), 0x3333ff, "Fitted Data");
		layer1->setXData(DoubleArray(pos, N));
		layer1->setLineWidth(2);

		ScatterLayer *scatter2 = c->addScatterLayer(DoubleArray(&targetPos, 1), DoubleArray(&targetIntensity, 1), "Center Point", Chart::DiamondSymbol, 15, 0xff0000);
	}
	else if (isFitted == 4)
	{
		double* pos_sorted = new double[N];
		double* intensity_sorted = new double[N];

		vector <pair<double, double>> v_pair;
		v_pair.reserve(N);

		for (int i = 0; i < N; i++)
		{
			v_pair.push_back(pair<double, double>(pos[i], intensity[i]));
		}

		//sort(v_pair.begin(), v_pair.end(),less<>());//greater
		sort(v_pair.begin(), v_pair.end());

		for (int i = 0; i < v_pair.size(); i++)
		{
			pos_sorted[i] = v_pair[i].first;
			intensity_sorted[i] = v_pair[i].second;
		}

		//LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity_sorted, N), 0x3333ff, "Measure line");
		LineLayer *layer1 = c->addLineLayer(DoubleArray(intensity_sorted, N), 0x3333ff, "Fitted Data");
		layer1->setXData(DoubleArray(pos_sorted, N));
		layer1->setLineWidth(2);

		delete[] pos_sorted;
		delete[] intensity_sorted;

		ScatterLayer *scatter2 = c->addScatterLayer(DoubleArray(&targetPos, 1), DoubleArray(&targetIntensity, 1), "Center Point", Chart::DiamondSymbol, 15, 0xff0000);

	}


	if (preResultMLIndex > -1)
	{
		double preResultPos[2];
		double preResultIntensity[2];
		preResultPos[0] = pos[preResultMLIndex];
		preResultPos[1] = pos[preResultABSIndex];

		preResultIntensity[0] = intensity[preResultMLIndex];
		preResultIntensity[1] = intensity[preResultABSIndex];

		ScatterLayer *scatter3 = c->addScatterLayer(DoubleArray(preResultPos, N), DoubleArray(preResultIntensity, N), "Pre Align", Chart::DiamondSymbol, 15, 0xffff00);
	}

	// Output the chart
	c->makeChart();

	// Output the chart
	m_chartPhaseAlign.setChart(c);

	if (0 == m_chartPhaseAlign.getImageMapHandler())
	{
		// no existing image map - creates a new one
		//m_ChartHistogram.setImageMap(xyChart->getHTMLImageMap("clickable", "","title='x = {x}, y = {value}'"));
		//m_chartPhaseAlign.setImageMap(c->getHTMLImageMap("clickable", "", "title='x = {x}, y = {value}'"));
		m_chartPhaseAlign.setImageMap(c->getHTMLImageMap("", "", "title='x = {x}, y = {value}'"));
	}

	//free up resources
	//delete c;

	//Green (0x008800)
	//Red (0xff0000) 
	//Blue (0x3333ff)

}

void CPhaseDlg::PreTranslateMessagePhaseResultListBox(MSG* pMsg)
{
	switch (pMsg->wParam)
	{
	case 'C':
		if (GetKeyState(VK_CONTROL) < 0)
		{
			if (pMsg->hwnd == m_listPhaseResult.GetSafeHwnd() && GetFocus()->GetSafeHwnd() == m_listPhaseResult.GetSafeHwnd())
			{
				CopyListToClipboard(&m_listPhaseResult);
			}
		}
		break;
	case 'A':
		if (GetKeyState(VK_CONTROL) < 0)
		{
			m_listPhaseResult.SelItemRange(TRUE, 0, m_listPhaseResult.GetCount());
		}
		break;
	case VK_ESCAPE:
		m_listPhaseResult.SelItemRange(FALSE, 0, m_listPhaseResult.GetCount());
		//return TRUE;  //창 사라짐 방지
		break;
		//VK_ESCAPE
	default:
		break;
	}
}

void CPhaseDlg::PreTranslateMessageCwEdit(MSG* pMsg)
{
	if (pMsg->wParam == VK_RETURN)
	{
		//UpdateData(TRUE);
		//
		//
		//UpdateData(FALSE);
	}
}
BOOL CPhaseDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_LBUTTONDOWN)
	{
		if (pMsg->hwnd == GetDlgItem(IDC_CHART_PHASE_ALIGN)->GetSafeHwnd())
		{
			MouseChartClickMove();
		}
		//return TRUE;		
	}
	if (pMsg->message == WM_KEYDOWN)
	{
		// Phase 결과 리스트 박스 
		if (pMsg->hwnd == GetDlgItem(IDC_LIST_PHASE_RESULT)->GetSafeHwnd())
		{
			PreTranslateMessagePhaseResultListBox(pMsg);
		}
		// Cotinuous Wave Update 
		if (pMsg->hwnd == GetDlgItem(IDC_EDIT_CONTINUOUS_WAVE)->GetSafeHwnd() || GetDlgItem(IDC_EDIT_CW_DATE)->GetSafeHwnd())
		{
			//PreTranslateMessageCwEdit(pMsg);	
		}

		if (pMsg->wParam == VK_ESCAPE || pMsg->wParam == VK_RETURN)
			return TRUE;
	}

	return __super::PreTranslateMessage(pMsg);
}


// Z축 Focus 위치에서 30um 아래로  Align 시작함, Z축 이동 없어서 함수 실행후 z축 복귀 해야함
void CPhaseDlg::OnBnClickedButtonCoarseAlign()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonCoarseAlign() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = " Coarse Align 중입니다!";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	UpdateData(TRUE);
	int nRetrun = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		nRetrun = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;


	int dir = m_AlignDirection;
	int coarseAlignType = m_CoarseAlignType;

	double x_mm = g_pNavigationStage->GetCurrentPosX();
	double y_mm = g_pNavigationStage->GetCurrentPosY();


	// EuvPhaseCoarseAlign()에서 z축 세팅함, 삭제가능
	//1. Z축 이동 및 인터락세팅
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		MsgBoxAuto.DoModal(_T(" Z축  제어에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRetrun = -2;
		return;
	}
	UpdateData(FALSE);


	//2. Coarse Align 수행
	tuple<int, double, int, double, double> coarseReturn;
	coarseReturn = EuvPhaseCoarseAlign(x_mm, y_mm, dir, coarseAlignType, m_LsHalfPitch_um, m_checkFastAlign);
	nRetrun = get<0>(coarseReturn);


	////1. Z축 이동 및 인터락세팅
	//if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	//{
	//	MsgBoxAuto.DoModal(_T(" Z축  제어에 실패했습니다 ! "), 2);
	//	g_pWarning->ShowWindow(SW_HIDE);
	//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	//	nRetrun = -2;
	//	return;
	//}
	//UpdateData(FALSE);


	////3. Coarse Align 결과 좌표로 이동 (Z축 이동없음)
	//double ResultPostion_mm = get<1>(coarseReturn);

	//if (nRetrun == 0)
	//{
	//	if (dir == X_DIRECTION)
	//	{
	//		g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, y_mm);
	//		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(ResultPostion_mm, y_mm); //z축 이동없음

	//	}
	//	else if (dir == Y_DIRECTION)
	//	{
	//		g_pNavigationStage->MoveAbsolutePosition(x_mm, ResultPostion_mm);
	//		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x_mm, ResultPostion_mm); //z축 이동없음
	//	}
	//}
	//else
	//{
	//	MsgBoxAuto.DoModal(_T(" Coarse Align에 실패했습니다 ! "), 2);
	//	g_pWarning->ShowWindow(SW_HIDE);
	//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	//	nRetrun = -3;

	//	return;

	//}

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}

// Z축 Focus 위치에서 30um 아래로  Align 시작함, Z축 이동 없어서 함수 실행후 z축 복귀 해야함
void CPhaseDlg::OnBnClickedButtonFineAlign()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonFineAlign() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = " Fine Align 중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	UpdateData(TRUE);
	int nRetrun = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);


	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		nRetrun = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;


	int dir = m_AlignDirection;
	double x_mm = g_pNavigationStage->GetCurrentPosX();
	double y_mm = g_pNavigationStage->GetCurrentPosY();


	//EuvPhaseFineAlign()에서 z축 세팅함 삭제가능
	//1. Z축 이동 및 인터락세팅
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		MsgBoxAuto.DoModal(_T(" Z축 세팅에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRetrun = -2;
		return;
	}
	UpdateData(FALSE);

	int slope = m_Slope;
	double refAbs = m_RefelectanceAbs;
	double refMl = m_RefelectanceMl;


	//2. Fine Align 수행
	tuple<int, double> fineResult;

	BOOL moveAlignResultPos = TRUE;
	fineResult = EuvPhaseFineAlign(x_mm, y_mm, dir, slope, refAbs, refMl, m_checkFastAlign, moveAlignResultPos);
	nRetrun = get<0>(fineResult);
	double ResultPostion_mm = get<1>(fineResult);


	////1. Z축 이동 및 인터락세팅
	//if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	//{
	//	MsgBoxAuto.DoModal(_T(" Z축 세팅에 실패했습니다 ! "), 2);
	//	g_pWarning->ShowWindow(SW_HIDE);
	//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	//	nRetrun = -2;
	//	return;
	//}
	//UpdateData(FALSE);

	////3. Align 포인트로 이동 (Z축 이동 없음)
	//if (nRetrun == 0)
	//{
	//	if (dir == X_DIRECTION)
	//	{
	//		g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, y_mm);
	//		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(ResultPostion_mm, y_mm); //z축 이동없음
	//	}
	//	else if (dir == Y_DIRECTION)
	//	{
	//		g_pNavigationStage->MoveAbsolutePosition(x_mm, ResultPostion_mm);
	//		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x_mm, ResultPostion_mm); //z축 이동없음
	//	}
	//}
	//else
	//{
	//	MsgBoxAuto.DoModal(_T(" Fine Align에 실패했습니다 ! "), 2);
	//	g_pWarning->ShowWindow(SW_HIDE);
	//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	//	nRetrun = -3;
	//	return;
	//}


	// UI에 결과 업데이트 추가 필요
	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}


// Z축 Focus 위치에서 30um 아래로  Align 시작함, Z축 이동 없어서 함수 실행후 z축 복귀 해야함
// Stage 이동시 Z축 이동 안함
tuple<int, double, int, double, double> CPhaseDlg::EuvPhaseCoarseAlign(double posX_mm, double posY_mm, int nDir, int coarsAlignType, double LsHalfPitch_um, BOOL isFastMode)
{
	int nRet = 0;

	// 1. 변수 초기화
	//m_ResultPostionX_mm = posX_mm;
	//m_ResultPostionY_mm = posY_mm;
	m_IsStop = FALSE;

	// UI update
	m_AlignDirection = nDir;//UI 연결변수
	m_CoarseAlignType = coarsAlignType;//UI 연결변수
	UpdateData(FALSE);

	double* Pos;
	double* Intensity;
	int N = 0;
	int maxN = 0;
	int N_half_preAlign = 2;
	int N_preAlign;

	N_half_preAlign = 20;//5

	N_preAlign = N_half_preAlign * 2 + 1;

	if (coarsAlignType == COARSE_ALIGN_PAD)
		maxN = g_pConfig->m_nMaximumTryCount + 2; // +2는 low, high에서 한번씩 측정함
	else if (coarsAlignType == COARSE_ALIGN_LS)
		maxN = g_pConfig->m_nMaximumTryCount + N_preAlign + 1;  // +1은 N_preAlign시 ML, ABS 순서 바뀌면 pitch 만큼 가서 한번 더 측정

	Pos = new double[maxN];
	Intensity = new double[maxN];

	double low, high, mid;
	double intensityLow, intensityHigh, intensityMid, intensityTarget, tolerance;
	int BinarySearchSlope = 0;
	double refAbs = 0, refMl = 0;

	double x, y;
	double result_pos_mm;

	double pos_min;
	double pos_max;

	int iterCount = 0;

	int minIndex = -1, maxIndex = -1;

	if (!((nDir == X_DIRECTION || nDir == Y_DIRECTION) && (coarsAlignType == COARSE_ALIGN_PAD || coarsAlignType == COARSE_ALIGN_LS)))
	{
		delete[] Pos;
		delete[] Intensity;
		nRet = -1;
		return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
	}

	//2. LS Coarse Align 초기화 (low, half pitch만큼 이동해서 low, high 찾음)
	if (coarsAlignType == COARSE_ALIGN_LS)
	{
		double stepWidth;
		if (LsHalfPitch_um == 0)
		{
			delete[] Pos;
			delete[] Intensity;
			nRet = -8;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}

		stepWidth = LsHalfPitch_um / 1000. / 2.;

		double min, max;

		if (nDir == X_DIRECTION)
		{
			pos_min = posX_mm + (0 - N_half_preAlign)*stepWidth;
			pos_max = posX_mm + (N_preAlign - N_half_preAlign)*stepWidth;
		}
		else if (nDir == Y_DIRECTION)
		{
			pos_min = posY_mm + (0 - N_half_preAlign)*stepWidth;
			pos_max = posY_mm + (N_preAlign - N_half_preAlign)*stepWidth;
		}


		//최초 위치로 이동
		if (nDir == X_DIRECTION)
		{
			x = posX_mm + (0 - N_half_preAlign)*stepWidth;
			y = posY_mm;
		}
		else if (nDir == Y_DIRECTION)
		{
			y = posY_mm + (0 - N_half_preAlign)*stepWidth;
			x = posX_mm;
		}

		double current_x_mm = g_pNavigationStage->GetCurrentPosX();
		double current_y_mm = g_pNavigationStage->GetCurrentPosY();

		if (abs(current_x_mm - posX_mm) > 0.001 || abs(current_y_mm - posY_mm) > 0.001)
		{
			g_pNavigationStage->MoveAbsolutePosition(x, y);
		}

		if (isFastMode)
		{
			//1. Z축 이동 및 인터락세팅
			if (g_pAdam->MoveZCapsensorFastAlignFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, m_FastAlignZdistanceFromFocus_um) != 0)
			{
				delete[] Pos;
				delete[] Intensity;
				nRet = -2;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}
			UpdateData(FALSE);
		}

		for (int i = 0; i < N_preAlign; i++)
		{
			if (nDir == X_DIRECTION)
			{
				x = posX_mm + (i - N_half_preAlign)*stepWidth;
				y = posY_mm;
			}
			else if (nDir == Y_DIRECTION)
			{
				y = posY_mm + (i - N_half_preAlign)*stepWidth;
				x = posX_mm;
			}


			if (m_IsStop)
			{
				if (isFastMode)
				{
					g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
					UpdateData(FALSE);
				}
				delete[] Pos;
				delete[] Intensity;
				nRet = -7;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}

			// 두번째 포인트 이동
			if (isFastMode)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y); //z축 이동없음
			}
			else
			{
				g_pNavigationStage->MoveAbsolutePosition(x, y);
			}

			if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
			{

				if (isFastMode)
				{
					g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
					UpdateData(FALSE);
				}
				delete[] Pos;
				delete[] Intensity;
				nRet = -2;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}
			Intensity[i] = g_pXrayCamera->GetIntensity();
			//Intensity[i] = g_pXrayCamera->intensity_simulation(x, y);
			Pos[i] = nDir ? y : x;
			N++;


			// Chart Update
			minIndex = 0;
			maxIndex = 0;
			min = Intensity[0];
			max = Intensity[0];

			for (int j = 0; j <= i; j++)
			{
				if (Intensity[j] < min)
				{
					minIndex = j;
					min = Intensity[j];
				}
				else if (Intensity[j] > max)
				{
					maxIndex = j;
					max = Intensity[j];
				}
			}

			g_pPhase->chartAlignUpdate(Pos, Intensity, i + 1, pos_min, pos_max, min, max, 0);

		}

		intensityLow = max;
		low = Pos[maxIndex];

		if (minIndex > maxIndex)
		{
			intensityHigh = min;
			high = Pos[minIndex];
		}
		else
		{
			high = Pos[maxIndex] + 2 * stepWidth;

			if (nDir == X_DIRECTION)
			{
				x = high;
				y = posY_mm;
			}
			else if (nDir == Y_DIRECTION)
			{
				x = posX_mm;
				y = high;
			}

			if (m_IsStop)
			{
				if (isFastMode)
				{
					g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
					UpdateData(FALSE);
				}
				delete[] Pos;
				delete[] Intensity;
				nRet = -7;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}


			//스캔 포인트 이동			
			if (isFastMode)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y); //z축 이동없음
			}
			else
			{
				g_pNavigationStage->MoveAbsolutePosition(x, y);
			}


			if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
			{

				if (isFastMode)
				{
					g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
					UpdateData(FALSE);
				}

				delete[] Pos;
				delete[] Intensity;
				nRet = -2;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}

			intensityHigh = g_pXrayCamera->GetIntensity();
			//intensityHigh = g_pXrayCamera->intensity_simulation(x, y);
			Intensity[N] = intensityHigh;
			Pos[N] = nDir ? y : x;
			minIndex = N;
			N++;

			if (high > pos_max)
				pos_max = high;
		}

	}
	//2. PAD Coarse Align 초기화
	// 지정된 위치에서 좌우로 정해진 폭 만큼 밝기 측정

	else if (coarsAlignType == COARSE_ALIGN_PAD)
	{
		//Binary search 초기화
		if (nDir == X_DIRECTION)
		{
			low = posX_mm - g_pConfig->m_dHorizentalMargin_um / 1000.;  //right
			high = posX_mm + g_pConfig->m_dHorizentalMargin_um / 1000.; //left

			x = low;
			y = posY_mm;
		}
		else if (nDir == Y_DIRECTION)
		{
			low = posY_mm - g_pConfig->m_dVerticalMargin_um / 1000.;  //top
			high = posY_mm + g_pConfig->m_dVerticalMargin_um / 1000.; //bottom	

			x = posX_mm;
			y = low;
		}

		pos_min = low;
		pos_max = high;

		if (m_IsStop)
		{
			delete[] Pos;
			delete[] Intensity;
			nRet = -7;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}

		//첫번째 이동

		double current_x_mm = g_pNavigationStage->GetCurrentPosX();
		double current_y_mm = g_pNavigationStage->GetCurrentPosY();


		if (abs(current_x_mm - x) > 0.001 || abs(current_y_mm - y) > 0.001)
		{
			g_pNavigationStage->MoveAbsolutePosition(x, y);
		}

		if (isFastMode)
		{
			//1. Z축 이동 및 인터락세팅
			if (g_pAdam->MoveZCapsensorFastAlignFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, m_FastAlignZdistanceFromFocus_um) != 0)
			{
				delete[] Pos;
				delete[] Intensity;
				nRet = -2;
				return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
			}
			UpdateData(FALSE);
		}


		if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
		{

			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -2;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}
		intensityLow = g_pXrayCamera->GetIntensity();
		Intensity[N] = intensityLow;
		Pos[N] = nDir ? y : x;
		N++;

		g_pPhase->chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, intensityLow, intensityLow, 0);

		if (nDir == X_DIRECTION)  x = high;
		else if (nDir == Y_DIRECTION) y = high;


		if (m_IsStop)
		{
			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -7;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}

		//두번째 Point 이동		
		if (isFastMode)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y); //z축 이동없음
		}
		else
		{
			g_pNavigationStage->MoveAbsolutePosition(x, y);
		}


		if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
		{

			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -2;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}
		intensityHigh = g_pXrayCamera->GetIntensity();
		Intensity[N] = intensityHigh;
		Pos[N] = nDir ? y : x;
		N++;
	}


	//3.Coarse Align 수행 (Aintensity가  (ML + ABS)/2 되는 지점 찾음 

	double ML = max(intensityLow, intensityHigh);
	double ABS = min(intensityLow, intensityHigh);
	double ratio = 0.5;

	//intensityTarget = (intensityLow + intensityHigh) / 2;
	intensityTarget = ABS * ratio + ML * (1 - ratio);

	tolerance = intensityTarget * g_pConfig->m_dCoarseAlignIntensityTolerancePercent / 100;

	//low high 차이가 없으면 return
	if (abs(intensityHigh - intensityLow) < tolerance)
	{

		if (isFastMode)
		{
			g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
			UpdateData(FALSE);
		}

		delete[] Pos;
		delete[] Intensity;
		nRet = -3;
		return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
	}

	//증가함수, 감소함수
	if (intensityLow < intensityHigh)
	{
		BinarySearchSlope = 1;
		refAbs = intensityLow;
		refMl = intensityHigh;
	}
	else if (intensityLow > intensityHigh)
	{
		BinarySearchSlope = -1;
		refAbs = intensityHigh;
		refMl = intensityLow;
	}

	// UI update
	m_Slope = BinarySearchSlope;
	m_RefelectanceAbs = refAbs;
	m_RefelectanceMl = refMl;
	UpdateData(FALSE);

	////////////////////////////////////////////////////////////////////////////////
	// refAbs  refMl BinarySearchSlope
	// low high 위치 intensityTarget
	//////////////////////////////////////////////////////////////////////////////////////////////////////

	//g_pPhase->chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 0);
	iterCount = 0;
	chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 0, nullptr, 0, 0, maxIndex, minIndex);

	// Search
	while (low <= high)//항상 만족
	{
		mid = (low + high) / 2;

		if (nDir == X_DIRECTION)  x = mid;
		else if (nDir == Y_DIRECTION) y = mid;


		if (m_IsStop)
		{
			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -7;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}


		//Scan point 이동		
		if (isFastMode)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y); //z축 이동없음
		}
		else
		{
			g_pNavigationStage->MoveAbsolutePosition(x, y);
		}

		if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
		{

			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -2;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}
		intensityMid = g_pXrayCamera->GetIntensity();
		Intensity[N] = intensityMid;
		Pos[N] = nDir ? y : x;
		N++;
		iterCount++;
		//g_pPhase->chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 0);
		chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 0, nullptr, 0, 0, maxIndex, minIndex);
		if (abs(intensityMid - intensityTarget) < tolerance)
		{
			result_pos_mm = mid;
			break;
		}
		else if (intensityMid > intensityTarget)
		{
			if (BinarySearchSlope == 1)
				high = mid;
			else
				low = mid;
		}
		else if (intensityMid < intensityTarget)
		{
			if (BinarySearchSlope == 1)
				low = mid;
			else
				high = mid;
		}
		else
		{
			// 발생하지 않음
			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -4;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}

		if (iterCount >= g_pConfig->m_nMaximumTryCount)
		{

			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				UpdateData(FALSE);
			}

			delete[] Pos;
			delete[] Intensity;
			nRet = -5;
			return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
		}
	}

	//g_pPhase->chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 1);
	chartAlignUpdate(Pos, Intensity, N, pos_min, pos_max, refAbs, refMl, 1, nullptr, 0, 0, maxIndex, minIndex);

	if (isFastMode)
	{
		//1. Z축 복귀
		g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
		UpdateData(FALSE);
	}

	delete[] Pos;
	delete[] Intensity;

	if (nDir == X_DIRECTION)  m_ResultPostionX_mm = result_pos_mm;
	else if (nDir == Y_DIRECTION) m_ResultPostionY_mm = result_pos_mm;
	UpdateData(FALSE);

	return make_tuple(nRet, result_pos_mm, BinarySearchSlope, refAbs, refMl);
}

#if FALSE
int CPhaseDlg::SetZInterlockPhase()
{
	int nRet = 0;

	return -100; // 21.01.14 확인 필요. ihlee

	// Focus setting    
	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);  // 21.01.14 확인 필요. ihlee
	//WaitSec(2);

	//if (int(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == 0)
	//{
		//z축이동 후 reference focus 세팅
	int nRefCapSencor;
	double dTargetFocus = 0;

	// Mask center 좌표계
	double x = -g_pNavigationStage->GetPosmm(STAGE_X_AXIS) + g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].x;
	double y = -g_pNavigationStage->GetPosmm(STAGE_Y_AXIS) + g_pConfig->m_stStagePos[SYSTEM_EUV_MASKCENTER_POSITION].y;

	//사분면 기준으로 사용할 capsensor 선정
	if (x >= 0 && y < 0) { nRefCapSencor = CAP_SENSOR3; dTargetFocus = g_pConfig->m_dCap3MeasurePos_um; }
	else if (x > 0 && y >= 0) { nRefCapSencor = CAP_SENSOR4; dTargetFocus = g_pConfig->m_dCap4MeasurePos_um; }
	else if (x <= 0 && y > 0) { nRefCapSencor = CAP_SENSOR1; dTargetFocus = g_pConfig->m_dCap1MeasurePos_um; }
	else if (x < 0 && y <= 0) { nRefCapSencor = CAP_SENSOR2; dTargetFocus = g_pConfig->m_dCap2MeasurePos_um; }
	else { nRet = -2;  return nRet; }

	if (MoveZCapsensor(dTargetFocus, 12, TRUE, nRefCapSencor) != 0)
	{
		nRet = -3;
		return nRet;
	}

	//Focus 위치 저장		
	g_pMaskMap->m_dZFocusPos = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];
	g_pConfig->m_dZFocusPos_um = g_pMaskMap->m_dZFocusPos;
	g_pConfig->SaveRecoveryData();

	g_pMaskMap->UpdateData(FALSE);
	/*}
	else
	{
		nRet = -4;
		return nRet;
	}*/

	return nRet;
}
#endif


// Z축 Focus 위치에서 30um 아래로  Align 시작함, Z축 이동 없어서 함수 실행후 z축 복귀 해야함
// Stage 이동시 Z축 이동 안함
tuple<int, double> CPhaseDlg::EuvPhaseFineAlign(double posX_mm, double posY_mm, int nDir, int slope, double refAbs, double refMl, BOOL isFastMode, BOOL isMoveResultPos)
{
	//m_ResultPostionX_mm = posX_mm;
	//m_ResultPostionY_mm = posY_mm;
	//UpdateData(FALSE);

	int nRet = 0;
	m_IsStop = FALSE;

	m_AlignDirection = nDir;//UI 연결변수
	m_Slope = slope;
	m_RefelectanceAbs = refAbs;
	m_RefelectanceMl = refMl;
	UpdateData(FALSE);

	// Fine align by using curve fitting
	double* Pos;
	double*  Intensity;

	double*  PosX;
	double*  PosY;

	int N;
	int N_half;
	double stepWidth;

	if (slope == 0)
	{
		slope = 1;
	}

	if (nDir == X_DIRECTION)
	{
		N_half = g_pConfig->m_nHalfNumberOfStepX;
		N = N_half * 2 + 1;
		stepWidth = g_pConfig->m_dStepWidthX_um / 1000.;

		PosX = new double[N];
		PosY = new double[N];
		Intensity = new double[N];

		for (int i = 0; i < N; i++)
		{
			PosX[i] = posX_mm + (i - N_half)*stepWidth;
			PosY[i] = posY_mm;
		}
		Pos = PosX;
	}
	else if (nDir == Y_DIRECTION)
	{
		N_half = g_pConfig->m_nHalfNumberOfStepY;
		N = N_half * 2 + 1;
		stepWidth = g_pConfig->m_dStepWidthY_um / 1000.;


		PosX = new double[N];
		PosY = new double[N];
		Intensity = new double[N];

		for (int i = 0; i < N; i++)
		{
			PosX[i] = posX_mm;
			PosY[i] = posY_mm + (i - N_half)*stepWidth;
		}
		Pos = PosY;
	}

	//2. 최초 위치 이동
	g_pNavigationStage->MoveAbsolutePosition(PosX[0], PosY[0]);

	//3. Z축 세팅 -30지점
	if (isFastMode)
	{
		if (g_pAdam->MoveZCapsensorFastAlignFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, m_FastAlignZdistanceFromFocus_um) != 0)
		{
			nRet = -7;
			delete[] PosX;
			delete[] PosY;
			delete[] Intensity;
			return  make_tuple(nRet, 0.0);
		}
	}

	for (int i = 0; i < N; i++)
	{
		if (m_IsStop)
		{
			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
			}
			nRet = -7;
			delete[] PosX;
			delete[] PosY;
			delete[] Intensity;
			return  make_tuple(nRet, 0.0);
		}

		if (i > 0) //i=0 지점에서 시작함. 
		{
			if (isFastMode)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(PosX[i], PosY[i]); //z축 이동없음
			}
			else
			{
				g_pNavigationStage->MoveAbsolutePosition(PosX[i], PosY[i]);
			}
			//WaitSec(1); // 삭제??? Test 후 삭제.
		}

		if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
		{
			if (isFastMode)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
			}
			nRet = -2;
			delete[] PosX;
			delete[] PosY;
			delete[] Intensity;
			return  make_tuple(nRet, 0.0);
		}
		Intensity[i] = g_pXrayCamera->GetIntensity();

		// Chart Update
		g_pPhase->chartAlignUpdate(Pos, Intensity, i + 1, Pos[0], Pos[N - 1], refAbs, refMl, 0);

	}

	tuple<double, double, double, double, double*, int> fineAlignResultPython;

	//Python CurveFitting	
	if (g_pConfig->m_nFineAlignAlgorithmType == 2)
	{
		fineAlignResultPython = g_pPhase->PhaseFineAlignCurveFit2(Pos, Intensity, N, nDir, slope, refAbs, refMl);
	}
	else
	{
		fineAlignResultPython = g_pPhase->PhaseFineAlignCurveFit(Pos, Intensity, N, nDir, slope, refAbs, refMl);
	}

	double result_pos_mm = get<0>(fineAlignResultPython);

	if (get<4>(fineAlignResultPython) == nullptr)
	{
		int error = 0;
	}
	// Chart Update
	g_pPhase->chartAlignUpdate(Pos, Intensity, N, Pos[0], Pos[N - 1], refAbs, refMl, 2, get<4>(fineAlignResultPython), get<0>(fineAlignResultPython), get<1>(fineAlignResultPython));


	if (isMoveResultPos)
	{
		double x, y;

		if (nDir == X_DIRECTION)
		{
			x = result_pos_mm;
			y = posY_mm;
		}
		else if (nDir == Y_DIRECTION)
		{
			y = result_pos_mm;
			x = posX_mm;
		}

		if (isFastMode)
		{
			g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y);
		}
		else
		{
			g_pNavigationStage->MoveAbsolutePosition(x, y);
		}

	}

	if (isFastMode)
	{
		g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
	}

	delete[] PosX;
	delete[] PosY;
	delete[] Intensity;

	if (nDir == X_DIRECTION)  m_ResultPostionX_mm = result_pos_mm;
	else if (nDir == Y_DIRECTION) m_ResultPostionY_mm = result_pos_mm;
	UpdateData(FALSE);

	return make_tuple(nRet, result_pos_mm);
}

int CPhaseDlg::EuvPhaseFineAlign2(double posX_mm, double posY_mm, double &result_pos_mm, int nDir, int slope, double refAbs, double refMl)
{
	int nRet = 0;
	m_AlignDirection = nDir;//UI 연결변수
	UpdateData(FALSE);

	// Fine align by using curve fitting
	double* Pos;
	double*  Intensity;

	double*  PosX;
	double*  PosY;

	int N;
	int N_half;
	double stepWidth;

	if (nDir == X_DIRECTION)
	{
		//N_half = g_pConfig->m_nHalfNumberOfStepX;
		N_half = 4;
		N = N_half * 2 + 1;
		stepWidth = g_pConfig->m_dStepWidthX_um / 1000;

		PosX = new double[N];
		PosY = new double[N];
		Intensity = new double[N];

		for (int i = 0; i < N; i++)
		{
			PosX[i] = posX_mm + (i - N_half)*stepWidth;
			PosY[i] = posY_mm;
		}
		Pos = PosX;
	}
	else if (nDir == Y_DIRECTION)
	{
		N_half = g_pConfig->m_nHalfNumberOfStepY;
		N = N_half * 2 + 1;
		stepWidth = g_pConfig->m_dStepWidthY_um;


		PosX = new double[N];
		PosY = new double[N];
		Intensity = new double[N];

		for (int i = 0; i < N; i++)
		{
			PosX[i] = posX_mm;
			PosY[i] = posY_mm + (i - N_half)*stepWidth;
		}
		Pos = PosY;
	}

	for (int i = 0; i < N; i++)
	{
		g_pNavigationStage->MoveAbsolutePosition(PosX[i], PosY[i]);
		WaitSec(1);


		if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
		{
			nRet = -2;
			return nRet;
		}
		Intensity[i] = g_pXrayCamera->GetIntensity();

		// Chart Update
		g_pPhase->chartAlignUpdate(Pos, Intensity, i + 1, Pos[0], Pos[N - 1], refAbs, refMl, 0);

	}


	// 아랫부분 알고리즘 추가 필요..
	//Python CurveFitting	
	//m_nNumOfStdPoint 만큼  STD 구해서 최소가 되는 위치로 선정

	int Nstd = g_pConfig->m_nNumOfStdPoint;
	double* Std = new double[N - Nstd];
	double* Mean = new double[N - Nstd];

	for (int i = 0; i < N - Nstd; i++)
	{
		Mean[i] = 0;
		for (int j = 0; j < Nstd; j++)
		{
			Mean[i] = Mean[i] + Intensity[i + j];
		}
		Mean[i] = Mean[i] / Nstd;

		Std[i] = 0;
		for (int j = 0; j < Nstd; j++)
		{
			Std[i] = Std[i] + (Intensity[i + j] - Mean[i])*(Intensity[i + j] - Mean[i]);
		}

		Std[i] = sqrt(Std[i] / Nstd);
	}

	double MinStd;
	int minIndex = 0;

	for (int i = 0; i < N - Nstd; i++)
	{
		if (i == 0)
		{
			minIndex = 0;
			MinStd = Std[0];
		}

		if (Std[i] < MinStd)
		{
			minIndex = i;
			MinStd = Std[i];
		}
	}

	result_pos_mm = (Pos[minIndex] + Pos[minIndex + Nstd - 1]) / 2;
	double result_intensity = (Intensity[minIndex] + Intensity[minIndex + Nstd - 1]) / 2;

	// Chart Update
	g_pPhase->chartAlignUpdate(Pos, Intensity, N, Pos[0], Pos[N - 1], refAbs, refMl, 4, NULL, result_pos_mm, result_intensity);



	delete[] Std;
	delete[] Mean;

	delete[] PosX;
	delete[] PosY;
	delete[] Intensity;

	return nRet;

}
#if FALSE
int CPhaseDlg::MoveZCapsensor(double targetPos_um, int maxTry, bool resetInterlock, int nRefCapSencor)
{
	return -1; // 21.01.14 확인 필요. ihlee

	double capPos_um = 0;
	double stagePos_um = 0;
	double errorPos_um;
	double stageSetPos_um;
	int tryCount = 0;


	m_CapSensorSetValue = targetPos_um;
	m_CapSensorNumber = nRefCapSencor;
	UpdateData(FALSE);

	switch (nRefCapSencor)
	{
	case CAP_SENSOR1:
		if (targetPos_um < g_pConfig->m_dCap1PosLimitMin_um || targetPos_um>g_pConfig->m_dCap1PosLimitMax_um)
			return -1;
		break;
	case CAP_SENSOR2:
		if (targetPos_um < g_pConfig->m_dCap2PosLimitMin_um || targetPos_um>g_pConfig->m_dCap2PosLimitMax_um)
			return -1;
		break;
	case CAP_SENSOR3:
		if (targetPos_um < g_pConfig->m_dCap3PosLimitMin_um || targetPos_um>g_pConfig->m_dCap3PosLimitMax_um)
			return -1;
		break;
	case CAP_SENSOR4:
		if (targetPos_um < g_pConfig->m_dCap4PosLimitMin_um || targetPos_um>g_pConfig->m_dCap4PosLimitMax_um)
			return -1;
		break;
	default:
		return -2;
		break;
	}

	while (TRUE)
	{
		//g_pAdam->GetCurrentCapSensorValue(); // 잘동작했는지 체크 필요..

		if (g_pAdam->Command_AverageRunAfterTimeout() != 0)
		{
			return -3;
		}


		stagePos_um = g_pScanStage->m_dPIStage_GetPos[Z_AXIS];

		switch (nRefCapSencor)
		{
		case CAP_SENSOR1:
			capPos_um = g_pAdam->AdamData.m_dCapsensor1;
			break;
		case CAP_SENSOR2:
			capPos_um = g_pAdam->AdamData.m_dCapsensor2;
			break;
		case CAP_SENSOR3:
			capPos_um = g_pAdam->AdamData.m_dCapsensor3;
			break;
		case CAP_SENSOR4:
			capPos_um = g_pAdam->AdamData.m_dCapsensor4;
			break;
		default:
			return -4;
			break;
		}

		m_CapSensorGetValue = capPos_um;
		UpdateData(FALSE);

		errorPos_um = targetPos_um - capPos_um;

		if (abs(errorPos_um) < g_pConfig->m_dZControlTolerance_um)
			break;
		if (tryCount >= maxTry)
			return -5;

		// 수정함 테스트 필요 ihlee 2020.08.12
		//1차로 30um 2차: 10um 3차: 5um 4차이후: error 만큼 이동
		if (abs(errorPos_um) > 31)
		{
			double error_abs = abs(errorPos_um) - 30;
			errorPos_um = (errorPos_um > 0) ? error_abs : -error_abs;
		}
		else if (abs(errorPos_um) > 11)
		{
			double error_abs = abs(errorPos_um) - 10;
			errorPos_um = (errorPos_um > 0) ? error_abs : -error_abs;
		}
		else if (abs(errorPos_um) > 6)
		{
			double error_abs = abs(errorPos_um) - 5;
			errorPos_um = (errorPos_um > 0) ? error_abs : -error_abs;
		}
		else
		{
			errorPos_um = errorPos_um;
		}

		stageSetPos_um = stagePos_um + errorPos_um;

		if (stageSetPos_um < g_pConfig->m_dZStageLimitMin_um || stageSetPos_um > g_pConfig->m_dZStageLimitMax_um)
		{
			return -6;
		}

		if (resetInterlock)
		{
			//인터락 재설정
			g_pMaskMap->m_dZInterlockPos = floor(stageSetPos_um + g_pConfig->m_dInterlockMargin_um);
			g_pConfig->m_dZInterlock_um = g_pMaskMap->m_dZInterlockPos;
			g_pConfig->SaveRecoveryData();
			g_pScanStage->m_dZUpperLimit = g_pMaskMap->m_dZInterlockPos;
			g_pMaskMap->UpdateData(FALSE);
		}


		g_pScanStage->MoveZAbsolute_SlowInterlock(stageSetPos_um);

		tryCount++;
	}

	return 0;
}
#endif

void CPhaseDlg::MouseChartClickMove()
{
	// TEST 필요
	UpdateData(TRUE);
	if (m_IsMouseMoveClick)
	{
		XYChart* pChart = (XYChart*)m_chartPhaseAlign.getChart();
		double pos;
		double currentPosX_mm, currentPosY_mm;

		if (m_chartPhaseAlign.isMouseOnPlotArea())
		{
			pos = pChart->getXValue(m_chartPhaseAlign.getPlotAreaMouseX());
			//pos = pChart->getXValue(m_chartPhaseAlign.getChartMouseX());
			currentPosX_mm = g_pNavigationStage->GetCurrentPosX();
			currentPosY_mm = g_pNavigationStage->GetCurrentPosY();

			if (m_AlignDirection == X_DIRECTION)
			{
				g_pNavigationStage->MoveAbsolutePosition(pos, currentPosY_mm);
				//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(pos, currentPosY_mm); //z축 이동없음
			}
			else if (m_AlignDirection == Y_DIRECTION)
			{
				g_pNavigationStage->MoveAbsolutePosition(currentPosX_mm, pos);
				//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(currentPosX_mm, pos); //z축 이동없음
			}
		}
		int test = 0;
	}

	UpdateData(FALSE);
}

void CPhaseDlg::OnBnClickedCheckMouseClickMove()
{
	UpdateData(TRUE);
}

void CPhaseDlg::OnBnClickedButtonReadCapsensor()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonReadCapsensor() 버튼 클릭!"));

	g_pAdam->Command_AverageRunAfterTimeout();

	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonSetMeasurePos1FromAlignResult()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonSetMeasurePos1FromAlignResult() 버튼 클릭!"));

	UpdateData(TRUE);

	m_MeasPt1PosX_mm = m_ResultPostionX_mm;
	m_MeasPt1PosY_mm = m_ResultPostionY_mm;

	g_pNavigationStage->ConvertToMaskFromStage(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm, m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
	g_pPhase->UpdateData(FALSE);

	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonSetMeasurePos2FromAlignResult()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonSetMeasurePos2FromAlignResult() 버튼 클릭!"));

	UpdateData(TRUE);

	m_MeasPt2PosX_mm = m_ResultPostionX_mm;
	m_MeasPt2PosY_mm = m_ResultPostionY_mm;

	g_pNavigationStage->ConvertToMaskFromStage(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm, m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
	g_pPhase->UpdateData(FALSE);

	UpdateData(FALSE);
}

void CPhaseDlg::OnBnClickedButtonPhaseAlignX()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonPhaseAlignX() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = "X축 EUV Align을 수행합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;


	//Camera setting
	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

	int dir = X_DIRECTION;
	int coarseAlignType = m_CoarseAlignType;
	double LsHalfPitch = m_LsHalfPitch_um;

	double target_posx_mm = g_pNavigationStage->GetCurrentPosX();
	double target_posy_mm = g_pNavigationStage->GetCurrentPosY();

	//Z축 인터락
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		MsgBoxAuto.DoModal(_T(" Z축 제어에 실패했습니다. ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -2;
		return;
	}
	UpdateData(FALSE);

	int slope;
	double refABS, refML;
	int retValue;
	tuple<int, double, int, double, double> coarseReturn;
	tuple<int, double> fineReturn;

	//X	
	coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, target_posy_mm, dir, coarseAlignType, LsHalfPitch);
	retValue = get<0>(coarseReturn);
	m_ResultPostionX_mm = get<1>(coarseReturn);
	UpdateData(FALSE);
	slope = get<2>(coarseReturn);
	refABS = get<3>(coarseReturn);
	refML = get<4>(coarseReturn);
	if (retValue != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Coarse Align에 실패했습니다 ! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -4;
		return;
	}

	fineReturn = g_pPhase->EuvPhaseFineAlign(m_ResultPostionX_mm, target_posy_mm, dir, slope, refABS, refML);
	retValue = get<0>(fineReturn);
	m_ResultPostionX_mm = get<1>(fineReturn);
	UpdateData(FALSE);
	if (retValue != 0)
	{
		MsgBoxAuto.DoModal(_T(" Fine Align에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -5;
		return;
	}

	g_pNavigationStage->MoveAbsolutePosition(m_ResultPostionX_mm, target_posy_mm);
	WaitSec(1);

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonPhaseEuvAlign()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonPhaseEuvAlign() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = " EUV Align을 수행합니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;


	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);
	// 강제로 세팅  
	int coarseAlignType = COARSE_ALIGN_PAD;
	m_CoarseAlignType = coarseAlignType;
	UpdateData(FALSE);

	double target_posx_mm = g_pNavigationStage->GetCurrentPosX();
	double target_posy_mm = g_pNavigationStage->GetCurrentPosY();

	//Z축 이동 및 인터락세팅
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Z축 제어에 실패했습니다 ! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -2;
		return;
	}
	UpdateData(FALSE);

	double search_posX;
	double search_posY;

	int slope;
	double refABS, refML;
	int retValue;
	tuple<int, double, int, double, double> coarseReturn;
	tuple<int, double> fineReturn;

	//X	
	switch (m_AlignMarkType)
	{
	case DOWN_RIGHT://┏ down right
		search_posX = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
		search_posY = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
		break;

	case DOWN_LEFT:// ┓down left
		search_posX = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
		search_posY = target_posy_mm + g_pConfig->m_dVerticalMargin_um / 1000.0;
		break;

	case UP_RIGHT://┗(up right  
		search_posX = target_posx_mm - g_pConfig->m_dHorizentalMargin_um / 1000.0;
		search_posY = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
		break;

	case UP_LEFT:// ┛up left
		search_posX = target_posx_mm + g_pConfig->m_dHorizentalMargin_um / 1000.0;
		search_posY = target_posy_mm - g_pConfig->m_dVerticalMargin_um / 1000.0;
		break;

	default:
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Align Sequence가 중지되었습니다! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -3;
		return;
		break;
	}
	coarseReturn = g_pPhase->EuvPhaseCoarseAlign(target_posx_mm, search_posY, X_DIRECTION, COARSE_ALIGN_PAD);
	retValue = get<0>(coarseReturn);
	m_ResultPostionX_mm = get<1>(coarseReturn);
	UpdateData(FALSE);
	slope = get<2>(coarseReturn);
	refABS = get<3>(coarseReturn);
	refML = get<4>(coarseReturn);
	if (retValue != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" X축 Coarse Align에 실패했습니다 ! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -4;
		return;
	}

	fineReturn = g_pPhase->EuvPhaseFineAlign(m_ResultPostionX_mm, search_posY, X_DIRECTION, slope, refABS, refML);
	retValue = get<0>(fineReturn);
	m_ResultPostionX_mm = get<1>(fineReturn);
	UpdateData(FALSE);
	if (retValue != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T("  X축 Fine Align에 실패했습니다."), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -5;
		return;
	}
	//Y

	coarseReturn = g_pPhase->EuvPhaseCoarseAlign(search_posX, target_posy_mm, Y_DIRECTION, COARSE_ALIGN_PAD);
	retValue = get<0>(coarseReturn);
	m_ResultPostionY_mm = get<1>(coarseReturn);
	UpdateData(FALSE);
	slope = get<2>(coarseReturn);
	refABS = get<3>(coarseReturn);
	refML = get<4>(coarseReturn);
	if (retValue != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" T축 Coarse Align에 실패했습니다 ! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -5;
		return;
	}

	fineReturn = g_pPhase->EuvPhaseFineAlign(search_posX, m_ResultPostionY_mm, Y_DIRECTION, slope, refABS, refML);
	retValue = get<0>(fineReturn);
	m_ResultPostionY_mm = get<1>(fineReturn);
	UpdateData(FALSE);
	if (retValue != 0)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" Y축 Fine Align에 실패했습니다 ! "), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -6;
		return;
	}

	g_pNavigationStage->MoveAbsolutePosition(m_ResultPostionX_mm, m_ResultPostionY_mm);
	WaitSec(1);

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}


//Z축 이동없이 측정
void CPhaseDlg::OnBnClickedButtonPhaseMeasure()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonPhaseMeasure() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = "Phase 측정을 시작합니다 ! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	CString strValue;
	m_listPhaseResult.ResetContent();
	double phase_correction = 0;
	double phase_measure = 0;

	// Camera setting 
	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

	//m_NumOfPhaseMeasureRepeat
	double *phase1, *phase2, *phaseDel, *phaseDel1, *phaseDel2, *phaseDelCorrect;
	phase1 = new double[m_NumOfPhaseMeasureRepeat];
	phase2 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel1 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel2 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDelCorrect = new double[m_NumOfPhaseMeasureRepeat];

	//Z축 이동 및 인터락세팅
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		nRet = -1;
		delete[] phase1;
		delete[] phase2;
		delete[] phaseDel;
		delete[] phaseDel1;
		delete[] phaseDel2;
		delete[] phaseDelCorrect;

		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T("  Z축 제어에 실패했습니다 !"), 2);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -2;

		return;
	}
	UpdateData(FALSE);


	// Center Frequncy Setting
	//g_pNavigationStage->MoveAbsolutePosition((m_MeasPt1PosX_mm + m_MeasPt2PosX_mm) / 2, (m_MeasPt1PosY_mm + m_MeasPt2PosY_mm) / 2);
	//WaitSec(1);

	//nRet = SetCenterFrequency();

	for (int j = 0; j < m_NumOfPhaseMeasureRepeat; j++)
	{
		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
			continue;


		phase1[j] = PhaseMeasure(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm);
		m_PhasePoint1 = phase1[j];
		UpdateData(FALSE);

		phase2[j] = PhaseMeasure(m_MeasPt2PosX_mm, m_MeasPt2PosY_mm);
		m_PhasePoint2 = phase2[j];
		UpdateData(FALSE);

		phaseDel1[j] = AngleDiffrence(m_ReferencePhase, phase1[j]);
		phaseDel2[j] = AngleDiffrence(phase2[j], m_ReferencePhase);
		//phaseDel[i] = (phaseDel1[i] + phaseDel2[i])/2.0*180.0/ 3.14159265358979323846;

		// 파장 보상식 추가해야함 
		// lamda = 0.01692308*cw + 0.02053846;  2020.08.19  cw: 798.61 lamda: 13.5355
		// phase lamda 반비례 ? 
		// phase_correction = phase_measure * 13.5/m_CenterWaveLength;

		phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;

		// 2020.08.19  cw: 798.61 lamda: 13.5355
		// 2020.08.20  cw: 798.83 lamda: 13.5392
		//double CW = 798.83;
		//double m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
		phaseDel[j] = phase_measure;
		phase_correction = phase_measure * 13.56 / m_CenterWaveLengthFromCw;
		phaseDelCorrect[j] = phase_correction;
		m_PhaseResult = phase_correction;

		//strValue.Format("%d: \t%3.3f \t%3.3f", j, phaseDel[j], phaseDelCorrect[j]);
		strValue.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
		m_listPhaseResult.AddString(strValue);


		CString strResult;
		strResult.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j + 1, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
		g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));

		//m_CenterFreqIndex m_CenterWaveLengthFromFft
		UpdateData(FALSE);
	}

	double phase1_avr, phase2_avr, phaseDel_avr;
	double phase1_sum = 0, phase2_sum = 0, phaseDel_sum = 0;

	for (int i = 0; i < m_NumOfPhaseMeasureRepeat; i++)
	{
		phase1_sum = phase1_sum + phase1[i];
		phase2_sum = phase2_sum + phase2[i];
		phaseDel_sum = phaseDel_sum + phaseDelCorrect[i];
	}
	phase1_avr = phase1_sum / m_NumOfPhaseMeasureRepeat;
	phase2_avr = phase2_sum / m_NumOfPhaseMeasureRepeat;
	phaseDel_avr = phaseDel_sum / m_NumOfPhaseMeasureRepeat;


	m_PhasePoint1 = phase1_avr;
	m_PhasePoint2 = phase2_avr;
	m_PhaseResult = phaseDel_avr;

	delete[] phase1;
	delete[] phase2;
	delete[] phaseDel;
	delete[] phaseDel1;
	delete[] phaseDel2;
	delete[] phaseDelCorrect;

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}


//Z축 이동없이 측정
double CPhaseDlg::PhaseMeasure(double posX_mm, double posY_mm, BOOL isMaskCoordinate)
{
	double phaseResult = 0;

	double current_x_mm = g_pNavigationStage->GetCurrentPosX();
	double current_y_mm = g_pNavigationStage->GetCurrentPosY();


	if (abs(current_x_mm - posX_mm) > 0.00002 || abs(current_y_mm - posY_mm) > 0.00002)
	{
		//g_pNavigationStage->MoveAbsolutePosition(x, y);
		g_pNavigationStage->MoveAbsolutePosition(posX_mm, posY_mm);
		WaitSec(1);
	}

	//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(posX_mm, posY_mm); //z축 이동 없음	

	//3. Image Grab
	if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
	{
		return 0.;
	}

	//4. Image projection & FFT	
	int DataSize = g_pXrayCamera->m_MilImageWidth;
	int Nfft = g_pConfig->m_nNfft;

	int NfftResult = Nfft / 2 + 1;
	double T = 13.5;//um ccd 간격

	double *Intensity, *magnitude, *phase, *frequency;
	double *IntensityShift;

	//FFT	
	Intensity = new double[DataSize];
	magnitude = new double[NfftResult];
	phase = new double[NfftResult];
	frequency = new double[NfftResult];

	IntensityShift = new double[DataSize];;
	// projection
	ImageProjection(g_pXrayCamera->m_MilImageChild, Intensity, DataSize);



	//Shift	
	int indexIntensity = 0;
	for (int i = 0; i < DataSize; i++)
	{
		indexIntensity = (i + m_ShiftPos) % DataSize;
		IntensityShift[i] = Intensity[indexIntensity];
	}

	EPhaseFFT(DataSize, IntensityShift, Nfft, T, magnitude, phase, frequency);

	phaseResult = phase[m_CenterFreqIndex];

	delete[] Intensity;
	delete[] magnitude;
	delete[] phase;
	delete[] frequency;
	delete[] IntensityShift;

	return phaseResult;
}


void CPhaseDlg::BackGroundSet(double posX_mm, double posY_mm)
{
	//1. 위치 이동
	//g_pNavigationStage->MoveAbsolutePosition(posX_mm, posY_mm);
	//WaitSec(1);

	//확인 필요 ihlee 21.03.29	
	//BOOL bCorrectBackGroundOld = m_bCorrectBackGround;
	g_pXrayCamera->m_bCorrectBackGround = FALSE;
	g_pXrayCamera->UpdateData(FALSE);

	//2. Z 축내림
	g_pScanStage->Move_Origin();
	WaitSec(3);

	if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
	{
		return;
	}

	//BG 획득
	if (g_pXrayCamera->m_MilImageBackGroundMeasure != M_NULL)
	{
		MbufFree(g_pXrayCamera->m_MilImageBackGroundMeasure);
		g_pXrayCamera->m_MilImageBackGroundMeasure = M_NULL;
	}

	MbufClone(g_pXrayCamera->m_MilImageOriginal, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, M_DEFAULT, &g_pXrayCamera->m_MilImageBackGroundMeasure);
	//BG 세팅	
	g_pXrayCamera->SetMeasureBackGround();

	//SetMeasureBackGround();
	g_pXrayCamera->m_bCorrectBackGround = TRUE;
	g_pXrayCamera->UpdateData(FALSE);

}





// 현재위치에서 Z축 측정 위치로 이동, 이미지 획득후 계산 
void CPhaseDlg::OnBnClickedButtonGetCenterFrequency()
{

	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonGetCenterFrequency() 버튼 클릭!"));
	g_pWarning->m_strWarningMessageVal = " Center Frequency 측정중입니다! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;



	//1. 카메라 세팅
	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

	////2. Z축 초기 위치로 이동? 
	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//WaitSec(2);


#if FALSE

	// 지정된 위치 있으면 지정된 위치로 이동
	double currentPosX_mm = g_pNavigationStage->GetCurrentPosX();
	double currentPosY_mm = g_pNavigationStage->GetCurrentPosY();

	double target_posx_mm = currentPosX_mm;
	double target_posy_mm = currentPosY_mm;


	if (round(g_pScanStage->m_dPIStage_GetPos[Z_AXIS]) == Z_INITIAL_POS_UM)//asdf
	{

		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//EUV 영역으로 이동
		WaitSec(1);
		g_pNavigationStage->MoveAbsolutePosition(target_posx_mm, target_posy_mm);
		//g_pNavigationStage->MoveAbsoluteXY_UntilInposition(target_posx_mm, target_posy_mm);		//정확히 못간 경우 있을 수 있으므로 한번 더 가자
		WaitSec(1);
	}
	else
	{
		MsgBoxAuto.DoModal(_T("Z축 이동에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -2;
		return;
	}

#endif

	//3. Z축 인터락 설정
	//if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, 5) != 0)
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		MsgBoxAuto.DoModal(_T("Z축 제어에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -3;
		return;
	}
	UpdateData(FALSE);

	//4. SetCenterFrequency
	nRet = SetCenterFrequency();

	if (nRet != 0)
	{
		MsgBoxAuto.DoModal(_T("Center Frequency 측정에 실패했습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -4;
		return;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}



int CPhaseDlg::SetCenterFrequency()
{
	int nRet = 0;

	//4. Image Grab
	if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
	{
		nRet = -3;
		return nRet;
	}
	//5. Image projection & FFT	
	int DataSize = g_pXrayCamera->m_MilImageWidth;
	int Nfft = g_pConfig->m_nNfft;

	int NfftResult = Nfft / 2 + 1;
	double T = 13.5;//um ccd 간격

	double *Intensity, *magnitude, *phase, *frequency;
	double *IntensityShift;
	//FFT	
	Intensity = new double[DataSize];
	IntensityShift = new double[DataSize];
	magnitude = new double[NfftResult];
	phase = new double[NfftResult];
	frequency = new double[NfftResult];

	// projection
	ImageProjection(g_pXrayCamera->m_MilImageChild, Intensity, DataSize);


	int maxIndex = 0;
	double maxIntensity = Intensity[0];

	for (int i = 0; i < DataSize; i++)
	{
		if (Intensity[i] > maxIntensity)
		{
			maxIntensity = Intensity[i];
			maxIndex = i;
		}
	}

	//Shift	
	int indexIntensity = 0;

	m_ShiftPos = maxIndex;

	for (int i = 0; i < DataSize; i++)
	{
		indexIntensity = (i + m_ShiftPos) % DataSize;
		IntensityShift[i] = Intensity[indexIntensity];
	}

	EPhaseFFT(DataSize, IntensityShift, Nfft, T, magnitude, phase, frequency);

	//분석 
	int start;
	int end;


	//0.5 Slit
	//start = 1000; //0,2
	//end = 1600;   //8,9

	//0.75 Slit
	start = 5; //0,2
	end = 15;   //8,9


	double max;
	int index;

	double *pMax;

	pMax = max_element(magnitude + start, magnitude + end);
	max = *pMax;

	//index = pMax - frequency;
	index = distance(magnitude, pMax);

	//Update Data 
	double ratio;
	//ratio = 13.5355*frequency[index];
	ratio = 0.0025177001953125; //2020.08.19  cw: 798.61 lamda: 13.5355
	m_CenterWaveLengthFromFft = 1 / frequency[index] * ratio;

	m_CenterFreqIndex = index;
	m_ReferencePhase = phase[index];

	UpdateData(FALSE);

	delete[] Intensity;
	delete[] magnitude;
	delete[] phase;
	delete[] frequency;
	delete[] IntensityShift;

	return nRet;
}


void CPhaseDlg::OnBnClickedButtonMovePos1()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonMovePos1() 버튼 클릭!"));

	UpdateData(TRUE);

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	g_pNavigationStage->MoveAbsolutePosition(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm, TRUE);
	WaitSec(1);

	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonMovePos2()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonMovePos2() 버튼 클릭!"));

	UpdateData(TRUE);

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	g_pNavigationStage->MoveAbsolutePosition(m_MeasPt2PosX_mm, m_MeasPt2PosY_mm, TRUE);
	WaitSec(1);

	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;

	UpdateData(FALSE);
}


void CPhaseDlg::CopyListToClipboard(CListBox* pListCtrl)
{
	CString sResult;
	CString ListText;

	int nRow = pListCtrl->GetCount();

	for (int i = 0; i < nRow; ++i)
	{
		pListCtrl->GetText(i, ListText);

		sResult = sResult + ListText + _T("\r\n");
	}
	if (nRow > 0)
	{
		//클립보드 저장
		if (pListCtrl->OpenClipboard())
		{
			EmptyClipboard();

			int nLen = sResult.GetLength() * sizeof(WCHAR);
			HGLOBAL hGlobal = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, nLen);
			LPBYTE pGlobalData = (LPBYTE)GlobalLock(hGlobal);

			USES_CONVERSION_EX;
			CopyMemory(pGlobalData, T2CW_EX(sResult, _ATL_SAFE_ALLOCA_DEF_THRESHOLD), nLen);
			SetClipboardData(CF_UNICODETEXT, hGlobal);

			GlobalUnlock(hGlobal);
			GlobalFree(hGlobal);

			CloseClipboard();
		}
	}

}




HBRUSH CPhaseDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = __super::OnCtlColor(pDC, pWnd, nCtlColor);

	switch (nCtlColor)
	{
	case CTLCOLOR_EDIT:
	{
		if (pWnd->GetDlgCtrlID() == IDC_EDIT_CONTINUOUS_WAVE)
		{
			if (!isUpdatedCwData)
			{
				pDC->SetBkColor(PEACH);
				//hbr = m_brush;
			}
		}
		else if (pWnd->GetDlgCtrlID() == IDC_EDIT_CW_DATE)
		{
			if (!isUpdatedCwData)
			{
				pDC->SetBkColor(PEACH);
				//hbr = m_brush;
			}
		}
	}
	}

	return hbr;
}


void CPhaseDlg::OnEnChangeEditContinuousWave()
{
	isUpdatedCwData = FALSE;
	Invalidate(TRUE);
}


void CPhaseDlg::OnEnChangeEditCwDate()
{
	isUpdatedCwData = FALSE;
	Invalidate(TRUE);
}


void CPhaseDlg::OnBnClickedButtonSetCw()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonSetCw() 버튼 클릭!"));

	UpdateData(TRUE);
	m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
	g_pConfig->SavePhaseInfo();
	isUpdatedCwData = TRUE;
	Invalidate(TRUE);
}


void CPhaseDlg::OnBnClickedButtonResetCw()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonResetCw() 버튼 클릭!"));

	UpdateData(FALSE);
	isUpdatedCwData = TRUE;
	Invalidate(TRUE);
}


void CPhaseDlg::OnBnClickedButtonResetMeasurePosition()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonResetMeasurePosition() 버튼 클릭!"));

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_MeasPt1PosX_mm = 0;
	m_MeasPt1PosY_mm = 0;
	m_MeasPt2PosX_mm = 0;
	m_MeasPt2PosY_mm = 0;
	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonPhaseStop()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonPhaseStop() 버튼 클릭!"));

	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	m_IsStop = TRUE;
}


void CPhaseDlg::OnBnClickedButtonGetCurrentY()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonGetCurrentY() 버튼 클릭!"));

	UpdateData(TRUE);


	//m_MeasPt1PosX_mm = m_ResultPostionX_mm;
	m_MeasPt1PosX_mm = g_pNavigationStage->GetCurrentPosX();
	m_MeasPt1PosY_mm = g_pNavigationStage->GetCurrentPosY();

	g_pNavigationStage->ConvertToMaskFromStage(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm, m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);


	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonGetCurrentY2()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonGetCurrentY2() 버튼 클릭!"));

	UpdateData(TRUE);


	//m_MeasPt2PosX_mm = m_ResultPostionX_mm;
	m_MeasPt2PosX_mm = g_pNavigationStage->GetCurrentPosX();
	m_MeasPt2PosY_mm = g_pNavigationStage->GetCurrentPosY();

	g_pNavigationStage->ConvertToMaskFromStage(m_MeasPt2PosX_mm, m_MeasPt2PosY_mm, m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
	g_pPhase->UpdateData(FALSE);
	//g_pPhase->UpdateData(FALSE);

	UpdateData(FALSE);
}


void CPhaseDlg::OnBnClickedButtonMoveAndSetZInterlock()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonMoveAndSetZInterlock() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = " Z축 세팅중입니다. ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	//Z축 이동후 인터락 Set (210323 ihlee)

	//1. Z_INITIAL_POS_UM로 일단 내려감
	//g_pScanStage->MoveZAbsolute_SlowInterlock(Z_INITIAL_POS_UM);
	//WaitSec(1);

	//2. Z축 세팅
	nRet = g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);

	if (nRet != 0)
	{
		MsgBoxAuto.DoModal(_T(" Z축 세팅에 실패했습니다! "), 2);
		g_pWarning->ShowWindow(SW_HIDE);
		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
		nRet = -2;
		return;
	}

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}

void CPhaseDlg::PhasePositionUpdate(double PosX, double PosY)
{
	m_dAbsPosX = PosX;
	m_dAbsPosY = PosY;

	CString tmp;
	tmp.Format("%3.7f", m_dAbsPosX);
	if (m_ctrlPosX.m_hWnd != NULL)
		m_ctrlPosX.SetWindowText(tmp);

	tmp.Format("%3.7f", m_dAbsPosY);
	if (m_ctrlPosY.m_hWnd != NULL)
		m_ctrlPosY.SetWindowText(tmp);

	m_dRelPosX = (m_dAbsPosX - m_dRefPosX) * -1.0;
	m_dRelPosY = (m_dAbsPosY - m_dRefPosY) * -1.0;

	tmp.Format("%3.5f", m_dRelPosX);
	if (m_ctrlRelPosX.m_hWnd != NULL)
		m_ctrlRelPosX.SetWindowText(tmp);

	tmp.Format("%3.5f", m_dRelPosY);
	if (m_ctrlRelPosY.m_hWnd != NULL)
		m_ctrlRelPosY.SetWindowText(tmp);
}

void CPhaseDlg::OnBnClickedButtonGetRefPos()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonGetRefPos() 버튼 클릭!"));

	m_dRefPosX = m_dAbsPosX;
	m_dRefPosY = m_dAbsPosY;

	m_strRefPosX.Format("%0.5f", m_dRefPosX);
	m_strRefPosY.Format("%0.5f", m_dRefPosY);

	UpdateData(false);
}

void CPhaseDlg::InitEditBox()
{
	m_strRefPosX.Format("%0.5f", m_dRefPosX);
	m_strRefPosY.Format("%0.5f", m_dRefPosY);

	UpdateData(FALSE);
}

void CPhaseDlg::OnEnChangeEditRefPosX()
{
	UpdateData(TRUE);
	m_dRefPosX = atof(m_strRefPosX);
}


void CPhaseDlg::OnEnChangeEditRefPosY()
{
	UpdateData(TRUE);
	m_dRefPosY = atof(m_strRefPosY);
}





void CPhaseDlg::OnBnClickedButtonTestException()
{

	int TotalMeasureNum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum;
	double leftXmm, leftYmm, rightXmm, rightYmm;

	double leftxdiff_nm, leftydiff_nm;
	double rightxdiff_nm, rightydiff_nm;

	for (int i = 0; i < TotalMeasureNum; i++)
	{
		if (FALSE)
		{
			//Align Data 사용
			leftXmm = m_LeftX_mm[i];
			leftYmm = m_LeftY_mm[i];
			rightXmm = m_RightX_mm[i];
			rightYmm = m_RightY_mm[i];
		}
		else
		{
			//레시피 좌표사용
			g_pNavigationStage->ConvertToStageFromMask(leftXmm, leftYmm, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um);
			g_pNavigationStage->ConvertToStageFromMask(rightXmm, rightYmm, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightY_um);

			double leftxdiff_nm, leftydiff_nm;
			double rightxdiff_nm, rightydiff_nm;

			//rightXmm = rightXmm + 200.0 / 1000000.0;

			leftxdiff_nm = (m_LeftX_mm[i] - leftXmm) * 1000000;
			leftydiff_nm = (m_LeftY_mm[i] - leftYmm) * 1000000;
			rightxdiff_nm = (m_RightX_mm[i] - rightXmm) * 1000000;
			rightydiff_nm = (m_RightY_mm[i] - rightYmm) * 1000000;

			int test = 0;
		}

	}

	//g_pAdam->CloseTcpIpSocket();
	//g_pAdam->OpenTcpIpSocket(g_pConfig->m_chIP[ETHERNET_ADAM], g_pConfig->m_nPORT[ETHERNET_ADAM], FALSE, ADAM_RECEIVE_BUFFER_SIZE);

	//int test;
	//double dValue = -300;

	//dValue = -300.1;
	//test = round(dValue);

	//dValue = -300.5;
	//test = round(dValue);

	//dValue = -300.6;
	//test = round(dValue);

	//dValue = -299.9;
	//test = round(dValue);

	//dValue = -299.5;
	//test = round(dValue);

	//dValue = -299.4;
	//test = round(dValue);

	//dValue = -299.6;
	//test = round(dValue);





	//AfxMessageBox("Griddata Exception 발생");

	//try
	//{
	//	ExceptionTest1();
	//}
	//catch (int e)
	//{
	//	int test = 0;
	//}
	//catch (double e)
	//{
	//	int test = 1;
	//}
	//catch (PhaseException& e)
	//{
	//	int test = 0;
	//	CString strException;
	//	strException = e.what();

	//	e.Catched(__FILE__, __FUNCTION__, __LINE__, g_pLog);

	//	//PhaseException("Exception 마무리", __FILE__, __FUNCTION__, __LINE__, g_pLog);
	//}
	//catch (out_of_range & e)
	//{

	//}
	//catch (...)
	//{
	//	int test = 2;
	//}
}

void CPhaseDlg::ExceptionTest1()
{
	try
	{
		//ExceptionTest2();

		int a = 0;
		if (a == 0)
		{
			throw PhaseException("이상발생1", __FILE__, __FUNCTION__, __LINE__, g_pLog);
		}

	}
	catch (PhaseException & e)
	{
		//Exception 발생시. 정리할꺼 정리 메모리등..
		//Exception 전파
		//throw PhaseException("Exception 전달", __FILE__, __FUNCTION__, __LINE__, g_pLog);		

		e.CatchedAndThrow(__FILE__, __FUNCTION__, __LINE__, g_pLog);
		throw e;
	}
	catch (...)
	{

	}
}

void CPhaseDlg::ExceptionTest2()
{
	//Exception 발생상황
	int test = 0;

	throw PhaseException("이상발생: 원인", __FILE__, __FUNCTION__, __LINE__, g_pLog);

	int asdf = 0;

}



void CPhaseDlg::OnBnClickedButtonPhaseApply()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.

	g_pConfig->ReadPhaseInfo();
}


void CPhaseDlg::OnBnClickedButtonPhaseScan()
{
	UpdateData(TRUE);

	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	//m_ScanStep;//um
	//m_ScanNum;

	int dir = m_AlignDirection;

	double x_step_um = 0;
	double y_step_um = 0;

	double* Pos;
	double* Intensity;

	double pos_min;
	double pos_max;

	Pos = new double[m_ScanNum];
	Intensity = new double[m_ScanNum];

	double x_mm = g_pNavigationStage->GetCurrentPosX();
	double y_mm = g_pNavigationStage->GetCurrentPosY();

	if (dir == X_DIRECTION)
	{
		x_step_um = m_ScanStep;

		double temp1 = x_mm;
		double temp2 = x_mm + m_ScanStep * (m_ScanNum - 1) / 1000;
		pos_min = min(temp1, temp2);
		pos_max = max(temp1, temp2);
	}
	else if (dir == Y_DIRECTION)
	{
		y_step_um = m_ScanStep;

		double temp1 = y_mm;
		double temp2 = y_mm + m_ScanStep * (m_ScanNum - 1) / 1000;
		pos_min = min(temp1, temp2);
		pos_max = max(temp1, temp2);

	}

	double min, max;
	double x, y;


	// 카메라 세팅
	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

	//1. Z축 이동 및 인터락세팅
	if (m_checkFastAlign)
	{
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, m_FastAlignZdistanceFromFocus_um) != 0)
		{
			MsgBoxAuto.DoModal(_T(" Z축  제어에 실패했습니다 ! "), 2);
			g_pWarning->ShowWindow(SW_HIDE);
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
			//nRetrun = -2;
			delete[] Pos;
			delete[] Intensity;
			return;
		}
		UpdateData(FALSE);
	}
	else
	{
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
		{
			MsgBoxAuto.DoModal(_T(" Z축  제어에 실패했습니다 ! "), 2);
			g_pWarning->ShowWindow(SW_HIDE);
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
			//nRetrun = -2;
			delete[] Pos;
			delete[] Intensity;
			return;
		}
		UpdateData(FALSE);
	}

	for (int i = 0; i < m_ScanNum; i++)
	{
		if (g_pMaskMap->m_bAutoSequenceProcessing)
		{
			x = x_mm + i * x_step_um / 1000.0;
			y = y_mm + i * y_step_um / 1000.0;

			//1. Stage Move
			if (m_checkFastAlign)
			{
				g_pNavigationStage->MoveAbsoluteXY_UntilInposition(x, y);
			}
			else
			{
				g_pNavigationStage->MoveAbsolutePosition(x, y);
			}

			//2. Grab
			if (g_pXrayCamera->GrabTwice(FALSE) != GRAB_OK)
			{
				g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
				delete[] Pos;
				delete[] Intensity;
				g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
				return;
			}
			Intensity[i] = g_pXrayCamera->GetIntensity();

			Pos[i] = dir ? y : x;

			min = Intensity[0];
			max = Intensity[0];

			for (int j = 0; j <= i; j++)
			{
				if (Intensity[j] < min)
				{
					//minIndex = j;
					min = Intensity[j];
				}
				else if (Intensity[j] > max)
				{
					//maxIndex = j;
					max = Intensity[j];
				}
			}

			g_pPhase->chartAlignUpdate(Pos, Intensity, i + 1, pos_min, pos_max, min, max, 0);

		}
	}

	//if (m_checkFastAlign)
	//{
	//	g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue);
	//}

	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;

	delete[] Pos;
	delete[] Intensity;
}


void CPhaseDlg::OnBnClickedButtonPhaseMeasureFocus()
{
	//확인 필요 ihlee 21.03.29

	UpdateData(TRUE);


	//double yshift = -50.0 / 1000.0; //um 
	//yshift = 0;

	double *phase1, *phase2, *phaseDel, *phaseDel1, *phaseDel2, *phaseDelCorrect;
	double delZ = 0;
	int iterNum = m_NumFocus;
	UpdateData(TRUE);


	CAutoMessageDlg MsgBoxAuto(g_pPhase);
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonPhaseMeasureGap() 버튼 클릭!"));
	g_pWarning->m_strWarningMessageVal = "Phase 측정을 시작합니다 ! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);



	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	CString strValue;
	m_listPhaseResult.ResetContent();
	double phase_correction = 0;
	double phase_measure = 0;


	phase1 = new double[iterNum];
	phase2 = new double[iterNum];
	phaseDel = new double[iterNum];
	phaseDel1 = new double[iterNum];
	phaseDel2 = new double[iterNum];
	phaseDelCorrect = new double[iterNum];

	double pos1X = 0, pos1Y = 0, pos2X = 0, pos2Y = 0;


	for (int j = 0; j < iterNum; j++)
	{

		pos1X = m_MeasPt1PosX_mm - m_Xshfit_um / 1000.0 * j;
		pos1Y = m_MeasPt1PosY_mm;

		pos2X = m_MeasPt2PosX_mm - m_Xshfit_um / 1000.0 * j;
		pos2Y = m_MeasPt2PosY_mm;


		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
		{
			continue;
		}

		delZ = m_InitialFocusPos + m_ZStep_um * j;

		if (delZ >= 7.0) //9까지 갈 예정 7um 기본 position
		{
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
			continue;
		}

		//Z축 이동 및 인터락세팅
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, delZ) != 0)
		{
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
			continue;
		}
		UpdateData(FALSE);

#if FALSE
		// Fine Align 
		g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

		int dir = X_DIRECTION;
		int slope = 1;
		double refAbs = 0;
		double refMl = 200;
		double ResultPostion_mm = 0;
		int nRetrun = 0;

		nRetrun = EuvPhaseFineAlign2(pos1X, pos1Y, ResultPostion_mm, dir, slope, refAbs, refMl);

		if (nRetrun == 0)
		{
			g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, pos1Y);
		}
		WaitSec(1);

		pos1X = g_pNavigationStage->GetCurrentPosX();

		//1.2 point2		
		nRetrun = EuvPhaseFineAlign2(pos2X, pos2Y, ResultPostion_mm, dir, slope, refAbs, refMl);
		if (nRetrun == 0)
		{
			g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, pos2Y);
		}
		WaitSec(1);
		pos2X = g_pNavigationStage->GetCurrentPosX();

#endif

#if FALSE
		if (m_UseAlign)
		{
			// Fine Align 
			g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

			int dir = X_DIRECTION;
			int slope = 1;
			double refAbs = 0;
			double refMl = 10;
			double ResultPostion_mm = 0;

			tuple<int, double> fineResult;
			int nRetrun = 0;

			//1.1 point1
			slope = -1;
			fineResult = EuvPhaseFineAlign(pos1X, pos1Y, dir, slope, refAbs, refMl);
			nRetrun = get<0>(fineResult);
			ResultPostion_mm = get<1>(fineResult);
			if (nRetrun == 0)
			{
				g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, pos1Y);
			}
			WaitSec(1);
			pos1X = g_pNavigationStage->GetCurrentPosX();;

			if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
				continue;

			//Z축 이동 및 인터락세팅
			if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, delZ) != 0)
			{
				g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
				continue;
			}
			UpdateData(FALSE);

			//1.2 point2
			slope = 1;
			fineResult = EuvPhaseFineAlign(pos2X, pos2Y, dir, slope, refAbs, refMl);
			nRetrun = get<0>(fineResult);
			ResultPostion_mm = get<1>(fineResult);
			if (nRetrun == 0)
			{
				g_pNavigationStage->MoveAbsolutePosition(ResultPostion_mm, pos2Y);
			}
			WaitSec(1);
			pos2X = g_pNavigationStage->GetCurrentPosX();

			if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
				continue;
		}

		//Z축 이동 및 인터락세팅
		if (m_UseAlign)
		{
			if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue, delZ) != 0)
			{
				g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
				continue;
			}
			UpdateData(FALSE);
		}
#endif

		// Camera setting 
		g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
			continue;

		// Center Frequncy Setting
		//g_pNavigationStage->MoveAbsolutePosition((pos1X + pos2X) / 2, (pos1Y + pos2Y) / 2);
		//WaitSec(1);
		//SetCenterFrequency();

		//phase 측정
		phase1[j] = PhaseMeasure(pos1X, pos1Y);
		m_PhasePoint1 = phase1[j];
		UpdateData(FALSE);

		phase2[j] = PhaseMeasure(pos2X, pos2Y);
		m_PhasePoint2 = phase2[j];
		UpdateData(FALSE);

		phaseDel1[j] = AngleDiffrence(m_ReferencePhase, phase1[j]);
		phaseDel2[j] = AngleDiffrence(phase2[j], m_ReferencePhase);
		//phaseDel[i] = (phaseDel1[i] + phaseDel2[i])/2.0*180.0/ 3.14159265358979323846;

		// 파장 보상식 추가해야함 
		// lamda = 0.01692308*cw + 0.02053846;  2020.08.19  cw: 798.61 lamda: 13.5355
		// phase lamda 반비례 ? 
		// phase_correction = phase_measure * 13.5/m_CenterWaveLength;

		phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;

		// 2020.08.19  cw: 798.61 lamda: 13.5355
		// 2020.08.20  cw: 798.83 lamda: 13.5392
		//double CW = 798.83;
		//double m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
		phaseDel[j] = phase_measure;
		phase_correction = phase_measure * 13.56 / m_CenterWaveLengthFromCw;
		phaseDelCorrect[j] = phase_correction;
		m_PhaseResult = phase_correction;

		//strValue.Format("%d: \t%3.3f \t%3.3f", j, phaseDel[j], phaseDelCorrect[j]);
		strValue.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j, m_CenterFreqIndex, m_CenterWaveLengthFromFft, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
		m_listPhaseResult.AddString(strValue);

		CString strResult;
		strResult.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f \t%d \t%.2f \t%.2f", j + 1, m_CenterFreqIndex, m_CenterWaveLengthFromFft, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], pos1X, pos1Y, m_CapSensorNumber, m_CapSensorGetValue, delZ);
		g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));

		//m_CenterFreqIndex m_CenterWaveLengthFromFft
		UpdateData(FALSE);
	}


	delete[] phase1;
	delete[] phase2;
	delete[] phaseDel;
	delete[] phaseDel1;
	delete[] phaseDel2;
	delete[] phaseDelCorrect;

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);

}


void CPhaseDlg::OnBnClickedButton1()
{
	UpdateData(TRUE);

	//임시로 측정위치 세팅
	g_pConfig->ReadCalibrationInfo();
	if (m_Zoffset <= 5)
	{
		g_pConfig->m_dZdistanceCap1nStage_um = g_pConfig->m_dZdistanceCap1nStage_um + m_Zoffset;
		g_pConfig->m_dZdistanceCap2nStage_um = g_pConfig->m_dZdistanceCap2nStage_um + m_Zoffset;
		g_pConfig->m_dZdistanceCap3nStage_um = g_pConfig->m_dZdistanceCap3nStage_um + m_Zoffset;
		g_pConfig->m_dZdistanceCap4nStage_um = g_pConfig->m_dZdistanceCap4nStage_um + m_Zoffset;
	}
	else
	{
		m_Zoffset = 0;
		UpdateData(FALSE);
	}

}

//
//void CPhaseDlg::OnBnClickedButtonBgUpdateMeasure()
//{
//	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonBgUpdateMeasure() 버튼 클릭!"));
//
//	g_pWarning->m_strWarningMessageVal = "Phase 측정을 시작합니다 ! ";
//	g_pWarning->UpdateData(FALSE);
//	g_pWarning->ShowWindow(SW_SHOW);
//
//
//	UpdateData(TRUE);
//	int nRet = 0;
//
//	CAutoMessageDlg MsgBoxAuto(g_pPhase);
//
//	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
//	{
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
//		nRet = -1;
//		return;
//	}
//	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;
//
//	CString strValue;
//	m_listPhaseResult.ResetContent();
//	double phase_correction = 0;
//	double phase_measure = 0;
//
//	// Camera setting 
//	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);
//
//	//m_NumOfPhaseMeasureRepeat
//	double *phase1, *phase2, *phaseDel, *phaseDel1, *phaseDel2, *phaseDelCorrect;
//	phase1 = new double[m_NumOfPhaseMeasureRepeat];
//	phase2 = new double[m_NumOfPhaseMeasureRepeat];
//	phaseDel = new double[m_NumOfPhaseMeasureRepeat];
//	phaseDel1 = new double[m_NumOfPhaseMeasureRepeat];
//	phaseDel2 = new double[m_NumOfPhaseMeasureRepeat];
//	phaseDelCorrect = new double[m_NumOfPhaseMeasureRepeat];
//
//	//Z축 이동 및 인터락세팅
//	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
//	{
//		nRet = -1;
//		delete[] phase1;
//		delete[] phase2;
//		delete[] phaseDel;
//		delete[] phaseDel1;
//		delete[] phaseDel2;
//		delete[] phaseDelCorrect;
//
//		g_pWarning->ShowWindow(SW_HIDE);
//		MsgBoxAuto.DoModal(_T("  Z축 제어에 실패했습니다 !"), 2);
//		g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
//		nRet = -2;
//
//		return;
//	}
//	UpdateData(FALSE);
//
//
//	// Center Frequncy Setting
//	//g_pNavigationStage->MoveAbsolutePosition((m_MeasPt1PosX_mm + m_MeasPt2PosX_mm) / 2, (m_MeasPt1PosY_mm + m_MeasPt2PosY_mm) / 2);
//	//WaitSec(1);
//
//	//nRet = SetCenterFrequency();
//
//	for (int j = 0; j < m_NumOfPhaseMeasureRepeat; j++)
//	{
//		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
//			continue;
//
//		// BG 재설정
//
//		BackGroundSet((m_MeasPt1PosX_mm+ m_MeasPt2PosX_mm)/2, (m_MeasPt1PosY_mm+ m_MeasPt2PosY_mm)/2);
//
//		//Z축 이동 및 인터락세팅
//		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
//		{
//			nRet = -1;
//			delete[] phase1;
//			delete[] phase2;
//			delete[] phaseDel;
//			delete[] phaseDel1;
//			delete[] phaseDel2;
//			delete[] phaseDelCorrect;
//
//			g_pWarning->ShowWindow(SW_HIDE);
//			MsgBoxAuto.DoModal(_T("  Z축 제어에 실패했습니다 !"), 2);
//			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
//			nRet = -2;
//
//			return;
//		}
//		UpdateData(FALSE);
//
//
//		phase1[j] = PhaseMeasure(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm);
//		m_PhasePoint1 = phase1[j];
//		UpdateData(FALSE);
//
//		phase2[j] = PhaseMeasure(m_MeasPt2PosX_mm, m_MeasPt2PosY_mm);
//		m_PhasePoint2 = phase2[j];
//		UpdateData(FALSE);
//
//		phaseDel1[j] = AngleDiffrence(m_ReferencePhase, phase1[j]);
//		phaseDel2[j] = AngleDiffrence(phase2[j], m_ReferencePhase);
//		//phaseDel[i] = (phaseDel1[i] + phaseDel2[i])/2.0*180.0/ 3.14159265358979323846;
//
//		// 파장 보상식 추가해야함 
//		// lamda = 0.01692308*cw + 0.02053846;  2020.08.19  cw: 798.61 lamda: 13.5355
//		// phase lamda 반비례 ? 
//		// phase_correction = phase_measure * 13.5/m_CenterWaveLength;
//
//		phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;
//
//		// 2020.08.19  cw: 798.61 lamda: 13.5355
//		// 2020.08.20  cw: 798.83 lamda: 13.5392
//		//double CW = 798.83;
//		//double m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
//		phaseDel[j] = phase_measure;
//		phase_correction = phase_measure * 13.56 / m_CenterWaveLengthFromCw;
//		phaseDelCorrect[j] = phase_correction;
//		m_PhaseResult = phase_correction;
//
//		//strValue.Format("%d: \t%3.3f \t%3.3f", j, phaseDel[j], phaseDelCorrect[j]);
//		strValue.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
//		m_listPhaseResult.AddString(strValue);
//
//
//		CString strResult;
//		strResult.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j + 1, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
//		g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));
//
//		//m_CenterFreqIndex m_CenterWaveLengthFromFft
//		UpdateData(FALSE);
//	}
//
//	double phase1_avr, phase2_avr, phaseDel_avr;
//	double phase1_sum = 0, phase2_sum = 0, phaseDel_sum = 0;
//
//	for (int i = 0; i < m_NumOfPhaseMeasureRepeat; i++)
//	{
//		phase1_sum = phase1_sum + phase1[i];
//		phase2_sum = phase2_sum + phase2[i];
//		phaseDel_sum = phaseDel_sum + phaseDelCorrect[i];
//	}
//	phase1_avr = phase1_sum / m_NumOfPhaseMeasureRepeat;
//	phase2_avr = phase2_sum / m_NumOfPhaseMeasureRepeat;
//	phaseDel_avr = phaseDel_sum / m_NumOfPhaseMeasureRepeat;
//
//
//	m_PhasePoint1 = phase1_avr;
//	m_PhasePoint2 = phase2_avr;
//	m_PhaseResult = phaseDel_avr;
//
//	delete[] phase1;
//	delete[] phase2;
//	delete[] phaseDel;
//	delete[] phaseDel1;
//	delete[] phaseDel2;
//	delete[] phaseDelCorrect;
//
//	g_pWarning->ShowWindow(SW_HIDE);
//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
//	UpdateData(FALSE);
//}


void CPhaseDlg::OnBnClickedButtonBgUpdateMeasure()
{
	g_pLog->Display(0, _T("CPhaseDlg::OnBnClickedButtonBgUpdateMeasure() 버튼 클릭!"));

	g_pWarning->m_strWarningMessageVal = "Phase 측정을 시작합니다 ! ";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	UpdateData(TRUE);
	int nRet = 0;

	CAutoMessageDlg MsgBoxAuto(g_pPhase);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		nRet = -1;
		return;
	}
	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	CString strValue;
	m_listPhaseResult.ResetContent();
	double phase_correction = 0;
	double phase_measure = 0;

	// Camera setting 
	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

	//m_NumOfPhaseMeasureRepeat
	double *phase1, *phase2, *phaseDel, *phaseDel1, *phaseDel2, *phaseDelCorrect;
	phase1 = new double[m_NumOfPhaseMeasureRepeat];
	phase2 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel1 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDel2 = new double[m_NumOfPhaseMeasureRepeat];
	phaseDelCorrect = new double[m_NumOfPhaseMeasureRepeat];

	//Z축 이동 및 인터락세팅 //이거 뺴도됨
	//if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	//{
	//	nRet = -1;
	//	delete[] phase1;
	//	delete[] phase2;
	//	delete[] phaseDel;
	//	delete[] phaseDel1;
	//	delete[] phaseDel2;
	//	delete[] phaseDelCorrect;

	//	g_pWarning->ShowWindow(SW_HIDE);
	//	MsgBoxAuto.DoModal(_T("  Z축 제어에 실패했습니다 !"), 2);
	//	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	//	nRet = -2;

	//	return;
	//}
	//UpdateData(FALSE);


	// Center Frequncy Setting
	//g_pNavigationStage->MoveAbsolutePosition((m_MeasPt1PosX_mm + m_MeasPt2PosX_mm) / 2, (m_MeasPt1PosY_mm + m_MeasPt2PosY_mm) / 2);
	//WaitSec(1);

	//nRet = SetCenterFrequency();

	for (int j = 0; j < m_NumOfPhaseMeasureRepeat; j++)
	{
		if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
			continue;

		// BG 재설정

		BackGroundSet((m_MeasPt1PosX_mm + m_MeasPt2PosX_mm) / 2, (m_MeasPt1PosY_mm + m_MeasPt2PosY_mm) / 2);

		//Z축 이동 및 인터락세팅
		if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
		{
			nRet = -1;
			delete[] phase1;
			delete[] phase2;
			delete[] phaseDel;
			delete[] phaseDel1;
			delete[] phaseDel2;
			delete[] phaseDelCorrect;

			g_pWarning->ShowWindow(SW_HIDE);
			MsgBoxAuto.DoModal(_T("  Z축 제어에 실패했습니다 !"), 2);
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
			nRet = -2;

			return;
		}
		UpdateData(FALSE);


		phase1[j] = PhaseMeasure(m_MeasPt1PosX_mm, m_MeasPt1PosY_mm);
		m_PhasePoint1 = phase1[j];
		UpdateData(FALSE);

		phase2[j] = PhaseMeasure(m_MeasPt2PosX_mm, m_MeasPt2PosY_mm);
		m_PhasePoint2 = phase2[j];
		UpdateData(FALSE);

		phaseDel1[j] = AngleDiffrence(m_ReferencePhase, phase1[j]);
		phaseDel2[j] = AngleDiffrence(phase2[j], m_ReferencePhase);
		//phaseDel[i] = (phaseDel1[i] + phaseDel2[i])/2.0*180.0/ 3.14159265358979323846;

		// 파장 보상식 추가해야함 
		// lamda = 0.01692308*cw + 0.02053846;  2020.08.19  cw: 798.61 lamda: 13.5355
		// phase lamda 반비례 ? 
		// phase_correction = phase_measure * 13.5/m_CenterWaveLength;

		phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;

		// 2020.08.19  cw: 798.61 lamda: 13.5355
		// 2020.08.20  cw: 798.83 lamda: 13.5392
		//double CW = 798.83;
		//double m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
		phaseDel[j] = phase_measure;
		phase_correction = phase_measure * 13.56 / m_CenterWaveLengthFromCw;
		phaseDelCorrect[j] = phase_correction;
		m_PhaseResult = phase_correction;

		//strValue.Format("%d: \t%3.3f \t%3.3f", j, phaseDel[j], phaseDelCorrect[j]);
		strValue.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
		m_listPhaseResult.AddString(strValue);


		CString strResult;
		strResult.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j + 1, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
		g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));

		//m_CenterFreqIndex m_CenterWaveLengthFromFft
		UpdateData(FALSE);
	}

	double phase1_avr, phase2_avr, phaseDel_avr;
	double phase1_sum = 0, phase2_sum = 0, phaseDel_sum = 0;

	for (int i = 0; i < m_NumOfPhaseMeasureRepeat; i++)
	{
		phase1_sum = phase1_sum + phase1[i];
		phase2_sum = phase2_sum + phase2[i];
		phaseDel_sum = phaseDel_sum + phaseDelCorrect[i];
	}
	phase1_avr = phase1_sum / m_NumOfPhaseMeasureRepeat;
	phase2_avr = phase2_sum / m_NumOfPhaseMeasureRepeat;
	phaseDel_avr = phaseDel_sum / m_NumOfPhaseMeasureRepeat;


	m_PhasePoint1 = phase1_avr;
	m_PhasePoint2 = phase2_avr;
	m_PhaseResult = phaseDel_avr;

	delete[] phase1;
	delete[] phase2;
	delete[] phaseDel;
	delete[] phaseDel1;
	delete[] phaseDel2;
	delete[] phaseDelCorrect;

	g_pWarning->ShowWindow(SW_HIDE);
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	UpdateData(FALSE);
}

void CPhaseDlg::OnBnClickedButtonPhaseEuvManualAlign()
{
	int nReturn;
	nReturn = g_pMaskMap->MaskAlign_EUV_Phase(g_pMaskMap->m_bManualAlign);
	g_pScanStage->Move_Origin();
}


void CPhaseDlg::OnBnClickedButtonPhaseSetPoints()
{
	CMessageDlg MsgBox;
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		goto TERMINATE;
	}

	// 체크 사항 확인 
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		goto TERMINATE;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		goto TERMINATE;


	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		goto TERMINATE;
	}

	if (g_pMaskMap->m_bEUVAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		goto TERMINATE;
	}
	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" EUV ON 후 진행해주세요! "), 2);
		goto TERMINATE;
	}

	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	g_pWarning->m_strWarningMessageVal = " EUV Phase SetPoint 실행중입니다.";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);


	//Xray 카메라 세팅
	g_pXrayCamera->SetAlign(g_pConfig->m_dAlignExposureTime);

	int TotalMeasureNum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum;
	m_LeftX_mm.clear();
	m_RightX_mm.clear();
	m_LeftY_mm.clear();
	m_RightY_mm.clear();

	m_LeftX_mm.resize(TotalMeasureNum);
	m_RightX_mm.resize(TotalMeasureNum);
	m_LeftY_mm.resize(TotalMeasureNum);
	m_RightY_mm.resize(TotalMeasureNum);

	for (int i = 0; i < TotalMeasureNum; i++)
	{
		//Point  이동
		//Left, Right
		for (int j = 0; j < 2; j++)
		{
			//Left, Right 변수 정의
			double *Xmm, *Ymm;
			double TargetXum, TargetYum;

			if (j == 0) //left
			{
				Xmm = &m_LeftX_mm[i];
				Ymm = &m_LeftY_mm[i];
				TargetXum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um;
				TargetYum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um;
			}
			else if (j == 1)//right
			{
				Xmm = &m_RightX_mm[i];
				Ymm = &m_RightY_mm[i];
				TargetXum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightX_um;
				TargetYum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightY_um;
			}

			//Stage 이동
			g_pNavigationStage->MoveToMaskCenterCoordinate(TargetXum, TargetYum);

			//Align
			g_pMaskMap->m_bAutoSequenceProcessing = FALSE; //다른 함수 실행위해 임시

			m_AlignDirection = X_DIRECTION;
			UpdateData(FALSE);
			OnBnClickedButtonFineAlign();

			g_pWarning->m_strWarningMessageVal = " EUV Phase SetPoint 실행중입니다.";
			g_pWarning->UpdateData(FALSE);
			

			//수동 기다림
			g_pWarning->ShowWindow(SW_HIDE);
			if (MsgBox.DoModal(" OM 1st Align Mark를 화면 Center로 위치시킨 후 확인 버튼을 클릭하세요 !") == IDCANCEL)
			{
				MsgBoxAuto.DoModal(_T("중지되었습니다! "), 2);

				goto TERMINATE;

			}
			g_pMaskMap->m_bAutoSequenceProcessing = TRUE; //다른 함수 실행위해 임시

			*Xmm = g_pNavigationStage->GetPosmm(STAGE_X_AXIS);
			*Ymm = g_pNavigationStage->GetPosmm(STAGE_Y_AXIS);

			g_pWarning->ShowWindow(SW_SHOW);

		}

	}


TERMINATE:
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	g_pScanStage->Move_Origin();
}


void CPhaseDlg::OnBnClickedButtonPhaseProcess()
{
	CMessageDlg MsgBox;
	CAutoMessageDlg MsgBoxAuto(g_pMaskMap);

	CString strValue;

	UpdateData(TRUE);

	if (g_pMaskMap->m_bAutoSequenceProcessing == TRUE)
	{
		g_pWarning->ShowWindow(SW_HIDE);
		MsgBoxAuto.DoModal(_T(" 이미 Auto Sequence 가동 중. 진행할 수 없습니다 ! "), 2);
		goto TERMINATE;
	}

	// 체크 사항 확인 
	if (g_pConfig->m_nEquipmentMode == OFFLINE)
		goto TERMINATE;

	if (g_pScanStage == NULL || g_pNavigationStage == NULL || g_pRecipe == NULL || g_pXrayCamera == NULL || g_pWarning == NULL || g_pPhase == NULL || g_pEUVSource == NULL || g_pAP == NULL || g_pConfig == NULL)
		goto TERMINATE;


	if (g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList == NULL)
	{
		MsgBoxAuto.DoModal(_T(" Mask 측정 위치 정보가 없습니다! "), 2);
		goto TERMINATE;
	}

	if (g_pMaskMap->m_bEUVAlignComplete == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" Mask Align부터 진행해주세요! "), 2);
		goto TERMINATE;
	}
	if (g_pEUVSource->Is_EUV_On() == FALSE)
	{
		MsgBoxAuto.DoModal(_T(" EUV ON 후 진행해주세요! "), 2);
		goto TERMINATE;
	}


	g_pMaskMap->m_bAutoSequenceProcessing = TRUE;

	g_pWarning->m_strWarningMessageVal = " EUV PhaseProcess 실행중입니다.";
	g_pWarning->UpdateData(FALSE);
	g_pWarning->ShowWindow(SW_SHOW);

	// Camera setting 
	g_pXrayCamera->SetMeasure(g_pConfig->m_dMeasureExposureTime);

	int TotalMeasureNum = g_pMaskMap->m_MaskMapWnd.m_ProcessData.TotalMeasureNum;


	//Z축 이동 및 인터락세팅
	if (g_pAdam->MoveZCapsensorFocusPosition(TRUE, TRUE, 5, &m_CapSensorNumber, &m_CapSensorSetValue, &m_CapSensorGetValue) != 0)
	{
		MsgBoxAuto.DoModal(_T("Z축 제어에 실패했습니다 !"), 2);
		goto TERMINATE;
	}
	UpdateData(FALSE);

	m_listPhaseResult.ResetContent();
	double phase_correction = 0;
	double phase_measure = 0;

	for (int i = 0; i < TotalMeasureNum; i++)
	{
		int repeat = g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;

		vector<double> phase1(repeat, 0);
		vector<double> phase2(repeat, 0);
		vector<double> phaseDel(repeat, 0);
		vector<double> phaseDel1(repeat, 0);
		vector<double> phaseDel2(repeat, 0);
		vector<double> phaseDelCorrect(repeat, 0);

		double leftXmm, leftYmm, rightXmm, rightYmm;

		for (int j = 0; j < repeat; j++)
		{
			if (g_pMaskMap->m_bAutoSequenceProcessing == FALSE)
				continue;


			if (!m_UseAlign)
			{
				if (m_LeftX_mm.size() < TotalMeasureNum)
				{
					MsgBoxAuto.DoModal(_T("측정 point가 등록되지 않았습니다."), 2);
					goto TERMINATE;
				}
				//Align Data 사용
				leftXmm = m_LeftX_mm[i];
				leftYmm = m_LeftY_mm[i];
				rightXmm = m_RightX_mm[i];
				rightYmm = m_RightY_mm[i];
			}
			else
			{
				//레시피 좌표사용
				g_pNavigationStage->ConvertToStageFromMask(leftXmm, leftYmm, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].ReferenceCoodY_um);
				g_pNavigationStage->ConvertToStageFromMask(rightXmm, rightYmm, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightX_um, g_pMaskMap->m_MaskMapWnd.m_ProcessData.pMeasureList[i].dRightY_um);

				double leftxdiff_nm, leftydiff_nm;
				double rightxdiff_nm, rightydiff_nm;


				//5um->5.2um 로 패턴 보정
				//rightXmm = rightXmm + 200.0 / 1000000.0;

				//leftxdiff_nm = (m_LeftX_mm[i] - leftXmm) * 1000000;
				//leftydiff_nm = (m_LeftY_mm[i] - leftYmm) * 1000000;
				//rightxdiff_nm = (m_RightX_mm[i] - rightXmm) * 1000000;
				//rightydiff_nm = (m_RightY_mm[i] - rightYmm) * 1000000;

				int test = 0;
			}
			//phase1[j] = PhaseMeasure(m_LeftX_mm[i], m_LeftY_mm[i]);
			phase1[j] = PhaseMeasure(leftXmm, leftYmm);
			m_PhasePoint1 = phase1[j];
			UpdateData(FALSE);

			//phase2[j] = PhaseMeasure(m_RightX_mm[i], m_RightY_mm[i]);
			phase2[j] = PhaseMeasure(rightXmm, rightYmm);
			m_PhasePoint2 = phase2[j];
			UpdateData(FALSE);

			phaseDel1[j] = AngleDiffrence(m_ReferencePhase, phase1[j]);
			phaseDel2[j] = AngleDiffrence(phase2[j], m_ReferencePhase);
			//phaseDel[i] = (phaseDel1[i] + phaseDel2[i])/2.0*180.0/ 3.14159265358979323846;

			// 파장 보상식 추가해야함 
			// lamda = 0.01692308*cw + 0.02053846;  2020.08.19  cw: 798.61 lamda: 13.5355
			// phase lamda 반비례 ? 
			// phase_correction = phase_measure * 13.5/m_CenterWaveLength;

			phase_measure = (phaseDel1[j] + phaseDel2[j]) / 2.0*180.0 / 3.14159265358979323846;

			// 2020.08.19  cw: 798.61 lamda: 13.5355
			// 2020.08.20  cw: 798.83 lamda: 13.5392
			//double CW = 798.83;
			//double m_CenterWaveLengthFromCw = 0.01692308*g_pConfig->m_dContinuousWave + 0.02053846;
			phaseDel[j] = phase_measure;
			phase_correction = phase_measure * 13.56 / m_CenterWaveLengthFromCw;
			phaseDelCorrect[j] = phase_correction;
			m_PhaseResult = phase_correction;

			//strValue.Format("%d: \t%3.3f \t%3.3f", j, phaseDel[j], phaseDelCorrect[j]);
			strValue.Format("\t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", j, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
			m_listPhaseResult.AddString(strValue);


			CString strResult;
			strResult.Format("\t%d \t%d: \t%d \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.4f \t%.6f \t%.6f", i + 1, j + 1, m_CenterFreqIndex, g_pConfig->m_dContinuousWave, m_CenterWaveLengthFromCw, g_pPhase->m_ReferencePhase, phase1[j], phase2[j], phaseDel1[j], phaseDel2[j], phaseDel[j], phaseDelCorrect[j], m_MeaPt1MaskCoordX, m_MeaPt1MaskCoordY);
			g_pPhase->SaveLogFile(_T("phase_result_log.txt"), LPSTR(LPCTSTR(strResult)));

			//m_CenterFreqIndex m_CenterWaveLengthFromFft
			UpdateData(FALSE);
		}
	}

TERMINATE:
	g_pMaskMap->m_bAutoSequenceProcessing = FALSE;
	g_pWarning->ShowWindow(SW_HIDE);
	g_pScanStage->Move_Origin();
}
