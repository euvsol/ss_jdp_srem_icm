#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

IMPLEMENT_DYNAMIC(CLightControllerDlg, CDialogEx)

CLightControllerDlg::CLightControllerDlg(CWnd * pParent)
	: CDialogEx(IDD_LIGHT_CTRL_DIALOG, pParent)
{
}

CLightControllerDlg::~CLightControllerDlg()
{
}


void CLightControllerDlg::DoDataExchange(CDataExchange* pDX)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다.

	__super::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIGHT_CTRL_SLIDER, m_LightValueCtrl);
	DDX_Control(pDX, IDC_LIGHT_CTRL_EDIT, m_CurrentValue);
}

BEGIN_MESSAGE_MAP(CLightControllerDlg, CDialogEx)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_LIGHT_CTRL_ONOFF_BUTTON, &CLightControllerDlg::OnBnClickedLightCtrlOnoffButton)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIGHT_CTRL_SLIDER, &CLightControllerDlg::OnNMCustomdrawLightCtrlSlider)
	ON_BN_CLICKED(IDC_LIGHT_CTRL_SET_BUTTON, &CLightControllerDlg::OnBnClickedLightCtrlSetButton)
END_MESSAGE_MAP()

BOOL CLightControllerDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}

	return __super::PreTranslateMessage(pMsg);
}


BOOL CLightControllerDlg::OnInitDialog()
{
	__super::OnInitDialog();

	InitializeControls();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CLightControllerDlg::InitializeControls()
{
	m_LedIcon[0] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDOFF), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);
	m_LedIcon[1] = (HICON)::LoadImage(::AfxGetInstanceHandle(), MAKEINTRESOURCE(IDI_LEDONGREEN), IMAGE_ICON, 32, 32, LR_DEFAULTCOLOR);

	((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_CONNECT))->SetIcon(m_LedIcon[0]);
	((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_ON))->SetIcon(m_LedIcon[0]);

	m_LightValueCtrl.SetRange(0, 255);
	m_LightValueCtrl.SetRangeMin(0);
	m_LightValueCtrl.SetRangeMax(255);

	m_LightValueCtrl.SetTicFreq(1);
	m_LightValueCtrl.SetLineSize(5);
	m_LightValueCtrl.SetPageSize(10);

	m_LightValueCtrl.SetPos(180);
	SetDlgItemText(IDC_LIGHT_CTRL_EDIT, _T("180"));
}


int CLightControllerDlg::OpenDevice()
{
	int nRet = 0;

	nRet = OpenSerialPort(g_pConfig->m_chPORT[SERIAL_LIGHT_CTRL],
							g_pConfig->m_nBAUD_RATE[SERIAL_LIGHT_CTRL],
							g_pConfig->m_nUSE_BIT[SERIAL_LIGHT_CTRL],
							g_pConfig->m_nSTOP_BIT[SERIAL_LIGHT_CTRL],
							g_pConfig->m_nPARITY[SERIAL_LIGHT_CTRL]);

	if (nRet == 0)
	{
		((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_CONNECT))->SetIcon(m_LedIcon[1]);

		int ret = GetLightOnOffState();
		if (ret == 0 && m_bLightOnState == TRUE)
		{
			((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_ON))->SetIcon(m_LedIcon[1]);

			ret = GetLightValue();
			if (ret == 0)
			{
				CString temp;
				m_LightValueCtrl.SetPos(m_nLightValue);
				temp.Format("%d", m_nLightValue);
				SetDlgItemText(IDC_LIGHT_CTRL_EDIT, temp);
			}
		}
		else
		{
			ret = LightOn();
			if (ret == 0)
			{
				((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_ON))->SetIcon(m_LedIcon[1]);

				ret = GetLightValue();
				if (ret == 0)
				{
					CString temp;
					m_LightValueCtrl.SetPos(m_nLightValue);
					temp.Format("%d", m_nLightValue);
					SetDlgItemText(IDC_LIGHT_CTRL_EDIT, temp);
				}
			}
		}
	}

	return nRet;
}

int CLightControllerDlg::LightOn()
{
	int nRet;
	nRet = SetLightOnOff(TRUE);
	if (nRet == 0)
	{
		((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_ON))->SetIcon(m_LedIcon[1]);
		m_bLightOnState = TRUE;
	}

	return nRet;
}

int CLightControllerDlg::LightOff()
{
	int nRet;
	nRet = SetLightOnOff(FALSE);
	if (nRet == 0)
	{
		((CStatic*)GetDlgItem(IDC_LIGHT_CTRL_ON))->SetIcon(m_LedIcon[0]);
		m_bLightOnState = FALSE;
	}

	return nRet;
}

int CLightControllerDlg::SetLightValueDlg(int nValue)
{
	int nRet;
	CString sValue;
	sValue.Format(_T("%d"), nValue);
	m_CurrentValue.SetWindowText(sValue);
	nRet = SetLightValue(nValue);
	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);

	return nRet;
}

void CLightControllerDlg::OnDestroy()
{
	__super::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}

void CLightControllerDlg::OnBnClickedLightCtrlOnoffButton()
{
	int nRet = 0;

	if (m_bLightOnState == TRUE)
		nRet = LightOff();
	else
		nRet = LightOn();

	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);
}

void CLightControllerDlg::OnNMCustomdrawLightCtrlSlider(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMCUSTOMDRAW pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	
	int nPos;
	CString strPos;
	nPos = m_LightValueCtrl.GetPos();
	strPos.Format(_T("%d"), nPos);
	m_CurrentValue.SetWindowText(strPos);
	*pResult = 0;
}

void CLightControllerDlg::OnBnClickedLightCtrlSetButton()
{
	int nRet;
	CString sValue;
	m_CurrentValue.GetWindowText(sValue);
	nRet = SetLightValue(atoi(sValue));
	if (nRet != 0)
		g_pAlarm->SetAlarm(nRet);
}
