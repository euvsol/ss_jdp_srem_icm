﻿#pragma once


// CScanStageFlatnessTestDlg 대화 상자


#include <tuple>
#include <cmath>
#include <algorithm>

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>



class CScanStageFlatnessTestDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CScanStageFlatnessTestDlg)

public:
	CScanStageFlatnessTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CScanStageFlatnessTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PI_STAGE_FLATNESS_TEST_DIALOG };
//#endif
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.


	DECLARE_MESSAGE_MAP()
public:

	int PythonInitialize();
	PyObject* m_pyName, *m_pyModule, *m_pytip_tilt;

	virtual BOOL OnInitDialog();

	typedef struct {

		double a;
		double b;
		double c;

	}vectors;

	//struct Point {
	//	double x;
	//	double y;
	//};
	//Point m_TestStageumPos_um[20];


	HICON m_LedIcon[3];

	afx_msg void OnBnClickedCheckPiStagePoint1CalCtl();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnBnClickedCheckMaskFlatCtl();
	afx_msg void OnBnClickedCheckMaskFlatStopCtl();
	afx_msg void OnBnClickedCheckMaskFlatDefaultCtl();
	afx_msg void OnBnClickedCheckMaskFlatSetCtl();
	afx_msg void OnBnClickedCheckStageDel();
	afx_msg void OnBnClickedCheckStageAdd();
	afx_msg void OnBnClickedCheckPiStagePointApply();
	afx_msg void OnBnClickedCheckPiStagePointApply2();
	afx_msg void OnBnClickedCheckPiStagePointSave();

	CComboBox m_combox_cap;
	CEdit m_TestXPos[10];
	CEdit m_TestYPos[10];
	CEdit m_TestCapPos[10];
	CEdit m_TestErrorCap[10];
	CEdit m_XPos[10];
	CEdit m_YPos[10];
	CEdit m_CapPos[10];
	CEdit m_ErrorCap[10];
	CEdit m_point_user_num_ctl;
	CGridCtrl m_StagePosGrid;
	CProgressCtrl m_progress_mask_flat;

	static UINT PI_Stage_Check_Manual_Thread(LPVOID pParam);
	CWinThread		*m_pPIstageManualThread;		// PI Stage 평면도 측정 thread

	/* PI 스테이지 기울기 측정 진행 Flag */
	BOOL			m_PIstageManualThreadExitFlag;
	
	/* PI 스테이지 틸트 보정 측정 Point 갯수 지정 변수 */
	int m_User_Point_Num;

	/* PI 스테이지 틸트 보정 측정 Point 갯수 지정 변수 */
	int m_Add_Point_Num;
	int m_Default_Num;

	/* Point 측정 변수 */
	int m_Test_Point_Num;

	/* Point 벡터 측정 함수*/
	int PI_Stage_Check_Manual(int test_point);

	/* 각 Point 측정 완료 플래그 변수 */
	bool point_1;
	bool point_2;
	bool point_3;

	/* 각각 포인트의 X, Y, Z, CAP 값 변수*/
	double x1um, y1um, z1um;
	double x2um, y2um, z2um;
	double x3um, y3um, z3um;
	double cap1um, cap2um, cap3um, cap4um;

	/* 각각 Point 에서의 Vector 값 변수 */
	double n1_vec, n2_vec, n3_vec;

	/* Roll & Pich 계산 값 저장 변수 */
	double roll;
	double pitch;
	
	/* Roll & Pich 계산된 값 단위 환산 저장 변수  (u로 변환)*/
	double m_dtx_urad;
	double m_dty_urad;

	/* [보정 전] Roll & Pich 계산된 값 단위 환산 저장 변수 (u로 변환)*/
	double m_dResult_Tx_urad;
	double m_dResult_Ty_urad;

	/* [보정 후] Roll & Pich 계산된 값 단위 환산 저장 변수  (u로 변환)*/
	double test_result_roll_urad;
	double test_result_pitch_urad;

	/* 구해진 Roll & Pitch 을 통한 PI 스테이지에 실질적 보정되어야 하는 값 저장 변수 */
	double roll_revision_urad;
	double pitch_revision_urad;


	/* 보정 후 나온 Tx, Ty 값 저장 변수 */
	double m_dResultTxurad;
	double m_dResultTyurad;

	/* 레시피 값 읽는 변수 */
	double pi_flat_rcp_x[20];
	double pi_flat_rcp_y[20];

	/* Roll, Pitch 구하는 함수 */
	void Calculate_Vector(int n_point, BOOL Verification, double &Tx, double &Ty, int CapNum);

	/* 수정된 레시피 ReRead */
	void ReSetting_Pos_Recipy();

	/* 레시피 파일 그리는 함수 (최초 1번 실행)*/
	void InitPosGrid();

	/* 레시피 다시 ReRead*/
	void ReReadPointPosGrid();

	/* Stage Monitor 함수 */
	void PI_Stage_Flatness_Test_Monitor_Check();

	/* Mask 평탄도 측정 함수 */
	int Mask_Flatness();

	/* Mask 평탄도 측정 플러그 변수 */
	BOOL m_Mask_Flatness_Measrue_Start_Flag;

	/* Mask 평탄도 측정 X, Y 측정 시작 Default 값 저장 변수 */
	double mask_flatness_start_default_x_pos; 
	double mask_flatness_start_default_y_pos;

	/* 사용자 입력 좌표값 변수 & 실제 측정에 사용 되는 좌표 변수. */
	double get_x_pos;
	double get_y_pos;



	//void Point_Check_Enable_Btn(int Point_Num);
	int NPoint_Calculate(int n_point);

	//double* result_y_pos;
	//double* result_z_pos;
	//double* result_x_pos;

	// Point 저장 백터
	vector <double> set_y_pos_um;
	vector <double> set_x_pos_um;
	vector <double> set_cap1_pos_um;
	vector <double> set_cap2_pos_um;
	vector <double> set_cap3_pos_um;
	vector <double> set_cap4_pos_um;

	// 측정 값의 저장 백터
	vector <double> result_y_pos_um;
	vector <double> result_x_pos_um;
	vector <double> result_cap_pos_um;


	// 보정 후 Point 값의 저장 백터
	vector <double> after_result_y_pos_um;
	vector <double> after_result_x_pos_um;
	vector <double> after_result_cap_pos_um;

	// 보정 후 측정 값의 저장 백터
	vector <double> test_result_y_pos_um;
	vector <double> test_result_x_pos_um;
	vector <double> test_result_cap_pos_um;


	vector <double> error_cap_data_um;

	double xx = 0.0;
	double yy = 0.0;
	double xy = 0.0;
	double yz = 0.0;
	double xz = 0.0;


	char chStageumPositionString[128];
	void NPointErrorCapDataCalculate(int npoint, bool testmode = false);
	void Point_N_Calculate_Result();

	int Cap_Sel;
	int Stage_Coordinate_Sel;

	void OnStageTargetAdd();
	void OnStageTargetDel();
	void OnScanStageApply();
	void OnScanStageConfigSave();


	CComboBox m_combox_coordinate;



	/* MaskMap 에서 button 누를시 Auto 측정 */
	bool TipTilt_N_PointCheck(int Npoint);

};
