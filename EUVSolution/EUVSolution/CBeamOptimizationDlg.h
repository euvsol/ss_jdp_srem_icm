﻿#pragma once


// CSqOneOptimizationDlg 대화 상자

class CBeamOptimizationDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBeamOptimizationDlg)

public:
	CBeamOptimizationDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamOptimizationDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMOPTIMIZATION_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	bool m_isOptimization;
	int m_nOptimizationIteration;

	int cacGradient(double *rad);
	int cac2DGradient(double *rad);

	double m_dOptimizationParameterX;
	double m_dOptimizationParameterY;
	double m_dOptimizationParameterZ;
	double m_dOptimizationParameterRx;
	double m_dOptimizationParameterRy;
	double m_dOptimizationParameterRz;
	double m_dOptimizationParameterCx;
	double m_dOptimizationParameterCy;
	double m_dOptimizationParameterCz;
	double m_dOptimizationParameterIntensity;
	void runOptimization();
	void run2DOptimization();
//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
//	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButtonSqoptimizationRunoptimizatioin();
	afx_msg void OnBnClickedButtonSqoptimizationSetoptimizatioin();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
};
