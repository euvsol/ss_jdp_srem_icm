﻿// CXrayCameraConfigDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"

// CXrayCameraConfigDlg 대화 상자

IMPLEMENT_DYNAMIC(CXrayCameraConfigDlg, CDialogEx)

CXrayCameraConfigDlg* CXrayCameraConfigDlg::m_pDlgInst = NULL;

CXrayCameraConfigDlg::CXrayCameraConfigDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_XRAY_CAMERA_CONFIG_DIALOG, pParent)
{
	m_pDlgInst = this;
}

CXrayCameraConfigDlg::~CXrayCameraConfigDlg()
{
}

void CXrayCameraConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CXrayCameraConfigDlg, CDialogEx)		
	ON_BN_CLICKED(IDC_BUTTON_READ_CONFIG_XRAY_CAMERA, &CXrayCameraConfigDlg::OnBnClickedButtonReadConfigXrayCamera)	
	ON_BN_CLICKED(IDC_BUTTON_READ_XRAY_CAMERA, &CXrayCameraConfigDlg::OnBnClickedButtonReadXrayCamera)
	ON_BN_CLICKED(IDC_BUTTON_SET_XRAY_CAMERA, &CXrayCameraConfigDlg::OnBnClickedButtonSetXrayCamera)
END_MESSAGE_MAP()
// CXrayCameraConfigDlg 메시지 처리기



BOOL CXrayCameraConfigDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_XrayCameraInstance = CXRayCameraCtrl::GetXrayCameraInstance();

	GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL_DEVICE)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL2_DEVICE)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL_CONFIG)->EnableWindow(FALSE);
	GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL2_CONFIG)->EnableWindow(FALSE);
	
	//CXRayCameraDlg에서 Setting 함
	//m_XrayCameraInstance->ReadConfigFile(XRAY_CAMERA_CONFIG_FILE_FULLPATH);
	//m_XrayCameraInstance->SetXrayCameraControlParameterFromConfig();
	//m_XrayCameraInstance->GetXrayCameraControlParameterInDevice();

	UpdateConfigDataUi();
	UpdataDeviceDataUi();

	UpdateData(FALSE);

	//GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL_DEVICE)->ModifyStyle(BS_AUTOCHECKBOX, BS_CHECKBOX);
	//GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL2_DEVICE)->ModifyStyle(BS_AUTOCHECKBOX, BS_CHECKBOX);
	//GetDlgItem(IDC_CHECK_INVERT_OUTPUTSIGNAL_DEVICE)->ModifyStyle(BS_CHECKBOX, BS_AUTOCHECKBOX);	
	
	return TRUE;
}

void CXrayCameraConfigDlg::OnBnClickedButtonReadConfigXrayCamera()
{			
	m_XrayCameraInstance->ReadConfigFile(XRAY_CAMERA_CONFIG_FILE_FULLPATH);
	UpdateConfigDataUi();
}

void CXrayCameraConfigDlg::UpdateConfigDataUi()
{
	CString strValue;
	PicamEnumeratedType type;

	// ADC	
	type = PicamEnumeratedType_AdcQuality;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tAdcQuaulity).c_str();
	SetDlgItemTextA(IDC_EDIT_ADC_QUALITY_CONFIG, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fAdcSpeed);
	SetDlgItemTextA(IDC_EDIT_ADC_SPEED_CONFIG, strValue);

	type = PicamEnumeratedType_AdcAnalogGain;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tAdcAnalogGain).c_str();
	SetDlgItemTextA(IDC_EDIT_ADC_ANALOG_GAIN_CONFIG, strValue);

	// Sensor Temperature	
	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fSensorTemperatureSetPoint);
	SetDlgItemTextA(IDC_EDIT_SENSOR_TEMPERATURE_SETPOINT_CONFIG, strValue);

	// CCD ROI	
	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.x);
	SetDlgItemTextA(IDC_EDIT_ROI_X_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.y);
	SetDlgItemTextA(IDC_EDIT_ROI_Y_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.width);
	SetDlgItemTextA(IDC_EDIT_ROI_WIDTH_CONFIG, strValue);	

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.height);
	SetDlgItemTextA(IDC_EDIT_ROI_HEIGHT_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.x_binning);
	SetDlgItemTextA(IDC_EDIT_X_BINNING_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.Roi.y_binning);
	SetDlgItemTextA(IDC_EDIT_Y_BINNING_CONFIG, strValue);


	// READOUT CONTROL	
	type = PicamEnumeratedType_ReadoutControlMode;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tReadoutControlMode).c_str();
	SetDlgItemTextA(IDC_EDIT_READOUT_CONTROL_MODE_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.nReadoutPortCount);
	SetDlgItemTextA(IDC_EDIT_READ_OUT_PORT_COUNT_CONFIG, strValue);

	// SHUTTER		
	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fExposureTime);
	SetDlgItemTextA(IDC_EDIT_EXPOSURE_TIME_CONFIG, strValue);

	type = PicamEnumeratedType_ShutterTimingMode;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tShutterTimingMode).c_str();
	SetDlgItemTextA(IDC_EDIT_SHUTTER_TIMING_MODE_CONFIG, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fShutterDelayResolution);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_DELAY_RESOLUTION_CONFIG, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fShutterOpeningDelay);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_OPENING_DELAY_CONFIG, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.fShutterClosingDelay);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_CLOSING_DELAY_CONFIG, strValue);

	// HARDWARE IO	
	type = PicamEnumeratedType_TriggerResponse;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tTriggerResponse).c_str();
	SetDlgItemTextA(IDC_EDIT_TRIGGER_RESPONSE_CONFIG, strValue);

	type = PicamEnumeratedType_TriggerDetermination;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tTriggerDetermination).c_str();
	SetDlgItemTextA(IDC_EDIT_TRIGGER_DETERMINATION_CONFIG, strValue);

	type = PicamEnumeratedType_OutputSignal;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tOutputSignal).c_str();
	SetDlgItemTextA(IDC_EDIT_OUTPUTSIGNAL_CONFIG, strValue);

	type = PicamEnumeratedType_OutputSignal;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInConfig.tOutputSignal2).c_str();
	SetDlgItemTextA(IDC_EDIT_OUTPUTSIGNAL2_CONFIG, strValue);

	if (m_XrayCameraInstance->XrayCameraControlParameterInConfig.nbInvertOutputsignal == 1)
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL_CONFIG, TRUE);
	else
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL_CONFIG, FALSE);

	if (m_XrayCameraInstance->XrayCameraControlParameterInConfig.nbInvertOutputsignal2 == 1)
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL2_CONFIG, TRUE);
	else
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL2_CONFIG, FALSE);

	// SENSOR CLEANING	
	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.nCleanCycleCount);
	SetDlgItemTextA(IDC_EDIT_CLEAN_CYCLE_COUNT_CONFIG, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInConfig.nCleanCycleHeight);
	SetDlgItemTextA(IDC_EDIT_CLEAN_CYCLE_HEIGHT_CONFIG, strValue);

	UpdateData(FALSE);
}

void CXrayCameraConfigDlg::OnBnClickedButtonReadXrayCamera()
{
	PicamError error = PicamError_None;

	error = m_XrayCameraInstance->GetXrayCameraControlParameterInDevice();

	if (error == PicamError_None)
	{
		UpdataDeviceDataUi();
		UpdateData(FALSE);
	}
	else
	{
		CString strValue;		
		strValue = m_XrayCameraInstance->GetEnumString(PicamEnumeratedType_Error, error).c_str();
		AfxMessageBox(_T("Failed to read Xray Camera: ") + strValue);
	}	
}

void CXrayCameraConfigDlg::OnBnClickedButtonSetXrayCamera()
{
	PicamError error = PicamError_None;

	error = m_XrayCameraInstance->SetXrayCameraControlParameterFromConfig();

	if (error == PicamError_None)
	{
		error = m_XrayCameraInstance->GetXrayCameraControlParameterInDevice();

		if (error == PicamError_None)
		{
			UpdataDeviceDataUi();
			UpdateData(FALSE);
		}
		else
		{
			CString strValue;
			strValue = m_XrayCameraInstance->GetEnumString(PicamEnumeratedType_Error, error).c_str();
			AfxMessageBox(_T("Failed to read Xray Camera: ") + strValue);
		}		
	}
	else
	{
		CString strValue;
		strValue = m_XrayCameraInstance->GetEnumString(PicamEnumeratedType_Error, error).c_str();
		AfxMessageBox(_T("Failed to set Xray Camera: ") + strValue);
	}
}

void CXrayCameraConfigDlg::UpdataDeviceDataUi()
{
	CString strValue;
	PicamEnumeratedType type;

	// ADC	
	type = PicamEnumeratedType_AdcQuality;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tAdcQuaulity).c_str();
	SetDlgItemTextA(IDC_EDIT_ADC_QUALITY_DEVICE, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fAdcSpeed);
	SetDlgItemTextA(IDC_EDIT_ADC_SPEED_DEVICE, strValue);

	type = PicamEnumeratedType_AdcAnalogGain;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tAdcAnalogGain).c_str();
	SetDlgItemTextA(IDC_EDIT_ADC_ANALOG_GAIN_DEVICE, strValue);

	// Sensor Temperature	
	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fSensorTemperatureSetPoint);
	SetDlgItemTextA(IDC_EDIT_SENSOR_TEMPERATURE_SETPOINT_DEVICE, strValue);

	// CCD ROI	
	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.x);
	SetDlgItemTextA(IDC_EDIT_ROI_X_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.y);
	SetDlgItemTextA(IDC_EDIT_ROI_Y_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.width);
	SetDlgItemTextA(IDC_EDIT_ROI_WIDTH_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.height);
	SetDlgItemTextA(IDC_EDIT_ROI_HEIGHT_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.x_binning);
	SetDlgItemTextA(IDC_EDIT_X_BINNING_DEVICE, strValue);
	
	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.Roi.y_binning);
	SetDlgItemTextA(IDC_EDIT_Y_BINNING_DEVICE, strValue);

	// READOUT CONTROL	
	type = PicamEnumeratedType_ReadoutControlMode;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tReadoutControlMode).c_str();
	SetDlgItemTextA(IDC_EDIT_READOUT_CONTROL_MODE_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.nReadoutPortCount);
	SetDlgItemTextA(IDC_EDIT_READ_OUT_PORT_COUNT_DEVICE, strValue);

	// SHUTTER		
	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fExposureTime);
	SetDlgItemTextA(IDC_EDIT_EXPOSURE_TIME_DEVICE, strValue);

	type = PicamEnumeratedType_ShutterTimingMode;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tShutterTimingMode).c_str();
	SetDlgItemTextA(IDC_EDIT_SHUTTER_TIMING_MODE_DEVICE, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fShutterDelayResolution);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_DELAY_RESOLUTION_DEVICE, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fShutterOpeningDelay);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_OPENING_DELAY_DEVICE, strValue);

	strValue.Format(_T("%.2f"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.fShutterClosingDelay);
	SetDlgItemTextA(IDC_EDIT_SHUTTER_CLOSING_DELAY_DEVICE, strValue);

	// HARDWARE IO	
	type = PicamEnumeratedType_TriggerResponse;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tTriggerResponse).c_str();
	SetDlgItemTextA(IDC_EDIT_TRIGGER_RESPONSE_DEVICE, strValue);
#
	type = PicamEnumeratedType_TriggerDetermination;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tTriggerDetermination).c_str();
	SetDlgItemTextA(IDC_EDIT_TRIGGER_DETERMINATION_DEVICE, strValue);

	type = PicamEnumeratedType_OutputSignal;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tOutputSignal).c_str();
	SetDlgItemTextA(IDC_EDIT_OUTPUTSIGNAL_DEVICE, strValue);

	type = PicamEnumeratedType_OutputSignal;
	strValue = m_XrayCameraInstance->GetEnumString(type, m_XrayCameraInstance->XrayCameraControlParameterInDevice.tOutputSignal2).c_str();
	SetDlgItemTextA(IDC_EDIT_OUTPUTSIGNAL2_DEVICE, strValue);

	if (m_XrayCameraInstance->XrayCameraControlParameterInDevice.nbInvertOutputsignal == 1)
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL_DEVICE, TRUE);
	else
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL_DEVICE, FALSE);

	if (m_XrayCameraInstance->XrayCameraControlParameterInDevice.nbInvertOutputsignal2 == 1)
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL2_DEVICE, TRUE);
	else
		CheckDlgButton(IDC_CHECK_INVERT_OUTPUTSIGNAL2_DEVICE, FALSE);

	// SENSOR CLEANING	
	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.nCleanCycleCount);
	SetDlgItemTextA(IDC_EDIT_CLEAN_CYCLE_COUNT_DEVICE, strValue);

	strValue.Format(_T("%d"), m_XrayCameraInstance->XrayCameraControlParameterInDevice.nCleanCycleHeight);
	SetDlgItemTextA(IDC_EDIT_CLEAN_CYCLE_HEIGHT_DEVICE, strValue);

	//CXRayCameraDlg update
	g_pXrayCamera->UpdataXrayCameraParameter();

	UpdateData(FALSE);

}
