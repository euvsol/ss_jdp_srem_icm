﻿/**
 * OM Camera Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once


// CCameraDlg 대화 상자
typedef struct
{
	MIL_ID  MilDigitizer;
	MIL_ID  MilBasicDispImageBuf;
	MIL_ID  MilProcessedDispImageBuf;
	MIL_INT ProcessedImageCount;
	clock_t start, end;
	double result;
	FILE* fp = NULL;
} HookDataStruct;

class CCameraDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CCameraDlg)

public:
	CCameraDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CCameraDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CAMERA_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	BOOL			m_bGrab;
	BOOL			m_bConnected;

	DECLARE_MESSAGE_MAP()
public:
	int OpenDevice();

	MIL_ID			MilDigitizer;
	MIL_ID*			m_pMilProcImageBuf;		// Image Buffer For HookUp
	HookDataStruct	m_UserHookData;
	MIL_INT			m_MilProcImageBufSize;

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCamStartButton();
	afx_msg void OnBnClickedCamStopButton();
	afx_msg void OnBnClickedCamSaveButton();
	afx_msg void OnBnClickedCheckCrossline();
	afx_msg void OnBnClickedCheckMousemove();

	CCameraWnd	m_CameraWnd;
	BOOL m_bCrossLineCheck;
	BOOL m_bMouseMoveCheck;

	afx_msg void OnBnClickedCamShowZpButton();

	BOOL Is_CAM_Connected() { return m_bConnected; }

	void SaveOMImage();
	void GrabStart();
	void GrabStop();
	
	static MIL_INT MFTYPE ProcessingCallback(MIL_INT HookType, MIL_ID HookId, void* HookDataPtr);
	int m_nSetScaleMinValue;
	int m_nSetScaleMaxValue;
	afx_msg void OnBnClickedCamSetScaleButton();
	BOOL m_bDispScaleImage;

	BOOL DisplayScaleImage() { return m_bDispScaleImage; };
	afx_msg void OnBnClickedCamDispScaledCheck();

	void GetCurrentScaleValue(int nMin, int nMax);
	void InitScaleValue(int nMin, int nMax);
};
