﻿#pragma once


// CWarningDlg 대화 상자

class CWarningDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CWarningDlg)

public:
	CWarningDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CWarningDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_WARNING_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	CString m_strWarningMessageVal;
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnBnClickedOk();

	CFont Font;
	CColorStaticST m_StaticMessageCtrl;
	bool	n_bMessageMoxFlag;
};
