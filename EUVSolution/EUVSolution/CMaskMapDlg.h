﻿/**
 * Mask Map Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once
#include	"MaskMapWnd.h"
#include <tuple>

 // CMaskMapDlg 대화 상자

#define		OMTOEUV		1
#define		EUVTOOM		2

class CMaskMapDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CMaskMapDlg)

public:
	CMaskMapDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CMaskMapDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MASK_MAP_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	HICON m_hIcon;

	HICON m_LedIcon[3];
	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	afx_msg void OnBnClickedButtonRecipeFileSearch();
	afx_msg void OnBnClickedButtonRecipeFileLoad();
	afx_msg void OnBnClickedButtonMapSave();
	afx_msg void OnBnClickedCheckCodeShow();
	afx_msg void OnNMClickListFilelist(NMHDR *pNMHDR, LRESULT *pResult);

	CDateTimeCtrl m_DateTimeStartCtrl;
	COleDateTime m_DateTimeStart;
	CDateTimeCtrl m_DateTimeEndCtrl;
	COleDateTime m_DateTimeEnd;
	CListCtrl m_ListCtrlRecipeFile;

	CButton m_BtnSearchFileCtrl;
	CRichEditCtrl m_reCtrl;

	bool		bFinding;
	CFileFinder	m_filefinder;
	void FileSearchInFolder(CString strSearchFile, CString strBaseFolder);
	static void FileSearching(CFileFinder *pfilefinder, DWORD dwCode, void *pCustomParam);
	int		FindInList(LPCTSTR szFilename);
	void	AddFileToList(LPCTSTR szFilename);
	void	SetStatus(int nCount = 0, LPCTSTR szFolder = NULL);
	CString	GetListFilename(int nIndex);
	CPath GetCurSelListCtrl();
	void RecipeHeaderDisplay();
	BOOL OpenFile(char *fpath);


public:
	CMaskMapWnd	m_MaskMapWnd;
	int		m_nMapWidthPixelNo;
	int		m_nMapHeightPixelNo;
	BOOL m_bShowcode;
	BOOL m_bSelZoomCheck;
	BOOL m_bMaskMap;

	int LoadProcessData();

	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonMesurepointmove();
	afx_msg void OnBnClickedButtonMaskcoordmove();
	afx_msg void OnEnChangeEditPointno();
	afx_msg void OnEnChangeEditMaskcodx();
	afx_msg void OnEnChangeEditMaskcody();
	afx_msg void OnBnClickedCheckOmadam();
	afx_msg void OnBnClickedButtonGetPosition();
	afx_msg void OnEnChangeEditIncrementStepsize();
	afx_msg void OnBnClickedButtonXMinus();
	afx_msg void OnBnClickedButtonXPlus();
	afx_msg void OnBnClickedButtonYPlus();
	afx_msg void OnBnClickedButtonYMinus();
	afx_msg void OnBnClickedButtonStageOrigin();
	afx_msg void OnBnClickedButtonMoveorigin();
	afx_msg void OnBnClickedButtonDdlinit();
	afx_msg void OnBnClickedButtonScan();
	afx_msg void OnEnChangeEditIncrementUmstep();
	afx_msg void OnBnClickedButtonMoveXminus();
	afx_msg void OnBnClickedButtonMoveXplus();
	afx_msg void OnBnClickedButtonMoveYplus();
	afx_msg void OnBnClickedButtonMoveYminus();
	afx_msg void OnBnClickedButtonXyOrigin();
	afx_msg void OnBnClickedButtonMoveZplus();
	afx_msg void OnBnClickedButtonMoveZminus();
	afx_msg void OnBnClickedButtonZOrigin();
	afx_msg void OnEnChangeEditIncrementAnglestep();
	afx_msg void OnBnClickedButtonMoveTxminus();
	afx_msg void OnBnClickedButtonMoveTxplus();
	afx_msg void OnBnClickedButtonMoveTyplus();
	afx_msg void OnBnClickedButtonMoveTyminus();
	afx_msg void OnBnClickedButtonTxtyOrigin();
	afx_msg void OnEnChangeEditZFocuspos();
	afx_msg void OnTimer(UINT_PTR nIDEvent);


	CEdit m_CtrlStepSizeum;
	CEdit m_CtrlStepSizeurad;
	CEdit m_ZFocus2Ctrl;
	CEdit m_z_focus;
	CButton m_btnXMinusCtrl;
	CButton m_btnXPlusCtrl;
	CButton m_btnYPlusCtrl;
	CButton m_btnYMinusCtrl;
	CButton m_btnPIXMinusCtrl;
	CButton m_btnPIYMinusCtrl;
	CButton m_btnPIZMinusCtrl;
	CButton m_btnPIRXMinusCtrl;
	CButton m_btnPIRYMinusCtrl;
	CButton m_btnPIXPlusCtrl;
	CButton m_btnPIYPlusCtrl;
	CButton m_btnPIZPlusCtrl;
	CButton m_btnPIRXPlusCtrl;
	CButton m_btnPIRYPlusCtrl;

	void InitButtonSkin();


	CGridCtrl m_StagePosGrid;
	void InitStagePosGrid();

	afx_msg void OnStagePosGridClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridDblClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridRClick(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridStartEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	afx_msg void OnStagePosGridEndEdit(NMHDR *pNotifyStruct, LRESULT* pResult);
	CString m_strStagePosName;
	CString m_strStageStatus;
	CString m_strStageMovingDistance;
	double	m_dStageMovingDistance;

	afx_msg void OnDeltaposIncrementStepspin(NMHDR *pNMHDR, LRESULT *pResult);	
	afx_msg void OnDeltaposIncrementUmstepspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposIncrementUradstepspin(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonRecord();
	afx_msg void OnBnClickedButtonMaskload();
	afx_msg void OnBnClickedButtonMaskalign();
	afx_msg void OnBnClickedButtonMaskmeasureStart();
	afx_msg void OnBnClickedButtonMaskunload();
	afx_msg void OnEnChangeEditScanRepeatNumber();
	afx_msg void OnBnClickedButtonMovePosition();
	CEdit m_EditStepSizeCtrl;
	afx_msg void OnDeltaposIncrementUmstepspinz(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButtonXyMove();
	CString m_strXMovePos;
	CString m_strYMovePos;
	afx_msg void OnEnChangeEditXMovepos();
	afx_msg void OnEnChangeEditYMovepos();
	afx_msg void OnBnClickedButtonZoomreset();
	CEdit m_EditStagePosXCtrl;
	CEdit m_EditStagePosYCtrl;
	CEdit m_EditPIStagePosXCtrl;
	CEdit m_EditPIStagePosYCtrl;
	CEdit m_EditPIStagePosZCtrl;
	CEdit m_EditPIStagePosTipCtrl;
	CEdit m_EditPIStagePosTiltCtrl;
	afx_msg void OnEnChangeEditZInterlockpos();
	
	CButton m_CheckOMAdamCtrl;
	BOOL m_bOMDisplay;
	void ChangeOMEUVSet(int direction);

	BOOL m_bAutoSequenceProcessing;
	BOOL m_bOMAlignComplete;
	BOOL m_bEUVAlignComplete;

	int MaskZInterlock();	//Align 하기전 수행 -> 마스크 로딩 후 바로 수행 으로 변경필요
	int MaskAlign_OM(BOOL bManual);
	int MaskAlign_OM_OnlyLongTest(BOOL bManual = TRUE); //KJH Long Test 용 . OM align 형식적 진행을 위함.
	int MaskAlign_EUV_SREM(BOOL bAuto);	
	int MaskAlign_EUV_Phase(BOOL isManual); // Phase 관련 항목들은 추후 CPhaseDlg 또는 알고리즘으로 이동
	int WaferAlign_Notch_Litho(BOOL isManual);

	int MaskAlignPhase(BOOL bAuto, int nScopeType = SCOPE_ALL);	//nScopeType: 0(EUV), 1(SCOPE_OM4X), 2(SCOPE_OM100X), 3(SCOPE_ALL)	
	int MaskAlignOmPhase();
		
	int AutoRun(int nRepeatNo);
	int MaskImagingProcess();	
	int MaskImagingProcessNew();
	
	int NoEuvMaskImagingProcess();

	int MaskPhaseProcess();
	int PtrProcess();	
	int PtrScanProcess();
	int LithoDoseProcess();
	int LithoGratingProcess();
	int MaskLoad();
	int MaskUnload();

	BOOL Is_SEQ_Working() { return m_bAutoSequenceProcessing; }

	double m_dOMAlignPointLB_posx_mm, m_dOMAlignPointLB_posy_mm;
	double m_dOMAlignPointLT_posx_mm, m_dOMAlignPointLT_posy_mm;
	double m_dOMAlignPointRT_posx_mm, m_dOMAlignPointRT_posy_mm;
	double m_dEUVAlignPointLB_posx_mm, m_dEUVAlignPointLB_posy_mm;
	double m_dEUVAlignPointLT_posx_mm, m_dEUVAlignPointLT_posy_mm;
	double m_dEUVAlignPointRT_posx_mm, m_dEUVAlignPointRT_posy_mm;
	double m_dNotchAlignPoint1_posx_mm, m_dNotchAlignPoint1_posy_mm;
	double m_dNotchAlignPoint2_posx_mm, m_dNotchAlignPoint2_posy_mm;
	double m_dEUVNotchAlignPoint1_posx_mm, m_dEUVNotchAlignPoint1_posy_mm;
	double m_dEUVNotchAlignPoint2_posx_mm, m_dEUVNotchAlignPoint2_posy_mm;


	afx_msg void OnBnClickedButtonMaskmeasureAutostart();
	CButton m_bCheckCommentDisplayCtrl;
	afx_msg void OnBnClickedButtonStopAutosequence();
	CEdit m_EditLBAlignPointCoordXCtrl;
	CEdit m_EditLBAlignPointCoordYCtrl;
	CEdit m_EditMaskCenterCoordXCtrl;
	CEdit m_EditMaskCenterCoordYCtrl;
	CEdit m_EditMaskLBCoordXCtrl;
	CEdit m_EditMaskLBCoordYCtrl;
	afx_msg void OnBnClickedCheckMagnification();
	BOOL m_bLaserSwichFunction;
	afx_msg void OnBnClickedCheckLaserswichingButton();
	CButton m_CheckLaserSwichingCtrl;
	int m_nAutoRunRepeatSetNo;
	int m_nAutoRunRepeatResultNo;

	//int SetZInterlock();
	int m_nMeasurementPointNo;
	double m_dMaskCoordinateX_mm;
	double m_dMaskCoordinateY_mm;
	CButton m_CheckSwitchingOnOffCtrl;
	afx_msg void OnBnClickedCheckLaserSwitchOnoff();
	double m_dZInterlockPos;
	double m_dZFocusPos;
	double m_dZThroughFocusHeight;
	afx_msg void OnEnChangeEditZThroughfocusHeight();

	afx_msg void OnBnClickedCheckBidirectionScan();	
	afx_msg void OnBnClickedCheckScanOnly();
	CButton m_CheckBidirectionCtrl;
	CButton m_CheckLaserFrequency;
	CButton m_CheckScanOnlyCtrl;
	BOOL m_bBiDirectionFlag;
	afx_msg void OnBnClickedCheckOmalignonly();
	CButton m_CheckOMAlignOnlyCtrl;
	BOOL m_bOMAlignOnly;
	CButton m_CheckManualAlignCtrl;
	BOOL m_bManualAlign;
	afx_msg void OnBnClickedCheckAlignAutomanual();
	BOOL m_bLaserFeedback;
	afx_msg void OnStnClickedTextStageposgrid();
	afx_msg void OnBnClickedCheckOmAdamUi();
	CButton m_CheckOMAdamUi;
	BOOL m_bLaserFrequency5Kz;	//TRUE: 5Khz, FALSE: 1Khz
	afx_msg void OnBnClickedCheckLaserFrequency();


	/* ADAM - OM - SCAN STAGE CAL  Switch 변수*/
	int m_AdamOmtiltSwitch;
	afx_msg void OnBnClickedButtonMaskmeasureMacroStart();

	void MacroStart();
	BOOL m_bMacroSystemRunFlag;
	afx_msg void OnBnClickedCheckMaskmapReverseCoordinate();
	BOOL m_bReverseCoordinate;
	
	vector<double> CenterOfCircumCircle(vector<double> P1, vector<double> P2, vector<double> P3);

	double FindRotation(double x1, double y1, double x2, double y2);

	/* ELitho 설비에서 사용(임시) */
	int m_nEuvLitho_ScanGrid;	//nm
	CComboBox m_comboScanGridAtLitho;
	afx_msg void OnCloseupComboScangridLitho();
	CStatic m_Icon_NaviStageConnectState;
	afx_msg void OnBnClickedButtonNavigationstageConnect();

	void 		LithoDoseTimeCheck();
	int			m_nTimeSecCount;
	int			m_nTimeMinCount;

	int			LithoEndHour;
	int			LithoEndMin;
	afx_msg void OnBnClickedButtonClearPosition();
	afx_msg void OnBnClickedButtonBackupPosition();
	CComboBox m_tiptilt_cap;
	afx_msg void OnBnClickedButtonTiptiltStart();
	
	CButton m_TipTiltZSet;
	afx_msg void OnBnClickedCheckZSet();
	CEdit m_EditZHeight_Set;
};
