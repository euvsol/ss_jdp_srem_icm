﻿// CADAMSimpleRun.cpp: 구현 파일
//

// 내가 만든거...
//#include "stdafx.h"
//#include "EUVSolution.h"
//#include "CADAMSimpleRun.h"
//#include "afxdialogex.h"
//#include "Include.h"
//#include "resource.h"

#include "stdafx.h"
#include "Include.h"
#include "Extern.h"


using namespace std;

// CADAMSimpleRun 대화 상자

IMPLEMENT_DYNAMIC(CADAMSimpleRun, CDialogEx)

CADAMSimpleRun::CADAMSimpleRun(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_ADAM_SIMPLERUN_DIALOG, pParent)
{

}

CADAMSimpleRun::~CADAMSimpleRun()
{
}

void CADAMSimpleRun::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_STATIC_DETECTOR1, m_dSRDetector1Value);
	DDX_Control(pDX, IDC_STATIC_DETECTOR2, m_dSRDetector2Value);
	DDX_Control(pDX, IDC_STATIC_CAPSENSOR1, m_dSRCapSensor1Value);
	DDX_Control(pDX, IDC_STATIC_CAPSENSOR2, m_dSRCapSensor2Value);
	DDX_Control(pDX, IDC_STATIC_CAPSENSOR3, m_dSRCapSensor3Value);
	DDX_Control(pDX, IDC_STATIC_CAPSENSOR4, m_dSRCapSensor4Value);
	DDX_Control(pDX, IDC_STATIC_OPTICSENSOR1, m_dSROpticSensor1Value);
	DDX_Control(pDX, IDC_STATIC_OPTICSENSOR2, m_dSROpticSensor2Value);
	DDX_Control(pDX, IDC_STATIC_OPTICSENSOR3, m_dSROpticSensor3Value);
	DDX_Control(pDX, IDC_STATIC_PACKETNUM, m_dSRPacketNum);
	DDX_Control(pDX, IDC_STATIC_XPOS, m_dSRposX);
	DDX_Control(pDX, IDC_STATIC_YPOS, m_dSRposY);
}


BEGIN_MESSAGE_MAP(CADAMSimpleRun, CDialogEx)
	ON_BN_CLICKED(IDCANCEL, &CADAMSimpleRun::OnBnClickedCancel)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_SIMPLERUN_START_BUTTON, &CADAMSimpleRun::OnBnClickedSimplerunStartButton)
	ON_BN_CLICKED(IDC_SIMPLERUN_STOP_BUTTON, &CADAMSimpleRun::OnBnClickedSimplerunStopButton)
END_MESSAGE_MAP()


// CADAMSimpleRun 메시지 처리기


BOOL CADAMSimpleRun::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	//SetTimer(SIMPLE_RUN_TIMER, 100, NULL);		// 보통 OnInitDialog에 들어감. 원래 1000 이었는데, 500으로 변경함_KYD

	SetWindowPos(NULL, 2300, 500, 0, 0, SWP_NOSIZE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}



int CADAMSimpleRun::UpdateDataValue()
{
	int ret = 0;
	//CTime cTime = CTime::GetCurrentTime(); // 현재 시스템으로부터 날짜 및 시간을 얻어 온다.

	//CString strTime; // 반환되는 날짜와 시간을 저장할 CString 변수 선언
	CString tpacketNum, tXpos, tYpos, tDetector1, tDetector2, tCap1, tCap2, tCap3, tCap4, tOptic1, tOptic2, tOptic3;

	//strTime.Format("%02d초", cTime.GetSecond());   // 현재 시간 반환 // 현재 분 반환	 // 현재 초 반환

	//SYSTEMTIME cur_time;
	//GetLocalTime(&cur_time);
	//CString strPCTime;
	//strPCTime.Format("%03ld",cur_time.wMilliseconds);
	
	tpacketNum.Format(_T("%d"), g_pAdam->AdamData.m_nPacketNum);
	tXpos.Format(_T("%f"), g_pAdam->AdamData.m_dX_position);
	tYpos.Format(_T("%f"), g_pAdam->AdamData.m_dY_position);
	tDetector1.Format(_T("%f"), g_pAdam->AdamData.m_dDetector1);
	tDetector2.Format(_T("%f"), g_pAdam->AdamData.m_dDetector2);
	tCap1.Format(_T("%f"), g_pAdam->AdamData.m_dCapsensor1);
	tCap2.Format(_T("%f"), g_pAdam->AdamData.m_dCapsensor2);
	tCap3.Format(_T("%f"), g_pAdam->AdamData.m_dCapsensor3);
	tCap4.Format(_T("%f"), g_pAdam->AdamData.m_dCapsensor4);
	tOptic1.Format(_T("%f"), g_pAdam->AdamData.m_dOpticsensor1);
	tOptic2.Format(_T("%f"), g_pAdam->AdamData.m_dOpticsensor2);
	tOptic3.Format(_T("%f"), g_pAdam->AdamData.m_dOpticsensor3);

	// 여기에 데이터 업데이트 내용을 불러옴

	GetDlgItem(IDC_STATIC_PACKETNUM)->SetWindowTextA(tpacketNum);
	GetDlgItem(IDC_STATIC_XPOS)->SetWindowTextA(tXpos);
	GetDlgItem(IDC_STATIC_YPOS)->SetWindowTextA(tYpos);
	GetDlgItem(IDC_STATIC_DETECTOR1)->SetWindowTextA(tDetector1);
	GetDlgItem(IDC_STATIC_DETECTOR2)->SetWindowTextA(tDetector2);
	GetDlgItem(IDC_STATIC_CAPSENSOR1)->SetWindowTextA(tCap1);
	GetDlgItem(IDC_STATIC_CAPSENSOR2)->SetWindowTextA(tCap2);
	GetDlgItem(IDC_STATIC_CAPSENSOR3)->SetWindowTextA(tCap3);
	GetDlgItem(IDC_STATIC_CAPSENSOR4)->SetWindowTextA(tCap4);
	GetDlgItem(IDC_STATIC_OPTICSENSOR1)->SetWindowTextA(tOptic1);
	GetDlgItem(IDC_STATIC_OPTICSENSOR2)->SetWindowTextA(tOptic2);
	GetDlgItem(IDC_STATIC_OPTICSENSOR3)->SetWindowTextA(tOptic3);

	return ret;
}


void CADAMSimpleRun::OnBnClickedCancel()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	CDialogEx::OnCancel();
	KillTimer(SIMPLE_RUN_TIMER);
	DestroyWindow();

}


void CADAMSimpleRun::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	UpdateDataValue();
	CDialogEx::OnTimer(nIDEvent);
}


void CADAMSimpleRun::OnBnClickedSimplerunStartButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	//Command_ADAMStart();		//여기있는 ADAM Start 명령은 왜 안먹지? 한번 물어볼 것....KYD
	SetTimer(SIMPLE_RUN_TIMER, 200, NULL);
	g_pAdam -> Command_ADAMSimpleRun(); // ICD2에서 동작안함 ihlee
}


void CADAMSimpleRun::OnBnClickedSimplerunStopButton()
{
	// TODO: 여기에 컨트롤 알림 처리기 코드를 추가합니다.
	KillTimer(SIMPLE_RUN_TIMER);
}
