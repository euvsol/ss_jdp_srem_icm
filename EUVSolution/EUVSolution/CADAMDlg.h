﻿/**
 * ADAM Module Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

#define LB 0
#define LT 1
#define RT 2
#define RB 3

#define IMAGE_BUFFER_SIZE_FOR_FILE_SAVE		10

#include "CSqOne.h"

class CADAMDlg : public CDialogEx, public CADAMCtrl
{
	DECLARE_DYNAMIC(CADAMDlg)

	typedef struct _MOUSEPOSITION
	{
		void Set(MIL_INT DisplayPositionX, MIL_INT DisplayPositionY, MIL_DOUBLE BufferPositionX, MIL_DOUBLE BufferPositionY)
		{
			m_DisplayPositionX = DisplayPositionX;
			m_DisplayPositionY = DisplayPositionY;
			m_BufferPositionX = BufferPositionX;
			m_BufferPositionY = BufferPositionY;
		}
		_MOUSEPOSITION()
		{
			Set(M_INVALID, M_INVALID, M_INVALID, M_INVALID);
		}
		_MOUSEPOSITION& operator=(const _MOUSEPOSITION& MousePosition)
		{
			Set(MousePosition.m_DisplayPositionX,
				MousePosition.m_DisplayPositionY,
				MousePosition.m_BufferPositionX,
				MousePosition.m_BufferPositionY);

			return *this;
		}
		MIL_INT     m_DisplayPositionX;
		MIL_INT     m_DisplayPositionY;
		MIL_DOUBLE  m_BufferPositionX;
		MIL_DOUBLE  m_BufferPositionY;
	} MOUSEPOSITION;

public:
	CADAMDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CADAMDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ADAM_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

	/**
	* Description:   Display할 Image의 Width,Height pixel no
	*/
	int m_nDisplayImageWidth;
	int m_nDisplayImageHeight;

	/**
	* Description:   image data를 0~255로 normalization 시켜서 저장하기위한 버퍼. 이후 이 버퍼의 data를 Mil buffer로 copy
	*/
	//BYTE* m_pNormalized_Image_buffer;
	//BYTE* m_pNormalized_RegridImage_buffer;

	UINT8* m_8URawImage_bufferForDisp;
	UINT8* m_8URegridImage_bufferForDisp;

	/**
	* Description:   ADAM에서 날아온 Raw Image를 저장하는 Image File의 File Name
	*/


	CString m_strRegirdImageFileName;
	CString m_strAveragedImageFileName;



	/**
	* Description:   ADAM에서 날아온 Raw Image의 BMP File의 File Name
	*/
	CString m_strRawImageBMPFileName;



	/**
	* Description:   Interpolatoin 된 FOV data를 file로 저장하는 함수_KYD
	* return:		이상없으면 0, 그외 Error 를 Return
	*/
	//int		RegridImageFileSave();
	//int		AveragedImageFileSave();

	/**
	* Description:   ADAM에서 날아온 FOV data를 BMP file로 저장하는 함수.
	* return:		이상없으면 0, 그외 Error 를 Return
	*/
	int		RawImageBMPFileSave();


	/**
	* Description:   ADAM에서 날아온 FOV data를 화면에 Display하는 함수.
	* return:		이상없으면 0, 그외 Error 를 Return
	*/
	virtual int		DisplayRawImage();

	/**
	* Description:  Regrid & Interpolation 된 이미지를 Display 하는 함수
	* return:		이상없으면 0, 그외 Error 를 Return
	*/

	virtual int		DisplayFilteredImage(double* image);

	//int		RunFilter();

public:
	//int		FilteredImageFileSave(int nDefectNo=0);
	//void FilteredImageFileSave_Resize(int removeEdge, int nDefectNo, double **ImageBuffer, double capPos);
	//int		FilteredImageFileSave_Resize(int removeEdge, int nDefectNo);
	//int		RawImageFileSave();
	virtual int		RawImageFileSave();
	virtual int		RawImageFileSave3D(int scanBufIndex = -1);

	/**
	* Description:   Test Image를 생성하는 함수.
	* return:		이상없으면 0, 그외 Error 를 Return
	*/

	/**
	* Description:   아래 함수들은 MFC Message Handler 함수 및 가상함수
	*/
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	afx_msg void OnBnClickedAdamRunButton();
	afx_msg void OnBnClickedAdamStopButton();
	afx_msg void OnBnClickedAdamLifresetButton();

	//afx_msg void OnPaint();
	

	afx_msg void OnBnClickedImageLoadButton();
	afx_msg void OnBnClickedImageSaveButton();

	afx_msg void OnBnClickedGraphOnButton();

	CComboBox m_comboFovSelect;
	CComboBox m_comboScanGridSelect;
	CComboBox m_comboScanNumber;
	CComboBox m_comboDetectorSelection;

	afx_msg void OnCbnCloseupComboFovSelect();
	afx_msg void OnCbnCloseupComboScangridSelect();
	afx_msg void OnCbnCloseupComboScanNum();

	/**
	* Description:  Value의 추이를 확인하는 용도의 Graph
	*/
	int m_nUpperCutPixelNum;
	int m_nLeftCutPixelNum;


	CComboBox m_comboInterpolationFridSelect;
	afx_msg void OnCbnCloseupComboInterpolationGrid();

	afx_msg void OnCbnCloseupComboDetoctorSelection();
	afx_msg void OnBnClickedCornerFindButton();

	//POINT m_ptEUVMousePoint;
	POINT m_ptEUVLeftButtonDownPoint;//삭제 예정 ihlee
	BOOL m_bEUVMouseMoveLineDisplay;//삭제 예정 ihlee
	BOOL m_bEUVCrossLineDisplay;
	BOOL m_bEUVMouseMove;

	afx_msg void OnBnClickedCheckEuvCrossline();
	afx_msg void OnBnClickedCheckEuvMousemove();
	//afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//afx_msg void OnMouseMove(UINT nFlags, CPoint point);

	afx_msg void OnBnClickedAverageDataSaveButton();

	CEdit m_EditCornerThreshold;
	double CornerThreshold;

	BOOL m_bEUVEdgeFindSuccess;
	int m_nEdgeFindResultX;
	int m_nEdgeFindResultY;
	int GetCornerPixel(int* result_pixelx, int* result_pixely);

	int MoveParCenter_ForFOVChange(int nOldFov, int nNewFov);					//FOV Change시 Par Center 조정, On-the-fly로 이동

	CMacProgressCtrl m_ProgressEUVImagingCtrl;
	afx_msg void OnBnClickedAdamOresetButton();

	BOOL Is_ADAM_Connected() { return m_bConnected; }

	afx_msg void OnBnClickedButtonAdamTest();
	afx_msg void OnBnClickedAdamCapReadButton();
	afx_msg void OnBnClickedButtonAdamPortReopen();

	HICON m_LedIcon[3];

	/**
	* Description:   Mil 사용하기위한 기본 Default MIL ID 및 Default Buffer
	*/
	//MIL_ID  m_MilApplication;
	MIL_ID	m_MilSystem;
	//MIL_ID	m_MilDisplayGray;
	//MIL_ID	m_MilDisplayColor;//삭제 예정 ihlee
	MIL_ID	m_MilSaveBmpImage;


	//MIL_ID m_MilSystem = M_NULL;
	MIL_ID m_MilDisplay = M_NULL;
	MIL_ID m_MilColorLut = M_NULL;
	MIL_ID m_MilGraphListCross = M_NULL;

	MIL_INT m_CrossLabelCenter =-1;
	MIL_INT m_CrossLabelMove = -1;;
	MIL_INT m_MatchRectLabelCenter = -1;
	MIL_INT m_MatchCrossLabelCenter = -1;;

	MIL_ID m_MilGraphContext = M_NULL;

	MIL_ID m_MilRawImageForDisp = M_NULL;
	MIL_ID m_MilRegridImageForDisp = M_NULL;

	MIL_ID m_MilRawImageForDispChild = M_NULL;
	MIL_ID m_MilRegridImageForDispChild = M_NULL;

	//MIL_ID m_MilOverlayImage = M_NULL;

	int m_DispType;

	void MilInitialize();
	void MilDestroy();

	static MIL_INT MFTYPE MilMouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);	
	static MIL_INT MFTYPE MilMouseLeftButtonDoubleClickFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);
	static int IsMouseInImage(MOUSEPOSITION* mousePos, MIL_ID EventID);

	MIL_INT m_MilDispWidth, m_MilDispHeight;

	int m_MilRawImageForDispWidth;
	int m_MilRawImageForDispHeight;

	int m_MilRegridImageForDispWidth;
	int m_MilRegridImageForDispHeight;


	void DispMousePosition(MOUSEPOSITION MousePosition);
	const int m_constLutMax = 256 - 1;

	CWnd *m_pWndDisplay = NULL;
	void SelectDisp(MIL_ID milImage);
	
	//void EUVMoveLineDraw(MOUSEPOSITION MousePos);
	void EUVMoveLineDraw(double posX, double posY);
	void EUVMatchPointDraw(double posX, double posY);

	static CADAMDlg* m_pAdamDlgStaticInst;

	void virtual AdamUIControl(BOOL isEnable);

	void virtual CapSensorDataUiUpdate();
	void virtual CurrentScanNumUiUpdate();
	void SetAdamFovInfoUi();

	void virtual InitAdamDlgForScan();

	afx_msg void OnBnClickedButton3();
	afx_msg void OnCbnSelchangeComboScangridSelect();
	afx_msg void OnBnClickedRawImageSaveButton();
	afx_msg void OnBnClickedButtonAdamPortClose();

	CEdit m_editCtrlCap1;
	CEdit m_editCtrlCap2;
	CEdit m_editCtrlCap3;
	CEdit m_editCtrlCap4;
	afx_msg void OnBnClickedCheckCapsensorAutoRead();
	CButton m_ctrlCapSensorAutoRead;
	BOOL m_bCapAutoRead = FALSE;
	afx_msg void OnBnClickedButtonAdamAverageCount();
	unsigned int m_AverageCount;
	afx_msg void OnBnClickedButtonAdamFit();	

	CWinThread* m_pCapReadThread = NULL;
	BOOL m_bCapReadThreadExitFlag = FALSE;
	static UINT	CapReadThread(LPVOID pParam);

	CEdit m_editCtrlLifX;
	CEdit m_editCtrlLifY;
	afx_msg void OnBnClickedAdamScan();
	afx_msg void OnBnClickedFilteredDataSaveButton();
	afx_msg void OnBnClickedCheckAdamSaveEveryRawImage();
	afx_msg void OnBnClickedCheckDispNormalized();
	BOOL IsDisplayNormalizedImage;
	afx_msg void OnBnClickedCheckAdamSaveI0NormalizedImage();
	afx_msg void OnBnClickedButtonAdamInitEuvAlign();
	afx_msg void OnBnClickedButtonAdamApplyAlign();
	afx_msg void OnBnClickedButtonAdamAlginLt();
	afx_msg void OnBnClickedButtonAdamAlginRt();
	afx_msg void OnBnClickedButtonAdamAlginLb();
	afx_msg void OnBnClickedButtonAdamAlginRb();
	afx_msg void OnBnClickedCheckAdamUse4ptAlign();

	BOOL bIsUse4ptAlign;
	// LB LT RT RB 0, 1, 2, 3
	double XAlignDesign_um[4] = { -64950,-64950,64950,64950 };
	double YAlignDesign_um[4] = { -51450, 51550, 51550,-51450 };
	double XAlignMeasure_um[4];
	double YAlignMeasure_um[4];

	double H_StageToMask[9];
	double H_MaskToStage[9];
	void MakeRainbowColorMap();

	static UINT	ImageSaveThreadFun(LPVOID pParam);
	double*** m_ImageBufferForSave;
	void FilteredImageFileSaveByThread(int _removeEdge, int _DefectNo, int _ThroughFocusNo, double capPos_um, int refcap, double focusPos);
	int m_ImageSaveThreadCount;

	struct ImageSaveThreadParam
	{
		CADAMDlg* pAdam;
		int removeEdge;
		int defectNo;
		int throughFocusNo;
		int threadCount;
		double capPos;
		int refCap;
		double focusPos;
	};

	afx_msg void OnBnClickedButtonAdamMoveZFocusPos();

	int MoveZCapsensor(double targetPos_um, int maxTry, int capNum, double *measureCapPos = NULL);
	int MoveZCapsensorFocusPosition(BOOL isSetFocusPos = FALSE, BOOL isSetInterlock = FALSE, double interlockMargin_um = 5, int *refCap = NULL, double *refCapPos= NULL, double *measureCapPos = NULL, double delta_target =0.0);

	int MoveZCapsensorFastAlignFocusPosition(BOOL isSetFocusPos = FALSE, BOOL isSetInterlock = FALSE, double interlockMargin_um = 5, int *refCap = NULL, double *refCapPos = NULL, double *measureCapPos = NULL, double delta_target = 0.0);


	double m_refGap_um = 7;
	int MoveZToGapPosition(double targetGag_um);



	CEdit m_editCtrlCurrentScanNum;
	afx_msg void OnBnClickedButton5();
	afx_msg void OnBnClickedButtonAdamMoveZFocusPosAndSetInterlock();
	afx_msg void OnBnClickedButton7();

	CWinThread*		m_RawImageDisplayThread = NULL;
	static UINT	RawImageDisplayThread(LPVOID pParam);
	BOOL m_bRawImageDisplayThreadExitFlag = FALSE;
	CEdit m_ctrlMeasureStatus;
	CEdit m_ctrlThroughFocusStatus;
	afx_msg void OnBnClickedButtonBeamOpen();
	afx_msg void OnBnClickedButtonBeamClose();

	void UpdateBeamReceiveData(char* data);
	afx_msg void OnBnClickedButtonBeamReopen();



	afx_msg void OnStnClickedEuvDisplay();
	BOOL m_IsUseRegridRawImage;
	afx_msg void OnBnClickedCheckAdamRegridRawImage();
	afx_msg void OnBnClickedButtonAdamI0Apply();
	afx_msg void OnBnClickedButtonAdamI0Save();
	afx_msg void OnBnClickedButtonAdamI0Restore();
	afx_msg void OnBnClickedCheckAdamHighFovInterpolation();
	CButton m_checkIsUseHighFovInterpolation;
	afx_msg void OnClickedCheckAdamThroughFocusCrossStep();
	BOOL m_IsCrossStep;
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton6();	
	void BeamOpen();	
	afx_msg void OnBnClickedButton8();
	afx_msg void OnBnClickedButton11();
	afx_msg void OnBnClickedButton10();
	afx_msg void OnBnClickedButton9();
	
	afx_msg void OnBnClickedButton1();
	
	afx_msg void OnBnClickedCheck3umFov();
	afx_msg void OnBnClickedButton12();


	void ManualScanThreadFun();

	afx_msg void OnBnClickedCheckResetStartPos();
	
	CString m_Sq1GetString ="1";
	CString m_Sq1SetString="2";

	CSqOne m_Sq1;	

	afx_msg void OnBnClickedButton13();
};
