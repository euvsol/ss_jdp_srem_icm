﻿#pragma once

#include "ChartViewer.h"
#include "afxwin.h"

// CChartdirDlg 대화 상자

class CChartdirDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CChartdirDlg)

public:
	CChartdirDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CChartdirDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CHART_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:

	double dataX[10];
	double dataY[10];

	double dataZ[100];

	//double data_X[10];
	//double data_Y[10];
	//double data_Z[10];

	const int dataXLength = (int)(sizeof(dataX) / sizeof(*dataX));
	const int dataYLength = (int)(sizeof(dataY) / sizeof(*dataY));

	SurfaceChart *surface_chart;
	CChartViewer m_ChartViewer;
	virtual BOOL OnInitDialog();
		
	afx_msg void OnViewPortChanged();
	afx_msg void OnMouseMoveChart();
	afx_msg void OnMouseUpChart();

	void drawChart(CChartViewer *viewer);
	void DataRead();
	void DataReset();

	// 3D view angles
	double m_elevationAngle;
	double m_rotationAngle;

	// Keep track of mouse drag
	int m_lastMouseX;
	int m_lastMouseY;
	bool m_isDragging;


	CFont font;

	afx_msg void OnBnClickedCheckReset();
	afx_msg void OnBnClickedCheck();
};
