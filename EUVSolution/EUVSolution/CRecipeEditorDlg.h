﻿/**
 * Equipment Recipe Editor Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

#define TOTAL_PATTERNMATCH_NUMBER	10  //0:Test, 1:4X LT Align, 2:4X RT Align, 3:4X LB Align, 4:100X LT Align, 5:100X RT Align, 6:100X LB Align, 7:EUV LT Align, 8:EUV RT Align, 9:EUV LB Align
 /* Image rotation values. */
#define ROTATED_FIND_ROTATION_DELTA_ANGLE  10
#define ROTATED_FIND_ROTATION_ANGLE_STEP   1
#define ROTATED_FIND_RAD_PER_DEG           0.01745329251
/* Minimum accuracy for the search angle. */
#define ROTATED_FIND_MIN_ANGLE_ACCURACY        0.25
/* Angle range to search. */
#define ROTATED_FIND_ANGLE_DELTA_POS           ROTATED_FIND_ROTATION_DELTA_ANGLE
#define ROTATED_FIND_ANGLE_DELTA_NEG           ROTATED_FIND_ROTATION_DELTA_ANGLE

#define PM_TEST_POSITION		0
#define PM_1STALIGN_POSITION	1
#define PM_2NDALIGN_POSITION	2
#define PM_3RDALING_POSITION	3

 // CRecipeEditorDlg 대화 상자

class CRecipeEditorDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CRecipeEditorDlg)

public:
	CRecipeEditorDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CRecipeEditorDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_RECIPE_EDIT_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCheckOmAlign1st();
	afx_msg void OnBnClickedCheckOmAlign2nd();
	afx_msg void OnBnClickedCheckOmAlign3rd();
	afx_msg void OnBnClickedCheckEuvAlign1st();
	afx_msg void OnBnClickedCheckEuvAlign2nd();
	afx_msg void OnBnClickedCheckEuvAlign3rd();
	afx_msg void OnBnClickedCheckTestPoint();
	afx_msg void OnBnClickedButtonMovePatternmatch();
	afx_msg void OnBnClickedButtonMakePatternmatch();
	afx_msg void OnBnClickedButtonDeletePatternmatch();
	afx_msg void OnBnClickedButtonTestPatternmatch();
	afx_msg void OnBnClickedButtonSavePatternmatch();


	CButton m_CheckOM1stAlignCtrl;
	CButton m_CheckOM2ndAlignCtrl;
	CButton m_CheckOM3rdAlignCtrl;
	CButton m_CheckOM100X1stAlignCtrl;
	CButton m_CheckOM100X2ndAlignCtrl;
	CButton m_CheckOM100X3rdAlignCtrl;
	CButton m_CheckEUV1stAlignCtrl;
	CButton m_CheckEUV2ndAlignCtrl;
	CButton m_CheckEUV3rdAlignCtrl;
	CButton m_CheckTestPointCtrl;

	afx_msg void OnPaint();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedCheckOm100Align1st();
	afx_msg void OnBnClickedCheckOm100Align2nd();
	afx_msg void OnBnClickedCheckOm100Align3rd();
	afx_msg void OnBnClickedCheckPmAreaRect();


	MIL_ID	m_MilSystem;
	MIL_ID  m_MilDisplayModel[TOTAL_PATTERNMATCH_NUMBER];
	MIL_ID  m_MilImageModel[TOTAL_PATTERNMATCH_NUMBER];
	MIL_ID  m_MilPMModel[TOTAL_PATTERNMATCH_NUMBER];
	MIL_ID  m_MilGrayModel;
	MIL_ID  m_MilResultModel;

	BOOL	m_bPMRectDisplay;
	BOOL	m_bPMSuccess;
	double	m_dPMScore;
	double	m_dPMResultPosX;
	double	m_dPMResultPosY;
	double  m_dPMResultAngle;
	int		m_nSelectedViewType;

	void	CheckAlignPoint(int nPoint);
	void	MovePMModel(int nViewType);
	void	MakePMModel(int nViewType);
	int		SearchPMModel(int nViewType);

	int		nSuccessScore;

	CButton m_ButtonTestModelCtrl;
 };
