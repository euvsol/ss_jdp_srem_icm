﻿#pragma once


// CSqOneConfig 대화 상자

class CSqOneConfigDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CSqOneConfigDlg)

public:
	CSqOneConfigDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSqOneConfigDlg();

	// 대화 상자 데이터입니다.
	//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SQONECONFIG_DIALOG };
	//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonSqconfigSetinitialparameter();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	double m_dBeamInitialSetHorizontal;
	double m_dBeamInitialSetVertical;
	double m_dBeamInitialSetPitch;
	double m_dBeamInitialSetYaw;
	virtual BOOL OnInitDialog();
	virtual BOOL DestroyWindow();

	void initializePositionDB();
};
