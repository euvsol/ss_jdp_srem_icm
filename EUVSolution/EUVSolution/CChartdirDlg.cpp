﻿// CChartdirDlg.cpp: 구현 파일
//
#include "stdafx.h"
#include "Include.h"
#include "Extern.h"



// CChartdirDlg 대화 상자

IMPLEMENT_DYNAMIC(CChartdirDlg, CDialogEx)

CChartdirDlg::CChartdirDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_CHART_DIALOG, pParent)
{
	m_elevationAngle = 30;
	m_rotationAngle = 45;

	// Keep track of mouse drag
	m_isDragging = false;
	m_lastMouseX = -1;
	m_lastMouseY = -1;
}

CChartdirDlg::~CChartdirDlg()
{
	//delete m_ChartViewer.getChart();

	if (surface_chart != NULL)
	{
		delete surface_chart;
		surface_chart = NULL;
	}
}

void CChartdirDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_CHART_VIEW, m_ChartViewer);
}


BEGIN_MESSAGE_MAP(CChartdirDlg, CDialogEx)
	ON_CONTROL(CVN_ViewPortChanged, IDC_CHART_VIEW, OnViewPortChanged)
	ON_CONTROL(CVN_MouseMoveChart, IDC_CHART_VIEW, OnMouseMoveChart)
	ON_CONTROL(BN_CLICKED, IDC_CHART_VIEW, OnMouseUpChart)
	ON_BN_CLICKED(IDC_CHECK_RESET, &CChartdirDlg::OnBnClickedCheckReset)
	ON_BN_CLICKED(IDC_CHECK, &CChartdirDlg::OnBnClickedCheck)
END_MESSAGE_MAP()


// CChartdirDlg 메시지 처리기


BOOL CChartdirDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Unlike most other GUI frameworks, MFC does not enable mouse capture by default.
	// We set the mouse usage to custom usage (-1) so that the CChartViewer will capture
	// the mouse on mouse down. This allows the mouse move event handler to determine if
	// the drag is initiated by the m_ChartViewer. Also, the click event will occur on 
	// mouse up, so it can be used to detect mouse up.
	m_ChartViewer.setMouseUsage(-1);

	// Update the viewport to display the chart
	m_ChartViewer.updateViewPort(true, false);
	font.CreateFont(23, 13, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PALETTE, _T("HY헤드라인M"));
	GetDlgItem(IDC_STATIC_MASK_FALT)->SetFont(&font);
	DataReset();
	drawChart(&m_ChartViewer);
//	if (m_ChartViewer.needUpdateChart())
		//drawChart(&m_ChartViewer);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

void CChartdirDlg::OnViewPortChanged()
{
	// Update the chart if necessary
//	m_ChartViewer.needUpdateChart();
	if (m_ChartViewer.needUpdateChart())
		drawChart(&m_ChartViewer);
}


//
// Draw the chart and display it in the given viewer
//
void CChartdirDlg::drawChart(CChartViewer *viewer)
{
	//CString test;
	//CString path;
	//
	////char *read_value = 0;
	//char read_value[100];
	//double read_value1[200];
	//char a;

	//int n = 0;

	//path = LOG_PATH;
	//test.Format(_T("%s\\test.txt"), path);

	//std::ifstream test_file_open;
	//test_file_open.open(test);

	//if (test_file_open.is_open())
	//{
	//	while (!test_file_open.eof())
	//	{

	//		test_file_open.getline(read_value, 100);
	//		read_value1[n] = atof(read_value);
	//		n++;
	//	}
	//}


	//test_file_open.close();
	//// The x and y coordinates of the grid
	////double dataX[] = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//double dataX[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//const int dataXLength = (int)(sizeof(dataX) / sizeof(*dataX));

	////double dataY[] = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//double dataY[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//const int dataYLength = (int)(sizeof(dataY) / sizeof(*dataY));

	//// The values at the grid points. In this example, we will compute the values using the
	//// formula z = x * sin(y) + y * sin(x).
	////double dataZ[dataXLength * dataYLength];
	////double dataZ[] = { 171.8643 ,	172.3704 ,	172.9231 ,	178.7904 ,	178.95 ,	179.133 ,	179.0477 ,	178.8736 ,	178.7965 ,	178.6486 ,	171.7658 ,	172.2974 ,	172.9941 ,	178.1977 ,	178.4334 ,	178.5717 ,	178.5758 ,	178.3838 ,	178.292 ,	178.1457 ,	172.208 ,	172.7895 ,	173.3747 ,	178.0239 ,	178.1663 ,	178.3179 ,	178.341 ,	178.2483 ,	178.1583 ,	178.0751 ,	176.9756 ,	177.264 ,	177.6602 ,	177.6749 ,	177.8767 ,	178.0965 ,	178.1088 ,	178.0529 ,	178.0471 ,	177.9912 ,	176.9723 ,	177.2849 ,	177.7274 ,	177.7265 ,	177.9518 ,	178.1774 ,	178.2712 ,	178.2227 ,	178.1347 ,	178.1693 ,	176.9669 ,	177.3281 ,	177.7737 ,	177.7988 ,	177.9993 ,	178.2157 ,	178.3203 ,	178.2992 ,	178.3188 ,	178.3072 ,	177.4092 ,	177.6771 ,	178.0934 ,	178.0735 ,	178.2423 ,	178.518 ,	178.6619 ,	178.5713 ,	178.6538 ,	178.7304 ,	177.9351 ,	178.2438 ,	178.6754 ,	178.6368 ,	178.8099 ,	179.0455 ,	179.1341 ,	179.16 ,	179.1438 ,	179.2288 ,	178.5328 ,	178.8175 ,	179.1567 ,	179.1027 ,	179.2744 ,	179.469 ,	179.6438 ,	179.6683 ,	179.7503 ,	179.8534 ,	179.2069 ,	179.4178 ,	179.6872 ,	179.6649 ,	179.8049 ,	180.0306 ,	180.1391 ,	180.1177 ,	180.3056 ,	180.411 };
	//double dataZ[(int)((sizeof(read_value1) / sizeof(*read_value1))/2)] = { 0, };
	//
	//for (int a = 0; a < 100; a++)
	//{
	//	*(dataZ+a) = *(read_value1+a);

	//}
	//int nn = 0;

	///*while (true)
	//{
	//	dataZ[nn] = read_value1[nn];
	//	nn++;
	//	if (n == 100) break;
	//}
	//*/

	////for (int yIndex = 0; yIndex < dataYLength; ++yIndex)
	////{
	////	double y = dataY[yIndex];
	////	for (int xIndex = 0; xIndex < dataXLength; ++xIndex)
	////	{
	////		double x = dataX[xIndex];
	////		dataZ[yIndex * dataXLength + xIndex] = x * sin(y) + y * sin(x);
	////	}
	////}

	// Create a SurfaceChart object of size 720 x 600 pixels
	surface_chart = new SurfaceChart(720, 600);

	// Set the center of the plot region at (330, 290), and set width x depth x height to
	// 360 x 360 x 270 pixels
	surface_chart->setPlotRegion(330, 290, 360, 360, 270);
	
	//background Setting 
	surface_chart->setBackground(LIGHT_GRAY, Transparent, 0);
	
	// Add a legend box at (55, 0) (top of the chart) using 8 pts Arial Font. Set
	// background and border to Transparent.
	//surface_chart->addLegend(55, 0, false, "", 8)->setBackground(Chart::Transparent);

	// Set the data to use to plot the chart
//	surface_chart->setData(DoubleArray(dataX, dataXLength), DoubleArray(dataY, dataYLength),
//		DoubleArray(dataZ, dataXLength * dataYLength));


	surface_chart->setData(DoubleArray(dataX, dataXLength), DoubleArray(dataY, dataYLength),
		DoubleArray(dataZ, dataXLength * dataYLength));

	//DataRead();
	// Spline interpolate data to a 80 x 80 grid for a smooth surface
	surface_chart->setInterpolation(80, 80);

	// Set the view angles
	surface_chart->setViewAngle(m_elevationAngle, m_rotationAngle);
	//c->setViewAngle(100, 100);

	// Check if draw frame only during rotation
	if (m_isDragging)
		surface_chart->setShadingMode(Chart::RectangularShading);

	// Add a color axis (the legend) in which the left center is anchored at (660, 270). Set
	// the length to 200 pixels and the labels on the right side.
	surface_chart->setColorAxis(650, 270, Chart::Left, 200, Chart::Right);


	// Title
	surface_chart->addTitle("Flatness Measurement", "arialdb.ttf", 20);

	// Set the x, y and z axis titles using 10 points Arial Bold font
	surface_chart->xAxis()->setTitle("X", "arialbd.ttf", 15);
	surface_chart->yAxis()->setTitle("Y", "arialbd.ttf", 15);

	// Set axis label font
	surface_chart->xAxis()->setLabelStyle("arial.ttf", 10);
	surface_chart->yAxis()->setLabelStyle("arial.ttf", 10);
	surface_chart->zAxis()->setLabelStyle("arial.ttf", 10);
	surface_chart->colorAxis()->setLabelStyle("arial.ttf", 10);

	// Output the chart
	delete viewer->getChart();
	viewer->setChart(surface_chart);


	// KJH 테스트 중
	//surface_chart->makeChart("surface.jpg");
}



//
// Rotate surface chart when mouse is moving over plotarea
//
void CChartdirDlg::OnMouseMoveChart()
{
	int mouseX = m_ChartViewer.getChartMouseX();
	int mouseY = m_ChartViewer.getChartMouseY();

	// Drag occurs if mouse button is down and the mouse is captured by the m_ChartViewer
	if (((GetKeyState(VK_LBUTTON) & 0x100) != 0) && (GetCapture() == &m_ChartViewer))
	{
		if (m_isDragging)
		{
			// The chart is configured to rotate by 90 degrees when the mouse moves from 
			// left to right, which is the plot region width (360 pixels). Similarly, the
			// elevation changes by 90 degrees when the mouse moves from top to buttom,
			// which is the plot region height (270 pixels).
			m_rotationAngle += (m_lastMouseX - mouseX) * 90.0 / 360;
			m_elevationAngle += (mouseY - m_lastMouseY) * 90.0 / 270;
			m_ChartViewer.updateViewPort(true, false);
		}

		// Keep track of the last mouse position
		m_lastMouseX = mouseX;
		m_lastMouseY = mouseY;
		m_isDragging = true;
	}
}

//
// Stops dragging on mouse up
//
void CChartdirDlg::OnMouseUpChart()
{
	m_isDragging = false;
	m_ChartViewer.updateViewPort(true, false);
}
void CChartdirDlg::DataReset()
{
	for (int x = 0; x < 10; x++)
	{
		dataX[x] = x+1;
		dataY[x] = x+1;
	}

	for (int x = 0; x < 100; x++)
	{
		dataZ[x] = 0;
	}

}
void CChartdirDlg::DataRead()
{
	CString get_cap_data;
	CString path;
	CString strDate, strTime;

	char read_value[100];
	double read_value1[200];

	int num = 0;


	path = LOG_PATH;

	GetSystemDateTime(&strDate, &strTime);
	path += "\\";
	path += strDate;

	get_cap_data.Format(_T("%s\\cap_data.txt"), path);

	std::ifstream data_file_open;
	data_file_open.open(get_cap_data);

	//double dataX[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

	for (int x = 0; x < 10; x++)
	{
		dataX[x] = x+1;
		dataY[x] = x+1;
	}

	//const int dataXLength = (int)(sizeof(dataX) / sizeof(*dataX));
	//const int dataYLength = (int)(sizeof(dataY) / sizeof(*dataY));
	//double dataY[] = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//double dataY[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//const int dataYLength = (int)(sizeof(dataY) / sizeof(*dataY));

	if (data_file_open.is_open())
	{
		while (!data_file_open.eof())
		{

			data_file_open.getline(read_value, 100);
			read_value1[num] = atof(read_value);
			num++;
		}

		data_file_open.close();

		for (int a = 0; a < 100; a++)
		{
			*(dataZ + a) = *(read_value1 + a);

		}
		// The x and y coordinates of the grid
		//double dataX[] = { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	
		// The values at the grid points. In this example, we will compute the values using the
		// formula z = x * sin(y) + y * sin(x).
		//double dataZ[dataXLength * dataYLength];
		//double dataZ[] = { 171.8643 ,	172.3704 ,	172.9231 ,	178.7904 ,	178.95 ,	179.133 ,	179.0477 ,	178.8736 ,	178.7965 ,	178.6486 ,	171.7658 ,	172.2974 ,	172.9941 ,	178.1977 ,	178.4334 ,	178.5717 ,	178.5758 ,	178.3838 ,	178.292 ,	178.1457 ,	172.208 ,	172.7895 ,	173.3747 ,	178.0239 ,	178.1663 ,	178.3179 ,	178.341 ,	178.2483 ,	178.1583 ,	178.0751 ,	176.9756 ,	177.264 ,	177.6602 ,	177.6749 ,	177.8767 ,	178.0965 ,	178.1088 ,	178.0529 ,	178.0471 ,	177.9912 ,	176.9723 ,	177.2849 ,	177.7274 ,	177.7265 ,	177.9518 ,	178.1774 ,	178.2712 ,	178.2227 ,	178.1347 ,	178.1693 ,	176.9669 ,	177.3281 ,	177.7737 ,	177.7988 ,	177.9993 ,	178.2157 ,	178.3203 ,	178.2992 ,	178.3188 ,	178.3072 ,	177.4092 ,	177.6771 ,	178.0934 ,	178.0735 ,	178.2423 ,	178.518 ,	178.6619 ,	178.5713 ,	178.6538 ,	178.7304 ,	177.9351 ,	178.2438 ,	178.6754 ,	178.6368 ,	178.8099 ,	179.0455 ,	179.1341 ,	179.16 ,	179.1438 ,	179.2288 ,	178.5328 ,	178.8175 ,	179.1567 ,	179.1027 ,	179.2744 ,	179.469 ,	179.6438 ,	179.6683 ,	179.7503 ,	179.8534 ,	179.2069 ,	179.4178 ,	179.6872 ,	179.6649 ,	179.8049 ,	180.0306 ,	180.1391 ,	180.1177 ,	180.3056 ,	180.411 };
		//double dataZ[(int)((sizeof(read_value1) / sizeof(*read_value1)) / 2)] = { 0, };
		//*dataZ = { 0, };



	//	surface_chart->setData(DoubleArray(dataX, dataXLength), DoubleArray(dataY, dataYLength),
	//		DoubleArray(dataZ, dataXLength * dataYLength));
	}
	else 
	{
		for (int a = 0; a < 100; a++)
		{
			*(dataZ + a) = 0;

		}
	//	double dataZ[(int)((sizeof(read_value1) / sizeof(*read_value1)) / 2)] = { 0, };
	//	surface_chart->setData(DoubleArray(dataX, dataXLength), DoubleArray(dataY, dataYLength),
	//		DoubleArray(dataZ, dataXLength * dataYLength));
	}

}

//void CChartdirDlg::OnBnClickedCheck1()
//{
//
//}


void CChartdirDlg::OnBnClickedCheckReset()
{
	DataReset();
	drawChart(&m_ChartViewer);
}


void CChartdirDlg::OnBnClickedCheck()
{
	DataRead();
	drawChart(&m_ChartViewer);
}
