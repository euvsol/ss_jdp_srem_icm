﻿#pragma once


// CBeamOptimizationConfigDlg 대화 상자

class CBeamAlignConfigDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CBeamAlignConfigDlg)

public:
	CBeamAlignConfigDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CBeamAlignConfigDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_BEAMALIGNCONFIG_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL DestroyWindow();
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	void changeConfig(BOOL isSimulator);

	CString m_strOmegaPitch;
	CString m_strOmegaYaw;
	CString m_strMaxIntensity;
	CString m_strMinIntensity;
	CString m_strDetPercent;
	CString m_strDetIntensity;
	CString m_strSatPercent;
	CString m_strSatIntensity;
	CString m_strIncrementPercent;
	CString m_strGridPercent;
	CString m_strFineScanGrid;

	afx_msg void OnBnClickedBeamconfigButtonSetintensityparameter();

};
