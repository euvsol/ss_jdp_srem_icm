﻿/**
 * System Test Module Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * Description: 각종 Sequence Test Command, Test Data Result Display
 */
#pragma once


#define		STOP_TEST						0
#define		MASK_SLIP_TEST					1
#define		EUV_STAGE_REPEATABILITY_TEST	2
#define		OM_STAGE_REPEATABILITY_TEST		3
#define		STAGE_LONGRUN_TEST				4
#define		LLC_PUMP_VENT_LONGRUN_TEST		5
// CSystemTestDlg 대화 상자

class CSystemTestDlg : public CDialogEx, public CECommon
{
	DECLARE_DYNAMIC(CSystemTestDlg)

public:
	CSystemTestDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CSystemTestDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SYSTEM_TEST_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	afx_msg void OnBnClickedLoadTestButton();
	afx_msg void OnBnClickedUnloadTestButton();
	afx_msg void OnBnClickedSeqStopButton();
	afx_msg void OnBnClickedLongrunTestButton();
	BOOL m_bTestStop;
	BOOL m_Mask_Measrue_Start;
	BOOL m_SH_Stage_Measrue_Start;
	BOOL m_PI_Stage_Measrue_Start;
	BOOL m_Source_Measrue_Start;
	BOOL m_OMEUV_Measrue_Start;
	int m_nRepeatNo;
	afx_msg void OnEnChangeEditTestRepeatno();
	int m_nCurrentNo;
	afx_msg void OnBnClickedCheckStagelongrunTest();
	afx_msg void OnBnClickedCheckOmStagerepeatTest();
	afx_msg void OnBnClickedCheckEuvStagerepeatTest();
	afx_msg void OnBnClickedCheckMasksliptest();
	afx_msg void OnBnClickedCheckTestend();

	int m_nTestType;
	void CheckTESTTYPE(int nTest);
	CButton m_CheckTestEndCtrl;
	CButton m_CheckMaskSlipTestCtrl;
	CButton m_CheckEUVStageRepeatTestCtrl;
	CButton m_CheckOMStageRepeatTestCtrl;
	CButton m_CheckStageLongRunTestCtrl;

	void RunMaskSlipTest();
	void SaveTestResult(int nTestType, double param1, double param2, double param3 = 0.0, double param4 = 0.0);

	int AutoRun(int nRepeatNo);
	int MaskLoad();
	int MaskUnload();
	int FindMark();

	BOOL m_bAutoSequenceProcessing;

	static UINT LLC_Vacuum_Run_Thread(LPVOID pParam);
	static UINT Source_Test_VacuumThread(LPVOID pParam);
	static UINT OffSet_Setting_VacuumThread(LPVOID pParam);
	static UINT	Stage_Check_Thread(LPVOID pParam);
	static UINT	PI_Stage_Check_Thread(LPVOID pParam);
	static UINT PI_Stage_Check_Manual_Thread(LPVOID pParam);


	CWinThread		*m_pVaccum_Run_Thread;
	CWinThread		*m_pLongRunThread;
	CWinThread		*m_pstageThread;		// Stage inposition, load 측정 thread
	CWinThread		*m_pSourceTestThread;	// source test thread
	CWinThread		*m_pOffsetThread;		// off set setting thread
	CWinThread		*m_pPIstageThread;		// PI Stage 평면도 측정 thread
	CWinThread		*m_pPIstageManualThread;		// PI Stage 평면도 측정 thread

	BOOL			m_bVacuum_Run_ThreadStop;
	BOOL			m_pSourceTestThreadStop;
	BOOL			m_pOffsetThreadStop;
	BOOL			m_stageThreadExitFlag;
	BOOL			m_PIstageThreadExitFlag;
	BOOL			m_PIstageManualThreadExitFlag;

	typedef struct {

		double a;
		double b;
		double c;

	}vectors;



	
	int SoureTestThreadStop();
	int OffsetThreadStop();

	CString str;

	/* PI 스테이지 틸트 보정 측정 Point 갯수 지정 변수 */
	int m_Point_Num;

	int LLC_Vacuum_Test_State;

	/* Long Run Test 횟수 사용자 지정 변수 */
	int num;

	int LLC_Start_Long_Run_Test();
	void LLC_long_run_test();

	int set_num ;
	void Long_Run_Error();
	void Long_Run_Stop();
	void LLC_Start_Long_Run_Test_View();

	int Mask_Flat();
	int Source_Check();
	int Source_Offset_Check();

	double OM_LB_align_posx_mm, OM_LB_align_posy_mm;
	double OM_LT_align_posx_mm, OM_LT_align_posy_mm;
	double OM_RT_align_posx_mm, OM_RT_align_posy_mm;
	double EUV_LB_align_posx_mm, EUV_LB_align_posy_mm;
	double EUV_LT_align_posx_mm, EUV_LT_align_posy_mm;
	double EUV_RT_align_posx_mm, EUV_RT_align_posy_mm;

	clock_t	m_start_time, m_finish_time;
	clock_t	start, current_time;

	afx_msg void OnBnClickedLlcTestButton();
	afx_msg void OnBnClickedLoadTestButton3();
	afx_msg void OnBnClickedCheckMaskFlat();
	CProgressCtrl m_progress_mask_flat;
	afx_msg void OnBnClickedCheckMaskFlatSet();


	char cap_read_value[200]; // 평탄도 측정 cap data buffer 

	double default_x_pos; // Default 변수 값
	double default_y_pos;

	   
	double get_x_pos; // 사용자 입력 좌표값 변수 & 실제 측정에 사용 되는 좌표 변수.
	double get_y_pos;
	CString get_x_pos_str;
	CString get_y_pos_str;

	afx_msg void OnTimer(UINT_PTR nIDEvent);

	void Stage_axis_Check();
	void SH_Stage_Check_Axis_Check();
	void Source_measurement_axis_check();

	afx_msg void OnBnClickedCheckMaskFlatDefault();
	afx_msg void OnBnClickedCheckOffsetStart();

	CProgressCtrl m_progress_source_contamination;
	afx_msg void OnBnClickedCheckMaskFlatStop();

	bool flatness_measurement_stop;
	afx_msg void OnBnClickedStageMeasurementStart();

	int ACS_Stage_Check();
	CProgressCtrl m_progress_stage_check;
	afx_msg void OnBnClickedStageMeasurementStop();



	void test();
	int PI_Stage_Check();
	int PI_Stage_Check_Manual(int point);
	void PI_Stage_Check_Axis_Check();

	afx_msg void OnBnClickedCheckPiStagePoint1Manual();


	int check_point;
	double x_pos_ref, y_pos_ref;	//Reference x, y 
	CButton mCheck_Stage_Move;
	CEdit m_ref_pos_x;
	CEdit m_ref_pos_y;
	afx_msg void OnBnClickedCheckStageMovePos();

	void 		ReSetting_Pos_Recipy();
	afx_msg void OnBnClickedCheckPiStagePoint2Manual();
	afx_msg void OnBnClickedCheckPiStagePoint3Manual();
	afx_msg void OnBnClickedCheckPiStagePointAuto();

	void InitPosGrid();
	void ReadPosGrid();
	CGridCtrl m_StagePosGrid;

	HICON m_LedIcon[3];

	double x1um, y1um, z1um, cap1um;
	double x2um, y2um, z2um, cap2um;
	double x3um, y3um, z3um, cap3um;

	double n1_vec, n2_vec, n3_vec;
	double roll;
	double pitch;
	double roll_urad;
	double pitch_urad;

	double roll_revision_urad;
	double pitch_revision_urad;

	double pi_flat_rcp_x[20];
	double pi_flat_rcp_y[20];

	afx_msg void OnBnClickedCheckPiStagePoint1Cal();
	
	/* 각 Point 측정 완료 플래그 변수 */
	bool point_1;
	bool point_2;
	bool point_3;

	void PI_Stage_Faltness_Monitor_Check();
	CButton m_PIXMinCtrl;
	CButton m_PIXPlusCtrl;
	CButton m_PIYPlusCtrl;
	CButton m_PIYMinCtrl;
	CButton m_PIXTHETAMin;
	CButton m_PIXTHETAPlus;
	CButton m_PIYTHETAPlus;
	CButton m_PIYTHETAMin;

	void Calculate_Vector();

	afx_msg void OnBnClickedCheckPiStageRead();
	afx_msg void OnBnClickedCheckStageMovePos2();
	CEdit m_point_user_num;
	BOOL m_bRepeatabilityCheck;
	double m_dRepeatPosX;
	double m_dRepeatPosY;
};
