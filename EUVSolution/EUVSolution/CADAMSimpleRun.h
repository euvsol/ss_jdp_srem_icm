﻿#pragma once

#define SIMPLE_RUN_TIMER  1		// Simple run을 구현하기 위한 timer 이름

// CADAMSimpleRun 대화 상자

class CADAMSimpleRun : public CDialogEx
{
	DECLARE_DYNAMIC(CADAMSimpleRun)

public:
	CADAMSimpleRun(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CADAMSimpleRun();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ADAM_SIMPLERUN_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCancel();


	int		UpdateDataValue();


	virtual BOOL OnInitDialog();

	CStatic m_dSRPacketNum;
	CStatic m_dSRDetector1Value;
	CStatic m_dSRDetector2Value;
	CStatic m_dSRCapSensor1Value;
	CStatic m_dSRCapSensor2Value;
	CStatic m_dSRCapSensor3Value;
	CStatic m_dSRCapSensor4Value;
	CStatic m_dSROpticSensor1Value;
	CStatic m_dSROpticSensor2Value;
	CStatic m_dSROpticSensor3Value;


	afx_msg void OnTimer(UINT_PTR nIDEvent);
	
	afx_msg void OnBnClickedSimplerunStartButton();
	afx_msg void OnBnClickedSimplerunStopButton();
	CStatic m_dSRposX;
	CStatic m_dSRposY;
};
