﻿#pragma once


// CDigitalOutputPart 대화 상자

class CDigitalOutputPart : public CDialogEx
{
	DECLARE_DYNAMIC(CDigitalOutputPart)

public:
	CDigitalOutputPart(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalOutputPart();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_PART_OUTPUT_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();
	virtual BOOL OnInitDialog();


	HICON	m_LedIcon[3];

	CBrush  m_brush;
	CBrush  m_brush2;
	CFont	m_font;

	void InitControls_DO_PART();
	void OnUpdateDigitalOutput_Part();
	void SetOutputPartBitOnOff(int nIndex);

	afx_msg void OnBnClickedCheckDigitaloutPartY000();
	afx_msg void OnBnClickedCheckDigitaloutPartY001();
	afx_msg void OnBnClickedCheckDigitaloutPartY002();
	afx_msg void OnBnClickedCheckDigitaloutPartY003();
	afx_msg void OnBnClickedCheckDigitaloutPartY004();
	afx_msg void OnBnClickedCheckDigitaloutPartY005();
	afx_msg void OnBnClickedCheckDigitaloutPartY006();
	afx_msg void OnBnClickedCheckDigitaloutPartY007();
	afx_msg void OnBnClickedCheckDigitaloutPartY008();
	afx_msg void OnBnClickedCheckDigitaloutPartY009();
	afx_msg void OnBnClickedCheckDigitaloutPartY010();
	afx_msg void OnBnClickedCheckDigitaloutPartY011();
	afx_msg void OnBnClickedCheckDigitaloutPartY012();
	afx_msg void OnBnClickedCheckDigitaloutPartY013();
	afx_msg void OnBnClickedCheckDigitaloutPartY014();
	afx_msg void OnBnClickedCheckDigitaloutPartY015();
};
