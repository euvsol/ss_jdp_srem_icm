﻿#pragma once


// CDigitalInput 대화 상자

class CDigitalInput : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CDigitalInput)

public:
	CDigitalInput(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CDigitalInput();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IO_INPUT_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	bool						m_Message_ThreadExitFlag;
	CWinThread*					m_Message_Thread;
	static UINT					Message_UpdataThread(LPVOID pParam);

	DECLARE_MESSAGE_MAP()

public:
	int		m_nChannel;
	HICON	m_LedIcon[3];

	void InitControls_DI(int nChannel);
	bool OnUpdateDigitalInput();

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnDestroy();

	CBrush  m_brush;
	CFont	m_font;

	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	int cnt_llc_dry_pump_on;
	int cnt_llc_dry_pump_alarm;
	int cnt_llc_dry_pump_alarm_error_reset_cnt;
	int cnt_llc_dry_pump_warning;
	int cnt_mc_dry_pump_on;
	int cnt_mc_dry_pump_alarm;
	int cnt_mc_dry_pump_warning;
	int cnt_mc_dry_pump_alarm_error_reset_cnt;
	int cnt_llc_tmp_water_leak;
	int cnt_mc_tmp_water_leak;
	int cnt_smoke_sensor;
	int cnt_water_return_temp_alarm;
	int cnt_main_air_alarm;
	int	cnt_main_water_alarm;

	int Dry_Pump_State;
	void Message_Thread();
	
};
