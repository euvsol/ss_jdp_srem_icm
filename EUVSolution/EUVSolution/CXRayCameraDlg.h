/**
 * Xray Camera Control Dialog Class
 *
 * Copyright 2020 by E-SOL, Inc.,
 *
 */
#pragma once

#define GRAB_OK 0
#define GRAB_TIMEOUT 1
#define GRAB_STOPPED 2
#define GRAB_PROGRESS 3

class CXRayCameraDlg : public CDialogEx, public CXRayCameraCtrl, public IObserver
{
	DECLARE_DYNAMIC(CXRayCameraDlg)

	typedef struct _MOUSEPOSITION
	{
		void Set(MIL_INT DisplayPositionX, MIL_INT DisplayPositionY, MIL_DOUBLE BufferPositionX, MIL_DOUBLE BufferPositionY)
		{
			m_DisplayPositionX = DisplayPositionX;
			m_DisplayPositionY = DisplayPositionY;
			m_BufferPositionX = BufferPositionX;
			m_BufferPositionY = BufferPositionY;
		}
		_MOUSEPOSITION()
		{
			Set(M_INVALID, M_INVALID, M_INVALID, M_INVALID);
		}
		_MOUSEPOSITION& operator=(const _MOUSEPOSITION& MousePosition)
		{
			Set(MousePosition.m_DisplayPositionX,
				MousePosition.m_DisplayPositionY,
				MousePosition.m_BufferPositionX,
				MousePosition.m_BufferPositionY);

			return *this;
		}
		MIL_INT     m_DisplayPositionX;
		MIL_INT     m_DisplayPositionY;
		MIL_DOUBLE  m_BufferPositionX;
		MIL_DOUBLE  m_BufferPositionY;
	} MOUSEPOSITION;

public:

	static CXRayCameraDlg* m_pDlgInst;

	CXRayCameraDlg(CWnd* pParent = nullptr);
	virtual ~CXRayCameraDlg();

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void DoDataExchange(CDataExchange* pDX);

	enum { IDD = IDD_XRAY_CAMERA_DIALOG };
		
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);	
	afx_msg void OnBnClickedSetSensorTempButton();			
	afx_msg void OnBnClickedConnectCameraButton();
	afx_msg void OnBnClickedDisconnectCameraButton();		
	afx_msg void OnChangeEditSaveFileName();
	afx_msg void OnClickedCheckAddDateToFilename();
	afx_msg void OnClickedCheckIncrementFileName();
	afx_msg void OnEnChangeEditIncrementStartNumber();
	afx_msg void OnCbnSelchangeComboIncrementDigit();
	afx_msg void OnEnChangeEditImageRoiX();
	afx_msg void OnEnChangeEditImageRoiY();
	afx_msg void OnEnChangeEditImageRoiWidth();
	afx_msg void OnEnChangeEditImageRoiHeight();
	afx_msg void OnBnClickedButtonFitDisplay();
	afx_msg void OnBnClickedCheckSaveImage();
	afx_msg void OnBnClickedCheckAutoScaleImage();	
	afx_msg void OnEnChangeEditCameraRoiX();
	afx_msg void OnEnChangeEditCameraRoiY();
	afx_msg void OnEnChangeEditCameraRoiWidth();
	afx_msg void OnEnChangeEditCameraRoiHeight();
	afx_msg void OnEnChangeRoiXBinningEdit();
	afx_msg void OnEnChangeEditAutoscaleLow();
	afx_msg void OnEnChangeEditAutoscaleHigh();
	//afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonGrabStop();	
	afx_msg void OnEnChangeExposureTimeEdit();
	
	//Xray Camera
	int OpenDevice();
	
	void EmergencyStop();
	//Phase 측정	

	void UpdataXrayCameraParameter();

	int SetMeasure(double exposoureTime_ms);
	int SetAlign(double exposoureTime_ms);
	int MilRealloc(int width, int height);

	void SetMeasureBackGround();
	void SetAlignBackGround();


	BOOL Is_XRAY_Connected() { return m_bConnected; }

protected:	

private:
	//Xray Camera
	//static void CopyXrayImageToMilBuffer();

	void GetPeriodicDeviceStatus();
	void CreateBitmapInfo(int w, int h, int bpp);	
	void UpdateRoi(int x, int y, int width, int height);		
	int SetCcdRoi(int x, int y, int width, int height, int x_binning, int y_binning);
	void GetCcdRoi();
	void SaveImage();
	void SaveParameterData(CString sSector, CString sKey, CString sValue);
	

	//MIL 		
	static MIL_INT MFTYPE MouseMoveFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);	
	static MIL_INT MFTYPE MouseLefeButtonUpFct(MIL_INT HookType, MIL_ID EventID, void* UserDataPtr);

	void MilInitialize();
	void MilDestroy();
	void CalMilimgaeStatistics();
	void UpdateDisplayLut();
	void SetMousePosition(MOUSEPOSITION MousePosition);

	//Phase 측정
	int MoveGrab(double x, double y);


private:
	BITMAPINFO* m_pBitmapInfo;
	
	const int m_constLutMax = 65535;
	
	//Camera setting 	
	//double m_ExposureTime;
	//double m_SensorTemperatureSetPoint;


	int m_ImageRoiX;
	int m_ImageRoiY;
	int m_ImageRoiWidth;
	int m_ImageRoiHeight;

	BOOL m_bOnline;
	BOOL m_bAcquisitionRunning = FALSE;

	//Phase 측정
	int m_nIncrementNumber;	
	int m_nPhaseNumOfPoint = 35;
	
	double m_dPhaseStartPosX = 0;
	double m_dPhaseStartPosY = 0;
	double m_dPhaseStartPosX2 = 0;
	double m_dPhaseStartPosY2 = 0;
	double m_dPhaseStepDistance = 0.0001;
	double m_dPhaseLsWidth = 0.100;
	double m_dRefPosX = 0;
	double m_dRefPosY = 0;
	double m_dAbsPosX = 0;
	double m_dAbsPosY = 0;
	double m_dRelPosX = 0;
	double m_dRelPosY = 0;

	//UI
	UINT m_ExposureTimeProgressStep = 100;
	int m_ExposureTimeProgressPos = 0;
	int m_LutLow;
	int m_LutHigh;
	BOOL isUpdatedLut = TRUE;


	HICON m_LedIcon[3];

	BOOL m_bAutoScaleImage;
	BOOL m_bSaveImage;
	BOOL m_bManualGrab;
	BOOL m_bAddIncrementToFileName;
	BOOL m_bAddDateToFilename;	
	
	CString m_strImageStatistics;
	CString m_strMousePosition;
	CString m_strSaveFileName;
	CString m_strSavePath;
	
	CComboBox m_AdcAnalogGainSelect;
	CComboBox m_AdcQualitySelect;
	CComboBox m_AdcSpeedSelect;
	
	CStatic m_CurrTempCtrl;
	CStatic m_FanStatusCtrl;
	CStatic m_TempStatusCtrl;	
	
	MIL_ID m_MilSystem = M_NULL;
	MIL_ID m_MilDisplay = M_NULL;

public:

	MIL_ID m_MilImageOriginal = M_NULL;
	MIL_ID m_MilImageBackGroundMeasure = M_NULL;
	MIL_ID m_MilImageBackGroundAlign = M_NULL;
	MIL_ID m_MilImageBackGround = M_NULL; // Alloc 안하고 Measure, Align 모드에 따라 변경
	
	MIL_ID m_MilGraphContext = M_NULL;
	MIL_ID m_MilGraphList = M_NULL;
	//MIL_ID m_MilOverlayImage = M_NULL;
	MIL_ID m_MilColorLut = M_NULL;

	MIL_INT m_RoiLabel=-1;
	
	
	double m_ImageMax = 0;
	double m_ImageMin = 0;
	double m_ImageMean = 0;
	double m_ImageSum = 0;
	double m_ImageBackGround = 0;

	int m_hisNumOfIntensities = 65536;
	long long *m_HistValues;

public:
		double m_ExposureTime_ms;
		double m_SensorTemperatureSetPoint;

		MIL_ID m_MilImage = M_NULL;
		MIL_ID m_MilImageChild = M_NULL;
		MIL_INT m_MilImageWidth = 0;
		MIL_INT m_MilImageHeight = 0;
		int m_CcdRoiX;
		int m_CcdRoiY;
		int m_CcdRoiWidth;
		int m_CcdRoiHeight;
		int m_CcdRoiXBinning;
		int m_CcdRoiYBinning;
private:

	CWnd *m_pWndDisplay = NULL;

	double m_ImageRotationAngle = 0;
		
	MOUSEPOSITION m_ImageMousePosition;	

	//CBrush m_brush;
	DECLARE_MESSAGE_MAP()
public:
				
	CChartViewer m_ChartHistogram;	

	void InitPlotHistogram();
	void PlotHistogram();

	void trackLineLabel(XYChart *c, int mouseX);
	//void updateImageMap(CChartViewer *viewer);

	afx_msg void OnBnClickedButtonSelectSavePath();

	int GrabTwice(bool IsManualGrab);
	int Grab(bool IsManualGrab);
	//int GrabOld(bool IsManualGrab);

	double GetIntensity();
	double intensity_simulation(double x, double y);

	afx_msg void OnBnClickedButtonSetAlign();
	afx_msg void OnBnClickedButtonSetMeasure();
	afx_msg void OnEnChangeRoiYBinningEdit();
		
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedCheckCorrectBackground();
	BOOL m_bCorrectBackGround;
	double m_TempSetpoint;

	BOOL isUpdatedCcdRoi = TRUE;
	BOOL isUpdatedImageRoi = TRUE;

	afx_msg void OnBnClickedButtonResetCcdRoi();
	afx_msg void OnBnClickedButtonSetCcdRoi();
	BOOL m_bCorrectRotation;
	afx_msg void OnBnClickedCheckRotationCorrection();	
	CProgressCtrl m_ctrlProgressReadoutTime;

	CWinThread*		m_pProgressBarThread =NULL;
	BOOL m_bProgressBarLoopFlag = FALSE;	
	CProgressCtrl m_ctrlProgressGrabExposureTime;		
	afx_msg void OnBnClickedButtonGrab();	
	afx_msg void OnBnClickedButtonSetImageRoi();	


	
	afx_msg void OnBnClickedButtonXraySetMeasureBackground();
	afx_msg void OnBnClickedButtonXraySetAlignBackground();
	afx_msg void OnBnClickedButtonPhaseSaveMeasureBackground();
	afx_msg void OnBnClickedButtonPhaseSaveAlignBackground();

	void getMeanValue(double &meanvalue);
};
/**
 * Xray Camera Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

