﻿#pragma once


// CPlasmaCleaner 대화 상자

class CPlasmaCleanerDlg : public CDialogEx , public CECommon
{
	DECLARE_DYNAMIC(CPlasmaCleanerDlg)

public:
	CPlasmaCleanerDlg(CWnd* pParent = nullptr);   // 표준 생성자입니다.
	virtual ~CPlasmaCleanerDlg();

// 대화 상자 데이터입니다.
//#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PLASMA_CLEANER_DIALOG };
//#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()

public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual BOOL DestroyWindow();

	CString m_strDispSetWatt;
	CString m_strDispSetHour;
	CString m_strDispSetMinute;
	CString m_strDispSetSecond;
	CString m_strDispActualWatt;
	CString m_strDispActualWattR;
	CString m_strDispActualHour;
	CString m_strDispActualMinute;
	CString m_strDispActualSecond;
	CString m_strDispActualVDC;
	CString m_strDispActualTorr;
	CString m_strDispBookHour;
	CString m_strDispBookMinute;

	HICON m_LedIcon[3];

	CTime m_Starttime;
	CTime m_Toggletime;

	void updateui();
	void setControlUI(bool status);
	int getSetValue();
	int checkSetParamter();

	BOOL m_bisManual;

	BOOL m_bisRunThreadExitFlag;
	BOOL m_bisToggleThreadExitFlag;
	BOOL m_bisOpenSerialThreadExitFlag;

	afx_msg void OnBnClickedPlasmaButtonRun();
	afx_msg void OnBnClickedPlasmaButtonCancel();
	afx_msg void OnBnClickedPlasmaButtonUpdate();
	afx_msg void OnBnClickedPlasmaButtonOpen();
	afx_msg void OnBnClickedPlasmaButtonClose();
	afx_msg void OnBnClickedPlasmaCheck();
	afx_msg void OnBnClickedPlasmaButtonTimerrun();
	afx_msg void OnBnClickedPlasmaButtonMinimize();

	afx_msg void OnDeltaposPlasmaSpinSethour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinSetminute(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinSetsecond(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinBookhour(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposPlasmaSpinBookminute(NMHDR *pNMHDR, LRESULT *pResult);

	static UINT PlasmaCleaningThread(LPVOID lParam);
	CWinThread *m_pRunThread;

	static UINT UpdateParameterThread(LPVOID lParam);
	CWinThread *m_pUpdateThread;

	static UINT ToggleThread(LPVOID lParam);
	CWinThread *m_pToggleThread;

	void runPlasmaCleaner();
	void setTimerWindowState(BOOL status);
	void stopPlasmaCleaner();
	afx_msg void OnBnClickedPlasmaButtonTcpopen();
	afx_msg void OnBnClickedPlasmaButtonTcpclose();

	int m_nActualWatt;
	int m_nActualWattR;
	int m_nActualHour;
	int m_nActualMinute;
	int m_nActualSecond;
	double m_dActualVDC;
	double m_dActualTorr;

	BOOL checkTime();
};
