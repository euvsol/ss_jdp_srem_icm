﻿
// Plasma_cleaningDlg.cpp: 구현 파일
//

#include "pch.h"
#include "framework.h"
#include "afxdialogex.h"
#include <vector>
#include "extern.h"

using namespace std;
#define TRAY_ICON_ID 1000 
#define WM_TRAYICON_MSG WM_USER + 1
#define WM_DLG_MINIMIZE WM_USER + 2
#define WM_DLG_SHOW WM_USER+3
#define WM_DLG_EXIT WM_USER+4
#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 응용 프로그램 정보에 사용되는 CAboutDlg 대화 상자입니다.

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

// 구현입니다.
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CPlasmacleaningDlg 대화 상자



CPlasmacleaningDlg::CPlasmacleaningDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PLASMA_CLEANING_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_CLEAN);
}

void CPlasmacleaningDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);

}

BEGIN_MESSAGE_MAP(CPlasmacleaningDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_TIMER()
	ON_WM_DESTROY()
END_MESSAGE_MAP()


// CPlasmacleaningDlg 메시지 처리기

BOOL CPlasmacleaningDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 시스템 메뉴에 "정보..." 메뉴 항목을 추가합니다.

	// IDM_ABOUTBOX는 시스템 명령 범위에 있어야 합니다.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	//ModifyStyleEx(WS_EX_APPWINDOW, WS_EX_PALETTEWINDOW );
	//ModifyStyle(0, WS_MINIMIZEBOX);
	// 이 대화 상자의 아이콘을 설정합니다.  응용 프로그램의 주 창이 대화 상자가 아닐 경우에는
	//  프레임워크가 이 작업을 자동으로 수행합니다.
	SetIcon(m_hIcon, TRUE);			// 큰 아이콘을 설정합니다.
	SetIcon(m_hIcon, FALSE);		// 작은 아이콘을 설정합니다.
	CRect rt;

	g_pBISSCom = new CBISSCommunication;
	g_pSREMCom = new CSREMCommunication;
	g_pPlasmaDlg = new CPlasmaCleanerDlg; 
	g_pPlasmaDlg->Create(CPlasmaCleanerDlg::IDD, this);
	g_pPlasmaDlg->GetWindowRect(&rt); 
	rt.OffsetRect(0,-40); 
	g_pPlasmaDlg->MoveWindow(rt);
	g_pPlasmaDlg->ShowWindow(SW_SHOW); 
	
	g_pBookDlg = new CPlasmaBookDlg;
	g_pBookDlg->Create(CPlasmaBookDlg::IDD, this);
	g_pBookDlg->GetWindowRect(&rt);
	g_pBookDlg->MoveWindow(rt);
	g_pBookDlg->ShowWindow(SW_HIDE);

	g_pSREMCom->openSocketServer();
	createTrayIcon();
	deleteIcon();
	return TRUE;  // 포커스를 컨트롤에 설정하지 않으면 TRUE를 반환합니다.
}

void CPlasmacleaningDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 대화 상자에 최소화 단추를 추가할 경우 아이콘을 그리려면
//  아래 코드가 필요합니다.  문서/뷰 모델을 사용하는 MFC 응용 프로그램의 경우에는
//  프레임워크에서 이 작업을 자동으로 수행합니다.

void CPlasmacleaningDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 그리기를 위한 디바이스 컨텍스트입니다.

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 클라이언트 사각형에서 아이콘을 가운데에 맞춥니다.
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;


		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// 사용자가 최소화된 창을 끄는 동안에 커서가 표시되도록 시스템에서
//  이 함수를 호출합니다.
HCURSOR CPlasmacleaningDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CPlasmacleaningDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	switch (nIDEvent)
	{
	case 0:
		break;
	default:
		break;
	}
	__super::OnTimer(nIDEvent);
}


void CPlasmacleaningDlg::OnDestroy()
{
	__super::OnDestroy();
	g_pPlasmaDlg->DestroyWindow();
	delete g_pPlasmaDlg; g_pPlasmaDlg = NULL;
	g_pBookDlg->DestroyWindow();
	delete g_pBookDlg; g_pBookDlg = NULL;
	delete g_pBISSCom; g_pBISSCom = NULL;
	delete g_pSREMCom; g_pSREMCom = NULL;
	deleteTrayIcon();


}

void CPlasmacleaningDlg::restoreDialog()
{
	::ShowWindow(AfxGetMainWnd()->m_hWnd, SW_RESTORE);
	::SetWindowPos(AfxGetMainWnd()->m_hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	::SetWindowPos(AfxGetMainWnd()->m_hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);

}

void CPlasmacleaningDlg::minimizeDialog()
{
	::ShowWindow(AfxGetMainWnd()->m_hWnd, SW_HIDE);
}

void CPlasmacleaningDlg::createTrayIcon()
{
	TCHAR atcModuleName[_MAX_PATH]; 
	CString sModuleName; 
	nid.cbSize = sizeof(nid); 
	nid.hWnd = this->m_hWnd; 
	// 메인 윈도우 핸들
	nid.uID = TRAY_ICON_ID; 
	// 아이콘 ID
	nid.uFlags = NIF_MESSAGE | NIF_ICON | NIF_TIP; 
	// 플래그 설정 
	nid.uCallbackMessage = WM_TRAYICON_MSG; 
	nid.hIcon = AfxGetApp()->LoadIcon(IDI_ICON_CLEAN); 
	// 아이콘 로드 nid.uTimeout = 1000; 
	::GetModuleFileName( ::AfxGetInstanceHandle(), atcModuleName, _MAX_PATH ); 
	sModuleName = PathFindFileName( atcModuleName ); 
	_tcscpy_s( atcModuleName, sModuleName.GetLength() + 1, sModuleName ); 
	lstrcpy( nid.szTip, atcModuleName ); 
	Shell_NotifyIcon( NIM_ADD, &nid );

}

void CPlasmacleaningDlg::deleteTrayIcon()
{
	nid.cbSize = sizeof(nid);
	nid.hWnd = this->m_hWnd;
	// 메인 윈도우 핸들 
	nid.uID = TRAY_ICON_ID;
	// 작업 표시줄(TaskBar)의 상태 영역에 아이콘을 삭제한다. 
	Shell_NotifyIcon(NIM_DELETE, &nid);
}

void CPlasmacleaningDlg::deleteIcon()
{
	LONG lExStyle = ::GetWindowLong(m_hWnd, GWL_EXSTYLE); 
	// 작업표시줄 아이콘 제거 
	lExStyle = lExStyle | WS_EX_TOOLWINDOW; 
	if (WS_EX_APPWINDOW & lExStyle) 
		lExStyle = lExStyle ^ WS_EX_APPWINDOW; 
	// 윈도우 스타일 설정 
	::SetWindowLong(m_hWnd, GWL_EXSTYLE, lExStyle);

}



BOOL CPlasmacleaningDlg::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN)
	{
		switch (pMsg->wParam)
		{
		case VK_ESCAPE:
			return TRUE;
		case VK_RETURN:
			return TRUE;
		default:
			break;
		}
	}
	return __super::PreTranslateMessage(pMsg);
}


LRESULT CPlasmacleaningDlg::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
	// TODO: 여기에 특수화된 코드를 추가 및/또는 기본 클래스를 호출합니다. 
	switch ( message ) 
	{ 
		// 콜백 메시지 
		case WM_TRAYICON_MSG: 
		{ 
			// 트레이 아이콘 ID 
			if ( TRAY_ICON_ID == wParam ) 
			{ 
				// 동작 
				switch ( lParam ) 
				{ 
					case WM_RBUTTONUP: // 우클릭 
					CPoint pntCursor;
					HMENU hMenu = CreatePopupMenu();
					AppendMenu(hMenu, MF_STRING, WM_DLG_SHOW, _T("Show"));
					AppendMenu(hMenu, MF_STRING, WM_DLG_MINIMIZE, _T("Minimize"));
					AppendMenu(hMenu, MF_STRING, WM_DLG_EXIT, _T("Exit"));
					SetForegroundWindow();
					GetCursorPos( &pntCursor );
					TrackPopupMenu( hMenu, TPM_LEFTALIGN | TPM_RIGHTBUTTON, pntCursor.x, pntCursor.y, 0, this->m_hWnd, NULL );
					break;
				}	
			} 
		}
	break;

	case WM_COMMAND: 
	{ 
		if (wParam == WM_DLG_MINIMIZE)
		{
			//OnBnClickedCancel();
			//OnOK();
			minimizeDialog();
		}
		else if (wParam == WM_DLG_SHOW)
		{
			restoreDialog();
		}
		else if (wParam == WM_DLG_EXIT)
		{
			DestroyWindow();
		}
	}
	break;

	}
	return __super::WindowProc(message, wParam, lParam);
}
